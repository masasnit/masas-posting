<!DOCTYPE html>
<html>
<!--
MASAS Posting Tool - Bulk Entry
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
-->
<head>
  <title>MASAS Posting Tool</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  
  <%= topCSSbulkentry %>
	
	
  <%= topJSbulkentry %>
  
  <script type="text/javascript">

// Path to the blank image should point to a valid location on your server
Ext.BLANK_IMAGE_URL = 'libs/ExtJS-3.4.2/resources/images/default/s.gif';
// Path for OpenLayers images
OpenLayers.ImgPath = 'libs/OpenLayers-2.13.1/img/';
// Customize the MASAS Icon location
OpenLayers.Format.MASASFeed.IconURL = 'http://icon.masas-sics.ca/';
// Adding retries because default is 0
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
// Setup browser console
if (!'console' in window || typeof console == 'undefined') {
    // when missing, use a mock console such as OpenLayers' default
    console = OpenLayers.Console;
} else {
    // use the browser's console for logging including from OpenLayers
    OpenLayers.Console = console;
}

// global object for shared values
Ext.namespace('MASAS');
// global object for this app
Ext.namespace('POST');

// bounding box - left,bottom,right,top using lon,lat
//POST.MAP_DEFAULT_VIEW = '-82.73,42.49,-81.66,43.37';
POST.USER_URI = 'http://masas-sics.ca/accounts/1';
// posting values
POST.FEED_URL = 'http://localhost:8080/tests/samples/sample-feed.xml';
//POST.FEED_URL = 'https://sandbox2.masas-sics.ca/hub/feed';
POST.USER_SECRET = 'sample';
// multiple values used for switching between hubs
POST.FEED_SETTINGS = [
    {title: 'Sample', url: 'http://localhost:8080/tests/samples/sample-feed.xml', secret: 'sample'}
];
// ajax proxy url, needs leading ?url=
POST.AJAX_PROXY_URL = '/go?url=';
// setup attachment proxy
POST.SETUP_ATTACH_URL = '/setup_attach';
// import an attachment URL
POST.IMPORT_ATTACH_URL = '/import_attach';
// local proxy url to support attachment posting, needs leading ?url=
POST.ATTACH_PROXY_URL = '/post_attach?url=';

// UA detection should determine if touch screen support is needed
//POST.TOUCH_ENABLE = true;

  </script>
</head>
<body>
  <div id="north" class="x-hide-display">
    <p>MASAS Posting Tool - if you can see this, your web browser
does not support this tool.</p>
  </div>
  
  <%= bottomJSbulkentry %>
  
</body>
</html>
