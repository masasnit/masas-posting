<!DOCTYPE html>
<html>
<!--
MASAS Posting Tool - Update/Cancel CAP Alert
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
-->
<head>
  <title>MASAS Posting Tool</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  
  <%= topCSSupdate %>
  
  <%= topJSupdate %>
  
  
  <script type="text/javascript">

// Path to the blank image should point to a valid location on your server
Ext.BLANK_IMAGE_URL = 'libs/ExtJS-3.4.2/resources/images/default/s.gif';
// Make components not stateful by default
Ext.Component.prototype.stateful = false;
// Assign a state provider
Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
    // 2 years = 730 days
    expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 730))
    //, path: ''
}));
// Path for OpenLayers images
OpenLayers.ImgPath = 'libs/OpenLayers-2.13.1/img/';
// Customize the MASAS Icon location
OpenLayers.Format.MASASFeed.IconURL = 'http://icon.masas-sics.ca/';
// Adding retries because default is 0
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
// Setup browser console
if (!'console' in window || typeof console == 'undefined') {
    // when missing, use a mock console such as OpenLayers' default
    console = OpenLayers.Console;
} else {
    // use the browser's console for logging including from OpenLayers
    OpenLayers.Console = console;
}

// global object for shared values
Ext.namespace('MASAS');
// global object for this app
Ext.namespace('POST');

// either Update/Cancel or Clone
POST.UPDATE_OPERATION = 'Update';

// initial CAP data to populate alert with, customize as necessary
POST.CAP_MESSAGE = {alert: {'@xmlns': 'urn:oasis:names:tc:emergency:cap:1.1',
    // customize identifier for each new alert
    identifier: 'TEST-2',
    // customize the sender for this user
    sender: 'user@example.com',
    sent: 'sent',
    status: 'status',
    msgType: 'msgType',
    scope: 'Public',
    code: 'profile:CAP-CP:0.3',
    references: 'references',
    info:[{
        language: 'en-CA',
        category: 'category',
        event: 'event',
        urgency: 'urgency',
        severity: 'severity',
        certainty: 'certainty',
        eventCode: {valueName: 'profile:CAP-CP:Event:0.3', value: 'value'},
        effective: 'effective',
        expires: 'expires',
        // customize the senderName for this user
        senderName: 'Example User',
        headline: 'headline',
        description: 'description',
        instruction: 'instruction',
        web: 'web',
        contact: 'contact',
        parameter: {},
        area: null
    }, {
        language: 'fr-CA',
        category: 'category',
        event: 'event',
        urgency: 'urgency',
        severity: 'severity',
        certainty: 'certainty',
        eventCode: {valueName: 'profile:CAP-CP:Event:0.3', value: 'value'},
        effective: 'effective',
        expires: 'expires',
        // customize the senderName for this user
        senderName: 'Example User',
        headline: 'headline',
        description: 'description',
        instruction: 'instruction',
        web: 'web',
        contact: 'contact',
        parameter: {},
        area: null
    }]
} };

// bounding box - left,bottom,right,top using lon,lat
//POST.MAP_DEFAULT_VIEW = '-82.73,42.49,-81.66,43.37';
POST.USER_URI = 'http://masas-sics.ca/accounts/1';
// posting values
POST.FEED_URL = 'http://localhost:8080/tests/samples/sample-feed.xml';
//POST.FEED_URL = 'https://sandbox2.masas-sics.ca/hub/feed';
POST.USER_SECRET = 'sample';
// multiple values used for switching between hubs
POST.FEED_SETTINGS = [
    {title: 'Sample', url: 'http://localhost:8080/tests/samples/sample-feed.xml', secret: 'sample'}
];
// extend to add only favorite feed for Clone
POST.FEED_SETTINGS.push({
    title: 'Favorite Feed', url: 'http://localhost:8080/favorite_feed',
    secret: POST.USER_SECRET, uri: POST.USER_URI, readOnly: true
});
// event list URL
POST.EVENT_LIST_URL = '/get_events';
// area zone search URL
POST.AREA_ZONE_URL = '/get_areas';
// address search URL
POST.ADDRESS_SEARCH_URL = '/address_search';
// ajax proxy url, needs leading ?url=
POST.AJAX_PROXY_URL = '/go?url=';
// geometry upload/convert/import
POST.GEOMETRY_IMPORT_URL = '/import_geometry';
// provide a list of email addresses if email forwarding is enabled
POST.EMAIL_ADDRESS_LIST = ['test@example.com'];
// email forwarding URL
POST.EMAIL_FORWARD_URL = '/forward_email';

// header and footer for email messages, comment out to remove
//POST.EMAIL_HEADER = 'This is the header';
//POST.EMAIL_FOOTER = '\nThis is the footer';
// set default integer value for expires interval, comment out if not using
//POST.DEFAULT_EXPIRES_INTERVAL = 48;

// UA detection should determine if touch screen support is needed
//POST.TOUCH_ENABLE = true;

  </script>
</head>
<body>
  <div id="north" class="x-hide-display">
    <p>MASAS Posting Tool - if you can see this, your web browser
does not support this tool.</p>
  </div>

  <%= bottomJSupdatecap %>

</body>
</html>
