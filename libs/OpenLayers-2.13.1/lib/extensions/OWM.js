/**
OpenWeatherMap - Derived from http://openweathermap.org/wiki/API/OpenLayer

Custom data sources from openweathermap.org  Currently using JSONP.

Usage:
var city = new OpenLayers.Layer.Vector.OWMWeather("Weather");
map.addLayers([city]);
*/

/*global OpenLayers */

/**
 * A specific format for parsing OpenWeatherMap Weather API JSON responses.
 */
OpenLayers.Format.OWMWeather = OpenLayers.Class(OpenLayers.Format, {
    read: function (obj) {
        if (obj.cod !== '200') {
            throw new Error(
                ['OWM failure response (', obj.cod, '): ', obj.message].join('') );
        }
        if (!obj || !obj.list || !OpenLayers.Util.isArray(obj.list )) {
            throw new Error('Unexpected OWM response');
        }
        var list = obj.list, x, y, point, feature, features = [];
        //console.log('time='+obj.calctime+', cnt='+obj.cnt +', '+ obj.message);
        for (var i = 0,l = list.length; i < l; i++) {
            feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point(list[i].coord.lon, list[i].coord.lat), 
                {
                    title: list[i].name,
                    station: list[i],
                    temp:  Math.round((10 * (list[i].main.temp - 273.15)) / 10)
                }
            );
            features.push(feature);
        }
        
        return features;
    }
});


/**
 * A layer that uses the OpenWeatherMap Weather API
 */
OpenLayers.Layer.Vector.OWMWeather = OpenLayers.Class(OpenLayers.Layer.Vector, {
    projection: new OpenLayers.Projection('EPSG:4326'),
    strategies: [new OpenLayers.Strategy.BBOX({resFactor: 1})],
    styleMap: new OpenLayers.StyleMap(
        new OpenLayers.Style({
            fontColor: 'black',
            fontSize: '12px',
            fontFamily: 'Arial, Courier New',
            graphicXOffset: -20,
            graphicYOffset: -5,
            labelAlign: 'lt',
            labelXOffset: '-25',
            labelYOffset: '-35',
            labelOutlineColor: 'white',
            labelOutlineWidth: 3,
            externalGraphic: '${icon}',
            graphicWidth: 50,
            label : '${temp}' + 'C ' + '${windspeed}' + 'km/h'
        }, {
            context: {
                icon:  function (feature) {
                    return feature.layer.options.getIcon(feature.attributes.station);
                },
                windspeed: function (feature) {
                    var speed = 0;
                    if (feature.attributes.station &&
                        feature.attributes.station.wind &&
                        feature.attributes.station.wind.speed) {
                        // convert from meters/second to kilometers/hour
                        speed = Math.round(feature.attributes.station.wind.speed * 3.6);
                    }
                    return speed;
                }
            }
        }
    )),
    
    initialize: function (name, options) {
        options = options || {};
        if (!options.iconsets) {
            options.iconsets = 'main';
        }
        if (!options.getIcon) {
            options.getIcon = this.getIcon;
        }
        if (!options.url) {
            options.url = 'http://openweathermap.org/data/2.1/find/city';
        }
        var newArguments = [];
        newArguments.push(name, options);
        OpenLayers.Layer.Vector.prototype.initialize.apply(this, newArguments);
        this.protocol = new OpenLayers.Protocol.Script({
            url: this.url,
            params: {
                cluster: 'yes',
                cnt: 200,
                format: 'json',
                layer: this
            },
            filterToParams: function (filter, params) {
                if (filter.type === OpenLayers.Filter.Spatial.BBOX) {
                    params.bbox = filter.value.toArray();
                    params.bbox.push(params.layer.map.getZoom());
                    if (filter.projection) {
                        params.bbox.push(filter.projection.getCode());
                    }
                }
                return params;
            },
            format: new OpenLayers.Format.OWMWeather(),
            callback: function () {}
        });
    },
    
    getIcon: function (station) {
        if (station.weather.length > 0) {
            return 'http://openweathermap.org/img/w/' + station.weather[0].icon + '.png';
        } else {
            return 'http://openweathermap.org/img/w/transparentd.png';
        }
    }
});
