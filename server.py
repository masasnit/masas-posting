#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           Development Web Server
Author:         Jacob Westfall
Created:        Feb 01, 2011
Updated:        Jan 13, 2014
Copyright:      Independent Joint Copyright (c) 2011-2014 MASAS Contributors.
License:        Released under the Modified BSD license.  See license.txt for
                the full text of the license.
Description:    This example serves MASAS html files for the posting tool,
                provides data for form fields, and acts as a proxy for
                special requests.
"""

import os, sys
import urllib2
try:
    import json
except ImportError:
    import simplejson as json
import mimetools
from cStringIO import StringIO
import smtplib
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    try:
        import cElementTree as ElementTree
    except ImportError:
        from elementtree import ElementTree
import zipfile
import xml.dom.minidom as minidom
import mimetypes
import datetime
import re

## http server, www.cherrypy.org, cherrypy 3.2+ required
import cherrypy

## MASAS modules
from utils import event_list
from utils import area_list
from utils import icon_list
from utils import category_list
from utils import template_list

from utils import shapefile

## get absolute path name from executable   
server_pathname = os.path.dirname(sys.argv[0])
## change directory to absolute path name
os.chdir(os.path.abspath(server_pathname))
## python 2.6 or higher required
if sys.version < '2.6':
    print "Python 2.6 or higher is required"
    sys.exit(1)

# SMTP server for forwarded emails
SMTP_SERVER = "localhost:25"
# Return address for forwarded emails
RETURN_ADDRESS = "test@example.com"


class RootPage(object):
    """ HTML server's / class """
    
    def __init__(self):
        """ Setup RootPage """
        
        self.attached_files = []
        self.attached_size = 0
        self.original_files = []
        # defaults
        self.attachment_types = {"application/atom+xml;type=entry": 1048576}
        
        # common types allowed on a Hub
        self.attachment_type_extension = {
            "image/jpeg": ".jpg",
            "image/png": ".png",
            "image/gif": ".gif",
            "image/svg+xml": ".svg",
            "text/plain": ".txt",
            "text/rtf": ".rtf",
            "application/pdf": ".pdf",
            "application/vnd.ms-powerpoint": ".ppt",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation": ".pptx",
            "application/msword": ".doc",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document": ".docx",
            "application/vnd.oasis.opendocument.text": ".odt",
            "application/vnd.oasis.opendocument.presentation": ".odp",
            "application/vnd.google-earth.kml+xml": ".kml",
            "audio/wav": ".wav",
            "audio/amr": ".amr",
        }
        # mimetypes is used as a backup
        mimetypes.init()
        
        self.import_entries = []
        self.import_attachments = []
        self.favorite_entries = []
    
    
    @cherrypy.expose
    def index(self):
        """ Create a listing of web pages """
        
        html_files = []
        for name in os.listdir("."):
            if name.endswith(".html"):
                html_files.append(name)
        html = ["<html>"]
        for new_file in html_files:
            html.append("""<a href="/%s"> %s </a>""" %(new_file, new_file))
            html.append("<br>")
        html.append("</html>")
        
        return str("\n".join(html))
    
    
    @cherrypy.expose
    def go(self, *args, **kwargs):
        """ Proxy for cross-domain javascript """
        
        if "url" not in kwargs:
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 3"
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 4"
        # default is 30 seconds which should be long enough to fetch a Feed
        # or an Entry, any longer and the user will wonder what is happening.
        # For POST or GET of other content, supplying a longer timeout
        # is possible because those operations will take longer with large
        # files, ie 3 minutes for 10 MB on most DSL service
        request_timeout = 30
        if "timeout" in kwargs:
            try:
                request_timeout = int(kwargs["timeout"])
                # cherrypy default is 5 mins max for a request
                if request_timeout > 300:
                    raise ValueError
            except:
                cherrypy.response.headers["Content-Type"] = "text/plain"
                return "\nIllegal Request, Error: 5"
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Viewing Tool Proxy"}
            if "Authorization" in cherrypy.request.headers:
                new_headers["Authorization"] = cherrypy.request.headers["Authorization"]
            if cherrypy.request.method == "POST":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # There is a bug in older python versions < 2.5 where only 200
                # is an acceptable response vs 201
            elif cherrypy.request.method == "PUT":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # consider using httplib2 which has builtin PUT method instead
                new_request.get_method = lambda: 'PUT'
            else:
                new_request = urllib2.Request(url, headers=new_headers)
            new_connection = urllib2.urlopen(new_request, timeout=request_timeout)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def setup_attach(self, *args, **kwargs):
        """ Clear out past attachments and prep for new ones """
        print "in setup_attach"
        if "reset" in kwargs and kwargs["reset"] == "yes":
            # reset any previously saved values, should only happen at the
            # beginning of a session, later changes to a Hub should only need
            # the attachment types changed
            self.attached_files = []
            self.attached_size = 0
            self.original_files = []
        # load the attachment config from the hub's service document
        new_connection = None
        try:
            if "feed" not in kwargs:
                raise cherrypy.HTTPError(400)
            if "secret" not in kwargs:
                raise cherrypy.HTTPError(400)
            hub_url = kwargs["feed"].strip()
            # remove the feed path to get the Hub root
            if hub_url.endswith("/"):
                hub_url = hub_url.replace("/feed/", "/")
            else:
                hub_url = hub_url.replace("/feed", "/")
            new_headers = {"User-Agent": "MASAS Posting Tool Proxy"}
            new_request = urllib2.Request("%s?secret=%s" %(hub_url,
                kwargs["secret"].strip()), headers=new_headers)
            # max 15 seconds to download service doc
            new_connection = urllib2.urlopen(new_request, timeout=15)
            # max 1 MB for a service doc
            new_data = new_connection.read(1048576)
            self.attachment_types = find_acceptable_types(new_data)
        except:
            self.attachment_types = {"application/atom+xml;type=entry": 1048576}
            raise cherrypy.HTTPError(400)
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def add_attach(self, *args, **kwargs):
        """ Helper method for attachments, stores new ones """
        
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        
        if "kml" in kwargs and kwargs["kml"] == "yes":
            # KML layer type handling
            new_file_name = "attach.kml"
            try:
                new_file_data = kwargs["attachment-file"]
            except:
                return "{ success: false, message: 'Attachment file invalid.' }"
            new_file_type = "application/vnd.google-earth.kml+xml"
        else:
            # all other attachment types
            try:
                # iso-8859-1 used later for these values
                new_file_name = unicode_convert(kwargs["attachment-file"].filename)
            except:
                return "{ success: false, message: 'Attachment filename invalid.' }"
            new_file_data = kwargs["attachment-file"].file.read()
            new_file_type = str(kwargs["attachment-file"].content_type)
        new_file_length = len(new_file_data)
        try:
            new_file_title = unicode_convert(kwargs["attachment-title"])
        except:
            return "{ success: false, message: 'Attachment title invalid.' }"
        
        # convert some non-standard file types that Internet Explorer uses
        if new_file_type == "image/pjpeg" or new_file_type == "image/x-citrix-jpeg" \
        or new_file_type == "image/x-citrix-pjpeg":
            new_file_type = "image/jpeg"
        if new_file_type == "image/x-png" or new_file_type == "image/x-citrix-png":
            new_file_type = "image/png"
        if new_file_type == "image/x-citrix-gif":
            new_file_type = "image/gif"
        # in addition, this supports determing the mimeType based on file
        # extension, could use python mimetypes module but would still need to
        # update it for some of these newer values
        mime_mapping = {
            "doc": "application/msword",
            "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "ppt": "application/vnd.ms-powerpoint",
            "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "pdf": "application/pdf",
            "atom": "application/atom+xml",
            "cap": "application/common-alerting-protocol+xml",
        }
        if new_file_type not in self.attachment_types:
            try:
                new_file_extension = os.path.splitext(new_file_name)[1][1:].lower()
                if new_file_extension in mime_mapping:
                    new_file_type = mime_mapping[new_file_extension]
            except:
                pass
        
        # allowable attachment types are defined by the Hub
        if new_file_type not in self.attachment_types:
            return "{ success: false, message: 'File type not supported. %s' }" \
                %new_file_type
        # size limits may be different for each type
        if new_file_length > self.attachment_types[new_file_type]:
            return "{ success: false, message: 'File size exceeded.' }"
        # the overall max size for an entire entry, including attachments,
        # is set in the atom entry type.  Need to also leave some room for
        # encoding and to add the Entry XML itself, 512 KB for now
        overall_max_size = \
            self.attachment_types["application/atom+xml;type=entry"] - 524288
        if new_file_length + self.attached_size > overall_max_size:
            return "{ success: false, message: 'Overall file size exceeded.' }"
        
        # new attachments are always added to the end to use for len - 1 to
        # get the index location.  Removals have None substituted to keep
        # ordering in place.
        self.attached_files.append((new_file_name, new_file_data,
            new_file_type, new_file_title, new_file_length))
        self.attached_size += new_file_length
        
        return "{ success: true, num: %s }" %(len(self.attached_files) - 1)
    
    
    @cherrypy.expose
    def import_attach(self, *args, **kwargs):
        """ Helper method for attachments, imports past attachments """
        
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        if "type" not in kwargs and not kwargs["type"]:
            raise cherrypy.HTTPError(400)
        if "title" not in kwargs and not kwargs["title"]:
            raise cherrypy.HTTPError(400)
        # iso-8859-1 used later for the title
        try:
            kwargs["title"] = unicode_convert(kwargs["title"])
        except:
            raise cherrypy.HTTPError(400)
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Posting Tool Proxy"}
            new_request = urllib2.Request(url, headers=new_headers)
            # 1 minute should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=60)
            # 10 MB max size on most hubs
            new_data = new_connection.read(10485760)
            new_data_size = len(new_data)
            if new_data_size + self.attached_size > 9616000:
                raise cherrypy.HTTPError(400)
            self.attached_files.append(("new.txt", new_data,
                kwargs["type"], kwargs["title"], new_data_size))
            self.original_files.append(("new.txt", new_data,
                kwargs["type"], kwargs["title"], new_data_size))
            self.attached_size += new_data_size
            # index used for reference, normally all that's need to be returned
            # for attachment handling
            cherrypy.response.headers["Attach-Num"] = len(self.attached_files) - 1
            if kwargs["type"] == "application/vnd.google-earth.kml+xml":
                # return KML so it can be added to the location map
                return new_data
        except:
            raise cherrypy.HTTPError(400)
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def remove_attach(self, *args, **kwargs):
        """ Helper method for attachments, removes previous ones """
        
        if cherrypy.request.method != "DELETE":
            raise cherrypy.HTTPError(400)
        if "num" not in kwargs and not kwargs["num"]:
            raise cherrypy.HTTPError(400)
        if not self.attached_files:
            # nothing to remove
            raise cherrypy.HTTPError(400)
        attach_num = int(kwargs["num"])
        try:
            if self.attached_files[attach_num] == None:
                # already removed
                raise cherrypy.HTTPError(400)
            self.attached_size -= self.attached_files[attach_num][4]
            self.attached_files[attach_num] = None
        except IndexError:
            raise cherrypy.HTTPError(400)
        
        return "Attachment %s removed" %attach_num
    
    
    @cherrypy.expose
    def post_attach(self, *args, **kwargs):
        """ Helper method for attachments, post as a multi-part entry """
        
        #NOTE: not checking, but on rare occasion multipart/related isn't an
        #      allowable type in the Hub service document, this won't work
        
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        attach_files = None
        if self.attached_files:
            # clean up any None placeholders where attachments were removed
            attach_files = [x for x in self.attached_files if x != None]
        new_connection = None
        try:
            entry_body = cherrypy.request.body.read()
            if attach_files:
                attach_files.insert(0, ("entry.xml", entry_body,
                    "application/atom+xml", "Entry XML", len(entry_body)) )
                boundary,multipart_body = multipart_encode(attach_files)
                new_headers = {"Content-Type": "multipart/related; boundary=%s" \
                    %boundary, "User-Agent": "MASAS Posting Tool Proxy"}
                new_request = urllib2.Request(url, multipart_body, new_headers)
            else:
                # treat as normal Entry post to handle cases where files were
                # attached, then all removed, but the proxy url changed to here
                new_headers = {"Content-Type": "application/atom+xml",
                    "User-Agent": "MASAS Posting Tool Proxy"}
                new_request = urllib2.Request(url, entry_body, new_headers)
            # 2 minutes should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=120)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            # upon success, reset session temporary storage
            self.attached_files = []
            self.attached_size = 0
            self.original_files = []
            
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def put_attach(self, *args, **kwargs):
        """ Helper method for attachments, put as a multi-part entry """
        
        #NOTE: not checking, but on rare occasion multipart/related isn't an
        #      allowable type in the Hub service document, this won't work
        
        if cherrypy.request.method != "PUT":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        attach_files = None
        if self.attached_files:
            # clean up any None placeholders where attachments were removed
            attach_files = [x for x in self.attached_files if x != None]
        if attach_files == self.original_files:
            # no change, no need to update attachments
            return "No Change"
        new_connection = None
        try:
            if len(attach_files) == 0:
                # assuming that all previous attachments are supposed to be
                # deleted from this Entry as part of the Update.
                new_headers = {"User-Agent": "MASAS Posting Tool Proxy"}
                new_request = urllib2.Request(url, headers=new_headers)
                # consider using httplib2 which has builtin DELETE method instead
                new_request.get_method = lambda: 'DELETE'
            elif len(attach_files) == 1:
                # a single attachment update
                new_headers = {"Content-Type": attach_files[0][2],
                    "Slug": attach_files[0][3],
                    "User-Agent": "MASAS Posting Tool Proxy"}
                new_request = urllib2.Request(url, attach_files[0][1], new_headers)
                new_request.get_method = lambda: 'PUT'
            else:
                boundary,multipart_body = multipart_encode(attach_files)
                new_headers = {"Content-Type": "multipart/related; boundary=%s" \
                    %boundary, "User-Agent": "MASAS Posting Tool Proxy"}
                new_request = urllib2.Request(url, multipart_body, new_headers)
                new_request.get_method = lambda: 'PUT'
            # 2 minutes should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=120)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            # upon success, reset session temporary storage
            self.attached_files = []
            self.attached_size = 0
            self.original_files = []
            
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def address_search(self, *args, **kwargs):
        """ Return a JSON listing of geocoded addresses, sample data provided """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        return """{"results": [{"address": "Main Street", "lat": "42.5", "lon": "-82"}] }"""
    
    
    @cherrypy.expose
    def forward_email(self, *args, **kwargs):
        """ Forwards an Entry or CAP message via email """
        
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
        #TODO: validation of the "to" address
        if "to" not in kwargs and not kwargs["to"]:
            raise cherrypy.HTTPError(400)
        if "subject" not in kwargs and not kwargs["subject"]:
            raise cherrypy.HTTPError(400)
        if "message" not in kwargs and not kwargs["message"]:
            raise cherrypy.HTTPError(400)
        
        new_msg = u"""From: %s
To: %s
Subject: %s
Content-Type: text/plain; charset="utf-8"
Sender: %s

%s
""" %(RETURN_ADDRESS, kwargs["to"], kwargs["subject"], RETURN_ADDRESS,
        kwargs["message"])
        
        try:
            smtp_connect = smtplib.SMTP(SMTP_SERVER)
            smtp_connect.sendmail(from_addr=RETURN_ADDRESS,
                to_addrs=kwargs["to"].encode("utf-8"),
                msg=new_msg.encode("utf-8"))
            smtp_connect.quit()
        except:
            raise cherrypy.HTTPError(400)
        
        return "Success"


## CAP


    @cherrypy.expose
    def cap_template(self, *args, **kwargs):
        """ Return either a JSON listing of all available templates or
        if a particular template is specified, return its JSON data.
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        # default is first group name, unless specified
        group = template_list.cap_groups[0]
        if "group" in kwargs and kwargs["group"] in template_list.cap_groups:
            group = kwargs["group"]
        
        if "template" in kwargs and kwargs["template"]:
            return json.dumps(template_list.cap[group][int(kwargs["template"])])
        else:
            temps = []
            for temp_num, each_temp in enumerate(template_list.cap[group]):
                temps.append({"name": each_temp["name"], "num": temp_num})
            return json.dumps({"templates": temps})
    
    
    @cherrypy.expose
    def get_events(self, *args, **kwargs):
        """ Return a JSON listing of CAP events.
        
        event_list_data = [{'category': 'Other', 'f_text': 'Autres alertes',
                'f_tooltip': 'Autres alertes', 'leaf': True, 'term': 'other',
                'text': 'Other', 'tooltip': 'Other', 'value': 'other'},
            {'category': 'Security', 'children': [{'category': 'Security',
                    'f_text': 'Crise civile', 'f_tooltip': 'Crise civile',
                    'leaf': True, 'term': 'ems/incident/civil/emergency',
                    'text': 'Civil Emergency', 'tooltip': 'Civil Emergency',
                    'value': 'civilEmerg'}],
                'cls': 'folder', 'f_text': 'Civil', 'f_tooltip': 'Civil',
                'term': 'ems/incident/civil', 'text': 'Civil', 'tooltip': 'Civil',
                'value': 'civil'}]
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        return json.dumps(event_list.event_list_data)
    
    
    @cherrypy.expose
    def get_areas(self, *args, **kwargs):
        """ Return a GeoJSON listing of applicable SGC areas given a point (lat,lon),
        radius (km), type (csd, cd, other).  Optionally, they may only want
        just a list of geocodes, as indicated by the geocode=yes parameter.  If no
        results found should either return proper empty GeoJSON or array with "none"
        for geocodes.
        
        area_data = {"type": "FeatureCollection",
            "features": [\
                {"type": "Feature",
                "geometry": {"type": "Polygon",
                    "coordinates": [[ [-78.44, 45.76], [-78.35, 42.94],
                        [-81.87, 44.40], [-78.44, 45.76] ]]
                    },
                "properties": {"e_name": "Ottawa", "f_name": "Ottawa", "sgc": "001"}
                },
                {"type": "Feature",
                "geometry": {"type": "Polygon",
                    "coordinates": [[ [-79.44, 46.76], [-78.35, 42.94],
                        [-81.87, 44.40], [-79.44, 46.76] ]]
                    },
                "properties": {"e_name": "Toronto", "f_name": "Toronto", "sgc": "002"}
                }
            ]
        }
        
        OR area_data = ["001", "002"]
        """
        
        #TODO: limiting to one geocode per feature for right now,
        #      could expand to multiples sgc: ['001', '002'] later??
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        if "geocode" in kwargs and kwargs["geocode"] == "yes":
            #NOTE: could return GeoJSON here too and just parse out the sgc
            #      values, but this saves potentially a lot of bandwidth
            return '["001", "002"]'
        else:
            return json.dumps(area_list.area_list_data)


## Entries


    @cherrypy.expose
    def entry_template(self, *args, **kwargs):
        """ Return either a JSON listing of all available templates or
        if a particular template is specified, return its JSON data.
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        # default is first group name, unless specified
        group = template_list.entry_groups[0]
        if "group" in kwargs and kwargs["group"] in template_list.entry_groups:
            group = kwargs["group"]
        
        if "template" in kwargs and kwargs["template"]:
            return json.dumps(template_list.entry[group][int(kwargs["template"])])
        else:
            temps = []
            for temp_num, each_temp in enumerate(template_list.entry[group]):
                temps.append({"name": each_temp["name"], "num": temp_num})
            return json.dumps({"templates": temps})
    
    
    @cherrypy.expose
    def get_categories(self, *args, **kwargs):
        """ Return a JSON listing of CAP based categories for an entry.
        
        category_data = [{"e_name": "Fire", "e_tip": "All Types of Fires",
            "f_name": "Fire", "f_tip": "French Tip", "value": "fire"}]
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        return json.dumps({"categories": category_list.category_list_data})
    
    
    @cherrypy.expose
    def get_icons(self, *args, **kwargs):
        """ Return a JSON listing/tree of EMS icons.
        
        icon_data = [{"text": "Incident", "cls": "folder", "children": [\
                {"text": "Fire", "cls": "folder", "term": "incident/fire",
                    "children":[{"text": "ForestFire", "leaf": True,
                    "term": "incident/fire/forestFire"}] },
                {"text": "Flood", "leaf": True, "term": "incident/flood"}] },
            {"text": "Operations", "cls": "folder", "children": [\
                {"text": "Emergency", "leaf": True, "term": "operations/emergency"}] },
            {"text": "Infrastructure", "cls": "folder", "children": [\
                {"text": "Government", "leaf": True, "term": "infrastructure/government"},
                {"text": "Safety", "leaf": True, "term": "infrastructure/safety"}] },
            {"text": "Other", "cls": "folder", "children": [\
                {"text": "Other", "leaf": True, "term": "other"}] }]
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        return json.dumps(icon_list.icon_list_tree)
    
    
    @cherrypy.expose
    def get_colour_data(self, *args, **kwargs):
        """ Return a JSON listing of a set of colour data for a matching
        category label in templates """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        
        if "label" in kwargs and kwargs["label"]:
            for each_dataset in template_list.colour_datasets:
                if kwargs["label"].lower() == each_dataset["label"].lower():
                    return json.dumps({"data": each_dataset["data"]})
        else:
            raise cherrypy.HTTPError(400)
        
        return "[]"


## Special feeds


    @cherrypy.expose
    def import_feed(self, *args, **kwargs):
        """ A user's import feed containing any Entries they have uploaded """
        
        if "entry" in kwargs:
            # direct access to an Entry
            try:
                entry_location = int(kwargs["entry"])
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=entry;charset="utf-8"'
                
                return self.import_entries[entry_location]
            except:
                raise cherrypy.HTTPError("400", "Entry Error")
        elif "attachment" in kwargs:
            # direct access to an Attachment
            try:
                attach_location = int(kwargs["attachment"])
                cherrypy.response.headers["Content-Type"] = \
                    self.import_attachments[attach_location][0]
                
                return self.import_attachments[attach_location][1]
            except:
                raise cherrypy.HTTPError("400", "Attachment Error")
        else:
            # generate a feed of any imported Entries
            try:
                feed = []
                declaration_match = re.compile(r"<\?xml.*\?>")
                entry_match = re.compile(r"<entry.*>")
                # start feed with minimum values
                feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Import Feed</name>
    </author>
    <id>import-feed</id>
    <link href="import_feed" rel="self" />
    <title type="text">Import Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
                # add revised entries suitable for a feed
                for entry in self.import_entries:
                    feed_entry = declaration_match.sub("", entry)
                    feed_entry = entry_match.sub("<entry>", feed_entry)
                    feed.append(feed_entry)
                # close feed
                feed.append("\n</feed>")
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=feed;charset="utf-8"'
                
                return "\n".join(feed)
            except:
                raise cherrypy.HTTPError("400", "Feed Error")
    
    
    @cherrypy.expose
    def favorite_feed(self, *args, **kwargs):
        """ A user's favorite feed containing any Entries they have selected """
        
        if "reset" in kwargs and kwargs["reset"] == "yes":
            self.favorite_entries = []
            return
        
        try:
            feed = []
            feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Favorite Feed</name>
    </author>
    <id>favorite-feed</id>
    <link href="favorite_feed" rel="self" />
    <title type="text">Favorite Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
            feed.extend(self.favorite_entries)
            feed.append("\n</feed>")
            cherrypy.response.headers["Content-Type"] = \
                'application/atom+xml;type=feed;charset="utf-8"'
            
            return "\n".join(feed)
        except:
            raise cherrypy.HTTPError("400", "Feed Error")
    
    
    @cherrypy.expose
    def import_geometry(self, *args, **kwargs):
        """ Import a shapefile returning GeoJSON """
        
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        features = []
        try:
            s_file = shapefile.Reader(shp=kwargs["attachment-file"].file)
            shapes = s_file.shapes()
            if len(shapes) < 1:
                raise ValueError("No geometry values found")
            if len(shapes) > 25:
                raise ValueError("Too many geometry values")
            for each_shape in shapes:
                # geo_interface returns proper GeoJSON
                features.append({"type": "Feature",
                    "geometry": each_shape.__geo_interface__})
        except:
            return "{ success: false, message: 'File invalid.' }"
        geojson = {"type": "FeatureCollection", "features": features}
        
        return "{ success: true, data: %s }" %json.dumps(geojson)


## Misc Functions


def unicode_convert(s):
    """ Ensure that an input string is in unicode, decoding using select
    types if necessary """
    
    if not isinstance(s, basestring):
        raise ValueError("Not a string")
    if not isinstance(s, unicode):
        try:
            s = unicode(s, "utf-8")
        except UnicodeDecodeError:
            try:
                s = unicode(s, "iso-8859-1")
            except UnicodeDecodeError:
                raise
    
    return s


def multipart_encode(files):
    """ Combine files for a multipart message POST/PUT """
    
    boundary = mimetools.choose_boundary()
    buffer = StringIO()
    for file_name, file_data, file_type, file_desc, file_size in files:
        buffer.write('--%s\r\n' % boundary)
        buffer.write('Content-Disposition: form-data; name="%s"; filename="%s"\r\n' \
            %(file_name.encode("iso-8859-1"), file_name.encode("iso-8859-1")))
        buffer.write('Content-Type: %s\r\n' %file_type)
        if file_desc:
            buffer.write('Content-Description: %s\r\n' \
                %file_desc.encode("iso-8859-1"))
        buffer.write('\r\n' + file_data + '\r\n')
    buffer.write('--' + boundary + '--\r\n\r\n')
    result = buffer.getvalue()
    
    return boundary, result


def find_acceptable_types(service_doc):
    """ Determine the mimetypes accepted by a Hub from its service document """
    
    #NOTE: assuming single collection "/feed" for now
    
    service_tree = ElementTree.XML(service_doc)
    accept_types = {}
    for accept in service_tree.findall(".//{http://www.w3.org/2007/app}accept"):
        size = accept.attrib.get("maxsize", "1048576")
        type = accept.text.lower().replace(" ", "")
        accept_types[type] = int(size)
    
    if not accept_types:
        raise ValueError("No accept types found")
    else:
        return accept_types


def url_download(url, request_timeout):
    """ Download a provided URL """
    
    connection = None
    try:
        proxy_headers = {"User-Agent": "MASAS Posting Tool Proxy"}
        request = urllib2.Request(url, headers=proxy_headers)
        connection = urllib2.urlopen(request, timeout=request_timeout)
        # 10 MB max size on most Hubs
        data = connection.read(10485760)
        return data
    finally:
        if connection:
            connection.close()



def run():
    """ Run method for standalone server"""
    
    server_config = {"server.socket_host": "0.0.0.0", "server.socket_port": 7272,
        # set to 10MB to prevent oversize uploads
        "server.max_request_body_size": 10485760}
    app_config = {"/": {"tools.staticdir.root": os.path.abspath(server_pathname),
        "tools.staticdir.on": True, "tools.staticdir.dir": "."}}
    cherrypy.config.update(server_config)
    cherrypy.tree.mount(RootPage(), "/", app_config)
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run()
