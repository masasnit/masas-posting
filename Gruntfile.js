//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  // All upfront config goes in a massive nested object.
  grunt.initConfig({
    // You can set arbitrary key-value pairs.
    distFolder: 'js',
    stagingFolder: 'stage',
    cssFolder: 'css',
    templateFolder: 'template',
    devFolder : 'dev',
    deployFolder : "deploy",
    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),
    // Grunt tasks are associated with specific properties.
    // these names generally match their npm package name.
      'template': {
          'production-html-template': {
              'options': {
                  'data': {

						'topCSSbulkentry' :  '<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/ext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/post-plugins-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/post-plugins-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/OpenLayers-2.13.1/style.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/css/post-min.css" rel="stylesheet" type="text/css"/>\n',
									
									
						'topJSbulkentry' : '<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/post-plugins-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/xml-json-min.js" type="text/javascript"></script>\n' +
									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>',
									
						'bottomJSbulkentry' : '<script src="./<%= deployFolder %>/js/bulk-entry-min.js" type="text/javascript"></script>',
						
						'topCSSclone' : '<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/ext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/post-plugins-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/OpenLayers-2.13.1/style.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/libs/GeoExt-1.2rc/css/geoext-all-min.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/css/post-min.css" rel="stylesheet" type="text/css"/>\n',
						
						'topJSclone' : '<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/post-plugins-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/xml-json-min.js" type="text/javascript"></script>\n' +
									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>',
									
						'bottomJSclonecap' : '<script src="./<%= deployFolder %>/js/update-cap-min.js" type="text/javascript"></script>',
						
						'bottomJScloneentry' : '<script src="./<%= deployFolder %>/js/update-entry-min.js" type="text/javascript"></script>',
						
						'topCSSnew' : '<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/ext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/post-plugins-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/OpenLayers-2.13.1/style.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/libs/GeoExt-1.2rc/css/geoext-all-min.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/css/post-min.css" rel="stylesheet" type="text/css"/>\n',
									
						'topJSnew' : '<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/post-plugins-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/xml-json-min.js" type="text/javascript"></script>\n' +
									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>',
									
						'bottomJSnewcap' : '<script src="./<%= deployFolder %>/js/new-cap-min.js" type="text/javascript"></script>',
						
						'bottomJSnewentry' : '<script src="./<%= deployFolder %>/js/new-entry-min.js" type="text/javascript"></script>',
						
						'topCSSupdate' : '<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/ext-all-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/ExtJS-3.4.2/css/post-plugins-min.css" rel="stylesheet" type="text/css"/>\n' +
									'<link href="<%= deployFolder %>/libs/OpenLayers-2.13.1/style.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/libs/GeoExt-1.2rc/css/geoext-all-min.css" rel="stylesheet" type="text/css"/>' +
									'<link href="<%= deployFolder %>/css/post-min.css" rel="stylesheet" type="text/css"/>\n',
									
						'topJSupdate' : '<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/ExtJS-3.4.2/post-plugins-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>\n' +
									'<script src="./<%= deployFolder %>/libs/xml-json-min.js" type="text/javascript"></script>\n' +
									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>',
									
						'bottomJSupdatecap' : '<script src="./<%= deployFolder %>/js/update-cap-min.js" type="text/javascript"></script>',
						
						'bottomJSupdateentry' : '<script src="./<%= deployFolder %>/js/update-entry-min.js" type="text/javascript"></script>',

                  }
              },
              'files': {
                  'bulk-entry.html': ['<%= templateFolder %>/bulk-entry.html.tpl'],
                  'clone-cap.html' :  ['<%= templateFolder %>/clone-cap.html.tpl'],
                  'clone-entry.html' :  ['<%= templateFolder %>/clone-entry.html.tpl'],
                  'new-cap.html' :  ['<%= templateFolder %>/new-cap.html.tpl'],
                  'new-entry.html' :  ['<%= templateFolder %>/new-entry.html.tpl'],
                  'update-cap.html' :  ['<%= templateFolder %>/update-cap.html.tpl'],
                  'update-entry.html' :  ['<%= templateFolder %>/update-entry.html.tpl'],
              }
          },
          'development-html-template': {
              'options': {
                  'data': {
                  		'topCSSbulkentry' : '<link href="libs/ExtJS-3.4.2/resources/css/ext-all.css" type="text/css" rel="stylesheet">\n' + 
                  					'<link href="libs/OpenLayers-2.13.1/theme/default/style.css" type="text/css" rel="stylesheet">\n' +
                  					'<link href="libs/GeoExt-1.2rc/resources/css/geoext-all.css" type="text/css" rel="stylesheet">\n' +
                  					'<link href="css/post.css" type="text/css" rel="stylesheet">',
                  					
                  		'topJSbulkentry' : '<script src="libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/RowExpander.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/ExtJS-3.4.2/examples/ux/CheckColumn.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/OpenLayers.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/extensions/LoadingPanel.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/OpenLayers-2.13.1/lib/extensions/MASAS.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/GeoExt-1.2rc/lib/GeoExt.js" type="text/javascript"></script>\n' +
                  					'<script src="libs/json2.js" type="text/javascript"></script>\n' +
  									'<script src="libs/JsonXml.js" type="text/javascript"></script>\n' +
  									'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n',
                  					
						'bottomJSbulkentry' : '<script src="js/src/common.js" type="text/javascript"></script>\n' +
									'<script src="js/postMap.js" type="text/javascript"></script>\n' +
									'<script src="js/bulk-entry.js" type="text/javascript"></script>\n',
						
						'topCSSclone' : '<link href="libs/ExtJS-3.4.2/resources/css/ext-all.css" type="text/css" rel="stylesheet">\n' +
								  '<link href="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/css/fileuploadfield.css" type="text/css" rel="stylesheet">\n' +
								  '<link href="libs/ExtJS-3.4.2/extensions/css/virtualkeyboard.css" type="text/css" rel="stylesheet">\n' +
								  '<link href="libs/OpenLayers-2.13.1/theme/default/style.css" type="text/css" rel="stylesheet">\n' +
								  '<link href="libs/GeoExt-1.2rc/resources/css/geoext-all.css" type="text/css" rel="stylesheet">\n' +
								  '<link href="css/post.css" type="text/css" rel="stylesheet">\n',
								  
						'topJSclone': '<script src="libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js" type="text/javascript"></script>\n' +
  							'<script src="libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>\n' +
  							'<script src="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/FileUploadField.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/RowExpander.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.plugins.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.tree.TreeFilterX.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/Reorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/ToolbarReorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/OpenLayers.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/LoadingPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/MASAS.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/OWM.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/GeoExt.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/extensions/GoogleStreetViewPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/json2.js" type="text/javascript"></script>\n' +
							'<script src="libs/JsonXml.js" type="text/javascript"></script>\n' +
							'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n',
							
						'bottomJSclonecap' :   '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/update-cap.js" type="text/javascript"></script>\n',
							
						'bottomJScloneentry' : '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/KML.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/update-entry.js" type="text/javascript"></script>\n',
							
						'topCSSnew' : '<link href="libs/ExtJS-3.4.2/resources/css/ext-all.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/css/fileuploadfield.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/ExtJS-3.4.2/extensions/css/virtualkeyboard.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/OpenLayers-2.13.1/theme/default/style.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/GeoExt-1.2rc/resources/css/geoext-all.css" type="text/css" rel="stylesheet">\n' +
							'<link href="css/post.css" type="text/css" rel="stylesheet">\n',
							
						'topJSnew' : '<script src="libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/FileUploadField.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.plugins.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.tree.TreeFilterX.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/Reorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/ToolbarReorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/OpenLayers.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/LoadingPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/MASAS.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/OWM.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/GeoExt.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/extensions/GoogleStreetViewPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/json2.js" type="text/javascript"></script>\n' +
							'<script src="libs/JsonXml.js" type="text/javascript"></script>\n' +
							'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n',
							
						'bottomJSnewcap' : '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/new-cap.js" type="text/javascript"></script>\n',
							
						'bottomJSnewentry' :   '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/KML.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/new-entry.js" type="text/javascript"></script>\n',
							
						'topCSSupdate' :   '<link href="libs/ExtJS-3.4.2/resources/css/ext-all.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/css/fileuploadfield.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/ExtJS-3.4.2/extensions/css/virtualkeyboard.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/OpenLayers-2.13.1/theme/default/style.css" type="text/css" rel="stylesheet">\n' +
							'<link href="libs/GeoExt-1.2rc/resources/css/geoext-all.css" type="text/css" rel="stylesheet">\n' +
							'<link href="css/post.css" type="text/css" rel="stylesheet">\n',
							
						'topJSupdate' : '<script src="libs/ExtJS-3.4.2/adapter/ext/ext-base-debug.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/fileuploadfield/FileUploadField.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/RowExpander.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.plugins.VirtualKeyboard.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/extensions/Ext.ux.tree.TreeFilterX.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/Reorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/ExtJS-3.4.2/examples/ux/ToolbarReorderer.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/OpenLayers.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/LoadingPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/MASAS.js" type="text/javascript"></script>\n' +
							'<script src="libs/OpenLayers-2.13.1/lib/extensions/OWM.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/GeoExt.js" type="text/javascript"></script>\n' +
							'<script src="libs/GeoExt-1.2rc/lib/extensions/GoogleStreetViewPanel.js" type="text/javascript"></script>\n' +
							'<script src="libs/json2.js" type="text/javascript"></script>\n' +
							'<script src="libs/JsonXml.js" type="text/javascript"></script>\n' +
							'<script src="http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false" type="text/javascript"></script>\n',
							
						'bottomJSupdateentry' :   '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/KML.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/update-entry.js" type="text/javascript"></script>\n',
							
						'bottomJSupdatecap' :   '<script src="js/postTemplates.js" type="text/javascript"></script>\n' +
							'<script src="js/src/AddressSearch.js" type="text/javascript"></script>\n' +
							'<script src="js/src/Layers.js" type="text/javascript"></script>\n' +
							'<script src="js/src/validators.js" type="text/javascript"></script>\n' +
							'<script src="js/src/common.js" type="text/javascript"></script>\n' +
							'<script src="js/postMap.js" type="text/javascript"></script>\n' +
							'<script src="js/mapLayers.js" type="text/javascript"></script>\n' +
							'<script src="js/mapTools.js" type="text/javascript"></script>\n' +
							'<script src="js/update-cap.js" type="text/javascript"></script>\n',
                  }
              },
              'files': {
                  'bulk-entry.html': ['<%= templateFolder %>/bulk-entry.html.tpl'],
                  'clone-cap.html' :  ['<%= templateFolder %>/clone-cap.html.tpl'],
                  'clone-entry.html' :  ['<%= templateFolder %>/clone-entry.html.tpl'],
                  'new-cap.html' :  ['<%= templateFolder %>/new-cap.html.tpl'],
                  'new-entry.html' :  ['<%= templateFolder %>/new-entry.html.tpl'],
                  'update-cap.html' :  ['<%= templateFolder %>/update-cap.html.tpl'],
                  'update-entry.html' :  ['<%= templateFolder %>/update-entry.html.tpl'],
              }
          }
      },
      'shell': {
        'build': {
            'options': {
                'stdout': true
            },
            'command': ['cd build', 'python build.py'].join('&&')
        }
    }
  }); // The end of grunt.initConfig

  // We've set up each task's configuration.
  // Now actually load the tasks.
  // This will do a lookup similar to node's require() function.
  grunt.loadNpmTasks('grunt-template');
  grunt.loadNpmTasks('grunt-shell');
  // Register our own custom task alias.
  grunt.registerTask('prod-build', ['shell:build','template:production-html-template']);
  grunt.registerTask('dev-build', ['template:development-html-template']);
};