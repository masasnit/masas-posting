#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:         Example Template List
Author:       Jacob Westfall
Copyright:    Independent Joint Copyright (c) 2011 MASAS Contributors.
              Released under the Modified BSD license.  See license.txt for
              the full text of the license.
Created:      Feb 01, 2011
Updated:      Oct 21, 2013
Description:  An example listing of template values for Entry and CAP.
"""

# template group names should be a single word only and the default group should
# be listed first

entry_groups = ["Desktop", "Tablet"]

# allows several templates to share the same colour values and also searching
# of values for updates
colour_datasets = [\
    {"label": "Test Colour",
     "data": [["", None], ["Black", "Black Test"], ["Gray", "Gray Test"]],
    },
]

entry = {
    "Desktop": [\
        {"name": "Test Entry",
         "category": "Other",
         "icon": "other",
         "en_title": "Test Entry",
         "en_content": "Testing a new Entry",
         "fr_title": "Test Entry FR",
         "fr_content": "Testing a new Entry FR",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Test Entry 2",
         "category": "Other",
         "icon": "other",
         "en_title": "Test Entry 2",
         "en_content": "Testing a new Entry 2",
         "fr_title": "Test Entry FR 2",
         "fr_content": "Testing a new Entry FR 2",
         "severity": "Minor",
         "colour": colour_datasets[0],
         "expires": 24,
         "update_allow": "all",
        },
        ],
    "Tablet": [\
        {"name": "Test Entry Tablet",
         "category": "Other",
         "icon": "other",
         "en_title": "Test Entry Tablet",
         "en_content": "Testing a new Entry Tablet",
         "fr_title": "Test Entry FR Tablet",
         "fr_content": "Testing a new Entry FR Tablet",
         "severity": "Minor",
         "expires": 24,
        },
        ],
}


cap_groups = ["Desktop"]

cap = {
    "Desktop": [\
        {"name": "Test CAP",
         "event": "other",
         "urgency": "Past",
         "severity": "Minor",
         "certainty": "Unlikely",
         "en_headline": "Test Alert",
         "en_description": "Testing a new Alert",
         "en_instruction": "Disregard this test",
         "fr_headline": "Test Alert FR",
         "fr_description": "Testing a new Alert FR",
         "fr_instruction": "Disregard this test FR",
         "expires": 24,
        }
        ],
}