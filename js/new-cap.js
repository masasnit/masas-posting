/**
Create New CAP Alert
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires postTemplates.js
@requires postMap.js
@requires src/common.js
@requires src/validators.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('POST');
// blank objects to start
POST.locationMapPanel = null;
POST.areaZoneStore = null;
POST.areaSearchGrid = null;
POST.reviewWindow = null;
POST.postingTimer = null;
POST.POST_RESULT = '';

// for progress bar indicator
POST.PROGRESS_AMOUNT = 0;
// complete is 1 so divide by number of pages
POST.PROGRESS_INCREMENT = 0.5;


Ext.onReady(function () {
    
    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();
    
    // Custom field validations
    Ext.form.VTypes.location = function (v) {
	var location_formfield = Ext.getCmp('cap-location');
	var v_result = [false, true];
	if (location_formfield.geom === 'point') {
	    v_result = POST.Validator.check_point(v);
	} else if (location_formfield.geom === 'line') {
	    v_result = POST.Validator.check_line(v);
	} else if (location_formfield.geom === 'polygon') {
	    v_result = POST.Validator.check_polygon(v);
	} else if (location_formfield.geom === 'circle') {
	    v_result = POST.Validator.check_circle(v);
	} else if (location_formfield.geom === 'box') {
	    v_result = POST.Validator.check_box(v); 
	}
	if (v_result[1] === false) {
	    alert('Please note that this Location is outside North America.');
	}
	
	return v_result[0];
    };
    Ext.form.VTypes.locationText = 'Must be a valid location';
    
    var vkeyboard_plugin = new Ext.ux.plugins.VirtualKeyboard();
    
    
    // setting up the location map, layers, and controls
    POST.Map.initialize_location_map();
    POST.Map.initialize_drawing_controls(false);
    
    
    var headerBox = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Posting Tool</div>' +
            '<div id="headerDetailBlock">Version 0.2</div>'
    });
    
    var progressBar = new Ext.ProgressBar({
        id: 'progressBar',
        width: 150,
        style: 'margin-left: 25px',
        text: 'Progress'
    });
    
    var cardNav = function (direction) {
        var card_panel = Ext.getCmp('cap-wizard-panel').getLayout();
        var current_card = parseInt(card_panel.activeItem.id.split('card-')[1], 10);
        
        /* Card validation */
        // validate this card's form items first
        var found_invalid_item = false;
        card_panel.activeItem.cascade(function (item) {
            if (item.isFormField) {
                if (!item.validate()) {
                    found_invalid_item = true;
                }
            }
        });
        // validation check is performed here, comment out to bypass
        if (found_invalid_item) {
            return;
        }
        
        // validation for event tree selection, effective, and expires on input card
        if (current_card === 0) {
            var event_selection = Ext.getCmp('cap-event').getSelectionModel().getSelectedNode();
            if (!event_selection) {
                alert('An event must be selected.');
                return;
            } else {
                if (!event_selection.attributes.term) {
                    alert('An event must be selected.');
                    return;
                }
            }
            var effective_dt = Ext.get('cap-effective-dt').getValue();
            var effective_tm = Ext.get('cap-effective-tm').getValue();
            if (effective_dt && !effective_tm) {
                alert('Effective time must be set.');
                return;
            } else if (!effective_dt && effective_tm) {
                alert('Effective date must be set.');
                return;
            }
            var expires_dt = Ext.get('cap-expires-dt').getValue();
            var expires_tm = Ext.get('cap-expires-tm').getValue();
            if (expires_dt && !expires_tm) {
                alert('Expires time must be set.');
                return;
            } else if (!expires_dt && expires_tm) {
                alert('Expires date must be set.');
                return;
            }
            //TODO: further validation of the values, such as effective must
            //      come before expires?
        }
        
        // special validation for area zones since its a grid
        if (current_card === 1) {
            if (POST.areaSearchGrid.doValidation) {
                if (!POST.areaSearchGrid.getSelectionModel().hasSelection()) {
                    alert('Area selection is required.');
                    return;
                }
            }
            // when a Custom Area is chosen, search for any applicable
            // SGC geocodes to use, async search as part of leaving this card
            if (!Ext.getCmp('areaCreateSet').collapsed) {
                POST.custom_geocode_search();
            }
        }
        
        // change to the next or previous card
        var next_card = current_card + direction;
        card_panel.setActiveItem(next_card);
        // update the next or previous buttons depending on what card is
        // now active in the stack
        if (next_card === 0) {
            Ext.getCmp('card-prev').setDisabled(true);
        } else if (next_card === 1) {
            Ext.getCmp('card-prev').setDisabled(false);
            Ext.getCmp('card-next').setDisabled(false);
        } else if (next_card === 2) {
            Ext.getCmp('card-next').setDisabled(true);
        }
        // progress bar update
        if (direction === 1) {
            POST.PROGRESS_AMOUNT += POST.PROGRESS_INCREMENT;
        } else if (direction === -1) {
            POST.PROGRESS_AMOUNT -= POST.PROGRESS_INCREMENT;
        }
        progressBar.updateProgress(POST.PROGRESS_AMOUNT);
        
        /* Other card operations */
        // cleanup stray GeoExt popup windows which stick around between
        // panel changes
        if (current_card === 1) {
            POST.Common.layer_popup_cleanup(POST.locationLayers.location);
        }
    };
    
    /* inputCAPCard */
    
    var templatePanel = new Ext.Panel({
        width: 200,
        height: 200,
        header: false,
        floating: true,
        autoScroll: true,
        layout: 'fit',
        tbar: new Ext.Toolbar({items: [
            'Templates',
            '->',
            {
                xtype: 'combo',
                width: 100,
                // the store holds the template group names and a group
                // selection defaulted to start but can be changed and will
                // reload the template list store accordingly
                store: POST.CAP_TEMPLATE_GROUPS,
                value: POST.CAP_TEMPLATE_GROUP_SELECT,
                editable: false,
                forceSelection: true,
                triggerAction: 'all',
                listeners: {
                    select: function (combo, record) {
                        POST.CAP_TEMPLATE_GROUP_SELECT = record.data.field1;
                        var t_store = Ext.getCmp('cap-template-list').store;
                        t_store.proxy.setUrl(OpenLayers.Util.urlAppend(POST.CAP_TEMPLATE_URL,
                        'group=' + POST.CAP_TEMPLATE_GROUP_SELECT));
                        t_store.reload();
                    }
                }
            }
        ]}),
        items: new Ext.list.ListView({
            id: 'cap-template-list',
            store: new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: OpenLayers.Util.urlAppend(POST.CAP_TEMPLATE_URL,
                        'group=' + POST.CAP_TEMPLATE_GROUP_SELECT)
                }),
                reader: new Ext.data.JsonReader({
                    root: 'templates',
                    fields: ['name', 'num']
                }),
                autoLoad: true
            }),
            hideHeaders: true,
            loadingText: 'Loading Templates',
            columns: [{
                header: 'Template',
                dataIndex: 'name'
            }],
            listeners: {
                click: function (view, idx, node) {
                    var record = view.getRecord(node);
                    OpenLayers.Request.GET({
                        url: POST.CAP_TEMPLATE_URL,
                        params: {
                            'group': POST.CAP_TEMPLATE_GROUP_SELECT,
                            'template': record.data.num,
                            // cache busting parameter used as well
                            '_dc': new Date().getTime()
                        },
                        failure: function () {
                            alert('Unable to load template.');
                            console.error('Unable to load template');
                        },
                        success: POST.load_template
                    });
                }
            }
        })
    });
    // must set this location relative to the inputCAPCard after rendering
    templatePanel.setPosition(700, 0);
    
    var statusCombo = new Ext.form.ComboBox({
        fieldLabel: '<b>Status</b>',
        name: 'cap-status',
        id: 'cap-status',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 100,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'name',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'tip'],
            // used by MASAS-X to set different values depending on Mode
            data: (POST.CUSTOM_STATUS_VALUES) ? POST.CUSTOM_STATUS_VALUES :
                [ ['Actual', 'Actual Entries'],
                ['Exercise', 'Exercises and other practice entries'],
                ['Test', 'Test Entries'] ]
        }),
        listeners: {
            afterrender: function (combo) {
                // need to set an initial selection this way because you don't
                // know what the first value might be up front when using
                // custom values, 
                var first_val = combo.getStore().getAt(0);
                combo.setValue(first_val.get('name'));
            }
        }
    });
    
    var eventTree = new Ext.tree.TreePanel({
        id: 'cap-event',
        autoScroll: true,
        animate: true,
        border: true,
        height: 175,
        width: 400,
        loader: new Ext.tree.TreeLoader({
            dataUrl: POST.EVENT_LIST_URL,
            requestMethod: 'GET',
            preloadChildren: true,
            // fix a preloadChildren bug, was to be fixed in ExtJS 3.3.2 but
            // doesn't appear to be as of 3.4.0, old ticket URL not longer working
            // http://code.extjs.com:8080/ext/ticket/1430
            load : function (node, callback, scope) {
                if (this.clearOnLoad) {
                    while (node.firstChild) {
                        node.removeChild(node.firstChild);
                    }
                }
                if (this.doPreload(node)) { // preloaded json children
                    this.runCallback(callback, scope || node, [node]);
                } else if (this.directFn || this.dataUrl || this.url) {   
                    // MB
                    if (this.preloadChildren) {
                        if (typeof(callback) !== 'function') {
                            callback = Ext.emptyFn;
                        }
                        callback = callback.createInterceptor(function (node) {
                            for (var i = 0; i < node.childNodes.length; i++) {
                                this.doPreload(node.childNodes[i]);
                            }
                        }, this);
                    }
                    // end-MB
                    this.requestData(node, callback, scope || node);
                }
            }
        }),
        root: {
            nodeType: 'async',
            text: 'Root Node'
        },
        rootVisible: false,
        listeners: {
            render: function () {
                this.getRootNode().expand();
            },
            click: function (node) {
                if (node.attributes.term !== null) {
                    var icon_url = POST.ICON_PREVIEW_URL + node.attributes.term +
                        '/large.png';
                    Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                        icon_url + '" alt="Icon"></div>');
                }
                // updating both english and french values, setRawValue means no validation
                if (node.attributes.text) {
                    Ext.getCmp('cap-en-headline').setRawValue(node.attributes.text +
                        ' alert issued by ' + POST.CAP_MESSAGE.alert.info[0].senderName + ' for (area)');
                }
                if (node.attributes.f_text) {
                    Ext.getCmp('cap-fr-headline').setRawValue(node.attributes.f_text +
                        ' alerte publie du ' + POST.CAP_MESSAGE.alert.info[1].senderName + ' pour (area)');
                }
            }
        },
        tbar: ['Search:', {
                xtype: 'trigger',
                width: 100,
                triggerClass: 'x-form-clear-trigger',
                onTriggerClick: function () {
                    this.setValue('');
                    eventTree.filter.clear();
                },
                id: 'filter',
                enableKeyEvents: true,
                listeners: {
                    keyup: {buffer: 150, fn: function (field, e) {
                        if (Ext.EventObject.ESC === e.getKey()) {
                            field.onTriggerClick();
                        } else {
                            var val = this.getRawValue();
                            var re = new RegExp('.*' + val + '.*', 'i');
                            eventTree.filter.clear();
                            eventTree.filter.filter(re, 'text');
                        }
                    }}
                }
        }, '->', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeExpand',
                text: 'Expand',
                tooltip: 'Expand the entire event tree',
                handler: function () {
                    eventTree.expandAll();
                }
        }), '-', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeCollapse',
                text: 'Collapse',
                tooltip: 'Collapse the entire event tree',
                handler: function () {
                    eventTree.collapseAll();
                }
        })
        ]
    });
    eventTree.filter = new Ext.ux.tree.TreeFilterX(eventTree);
    
    var intervalCombo = new Ext.form.ComboBox({
        name: 'cap-expires-interval',
        id: 'cap-expires-interval',
        allowBlank: true,
        editable: false,
        width: 125,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['interval', 'name', 'tip'],
            data: [ [null, '', 'Do not set expires'],
                [1, '1 Hour', 'Expires in 1 hour'],
                [6, '6 Hours', 'Expires in 6 hours'],
                [12, '12 Hours', 'Expires in 12 hours'],
                [24, '24 Hours', 'Expires in 1 day'],
                [48, '48 Hours', 'Expires in 2 days'] ]
        }),
        listeners: {
            select: function (combo, record, index) {
                if (record.data.interval) {
                    var dt_check = Ext.get('cap-expires-dt').getValue();
                    if (dt_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                }
                // any change removes highlighting of initial default
                combo.removeClass('expiresIntervalDefault');
            },
            afterrender: function (combo) {
                if (POST.DEFAULT_EXPIRES_INTERVAL) {
                    // if a default expires interval is provided, modify the
                    // display name, select the default, and highlight it to
                    // make clear to the user it was defaulted.
                    var d_idx = combo.getStore().find('interval',
                        POST.DEFAULT_EXPIRES_INTERVAL);
                    if (d_idx !== -1) {
                        var d_rec = combo.getStore().getAt(d_idx);
                        var d_val = d_rec.get('name');
                        if (d_val.search('-Default') === -1) {
                            d_rec.set('name', d_val + '-Default');
                            d_rec.commit();
                        }
                        combo.setValue(d_rec.get('interval'));
                        combo.addClass('expiresIntervalDefault');
                    }
                }
            }
        }
    });
    
    var inputCAPCard = new Ext.FormPanel({
        id: 'card-0',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        defaultType: 'textfield',
        items: [ templatePanel, statusCombo,
        {
            xtype: 'compositefield',
            fieldLabel: '<b>Event</b>',
            //width: 300,
            //defaults: { flex: 1 },
            items: [ eventTree,
            {
                xtype: 'displayfield',
                id: 'icon-preview-box',
                html: '<div style="padding: 15px;"></div>'
            }]
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Urgency</b>',
            id: 'cap-urgency',
            allowBlank: false,
            width: 500,
            items: [{boxLabel: 'Unknown', name: 'cap-urgency', inputValue: 'Unknown'},
                {boxLabel: 'Past', name: 'cap-urgency', inputValue: 'Past'},
                {boxLabel: 'Future', name: 'cap-urgency', inputValue: 'Future'},
                {boxLabel: 'Expected', name: 'cap-urgency', inputValue: 'Expected'},
                {boxLabel: 'Immediate', name: 'cap-urgency', inputValue: 'Immediate'}]
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Severity</b>',
            id: 'cap-severity',
            allowBlank: false,
            width: 500,
            items: [{boxLabel: 'Unknown', name: 'cap-severity', inputValue: 'Unknown'},
                {boxLabel: 'Minor', name: 'cap-severity', inputValue: 'Minor'},
                {boxLabel: 'Moderate', name: 'cap-severity', inputValue: 'Moderate'},
                {boxLabel: 'Severe', name: 'cap-severity', inputValue: 'Severe'},
                {boxLabel: 'Extreme', name: 'cap-severity', inputValue: 'Extreme'}]
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Certainty</b>',
            id: 'cap-certainty',
            allowBlank: false,
            width: 500,
            items: [{boxLabel: 'Unknown', name: 'cap-certainty', inputValue: 'Unknown'},
                {boxLabel: 'Unlikely', name: 'cap-certainty', inputValue: 'Unlikely'},
                {boxLabel: 'Possible', name: 'cap-certainty', inputValue: 'Possible'},
                {boxLabel: 'Likely', name: 'cap-certainty', inputValue: 'Likely'},
                {boxLabel: 'Observed', name: 'cap-certainty', inputValue: 'Observed'}]
        }, {
            fieldLabel: '<b>Headline</b>',
            name: 'cap-en-headline',
            id: 'cap-en-headline',
            width: '75%',
            minLength: 5,
            minLengthText: 'A good headline should say what and where',
            maxLength: 160,
            maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
            blankText: 'MASAS requires a headline value',
            allowBlank: false
        }, {
            xtype: 'textarea',
            fieldLabel: 'Description',
            name: 'cap-en-description',
            id: 'cap-en-description',
            height: 50,
            width: '75%'
        }, {
            xtype: 'textarea',
            fieldLabel: 'Instruction',
            name: 'cap-en-instruction',
            id: 'cap-en-instruction',
            height: 30,
            width: '75%'
        }, {
            fieldLabel: 'Contact',
            name: 'cap-en-contact',
            id: 'cap-en-contact',
            width: '50%'
        }, {
            fieldLabel: 'Web Link',
            name: 'cap-en-web',
            id: 'cap-en-web',
            width: '50%',
            allowBlank: true,
            vtype: 'url'
        }, {
            xtype: 'fieldset',
            checkboxToggle: true,
            collapsed: true,
            forceLayout: true,
            title: 'French',
            id: 'cap-add-french',
            defaultType: 'textfield',
            items: [{
                fieldLabel: '<b>Headline</b>',
                name: 'cap-fr-headline',
                id: 'cap-fr-headline',
                width: '75%',
                minLengthText: 'A good headline should say what and where',
                maxLength: 160,
                maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
                blankText: 'MASAS requires a headline value',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Description',
                name: 'cap-fr-description',
                id: 'cap-fr-description',
                height: 50,
                width: '75%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Instruction',
                name: 'cap-fr-instruction',
                id: 'cap-fr-instruction',
                height: 30,
                width: '75%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                fieldLabel: 'Contact',
                name: 'cap-fr-contact',
                id: 'cap-fr-contact',
                width: '50%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                fieldLabel: 'Web Link',
                name: 'cap-fr-web',
                id: 'cap-fr-web',
                width: '50%',
                allowBlank: true,
                vtype: 'url'
            }],
            listeners: {
                expand: function () {
                    // reposition the icon because it was hidden on render
                    Ext.getCmp('cap-fr-headline').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-description').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-instruction').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-contact').alignKeyboardIcon();
                    // enable validation of french values
                    Ext.getCmp('cap-fr-headline').allowBlank = false;
                    Ext.getCmp('cap-fr-headline').minLength = 5;
                    Ext.getCmp('cap-fr-area-description').show();
                    Ext.getCmp('cap-fr-area-description').allowBlank = false;
                },
                collapse: function () {
                    // disable validation of french values when not using
                    Ext.getCmp('cap-fr-headline').allowBlank = true;
                    Ext.getCmp('cap-fr-headline').minLength = null;
                    Ext.getCmp('cap-fr-area-description').hide();
                    Ext.getCmp('cap-fr-area-description').allowBlank = true;
                }
            }
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Effective',
            // width ensures the validation error icon will display beside fields
            width: 325,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Immediately',
                name: 'cap-effective-immediate',
                id: 'cap-effective-immediate',
                // setting a height so the bottom part of the date/time fields
                // won't be cut off
                height: 23,
                checked: true,
                handler: function (checkbox, checked) {
                    if (checked) {
                        Ext.getCmp('cap-effective-at').hide();
                        Ext.getCmp('cap-effective-dt').hide();
                        Ext.getCmp('cap-effective-dt').reset();
                        Ext.getCmp('cap-effective-tm').hide();
                        Ext.getCmp('cap-effective-tm').reset();
                    } else {
                        Ext.getCmp('cap-effective-at').show();
                        Ext.getCmp('cap-effective-dt').show();
                        Ext.getCmp('cap-effective-tm').show();
                    }
                }
            }, {
                xtype: 'displayfield',
                id: 'cap-effective-at',
                html: ' at:',
                style: {'padding': '3px 2px 2px 20px'}
            }, {
                xtype: 'datefield',
                name: 'cap-effective-dt',
                id: 'cap-effective-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date()
            }, {
                xtype: 'timefield',
                name: 'cap-effective-tm',
                id: 'cap-effective-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var effective_dt = Ext.get('cap-effective-dt').getValue();
                    if (!effective_dt || effective_dt.length === 0) {
                        effective_dt = null;
                    }
                    return POST.Validator.check_future_time(effective_dt, val);
                }
            }],
            listeners: {afterrender: function () {
                // the date/time fields can't have the hidden property set at
                // render because their special sizing results in problems, so
                // hide them afterwards
                Ext.getCmp('cap-effective-at').hide();
                Ext.getCmp('cap-effective-dt').hide();
                Ext.getCmp('cap-effective-tm').hide();
            }}
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Expires at',
            // width ensures the validation error icon will display beside fields
            width: 425,
            items: [{
                xtype: 'datefield',
                name: 'cap-expires-dt',
                id: 'cap-expires-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                listeners: {select: function () {
                    var it_check = Ext.getCmp('cap-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'timefield',
                name: 'cap-expires-tm',
                id: 'cap-expires-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var expires_dt = Ext.get('cap-expires-dt').getValue();
                    if (!expires_dt || expires_dt.length === 0) {
                        expires_dt = null;
                    }
                    return POST.Validator.check_future_time(expires_dt, val);
                },
                listeners: {select: function () {
                    var it_check = Ext.getCmp('cap-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'displayfield',
                html: 'or Expires in:',
                style: {'padding': '3px 2px 2px 20px'}
            }, intervalCombo ]
        } ]
    });
    
    /* locationCAPCard */
    
    // setup the map Layers and Tools toolbars
    POST.MapLayers.initialize();
    POST.MapTools.initialize(false);
    
    // create map panel
    POST.locationMapPanel = new GeoExt.MapPanel({
        collapsible: false,
        style: 'margin-bottom: 5px;',
        // 32% room left at the bottom for Location and Areas
        anchor: '100% 68%',
        map: POST.locationMap,
        tbar: POST.mapToolbarTop,
        bbar: POST.mapToolbarBottom
    });
    
    var areaSearchCombo = new Ext.form.ComboBox({
        id: 'area-search-radius',
        width: 100,
	editable: false,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        emptyText: 'within...',
        displayField: 'name',
        valueField: 'value',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['value', 'name'],
            data: [ [5, '5 km'], [50, '50 km'], [100, '100 km'] ]
        }),
        listeners: {select: POST.existing_area_search}
    });
    
    var areaTypeCombo = new Ext.form.ComboBox({
        id: 'area-search-type',
        width: 150,
	editable: false,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['value', 'name'],
            data: [ ['csd', 'Cities/Towns - CSD'], ['cd', 'Counties - CD'],
                ['other', 'Other'] ]
        }),
        value: 'csd',
        listeners: {select: POST.existing_area_search}
    });
    
    POST.areaZoneStore = new GeoExt.data.FeatureStore({
        layer: POST.locationLayers.sgc,
        fields: [
            {name: 'e_name', type: 'string'},
            {name: 'f_name', type: 'string'},
            {name: 'sgc', type: 'string'}
        ],
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.HTTP({
                url: POST.AREA_ZONE_URL,
                format: new OpenLayers.Format.GeoJSON({
                    internalProjection: POST.locationMap.getProjectionObject(),
                    externalProjection: new OpenLayers.Projection('EPSG:4326')
                })
            })
        }),
        autoLoad: false
    });
    
    POST.areaSearchGrid = new Ext.grid.GridPanel({
        hideHeaders: true,
        fieldLabel: '<b>Select</b>',
        name: 'cap-area-zones',
        id: 'cap-area-zones',
        width: 250,
        height: 75,
        doValidation: false,
        store: POST.areaZoneStore,
        columns: [{
            header: "Name",
            width: 245,
            dataIndex: "e_name"
        }],
        sm: new GeoExt.grid.FeatureSelectionModel() 
    });

    var locationCAPCard = new Ext.FormPanel({
        id: 'card-1',
        labelWidth: 100,
        // this layout type allows for % based sizing
        layout: 'anchor',
        items: [POST.locationMapPanel, {
            xtype: 'compositefield',
	    height: 35,
            items: [{
                // form field titles don't seem to display when using the
                // anchor layout so create one instead
                xtype: 'displayfield',
                id: 'cap-location-title',
                html: '<b>Location:</b> '
            }, {
                xtype: 'textfield',
                name: 'cap-location',
                id: 'cap-location',
                width: 400,
                vtype: 'location',
                validationEvent: false,
                blankText: 'A location is required',
                allowBlank: false
	    }, {
		boxLabel: '<span style="color: #606060">Use Alert Area for Location instead of this Point</span>',
		xtype: 'checkbox',
		style: 'margin-left: 100px',
		name: 'discard-event-location',
		id: 'discard-event-location',
		checked: false
	    }]
        }, {
            layout: 'column',
            border: false,
            items: [{
                xtype: 'fieldset',
                width: '40%',
                style: 'margin-right: 15px; padding: 5px;',
                checkboxToggle: true,
                // default for now is to use create new instead
                collapsed: true,
                title: 'Area: Defined Boundaries (CAP-CP)',
                id: 'areaSelectSet',
                items: [{
                    xtype: 'compositefield',
                    fieldLabel: '<b>Search</b>',
                    items: [ areaSearchCombo, areaTypeCombo ]
                }, POST.areaSearchGrid ],
                listeners: {
                    expand: function () {
                        POST.locationLayers.sgc.setVisibility(true);
                        // collapse the create fieldset, toggle validated fields
                        Ext.getCmp('areaCreateSet').collapse();
                        areaSearchCombo.allowBlank = false;
                        POST.areaSearchGrid.doValidation = true;
                    },
                    collapse: function () {
                        // hiding the SGC layer because if its not going to be used
                        // it can clutter the map
                        POST.locationLayers.sgc.setVisibility(false);
                        // don't allow both fieldsets to be collapsed
                        if (Ext.getCmp('areaCreateSet').collapsed) {
                            Ext.getCmp('areaCreateSet').expand();
                            return;
                        }
                        areaSearchCombo.allowBlank = true;
                        POST.areaSearchGrid.doValidation = false;
                    }
                }
            }, { xtype: 'displayfield', hideLabel: true, html: '<b> OR </b>' }, {
                xtype: 'fieldset',
                width: '40%',
                style: 'margin-left: 15px; padding: 5px;',
                checkboxToggle: true,
                // default for now is to use create new instead
                collapsed: false,
                title: 'Area: Custom Boundary',
                id: 'areaCreateSet',
                items: [{
                    xtype: 'displayfield',
                    hideLabel: true,
                    html: 'Using Location as the center of a new area circle'
                }, {
                    xtype: 'numberfield',
                    fieldLabel: '<b>Radius</b> (km)',
                    name: 'cap-radius',
                    id: 'cap-radius',
                    width: 50,
                    blankText: 'A radius in km between 0 and 500 is required',
                    allowBlank: false,
		    allowNegative: false,
		    maxValue: 500
                }, {
                    xtype: 'textfield',
                    fieldLabel: '<b>Description</b>',
                    name: 'cap-en-area-description',
                    id: 'cap-en-area-description',
                    width: 250,
                    maxLength: 100,
                    blankText: 'An area description is required',
                    allowBlank: false
                }, {
                    xtype: 'textfield',
                    fieldLabel: '<b>French</b>',
                    name: 'cap-fr-area-description',
                    id: 'cap-fr-area-description',
                    width: 250,
                    maxLength: 100,
                    blankText: 'A French area description is required',
                    allowBlank: true,
                    hidden: true
                }],
                listeners: {
                    expand: function () {
                        // collapse the select fieldset, toggle validated fields
                        Ext.getCmp('areaSelectSet').collapse();
                        Ext.getCmp('cap-radius').allowBlank = false;
                        Ext.getCmp('cap-en-area-description').allowBlank = false;
                        if (!Ext.getCmp('cap-add-french').collapsed) {
                            Ext.getCmp('cap-fr-area-description').allowBlank = false;
                        }
                    },
                    collapse: function () {
                        // don't allow both fieldsets to be collapsed
                        if (Ext.getCmp('areaSelectSet').collapsed) {
                            Ext.getCmp('areaSelectSet').expand();
                            return;
                        }
                        Ext.getCmp('cap-radius').allowBlank = true;
                        Ext.getCmp('cap-en-area-description').allowBlank = true;
                        Ext.getCmp('cap-fr-area-description').allowBlank = true;
                    }
                },
                // container to hold SGC geocode search results, with default
                sgcGeocode: ['none']
            }]
        }]
    });
    
    /* postCAPCard */
    
    var postCAPCard = new Ext.Panel({
        id: 'card-2',
        items: [{
            border: false,
            bodyStyle: 'margin-bottom: 50px',
            html: '<div class="postingCard" id="postingBox"><h1>Ready to Post</h1>' + 
                '<a href="#" onclick="POST.preview_cap_xml(); return false;" style="color: grey;">Preview</a>' +
                '<a href="#" onclick="POST.post_new_alert(); return false;" style="color: green;">Post Alert</a>' +
                '</div>'
        }]
    });
    
    if (POST.EMAIL_ADDRESS_LIST) {
        var emailToCombo = new Ext.form.ComboBox({
            fieldLabel: '<b>To</b>',
            name: 'email-to-address',
            id: 'email-to-address',
            width: 300,
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            store: POST.EMAIL_ADDRESS_LIST
        });
    
        postCAPCard.add({
            xtype: 'fieldset',
            id: 'email-to-fieldset',
            hidden: true,
            collapsible: true,
            collapsed: true,
            title: 'Email Forwarding',
            labelWidth: 70,
            listeners: {expand: POST.Common.generate_email_content },
            items: [ emailToCombo, {
                xtype: 'textfield',
                fieldLabel: '<b>Subject</b>',
                name: 'email-to-subject',
                id: 'email-to-subject',
                width: 400,
                minLength: 5,
                minLengthText: 'A subject is required',
                maxLength: 250,
                maxLengthText: 'Subject cannot be longer than 250 characters'
            }, {
                xtype: 'textarea',
                fieldLabel: '<b>Message</b>',
                name: 'email-to-message',
                id: 'email-to-message',
                height: 150,
                width: 800,
                minLength: 5,
                minLengthText: 'A message is required'
            }, {
                xtype: 'button',
                fieldLabel: ' ',
                labelSeparator: '',
                text: 'Send',
                width: 75,
                handler: POST.Common.post_email_message
            }]
        });
    }
    
    /* capPanel */
    
    // the top toolbar for the app
    var panel_tbar_items = [];
    // optional support for multiple hubs
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'}
        ]);
        var hubCombo = new Ext.form.ComboBox({
            allowBlank: false,
            editable: false,
            width: 125,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                afterrender: function (combo) {
                    var default_idx = combo.getStore().find('url', POST.FEED_URL);
                    if (default_idx === -1) {
                        console.error('Unable to find Hub selection default');
                        // first value in array instead
                        default_idx = 0; 
                    }
                    var default_val = combo.getStore().getAt(default_idx);
                    combo.setValue(default_val.get('title'));
                },
                select: function (combo, record) {
                    if (record.data.url) {
                        POST.FEED_URL = record.data.url;
                    }
                    if (record.data.secret) {
                        POST.USER_SECRET = record.data.secret;
                    }
                    if (record.data.uri) {
                        POST.USER_URI = record.data.uri;
                    }
                }
            }
        });
        panel_tbar_items = [
            {xtype: 'tbspacer', width: 15},
            {
                xtype: 'tbtext',
                style: {'fontWeight': 'bold', 'font-size': '13px'},
                text: 'Hub:'
            },
            {xtype: 'tbspacer', width: 25},
            hubCombo
        ];
    }
    // default items
    panel_tbar_items.push.apply(panel_tbar_items, [progressBar, '->', {
        text: '<b>Abort</b>',
        iconCls: 'abortButton',
        width: 75,
        handler: function () {
            window.close();
        }
    }]);
    
    var capPanel = new Ext.Panel({
        id: 'cap-wizard-panel',
        title: 'New CAP Alert',
        region: 'center',
        layout: 'card',
        activeItem: 0,
        bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        tbar: panel_tbar_items,
        bbar: new Ext.Toolbar({ items: [
            {
                id: 'card-prev',
                text: '<span style="font-weight: bold; font-size: 130%;">Back</span>',
                iconCls: 'previousButton',
                width: 100,
                handler: cardNav.createDelegate(this, [-1]),
                disabled: true
            }, { xtype: 'tbspacer', width: 150 },
            {
                id: 'card-next',
                text: '<span style="font-weight: bold; font-size: 130%;">Next</span>',
                iconCls: 'nextButton',
                iconAlign: 'right',
                width: 100,
                handler: cardNav.createDelegate(this, [1])
            }
        ], buttonAlign: 'center' }),
        // the panels (or "cards") within the layout
        items: [ inputCAPCard, locationCAPCard, postCAPCard]
    });
    
    
    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [headerBox, capPanel]
    });

});


/**
Loads a selected template's values

@param {Object} - the XHR returned JSON template
*/
POST.load_template = function (xhr) {
    try {
        var template = JSON.parse(xhr.responseText);
    } catch (err) {
        console.error('Load template JSON parse error: ' + err);
        alert('Unable to load template.');
        return;
    }
    //TODO: not resetting all form fields when a user switches between templates
    //      as it could overwrite other fields where the user has info,
    //      causing unintended side affects.  Need to review.
    //Ext.getCmp('card-0').getForm().reset();
    //Ext.getCmp('cap-event').collapseAll();
    //Ext.getCmp('cap-expires-interval').fireEvent('afterrender',
    //    Ext.getCmp('cap-expires-interval'));
    // all template values are optional
    if (template.event) {
        // need to seach multiple children (true) into the tree to find the
        // right icon match, then expand the tree to this node and select it
        var event_tree = Ext.getCmp('cap-event');
        // the preloadChildren patch in the tree loader is currently needed
        // for this findChild to work
        var node_match = event_tree.root.findChild('value', template.event, true);
        if (node_match) {
            event_tree.expandPath(node_match.getPath(), null,
                function (e_success, e_last) {
                    if (e_success) {
                        e_last.select();
                    }
                }
            );
            var icon_url = POST.ICON_PREVIEW_URL + node_match.attributes.term +
                '/large.png';
            Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                icon_url + '"></div>');
        } else {
            console.error('No match for event: ' + template.event);
            alert('Unable to find an event that matches template value.');
        }
    }
    if (template.urgency) {
        Ext.getCmp('cap-urgency').setValue(template.urgency);
    }
    if (template.severity) {
        Ext.getCmp('cap-severity').setValue(template.severity);
    }
    if (template.certainty) {
        Ext.getCmp('cap-certainty').setValue(template.certainty);
    }
    if (template.en_headline) {
        Ext.getCmp('cap-en-headline').setValue(template.en_headline);
    }
    if (template.fr_headline) {
        Ext.getCmp('cap-fr-headline').setValue(template.fr_headline);
    }
    if (template.en_description) {
        Ext.getCmp('cap-en-description').setValue(template.en_description);
    }
    if (template.en_instruction) {
        Ext.getCmp('cap-en-instruction').setValue(template.en_instruction);
    }
    if (template.fr_description) {
        Ext.getCmp('cap-fr-description').setValue(template.fr_description);
    }
    if (template.fr_instruction) {
        Ext.getCmp('cap-fr-instruction').setValue(template.fr_instruction);
    }
    if (template.expires) {
        Ext.getCmp('cap-expires-interval').setValue(template.expires);
    }
};


/**
Saves a location feature and its values, both to the on-screen display for the
user and for later processing.

@param {Object} - the OpenLayers feature to be saved
*/
POST.save_location_feature = function (feature) {
    console.debug('Saving Location');
    console.log(feature);
    // highlight the geometry that will be used for location
    if (feature.layer) {
        for (var f = 0; f < feature.layer.features.length; f++) {
            feature.layer.features[f].renderIntent = 'default';
        }
        feature.renderIntent = 'select';
        feature.layer.redraw();
    }
    // update the location form values with this geometry
    var location_input = document.getElementById('cap-location');
    var location_formfield = Ext.getCmp('cap-location');
    var location_title = Ext.getCmp('cap-location-title');
    // the notification message for the popup beside the location field
    var notification_str = 'Location Saved';
    if (location_formfield.geom) {
        notification_str = 'Updated Location';
    }
    if (feature.geometry) {
        var point_result = [];
        var feature_points = feature.geometry.getVertices();
        for (var i = 0; i < feature_points.length; i++) {
            var new_point = new OpenLayers.Geometry.Point(feature_points[i].x,
                feature_points[i].y);
            new_point.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            point_result.push(new_point.y.toPrecision(8) + ',' + new_point.x.toPrecision(8));
        }
        if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Point') {
            if (point_result.length === 1) {
                // setting the geom first so validation works when value added
                location_formfield.geom = 'point';
                location_input.value = point_result[0];
                location_title.update('<b>Point:</b>');
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.LineString') {
            if (point_result.length > 1) {
                location_formfield.geom = 'line';
                location_input.value = point_result.join(' ');
                location_title.update('<b>Line:</b>');
            }
        } else {
            console.error('Unknown geometry type');
            alert('Geometry type not supported.');
            return;
        }
        // display a temporary notification message to better inform the user when
        // the field value that will be used for location is saved/updated
        var msg = Ext.Msg.show({
            msg: notification_str,
            cls: 'locationNotificationMsg',
            closable: false,
            modal: false,
            minWidth: 180
        });
        msg.getDialog().alignTo('cap-location', 'tr?', [25, -25]);
        setTimeout(function () {
            Ext.Msg.hide();
        }, 2500);
    }
    // deselect whatever control was used to draw/save this feature to set
    // the map back up as originally viewed by user
    var feature_controls = POST.locationMap.getControlsBy('featureControl', true);
    for (var c = 0; c < feature_controls.length; c++) {
        feature_controls[c].deactivate();
    }
    console.debug(location_formfield.geom + ': ' + location_input.value);
};


/**
Using the entered Location, requests that the server perform a search of
all nearby SGC areas and return the results so they can be displayed for the
user's selection.
*/
POST.existing_area_search = function () {
    //TODO: only using the first point of a line here, expand to support other
    //      geometries later
    var location_txt = Ext.getCmp('cap-location').getValue();
    var location_vals = location_txt.split(" ");
    var point_value = location_vals[0];
    var search_radius = Ext.getCmp('area-search-radius').value;
    var search_type = Ext.getCmp('area-search-type').value;
    if (POST.Validator.check_point(point_value)[0] === false) {
        alert('A valid Location is needed for Area Search.');
        return;
    }
    if (!search_radius) {
        alert('Search Radius Missing.');
        return;
    }
    // options.url is what's used for actual loading
    //NOTE: not using cache busting parameter here as shouldn't be needed
    var new_url = OpenLayers.Util.urlAppend(POST.AREA_ZONE_URL,
        'point=' + point_value + '&radius=' + search_radius + '&type=' + search_type);
    POST.areaZoneStore.proxy.protocol.options.url = new_url;
    POST.areaZoneStore.load();
};


/**
For a custom area, currently just a circle, requests that the server perform a
search of all associated SGC areas, with the results added to this new custom
area as the applicable SGCs.
*/
POST.custom_geocode_search = function () {
    //TODO: only using the first point of a line here, expand to support other
    //      geometries later
    var location_txt = Ext.getCmp('cap-location').getValue();
    var location_vals = location_txt.split(" ");
    var point_value = location_vals[0];
    var search_radius = Ext.getCmp('cap-radius').getValue();
    OpenLayers.Request.GET({
        url: POST.AREA_ZONE_URL,
        //NOTE: not using cache busting parameter here as shouldn't be needed
        params: {'point': point_value, 'radius': search_radius, 'type': 'csd',
            'geocode': 'yes'},
        success: function (xhr) {
            try {
                // should be a list of any applicable geocodes
                Ext.getCmp('areaCreateSet').sgcGeocode = JSON.parse(xhr.responseText);
            } catch (err) {
                // not warning user, will continue with existing default 'none'
                console.error('Custom Geocode Error: ' + err);
            }
        },
        failure: function () {
            // not warning user, will continue with existing default 'none'
            console.error('Error geocoding custom boundary');
        }
    });
};


/**
Generate the new CAP message and its XML and Object from the entered values.

@return {String} - the new XML string
@return {Object} - the new CAP object
*/
POST.generate_cap_xml = function () {
    if (!POST.CAP_MESSAGE) {
        console.error('CAP Object missing');
        alert('Unable to load CAP Message.');
    }
    // cloning CAP_MESSAGE each time required for cases where a user
    // goes back and add/removes values after doing a preview which
    // results in a modified CAP_MESSAGE being used to build the post XML
    var new_message = MASAS.Common.clone_object(POST.CAP_MESSAGE);
    
    //TODO: consider using only UTC for this sent date??
    var sent_time = new Date();
    // using Ext extensions to Date for formatting
    new_message.alert.sent = sent_time.format('Y-m-d\\TH:i:sP');
    new_message.alert.status = Ext.get('cap-status').getValue();
    // info_block order for english users
    var en_info = new_message.alert.info[0];
    var fr_info = new_message.alert.info[1];
    
    var event_selection = Ext.getCmp('cap-event').getSelectionModel().getSelectedNode();
    if (!event_selection) {
        alert('Unable to create CAP XML - Event Data Missing.');
    }
    en_info.event = event_selection.attributes.text;
    fr_info.event = event_selection.attributes.f_text;
    en_info.category = event_selection.attributes.category;
    fr_info.category = event_selection.attributes.category;
    en_info.eventCode.value = event_selection.attributes.value;
    fr_info.eventCode.value = event_selection.attributes.value;
    
    en_info.urgency = Ext.getCmp('cap-urgency').getValue().getGroupValue();
    en_info.severity = Ext.getCmp('cap-severity').getValue().getGroupValue();
    en_info.certainty = Ext.getCmp('cap-certainty').getValue().getGroupValue();
    fr_info.urgency = Ext.getCmp('cap-urgency').getValue().getGroupValue();
    fr_info.severity = Ext.getCmp('cap-severity').getValue().getGroupValue();
    fr_info.certainty = Ext.getCmp('cap-certainty').getValue().getGroupValue();
    
    var effective_im = Ext.getCmp('cap-effective-immediate').getValue();
    var effective_dt = Ext.get('cap-effective-dt').getValue();
    var effective_tm = Ext.get('cap-effective-tm').getValue();
    // use a newly entered effective time
    if (!effective_im && effective_dt && effective_tm) {
        en_info.effective = effective_dt + 'T' + effective_tm + ':00' +
            sent_time.format('P');
        fr_info.effective = effective_dt + 'T' + effective_tm + ':00' +
            sent_time.format('P');
    } else {
        // effective not in use
        delete en_info.effective;
        delete fr_info.effective;
    }
    
    var expires_dt = Ext.get('cap-expires-dt').getValue();
    var expires_tm = Ext.get('cap-expires-tm').getValue();
    var expires_it = Ext.getCmp('cap-expires-interval').getValue();
    if (expires_dt && expires_tm) {
        en_info.expires = expires_dt + 'T' + expires_tm + ':00' +
            sent_time.format('P');
        fr_info.expires = expires_dt + 'T' + expires_tm + ':00' +
            sent_time.format('P');
    } else if (expires_it) {
        var expires_time = new Date();
        var expires_diff = parseInt(expires_it, 10);
        expires_time = MASAS.Common.adjust_time(expires_time, expires_diff);
        en_info.expires = expires_time.format('Y-m-d\\TH:i:00P');
        fr_info.expires = expires_time.format('Y-m-d\\TH:i:00P');
    } else {
        delete en_info.expires;
        delete fr_info.expires;
    }
    
    en_info.headline = MASAS.Common.xml_encode(Ext.get('cap-en-headline').getValue());
    fr_info.headline = MASAS.Common.xml_encode(Ext.get('cap-fr-headline').getValue());
    // clean out defaults for area that the user didn't fill in
    en_info.headline = en_info.headline.replace(' for (area)', '');
    fr_info.headline = fr_info.headline.replace(' pour (area)', '');
    var en_description = Ext.get('cap-en-description').getValue();
    if (en_description) {
        en_info.description = MASAS.Common.xml_encode(en_description);
    } else {
        delete en_info.description;
    }
    var fr_description = Ext.get('cap-fr-description').getValue();
    if (fr_description) {
        fr_info.description = MASAS.Common.xml_encode(fr_description);
    } else {
        delete fr_info.description;
    }
    var en_instruction = Ext.get('cap-en-instruction').getValue();
    if (en_instruction) {
        en_info.instruction = MASAS.Common.xml_encode(en_instruction);
    } else {
        delete en_info.instruction;
    }
    var fr_instruction = Ext.get('cap-fr-instruction').getValue();
    if (fr_instruction) {
        fr_info.instruction = MASAS.Common.xml_encode(fr_instruction);
    } else {
        delete fr_info.instruction;
    }
    var en_contact = Ext.get('cap-en-contact').getValue();
    if (en_contact) {
        en_info.contact = MASAS.Common.xml_encode(en_contact);
    } else {
        delete en_info.contact;
    }
    var fr_contact = Ext.get('cap-fr-contact').getValue();
    if (fr_contact) {
        fr_info.contact = MASAS.Common.xml_encode(fr_contact);
    } else {
        delete fr_info.contact;
    }
    var en_web = Ext.get('cap-en-web').getValue();
    if (en_web) {
        en_info.web = MASAS.Common.xml_encode(en_web);
    } else {
        delete en_info.web;
    }
    var fr_web = Ext.get('cap-fr-web').getValue();
    if (fr_web) {
        fr_info.web = MASAS.Common.xml_encode(fr_web);
    } else {
        delete fr_info.web;
    }
    
    if (Ext.getCmp('discard-event-location').checked) {
        // location will be based on area polygons instead
        delete en_info.parameter;
        delete fr_info.parameter;
    } else {
        var location_formfield = Ext.getCmp('cap-location');
        if (location_formfield.geom === 'point') {
            en_info.parameter.valueName = 'layer:CAPAN:eventLocation:point';
            fr_info.parameter.valueName = 'layer:CAPAN:eventLocation:point';
        } else if (location_formfield.geom === 'line') {
            en_info.parameter.valueName = 'layer:CAPAN:eventLocation:line';
            fr_info.parameter.valueName = 'layer:CAPAN:eventLocation:line';
        }
        en_info.parameter.value = location_formfield.getValue();
        fr_info.parameter.value = location_formfield.getValue();
    }
    
    if (Ext.getCmp('areaSelectSet').collapsed) {
        // custom area with any found geocodes that match
        //TODO: only using the first point of a line here, expand to support other
        //      geometries later
        var location_txt = Ext.getCmp('cap-location').getValue();
        var location_vals = location_txt.split(" ");
        var point_value = location_vals[0];
        var circle_val = point_value + ' ' + Ext.get('cap-radius').getValue();
        var geocode_list = Ext.getCmp('areaCreateSet').sgcGeocode;
        var new_geocodes = [];
        for (var i = 0; i < geocode_list.length; i++) {
            new_geocodes.push({valueName: 'profile:CAP-CP:Location:0.3',
                value: geocode_list[i]});
        }
        en_info.area = {areaDesc: MASAS.Common.xml_encode(Ext.get('cap-en-area-description').getValue()),
            circle: circle_val, geocode: new_geocodes};
        fr_info.area = {areaDesc: MASAS.Common.xml_encode(Ext.get('cap-fr-area-description').getValue()),
            circle: circle_val, geocode: new_geocodes};
    } else {
        var en_area_blocks = [];
        var fr_area_blocks = [];
        var zone_vals = POST.areaSearchGrid.getSelectionModel().getSelections();
        for (var i = 0; i < zone_vals.length; i++) {
            var each_zone = zone_vals[i];
            var zone_feature = each_zone.data.feature.clone();
            zone_feature.geometry.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            // accounting for both single polygons associated with an SGC and
            // multipolygons as provided in the areaSearchGrid GeoJSON data 
            var zone_points = [];
            var zone_poly = '';
            if (zone_feature.geometry.components.length > 1) {
                zone_poly = [];
                for (var j = 0; j < zone_feature.geometry.components.length; j++) {
                    zone_points = [];
                    for (var k = 0; k < zone_feature.geometry.components[j].components[0].components.length; k++) {
                        zone_points.push(zone_feature.geometry.components[j].components[0].components[k].y.toPrecision(8) +
                            ',' + zone_feature.geometry.components[j].components[0].components[k].x.toPrecision(8));
                    }
                    zone_poly.push(zone_points.join(' '));
                }
            } else {
                for (var j = 0; j < zone_feature.geometry.components[0].components.length; j++) {
                    zone_points.push(zone_feature.geometry.components[0].components[j].y.toPrecision(8) +
                        ',' + zone_feature.geometry.components[0].components[j].x.toPrecision(8));
                }
                zone_poly = zone_points.join(' ');
            }
            en_area_blocks.push({areaDesc: each_zone.data.e_name,
                polygon: zone_poly,
                geocode: {valueName: 'profile:CAP-CP:Location:0.3',
                    value: each_zone.data.sgc}
            });
            fr_area_blocks.push({areaDesc: each_zone.data.f_name,
                polygon: zone_poly,
                geocode: {valueName: 'profile:CAP-CP:Location:0.3',
                    value: each_zone.data.sgc}
            });
        }
        // check for single areas which don't get set as an array of objects
        if (en_area_blocks.length === 1) {
            en_info.area = en_area_blocks[0];
        } else {
            en_info.area = en_area_blocks;
        }
        if (fr_area_blocks.length === 1) {
            fr_info.area = fr_area_blocks[0];
        } else {
            fr_info.area = fr_area_blocks;
        }
    }
    
    if (Ext.getCmp('cap-add-french').collapsed) {
        // remove the french info block by converting from an array of two
        // objects to just one
        new_message.alert.info = en_info;
    }
    
    var new_xml = xmlJsonClass.json2xml(new_message, '');
    new_xml = MASAS.Common.format_xml(new_xml);
    new_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + new_xml;
    
    return [new_xml, new_message];
};


/**
Show a preview window to review the new CAP message both with a template for user
friendlier display and the XML.
*/
POST.preview_cap_xml = function () {
    // only one review window allowed
    if (POST.reviewWindow) {
        POST.reviewWindow.destroy();
    }
    
    var preview_xml_vals = POST.generate_cap_xml();
    var display_xml = preview_xml_vals[0];
    try {
        // XTemplate from post-templates.js
        var display_html = MASAS.Template.CAPToHTML.apply(preview_xml_vals[1]);
    } catch (err) {
        console.error('Preview CAP Template Error: ' + err);
        alert('CAP Template Error.');
        return;
    }
    display_xml = display_xml.replace(/</g, '&lt;');
    display_xml = display_xml.replace(/>/g, '&gt;');
    display_html += '<div class="reviewClose"><a href="#" onclick="' +
        'POST.reviewWindow.close(); return false;">Close Preview</a></div>' +
        '<div class="reviewXmlShow"><a href="#" onclick="' +
        'document.getElementById(\'review-xml-box\').style.display=\'block\'; return false;">' +
        'Show XML</a></div><div id="review-xml-box" style="display: none;">' +
        '<pre>' + display_xml + '</pre></div>';
    
    POST.reviewWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'CAP Preview <a class="titleClose" href="#" ' +
            'onclick="POST.reviewWindow.close(); return false;">Close</a>',
        closable: true,
        width: 700,
        height: 500,
        autoScroll: true,
        bodyStyle: 'background-color: white;',
        layout: 'fit',
        html: display_html
    });
    POST.reviewWindow.show(this);
};


/**
Post this new CAP message to the server.
*/
POST.post_new_alert = function () {
    var posting_html = '<h1>Posting Alert</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: the following is not working to prevent going back
    //Ext.getCmp('card-prev').setDisabled();
    var new_cap_xml_vals = POST.generate_cap_xml();
    console.debug('Posting New CAP Alert');
    var do_post = new OpenLayers.Request.POST({
        url: POST.FEED_URL,
        params: {'secret': POST.USER_SECRET},
        proxy: POST.AJAX_PROXY_URL,
        headers: {'Content-Type': 'application/common-alerting-protocol+xml'},
        data: new_cap_xml_vals[0],
        callback: POST.Common.post_complete_result
    });
};
