/**
MASAS Posting Tool - Map Layers
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings for the top toolbar and
map layers.

@requires src/Layers.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,google */

Ext.ns('POST.MapLayers');

/**
 * Setup map layers once application loaded and ready
 */
POST.MapLayers.initialize = function () {
    
    /* Base Layers */
    
    var base_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        ['OSM Tile Service', 'osm'],
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest']
    ];
    
    var base_layer_fields = [
        // required fields for all layer types, the LayerRecord already has
        // defined 'layer' and 'title' with 'name' mapping to title.
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // optional fields
        // use "defaultValue" to set a (non-falsy) value for something thats missing
        {name: 'base_layer', type: 'boolean', defaultValue: true},
        // layers that can't be deleted, defaults for all users
        {name: 'permanent', type: 'boolean'},
        {name: 'map_type', type: 'string'},
        // allows overriding the map default of 16 depending on the layer
        {name: 'zoom_levels', type: 'integer'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        // holds references to the toolbar button and layers menu item
        {name: 'button'},
        {name: 'menu'}
    ];
    
    POST.MapLayers.baseLayers = new MASAS.Layers.LayerStore({
        'storeId': 'baseLayers',
        'map': POST.locationMap,
        'fields': base_layer_fields,
        'layer_types': base_layer_types,
        'base_layers': true
    });
    
    var base_data = [];
    // Google Maps is normally the default due to being listed first,
    // however it may fail to load, which can happen with some networks with
    // restrictive firewalls.  In that case, OSM becomes the fallback default
    if ('google' in window) {
        base_data.push({
            'name': 'Google Streets',
            'url': 'http://maps.googleapis.com/streets',
            'type': 'google',
            'visibility': true,
            'base_layer': true,
            'permanent': true,
            'zoom_levels': 21,
            'description': 'Google Maps showing streets'
        });
        base_data.push({
            'name': 'Google Satellite',
            'url': 'http://maps.googleapis.com/satellite',
            'type': 'google',
            'visibility': false,
            'base_layer': true,
            'permanent': true,
            'map_type': 'satellite',
            'zoom_levels': 21,
            'description': 'Google Maps showing a hybrid of satellite and streets'
        });
    } else {
        console.error('Unable to load google maps, OSM fallback');
        alert('A problem with your network prevented the Google Map from ' +
            'loading properly.  Open Street Map will be used instead.  If ' +
            'you still want to use the Google Map, you can reload this ' +
            'webpage to try again.');
    }
    base_data.push({
        'name': 'OpenStreetMap',
        'url': 'http://otile1.mqcdn.com/tiles/1.0.0/map/',
        'type': 'osmm',
        'visibility': false,
        'base_layer': true,
        'permanent': true,
        'zoom_levels': 19,
        'attribution': 'MapQuest-OSM',
        'description': 'OpenStreetMap tiles from MapQuest'
    });
    /* Currently disabled as it lacks high resolution for Canada
    base_data.push({
        'name': 'Open Aerial',
        'url': 'http://otile1.mqcdn.com/tiles/1.0.0/sat/',
        'type': 'osma',
        'visibility': false,
        'base_layer': true,
        'permanent': true,
        'zoom_levels': 19,
        'attribution': 'MapQuest',
        'description': 'Open Aerial tiles from MapQuest.  Higher zoom levels' +
            ' may not be available.'
    });
    */
    base_data.push({
        'name': 'CBMT',
        'url': 'http://geogratis.gc.ca/maps/CBMT?LAYERS=CBMT',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'base_layer': true,
        'zoom_levels': 24,
        'permanent': true,
        'attribution': 'CBMT',
        'description': 'The Canada Base Map - Transportation (CBMT) web mapping' +
            ' services of the Earth Sciences Sector at Natural Resources Canada'
    });
    /* Currently disabled as its fairly slow to load
    base_data.push({
        'name': 'AGO Streets',
        'url': 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/export?BBOXSR=3857&IMAGESR=3857&format=jpg',
        'type': 'application/vnd.esri.arcgis.rest',
        'visibility': false,
        'base_layer': true,
        'permanent': true,
        'attribution': 'Esri',
        'description': 'This ArcGIS Online service presents highway-level data' +
            ' for the world.'
    });
    */
    
    POST.MapLayers.baseLayers.loadData(base_data);
    // start out with the first available layer
    POST.locationMap.setBaseLayer(POST.MapLayers.baseLayers.getAt(0).getLayer());
    
    /* Overlay Layers */
    
    var overlay_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest'],
        ['KML', 'application/vnd.google-earth.kml+xml']
    ];
    
    var overlay_layer_fields = [
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // in the store its a string, but is converted to float for layer
        {name: 'opacity', type: 'string'},
        {name: 'permanent', type: 'boolean'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'button'},
        {name: 'menu'}
    ];
    
    POST.MapLayers.overlayLayers = new MASAS.Layers.LayerStore({
        'storeId': 'overlayLayers',
        'map': POST.locationMap,
        'fields': overlay_layer_fields,
        'layer_types': overlay_layer_types
    });
    
    var overlay_data = [
    {
        'name': 'Weather Radar',
        'url': 'http://geo.weather.gc.ca/geomet/?LAYERS=RADAR_RDBR,RADAR_RDBS&STYLES=RADARURPREFLECT',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'legend_url': 'http://geo.weatheroffice.gc.ca/geomet/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&STYLE=RADARURPREFLECT&LAYER=RADAR_RDBR&format=image/png&WIDTH=100&HEIGHT=250',
        'attribution': 'Environment Canada',
        'description': 'Composite of American and Canadian weather radars at' +
            ' 4km resolution'
    },
    {
        'name': 'Wind Direction',
        'url': 'http://geo.weather.gc.ca/geomet/?LAYERS=GDPS.ETA_UU&STYLES=WINDARROWKMH',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'legend_url': 'http://geo.weatheroffice.gc.ca/geomet/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&STYLE=WINDARROWKMH&LAYER=GDPS.ETA_UU&format=image/png&WIDTH=100&HEIGHT=250',
        'attribution': 'Environment Canada',
        'description': 'Wind arrows in km/h, Global Deterministic Prediction' +
            ' System (GDPS) at 25 km resolution'
    },
    {
        'name': 'Weather Conditions',
        'url': 'http://openweathermap.org/data/2.1/find/city',
        'type': 'owm',
        'visibility': false,
        'permanent': true,
        'attribution': 'OpenWeatherMap',
        'description': 'Weather conditions from openweathermap.org'
    },
    {
        'name': 'Toporama Road',
        'url': 'http://wms.ess-ws.nrcan.gc.ca/wms/toporama_en?LAYERS=road_network',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'attribution': 'Natural Resources Canada',
        'description': 'The Road network layer represents road segments,' +
            ' including ferries, used for travel by land vehicles.'
    },
    {
        'name': 'Toporama Railway',
        'url': 'http://wms.ess-ws.nrcan.gc.ca/wms/toporama_en?LAYERS=railway',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'attribution': 'Natural Resources Canada',
        'description': 'The Railway layer represents the various structures' +
            ' required for the movement of trains and other rail-based vehicles.'
    },
    {
        'name': 'Toporama Structures',
        'url': 'http://wms.ess-ws.nrcan.gc.ca/wms/toporama_en?LAYERS=structures',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'attribution': 'Natural Resources Canada',
        'description': 'The Structures layer represents manmade structures,' +
            ' including bridges, tunnels and footbridges used for travel by' +
            ' land vehicles, trains or individuals.'
    },
    {
        'name': 'Toporama Power',
        'url': 'http://wms.ess-ws.nrcan.gc.ca/wms/toporama_en?LAYERS=power_network',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'attribution': 'Natural Resources Canada',
        'description': 'The Power network layer represents the main structures' +
            ' used to transport electricity, oil and gas.'
    },
    {
        'name': 'MODIS Fires',
        // SRS=4326 added to the URL
        'url': 'https://firms.modaps.eosdis.nasa.gov/wms/?LAYERS=fires24&SRS=EPSG:4326',
        'type': 'application/vnd.ogc.wms',
        'visibility': false,
        'permanent': true,
        'attribution': 'NASA/LANCE-FIRMS',
        'description': 'MODIS active fire data for the last 24 hours'
    }
    ];
    
    POST.MapLayers.overlayLayers.loadData(overlay_data);
    
    /* Layer menus */
    
    var base_menu = new MASAS.Layers.LayerMenu({
        ignoreParentClicks: (POST.TOUCH_ENABLE) ? true : false,
        layers: POST.MapLayers.baseLayers,
        createNewLayers: true
    });
    var overlay_menu = new MASAS.Layers.LayerMenu({
        ignoreParentClicks: (POST.TOUCH_ENABLE) ? true : false,
        layers: POST.MapLayers.overlayLayers,
        createNewLayers: true
    });
    var layers_menu = new Ext.menu.Menu({
        ignoreParentClicks: true,
        plain: true,
        items: [{
            text: 'Base Layer',
            menu: base_menu
        }, '-', {
            text: 'OverLays',
            menu: overlay_menu
        }]
    });
    
    /* Top toolbar on the map */
    
    // default buttons if no user selected buttons
    POST.MapLayers.toolbar_buttons = [];
    if ('google' in window) {
        POST.MapLayers.toolbar_buttons.push({
            'name': 'Google Streets',
            'type': 'google',
            'url': 'http://maps.googleapis.com/streets',
            'store': 'baseLayers'
        });
        POST.MapLayers.toolbar_buttons.push({
            'name': 'Google Satellite',
            'type': 'google',
            'url': 'http://maps.googleapis.com/satellite',
            'store': 'baseLayers'
        });
    } else {
        POST.MapLayers.toolbar_buttons.push({
            'name': 'OpenStreetMap',
            'type': 'osmm',
            'url': 'http://otile1.mqcdn.com/tiles/1.0.0/map/',
            'store': 'baseLayers'
        });
    }
    POST.MapLayers.toolbar_buttons.push({
        'name': 'Weather Radar',
        'type': 'application/vnd.ogc.wms',
        'url': 'http://geo.weather.gc.ca/geomet/?LAYERS=RADAR_RDBR,RADAR_RDBS&STYLES=RADARURPREFLECT',
        'store': 'overlayLayers'
    });
    
    POST.mapToolbarTop = {
        // allow extra items to be seen using a dropdown menu
        enableOverflow: true,
        // allow mouse users to click and drag to reorder items
        //TODO: should this be turned off for touch users ?
        plugins : [
            new Ext.ux.ToolbarReorderer({defaultReorderable: true})
        ],
        
        // custom state saving for this toolbar, allowing the user to populate
        // the toolbar with default and custom layers and save/restore them
        stateful: true,
        stateId: 'toolbar',
        applyState: function (state) {
            if (state && state.count && state.count > 0) {
                var saved_buttons = [];
                for (var i = 1; i <= state.count; i++) {
                    if (state.buttons['button' + i]) {
                        saved_buttons.push(state.buttons['button' + i]);
                    }
                }
                if (saved_buttons.length > 0) {
                    // replace the default with user defined list
                    POST.MapLayers.toolbar_buttons = saved_buttons;
                }
                console.debug('Restored ' +  saved_buttons.length +
                    ' toolbar buttons');
            }
        },
        
        items: [{
            xtype: 'tbspacer',
            width: 40,
            // don't allow the toolbar to reorder this item
            reorderable: false
        }, {
            text: 'Layers',
            reorderable: false,
            menu: layers_menu
        }, {
            xtype: 'tbspacer',
            width: 20,
            reorderable: false
        }],
        
        listeners: {
            // once the toolbar has been rendered and the state restore is
            // done, the buttons can be added
            afterrender: function (cmp) {
                var base_separator = false;
                for (var i = 0; i < POST.MapLayers.toolbar_buttons.length; i++) {
                    var btn_data = POST.MapLayers.toolbar_buttons[i];
                    var layer_store = Ext.StoreMgr.get(btn_data.store);
                    if (!layer_store) {
                        console.error('Unable to load layer store ' + btn_data.store);
                        continue;
                    }
                    // base layers should be on the left side with a separator.
                    // Once all of them have been added, use a separator prior
                    // to any overlays, assuming that is the order
                    if (btn_data.store === 'baseLayers') {
                        base_separator = true;
                    } else {
                        if (base_separator) {
                            cmp.addSeparator();
                            base_separator = false;
                        }
                    }
                    // either use an existing layer or add a new custom layer
                    var layer_idx = layer_store.findExact('url', btn_data.url);
                    if (layer_idx === -1) {
                        layer_store.loadData(btn_data, true);
                        layer_idx = layer_store.findExact('url', btn_data.url);
                    }
                    var layer_record = layer_store.getAt(layer_idx);
                    MASAS.Layers.add_toolbar_button(layer_record, cmp);
                    // silently update the menu to show the layer is on the toolbar
                    var layer_menu = layer_record.get('menu');
                    if (layer_menu) {
                        layer_menu.menu.toolbarCheckItem.setChecked(true, true);
                    }
                }
                
                // call doLayout to refresh the view which causes any
                // unrendered child Components to be rendered
                POST.locationMapPanel.doLayout();
            }
        }
    };
};
