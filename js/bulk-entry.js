/**
Bulk Entry
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires postMap.js
@requires src/common.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('POST');

// blank objects to start
POST.sourceHubCombo = null;
POST.selectStore = null;
POST.selectGridPanel = null;
POST.confirmStore = null;
POST.confirmGridPanel = null;
POST.COPY_FEED_TITLE = null;
POST.COPY_FEED_URL = null;
POST.COPY_USER_SECRET = null;

// for progress bar indicator
POST.PROGRESS_AMOUNT = 0;
// complete is 1 so divide by number of pages
POST.PROGRESS_INCREMENT = 0.50;


Ext.onReady(function () {
    
    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();
    
    
    // setting up select map, layers, and controls
    POST.Map.initialize_select_map();
    
    
    var headerBox = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Posting Tool</div>' +
            '<div id="headerDetailBlock">Version 0.2</div>'
    });
    
    var progressBar = new Ext.ProgressBar({
        id: 'progressBar',
        width: 150,
        style: 'margin-left: 25px',
        text: 'Progress'
    });
    
    var cardNav = function (direction) {
        var card_panel = Ext.getCmp('entry-wizard-panel').getLayout();
        var current_card = parseInt(card_panel.activeItem.id.split('card-')[1], 10);
        
        /* Card validation */
        // select entry(s), comment out to bypass
        if (current_card === 0) {
            var select_made = false;
            POST.selectStore.each(function (record) {
                if (record.get('select') === true) {
                    select_made = true;
                }
            });
            if (!select_made) {
                alert('Select an Entry(s) first.');
                return;
            }
        }
        
        // select operations, comment out to bypass
        if (current_card === 1) {
            var operations_made = false;
            if (Ext.getCmp('effective-operation') && !Ext.getCmp('effective-operation').collapsed) {
                var effective_im = Ext.getCmp('entry-effective-immediate').getValue();
                if (effective_im) {
                    operations_made = true;
                }
                var effective_dt = Ext.get('entry-effective-dt').getValue();
                var effective_tm = Ext.get('entry-effective-tm').getValue();
                if (effective_dt && effective_tm) {
                    operations_made = true;
                }
            }
            if (Ext.getCmp('expires-operation') && !Ext.getCmp('expires-operation').collapsed) {
                var expires_it = Ext.getCmp('entry-expires-interval').getValue();
                if (expires_it) {
                    operations_made = true;
                }
                var expires_dt = Ext.get('entry-expires-dt').getValue();
                var expires_tm = Ext.get('entry-expires-tm').getValue();
                if (expires_dt && expires_tm) {
                    operations_made = true;
                }
            }
            if (Ext.getCmp('copy-operation') && !Ext.getCmp('copy-operation').collapsed) {
                if (POST.COPY_FEED_URL) {
                    operations_made = true;
                }
                // check input fields that have their own validation
                card_panel.activeItem.cascade(function (item) {
                    if (item.isFormField) {
                        if (!item.validate()) {
                            operations_made = false;
                        }
                    }
                });
            }
            if (!operations_made) {
                alert('Operation(s) not setup.');
                return;
            }
        }
        
        // change to the next or previous card
        var next_card = current_card + direction;
        card_panel.setActiveItem(next_card);
        // update the next or previous buttons depending on what card is
        // now active in the stack
        if (next_card === 0) {
            Ext.getCmp('card-prev').setDisabled(true);
        } else if (next_card === 1) {
            Ext.getCmp('card-prev').setDisabled(false);
            Ext.getCmp('card-next').setDisabled(false);
        } else if (next_card === 2) {
            Ext.getCmp('card-next').setDisabled(true);
        }
        // progress bar update
        if (direction === 1) {
            POST.PROGRESS_AMOUNT += POST.PROGRESS_INCREMENT;
        } else if (direction === -1) {
            POST.PROGRESS_AMOUNT -= POST.PROGRESS_INCREMENT;
        }
        progressBar.updateProgress(POST.PROGRESS_AMOUNT);
        
        /* Other card operations */
        // only allow users to modify the source Hub at the start
        if (next_card !== 0) {
            if (POST.sourceHubCombo) {
                POST.sourceHubCombo.disable();
            }
        } else {
            if (POST.sourceHubCombo) {
                POST.sourceHubCombo.enable();
            }
        }
        
        // cleanup stray GeoExt popup windows which stick around between
        // panel changes
        if (current_card === 0) {
            POST.Common.layer_popup_cleanup(POST.selectLayers.select);
        }
    };
    
    /* selectEntryCard */
    
    var historyCombo = new Ext.form.ComboBox({
        id: 'history-interval',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 150,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'period',
        // default is current or future effective
        value: 1,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'period'],
            data: [
                ['Current/Active', 0],
                ['Current and Future', 1],
                ['Future Effective', 2],
                ['Past 24 Hours', 24],
                ['Past 48 Hours', 48],
                ['Past Week', 168],
                ['Past Month', 720]
            ]
        }),
        listeners: {select: POST.load_user_entries}
    });

    POST.selectStore = new GeoExt.data.FeatureStore({
        layer: POST.selectLayers.select,
        fields: [
            {name: 'icon', type: 'string'},
            {name: 'colour', type: 'string'},
            {name: 'title', type: 'string'},
            {name: 'published', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'updated', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'effective', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'expires', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'point', type: 'string'},
            {name: 'content', type: 'string'},
            {name: 'links', type: 'auto'},
            {name: 'CAP', type: 'string'},
            {name: 'select', type: 'boolean'}
        ],
        sortInfo: {field: 'updated', direction: "DESC"},
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.HTTP({
                url: '',
                format: new OpenLayers.Format.MASASFeed({
                    internalProjection: POST.selectMap.getProjectionObject(),
                    externalProjection: new OpenLayers.Projection('EPSG:4326')
                })
            })
        }),
        autoLoad: false,
        listeners: {
            load: function () {
                POST.selectControls.loading.minimizeControl();
                if (this.data.length > 0) {
                    this.suspendEvents();
                    this.each(function (rec) {
                        rec.set('published', MASAS.Common.adjust_time(rec.get('published'),
                            MASAS.Common.UTC_Local_Offset));
                        rec.set('updated', MASAS.Common.adjust_time(rec.get('updated'),
                            MASAS.Common.UTC_Local_Offset));
                        if (rec.get('effective')) {
                            rec.set('effective', MASAS.Common.adjust_time(rec.get('effective'),
                                MASAS.Common.UTC_Local_Offset));
                        }
                        rec.set('expires', MASAS.Common.adjust_time(rec.get('expires'),
                            MASAS.Common.UTC_Local_Offset));
                    }, this);
                    this.resumeEvents();
                    this.commitChanges();
                }
            },
            exception: function () {
                console.error('Error loading previous entries');
                alert('Unable to load previous Entries.');
            }
        }
    });
    
    // row expander
    var expander = new Ext.ux.grid.RowExpander({
        tpl: new Ext.Template('<p>{content}</p>')
    });
    
    // icons displayed in rows
    function renderEventIcon(value, metadata, record) {
        // apply a background colour with the icon
        if (record.data.colour && metadata) {
            metadata.css = record.data.colour.toLowerCase() + 'EntryBackground';
        }
        return '<img alt="Event" height="18" src="' + value + '">';
    }
    
    // expired entry check
    function renderEntryExpires(value, metadata, record) {
        if (!value) {
            value = record.data[this.renderIndex];
        }
        var expires_epoch = value.format('U') * 1000;
        var now_epoch = new Date().getTime();
        if (expires_epoch <= now_epoch) {
            metadata.css = "EntryExpiredTime";
        }
        return value.format('M j H:i:s');
    }
    
    var selectGridModelOptions = {};
    // update the selectFeature control used by the grid selection model
    // to support touch devices because the normal mouse based hover control
    // won't work
    if (POST.TOUCH_ENABLE) {
        selectGridModelOptions.config = {
            controlConfig: {
                eventListeners: {
                    // use feature 'highlight' events to prevent conflict
                    // with selection model's 'select' events
                    featurehighlighted: POST.Common.show_select_popup,
                    featureunhighlighted: POST.Common.close_select_popup
                }
            }
        };
    }
    
    // create grid panel configured with feature store
    POST.selectGridPanel = new Ext.grid.GridPanel({
        title: 'Previous Entries',
        anchor: '100% 35%',
        collapsible: false,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        store: POST.selectStore,
        columns: [expander, {
            header: 'Icon',
            width: 35,
            renderer: renderEventIcon,
            sortable: true,
            dataIndex: 'icon'
        }, {
            header: 'Title',
            sortable: true,
            dataIndex: 'title'
        }, {
            header: 'Published',
            renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
            width: 110,
            sortable: true,
            hidden: true,
            dataIndex: 'published'
        }, {
            header: 'Updated',
            renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
            width: 110,
            sortable: true,
            dataIndex: 'updated'
        }, {
            header: 'Effective',
            id: 'select-column-effective',
            renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
            width: 110,
            sortable: true,
            hidden: true,
            dataIndex: 'effective'
        }, {
            header: 'Expires',
            renderer: renderEntryExpires,
            width: 110,
            sortable: true,
            dataIndex: 'expires',
            renderIndex: 'expires'
        }, {
            xtype: 'checkcolumn',
            // add "select all" checkbox in the header
            header: '&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" ' +
                'onclick="POST.grid_select_all(this.checked);"></input>',
            sortable: false,
            dataIndex: 'select',
            width: 50
        }],
        autoExpandColumn: '2',
        plugins: expander,
        sm: new GeoExt.grid.FeatureSelectionModel(selectGridModelOptions)
    });
    
    // create map panel
    var selectMapPanel = new GeoExt.MapPanel({
        style: 'margin: 10px 0px;',
        // 40% room left at the top for historycombo and bottom for selectgrid
	anchor: '100% 60%',
        collapsible: false,
        map: POST.selectMap
    });
    
    var selectEntryCard = new Ext.FormPanel({
        id: 'card-0',
        labelWidth: 100,
        // this layout type allows for % based sizing
	layout: 'anchor',
        items: [{
            xtype: 'compositefield',
            height: 30,
            fieldLabel: '<b>Your Postings</b>',
            items: [ historyCombo,
                {
                    boxLabel: 'Only in Map View',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'map-view-filter',
                    id: 'map-view-filter',
                    checked: false,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }, {
                    boxLabel: 'Show Mine',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'history-entries-allowed',
                    id: 'history-entries-allowed',
                    checked: false,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }
            ]
        }, selectMapPanel, POST.selectGridPanel ]
    });
    
    /* operationsEntryCard */
    
    var intervalCombo = new Ext.form.ComboBox({
        name: 'entry-expires-interval',
        id: 'entry-expires-interval',
        allowBlank: true,
        editable: false,
        width: 125,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['interval', 'name', 'tip'],
            data: [ [null, '', 'Do not set expires'],
                [1, '1 Hour', 'Expires in 1 hour'],
                [6, '6 Hours', 'Expires in 6 hours'],
                [12, '12 Hours', 'Expires in 12 hours'],
                [24, '24 Hours', 'Expires in 1 day'],
                [48, '48 Hours', 'Expires in 2 days'],
                [99, 'Right Now', 'Expire Immediately, Cancels Entry'] ]
        }),
        listeners: {
            select: function (combo, record, index) {
                if (record.data.interval) {
                    var dt_check = Ext.get('entry-expires-dt').getValue();
                    if (dt_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                }
            }
        }
    });
    
    var operationsItems = [{
        xtype: 'displayfield',
        hideLabel: true,
        html: '<div style="margin-bottom: 25px;"><b>Available Operations</b></div>'
    }, {
        xtype: 'fieldset',
        checkboxToggle: true,
        collapsed: true,
        title: 'Modify Effective Time',
        //NOTE: validation of effective values is not as strict as for new/update
        //      to allow users to "correct" values manually
        id: 'effective-operation',
        items: [{
            xtype: 'compositefield',
            fieldLabel: '<b>Effective</b>',
            //TODO: setting a width ensured the validation error icon would
            //      appear beside the fields instead of the far right, but in
            //      this case, the width resulted in hiding the fields entirely
            //width: 325,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Immediately',
                name: 'entry-effective-immediate',
                id: 'entry-effective-immediate',
                // setting a height so the bottom part of the date/time fields
                // won't be cut off
                height: 23,
                checked: true,
                handler: function (checkbox, checked) {
                    // using disabled here instead of hide because for updates
                    // the fields are rendered when the first card is in view
                    // and so the fields overlap and can't use the render
                    // listener like in new-entry
                    if (checked) {
                        Ext.getCmp('entry-effective-dt').disable();
                        Ext.getCmp('entry-effective-dt').reset();
                        Ext.getCmp('entry-effective-tm').disable();
                        Ext.getCmp('entry-effective-tm').reset();
                    } else {
                        Ext.getCmp('entry-effective-dt').enable();
                        Ext.getCmp('entry-effective-tm').enable();
                    }
                }
            }, {
                xtype: 'displayfield',
                id: 'entry-effective-at',
                html: ' at:',
                style: {'padding': '3px 2px 2px 20px'}
            }, {
                xtype: 'datefield',
                name: 'entry-effective-dt',
                id: 'entry-effective-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                disabled: true
            }, {
                xtype: 'timefield',
                name: 'entry-effective-tm',
                id: 'entry-effective-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                disabled: true
            }]
        }]
    }, {
        xtype: 'displayfield',
        hideLabel: true,
        html: '<div style="margin: 50px 0px 15px 0px;"></div>'
    }, {
        xtype: 'fieldset',
        checkboxToggle: true,
        collapsed: true,
        title: 'Modify Expires Time',
        //NOTE: validation of expires values is not as strict as for new/update
        //      to allow users to "correct" values manually
        id: 'expires-operation',
        items: [{
            xtype: 'compositefield',
            fieldLabel: '<b>Expires at</b>',
            //width: 425,
            items: [{
                xtype: 'datefield',
                name: 'entry-expires-dt',
                id: 'entry-expires-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'timefield',
                name: 'entry-expires-tm',
                id: 'entry-expires-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'displayfield',
                html: '<b>or Expires in:</b>',
                style: {'padding': '3px 2px 2px 40px'}
            }, intervalCombo ]
        }]
    }, {
        xtype: 'displayfield',
        hideLabel: true,
        html: '<div style="margin: 50px 0px 15px 0px;"></div>'
    }];
    
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubCopyComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'}
        ]);
        var hubCopyCombo = new Ext.form.ComboBox({
            fieldLabel: '<b>Destination Hub</b>',
            allowBlank: false,
            editable: false,
            width: 200,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubCopyComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                select: function (combo, record) {
                    try {
                        POST.COPY_FEED_TITLE = record.data.title;
                        POST.COPY_FEED_URL = record.data.url;
                        POST.COPY_USER_SECRET = record.data.secret;
                    } catch (err) {
                        console.error('Copy Hub Error: ' + err);
                        alert('Unable to setup Copy Hub');
                    }
                }
            }
            //TODO: add listeners either to this combo or the source combo
            //      that would filter out the source Hub so you couldn't copy
            //      to the same Hub.  Is this needed?
        });
        operationsItems.push({
            xtype: 'fieldset',
            checkboxToggle: true,
            collapsed: true,
            title: 'Copy to Hub',
            id: 'copy-operation',
            items: [hubCopyCombo]
        });
    }
    
    //TODO: other operations possible: set the allow edit value, change the
    //      categories such as status from Draft to Actual, set the Allow
    //      Edit to All or specified users
    
    var operationsEntryCard = new Ext.FormPanel({
        id: 'card-1',
        labelWidth: 110,
        bodyStyle: 'padding: 10px;',
        items: operationsItems
    });
    
    /* postEntryCard */

    // create the data store
    POST.confirmStore = new Ext.data.ArrayStore({
        fields: [
            {name: 'fid', type: 'string'},
            {name: 'icon', type: 'string'},
            {name: 'title', type: 'string'},
            {name: 'result', type: 'string'}
        ]
    });
    
    // create the Grid
    POST.confirmGridPanel = new Ext.grid.GridPanel({
        title: 'Entries',
        height: 250,
        collapsible: false,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        store: POST.confirmStore,
        columns: [{
            header: 'Icon',
            width: 35,
            // shared with selectGrid
            renderer: renderEventIcon,
            sortable: true,
            dataIndex: 'icon'
        }, {
            header: 'Title',
            sortable: true, 
            dataIndex: 'title'
        }, {
            width: 75,
            sortable: true,
            dataIndex: 'result'
        }],
        autoExpandColumn: '1'
    });
    
    var postEntryCard = new Ext.Panel({
        id: 'card-2',
        items: [{
            border: false,
            bodyStyle: 'margin-bottom: 20px',
            html: '<div class="postingCard" id="postingBox"><h1>Ready to Post</h1>' +
                '<a href="#" onclick="POST.post_all(); return false;" ' +
                'style="color: green;"> Post </a></div>'
        }, {
            border: false,
            bodyStyle: 'margin-bottom: 30px',
            html: '<div id="postingConfirm"></div>'
        }, POST.confirmGridPanel],
        listeners: {
            show: function () {
                // add confirmation data to the grid
                POST.populate_confirm_card();
                // Renders the grid again, to properly show the vertical
                // scrollbar and autoexpand, since the size calculations are
                // different because the Panel wasn't visible to start
                POST.confirmGridPanel.syncSize();
            }
        }
    });
    
    /* entryPanel */
    
    // the top toolbar for the app
    var panel_tbar_items = [];
    // optional support for multiple hubs
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'}
        ]);
        POST.sourceHubCombo = new Ext.form.ComboBox({
            allowBlank: false,
            editable: false,
            width: 125,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                afterrender: function (combo) {
                    var default_idx = combo.getStore().find('url', POST.FEED_URL);
                    if (default_idx === -1) {
                        console.error('Unable to find Hub selection default');
                        // first value in array instead
                        default_idx = 0; 
                    }
                    var default_val = combo.getStore().getAt(default_idx);
                    combo.setValue(default_val.get('title'));
                },
                select: function (combo, record) {
                    if (record.data.url) {
                        POST.FEED_URL = record.data.url;
                    }
                    if (record.data.secret) {
                        POST.USER_SECRET = record.data.secret;
                    }
                    if (record.data.uri) {
                        POST.USER_URI = record.data.uri;
                    }
                    // reload existing entries only on the first card to allow
                    // the user to find postings by Hub
                    var card_panel = Ext.getCmp('entry-wizard-panel').getLayout();
                    var current_card = card_panel.activeItem.id.split('card-')[1];
                    if (current_card === '0') {
                        POST.load_user_entries();
                    }
                }
            }
        });
        panel_tbar_items = [
            {xtype: 'tbspacer', width: 15},
            {
                xtype: 'tbtext',
                style: {'fontWeight': 'bold', 'font-size': '13px'},
                text: 'Hub:'
            },
            {xtype: 'tbspacer', width: 25},
            POST.sourceHubCombo
        ];
    }
    // default items
    panel_tbar_items.push.apply(panel_tbar_items, [progressBar, '->', {
        text: '<b>Abort</b>',
        iconCls: 'abortButton',
        width: 75,
        handler: function () {
            window.close();
        }
    }]);
    
    var entryPanel = new Ext.Panel({
        id: 'entry-wizard-panel',
        title: 'Bulk Entry',
        region: 'center',
        layout: 'card',
        activeItem: 0,
        bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        tbar: panel_tbar_items,
        bbar: new Ext.Toolbar({ items: [
            {
                id: 'card-prev',
                text: '<span style="font-weight: bold; font-size: 130%;">Back</span>',
                iconCls: 'previousButton',
                width: 100,
                handler: cardNav.createDelegate(this, [-1]),
                disabled: true
            }, { xtype: 'tbspacer', width: 150 },
            {
                id: 'card-next',
                text: '<span style="font-weight: bold; font-size: 130%;">Next</span>',
                iconCls: 'nextButton',
                iconAlign: 'right',
                width: 100,
                handler: cardNav.createDelegate(this, [1])
            }
        ], buttonAlign: 'center' }),
        // the panels (or "cards") within the layout
        items: [ selectEntryCard, operationsEntryCard, postEntryCard ]
    });
    
    
    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [headerBox, entryPanel]
    });
    
    
    POST.selectMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
        new OpenLayers.Projection('EPSG:4326'), POST.selectMap.getProjectionObject()), 4);

    POST.load_user_entries();
    
});


/**
Load previous Entries created by this user according to selection criteria so
that the user can select which Entry they want to modify.
*/
POST.load_user_entries = function () {
    POST.selectControls.loading.maximizeControl();
    var history_val = Ext.getCmp('history-interval').getValue();
    var map_val = Ext.getCmp('map-view-filter').getValue();
    var feed_url = OpenLayers.Util.urlAppend(POST.FEED_URL,
        'secret=' + POST.USER_SECRET + '&lang=en');
    var allowed_val = Ext.getCmp('history-entries-allowed').getValue();
    // by default all editable by this author or created by this author
    if (allowed_val) {
        feed_url += '&author=' + POST.USER_URI;
    } else {
        feed_url += '&update_allow=' + POST.USER_URI;
    }
    // default is 0 for entries that are still current and so no datetime
    // parameters are added to the query
    if (history_val !== 0) {
        var since_date = new Date();
        // convert local to UTC first
        since_date = MASAS.Common.adjust_time(since_date,
            MASAS.Common.Local_UTC_Offset);
        // current or effective time starts right now, all others are a time
        // value in the past
        if (history_val > 2) {
            var diff_hours = parseInt(history_val, 10);
            // using a negative number for the past
            since_date = MASAS.Common.adjust_time(since_date,
                -Math.abs(diff_hours));
        }
        // using Ext extensions to Date for formatting
        var since_string = since_date.format('Y-m-d\\TH:i:s\\Z');
        feed_url += '&dtsince=' + since_string;
        // default search is updated which works for time values in the past
        if (history_val === 1) {
            // change search value so that anything not yet expired is assumed
            // to be current and/or future effective
            feed_url += '&dtval=expires';
        } else if (history_val === 2) {
            // not yet effective, in the future
            feed_url += '&dtval=effective';
        }
    }
    var selectLayer = POST.selectLayers.select;
    if (map_val) {
        // current viewport only, creating new bounds because of the transform
        var current_bounds = POST.selectMap.getExtent().toArray();
        var current_view = new OpenLayers.Bounds.fromArray(current_bounds);
        current_view.transform(POST.selectMap.getProjectionObject(),
            new OpenLayers.Projection('EPSG:4326'));
        feed_url += '&bbox=' + current_view.toBBOX(4);
        if (selectLayer.events.listeners.featuresadded.length > 0) {
            // update feed again whenever the map view changes
            POST.selectMap.events.register('moveend', null, POST.load_user_entries);
            // since the zoomToExtent created in post-map as anonymous, remove all
            selectLayer.events.remove('featuresadded');
        }
    } else {
        if (selectLayer.events.listeners.featuresadded.length === 0) {
            // restore normal behaviour of bringing all entries into focus
            POST.selectMap.events.unregister('moveend', null, POST.load_user_entries);
            selectLayer.events.register('featuresadded', null, function () {
                POST.selectMap.zoomToExtent(selectLayer.getDataExtent());
            });
        }
    }
    // cache busting parameter
    feed_url += '&_dc=' + new Date().getTime();
    // support proxies
    if (POST.AJAX_PROXY_URL) {
        feed_url = POST.AJAX_PROXY_URL + encodeURIComponent(feed_url);
    }
    console.debug('Loading User Entries');
    console.log(feed_url);
    POST.selectStore.proxy.protocol.options.url = feed_url;
    POST.selectStore.load();
    // showing the effective column in the select grid for both future and
    // recently created entries to provide more info when selecting whereas
    // "current" shouldn't need it unless the user manually enables it
    try {
        var select_columns = POST.selectGridPanel.getColumnModel();
        var effective_column = select_columns.getIndexById('select-column-effective');
        select_columns.setHidden(effective_column,
            (history_val === 0) ? true : false);
    } catch(err) {
        console.error('Show effective column error: ' + err);
    }
};


/**
Select all handler to update the selection grid

@param {Boolean} - the select all checkbox either checked or not
*/
POST.grid_select_all = function (box) {
    POST.selectStore.each(function (record) {
        record.set('select', box);
    });
};


/**
Adds the operations and selected Entries to the confirmation card
*/
POST.populate_confirm_card = function () {
    var op_review = '<b>Operations:</b><ul>';
    if (Ext.getCmp('effective-operation') && !Ext.getCmp('effective-operation').collapsed) {
        var effective_txt = '';
        var effective_im = Ext.getCmp('entry-effective-immediate').getValue();
        var effective_dt = Ext.get('entry-effective-dt').getValue();
        var effective_tm = Ext.get('entry-effective-tm').getValue();
        if (effective_im) {
            effective_txt += 'Immediately';
        } else {
            if (effective_dt && effective_tm) {
                effective_txt = effective_dt + ' ' + effective_tm;
            }
        }
        op_review += '<li style="margin-left: 25px;">Modify Effective Time: ' +
            effective_txt + '</li>';
    }
    if (Ext.getCmp('expires-operation') && !Ext.getCmp('expires-operation').collapsed) {
        var expires_txt = '';
        var expires_dt = Ext.get('entry-expires-dt').getValue();
        var expires_tm = Ext.get('entry-expires-tm').getValue();
        var expires_it = Ext.getCmp('entry-expires-interval').getValue();
        if (expires_dt && expires_tm) {
            expires_txt = expires_dt + ' ' + expires_tm;
        } else if (expires_it) {
            if (expires_it === 99) {
                expires_txt = 'Right Now';
            } else {
                expires_txt = expires_it + ' hrs';
            }
        }
        op_review += '<li style="margin-left: 25px;">Modify Expires Time: ' +
            expires_txt + '</li>';
    }
    if (Ext.getCmp('copy-operation') && !Ext.getCmp('copy-operation').collapsed) {
        op_review += '<li style="margin-left: 25px;">Copy to Another Hub: ' +
            POST.COPY_FEED_TITLE + '</li>';
    }
    op_review += '</ul>';
    document.getElementById('postingConfirm').innerHTML = op_review;
    
    var confirm_data = [];
    POST.selectStore.each(function (record) {
        if (record.get('select')) {
            confirm_data.push([record.get('fid'), record.get('icon'),
                record.get('title')]);
        }
    });
    //NOTE: loadData will cleanup previous data before adding new
    POST.confirmStore.loadData(confirm_data);
};


/**
Post all selected Entries with the respective operations

NOTE: sync loading all HTTP requests to keep them ordered.
*/
POST.post_all = function () {
    console.debug('Starting Post');
    var posting_html = '<h1>Posting</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.selectStore.each(function (record) {
        if (record.get('select')) {
            var select_result = '';
            try {
                // default is a PUT on existing hub
                var do_put = true;
                var do_attach = false;
                var entry = POST.load_entry(record.data.links.Atom.href);
                if (Ext.getCmp('effective-operation') &&
                !Ext.getCmp('effective-operation').collapsed) {
                    entry = POST.modify_effective_time(entry);
                }
                if (Ext.getCmp('expires-operation') &&
                !Ext.getCmp('expires-operation').collapsed) {
                    entry = POST.modify_expires_time(entry);
                }
                if (Ext.getCmp('copy-operation') &&
                !Ext.getCmp('copy-operation').collapsed) {
                    // a POST to the new hub used instead
                    do_put = false;
                    // determine if any attachments and prepare for posting
                    do_attach = POST.load_entry_attachments(entry);
                    if (do_attach) {
                        console.debug('Entry attachments loaded');
                    }
                }
                if (do_put) {
                    console.debug('PUT Entry ' + entry.entry.id);
                    POST.put_entry(entry);
                } else {
                    console.debug('POST Entry ' + entry.entry.id);
                    POST.post_entry(entry, do_attach);
                }
                select_result = '<span style="color: green;">Success</span>';
            } catch (err) {
                console.error('Posting Error: ' + err);
                select_result = '<span style="color: red;" title="' +
                    err + '">Error</span>';
            }
            
            try {
                var confirm_idx = POST.confirmStore.find('fid', record.get('fid'));
                if (confirm_idx > -1) {
                    var confirm_rec = POST.confirmStore.getAt(confirm_idx);
                    confirm_rec.set('result', select_result);
                    confirm_rec.commit();
                } else {
                    throw new Error('Unable to find ID to confirm');
                }
            } catch (err) {
                console.error('Confirm Error: ' + err);
            }
        }
    });
    console.debug('Post Complete');
    var result_html = '<h1 style="color: green;">Post Complete</h1>' +
        '<a href="#" onclick="window.close(); return false;" style="color: blue;">Return</a>';
    document.getElementById('postingBox').innerHTML = result_html;
};


/**
Load the Entry from a given URL

@param {String} - Entry link
*/
POST.load_entry = function (atom_link) {
    if (!atom_link) {
        throw new Error('Entry link missing');
    }
    console.debug('Loading Past Entry: ' + atom_link);
    var atom_get = new OpenLayers.Request.GET({
        url: atom_link,
        // secret on request line for now along with cache busting parameter
        params: {'secret': POST.USER_SECRET, '_dc': new Date().getTime()},
        proxy: POST.AJAX_PROXY_URL,
        async: false
    });
    if (atom_get.status !== 200) {
        throw new Error('Load Past Entry Error: ' + atom_get.responseText);
    }
    var xml_doc = MASAS.Common.parse_xml(atom_get.responseText);
    if (!xml_doc) {
        throw new Error('Error parsing past entry XML');
    }
    var entry_json = xmlJsonClass.xml2json(xml_doc, '  ');
    try {
        var entry_obj = JSON.parse(entry_json);
    } catch (err) {
        throw new Error('Past entry JSON parse error: ' + err);
    }
    return entry_obj;
};


/**
Modify the effective time for an Entry.  Does conversion from local time to UTC

@param {Object} - Entry object
*/
POST.modify_effective_time = function (entry) {
    console.debug('Updating effective time');
    var effective_im = Ext.getCmp('entry-effective-immediate').getValue();
    var effective_dt = Ext.get('entry-effective-dt').getValue();
    var effective_tm = Ext.get('entry-effective-tm').getValue();
    // use a newly entered effective time or immediate
    var effective_date = new Date();
    if (!effective_im && effective_dt && effective_tm) {
        // using Ext parsing and formatting extensions added to normal Date    
        var effective_str = effective_dt + 'T' + effective_tm + ':00';
        effective_date = Date.parseDate(effective_str, 'c');
    }
    effective_date = MASAS.Common.adjust_time(effective_date,
        MASAS.Common.Local_UTC_Offset);
    entry.entry['met:effective'] = effective_date.format('Y-m-d\\TH:i:00\\Z');
    
    return entry;
};


/**
Modify the expires time for an Entry.  Does conversion from local time to UTC

@param {Object} - Entry object
*/
POST.modify_expires_time = function (entry) {
    console.debug('Updating expires time');
    var expires_dt = Ext.get('entry-expires-dt').getValue();
    var expires_tm = Ext.get('entry-expires-tm').getValue();
    var expires_it = Ext.getCmp('entry-expires-interval').getValue();
    var expires_date = new Date();
    if (expires_dt && expires_tm) {
        // using Ext parsing and formatting extensions added to normal Date
        var expires_str = expires_dt + 'T' + expires_tm + ':00';
        expires_date = Date.parseDate(expires_str, 'c');
    } else if (expires_it) {
        // 99 is expires right now, all others are time value in the future
        if (expires_it !== 99) {
            var expires_diff = parseInt(expires_it, 10);
            expires_date = MASAS.Common.adjust_time(expires_date, expires_diff);
        }
    }
    expires_date = MASAS.Common.adjust_time(expires_date,
        MASAS.Common.Local_UTC_Offset);
    entry.entry['age:expires'] = expires_date.format('Y-m-d\\TH:i:00\\Z');
    
    return entry;
};

//TODO: add a checkbox on the operations page to "delete" the expires time?


/**
Setup the attachments on the proxy for posting

@param {Object} - Entry object
*/
POST.load_entry_attachments = function (entry) {
    var use_attach_proxy = false;
    // Clear out any old attachments stored by the proxy server
    // and setup allowable types and sizes
    //TODO: changing hubs might mean the new hub doesn't support same list
    //      of attachments
    OpenLayers.Request.GET({
        url: POST.SETUP_ATTACH_URL,
        params: {'feed': POST.FEED_URL, 'secret': POST.USER_SECRET,
            '_dc': new Date().getTime()},
        async: false,
        failure: function () {
            throw new Error('Unable to setup attachment handling');
        }
    });
    // there should always be multiple links to check, 
    if (entry.entry.link instanceof Array) {
        for (var l = 0; l < entry.entry.link.length; l++) {
            if (entry.entry.link[l]['@rel'] === 'enclosure' &&
            entry.entry.link[l]['@type'] !== 'application/common-alerting-protocol+xml') {
                var import_data = {
                    type: entry.entry.link[l]['@type'],
                    url: OpenLayers.Util.urlAppend(entry.entry.link[l]['@href'],
                        'secret=' + POST.USER_SECRET)
                };
                if ('@title' in entry.entry.link[l]) {
                    import_data.title = entry.entry.link[l]['@title'];
                } else {
                    import_data.title = 'Attachment';
                }
                var do_import = new OpenLayers.Request.POST({
                    url: POST.IMPORT_ATTACH_URL,
                    data: OpenLayers.Util.getParameterString(import_data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    async: false
                });
                if (do_import.status === 201 || do_import.status === 200) {
                    use_attach_proxy = true;
                } else {
                    throw new Error('Error importing attachment: ' +
                        entry.entry.link[l]['@href']);
                }
            }
        }
    }
    return use_attach_proxy;
};


/**
Generates the XML for an Entry

@param {Object} - Entry object
*/
POST.generate_entry_xml = function (entry) {
    var new_xml = xmlJsonClass.json2xml(entry, '');
    new_xml = MASAS.Common.format_xml(new_xml);
    new_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + new_xml;
    return new_xml;
};


/**
Posts an individual entry, either normally or using the attachment proxy.

@param {Object} - Entry object
@param {Boolean} - Use the attachment proxy because there are attachments
*/
POST.post_entry = function (entry, use_attach_proxy) {
    //TODO: look into a timeout for posting
    var entry_xml = POST.generate_entry_xml(entry);
    if (use_attach_proxy) {
        var req = new OpenLayers.Request.POST({
            url: POST.ATTACH_PROXY_URL +
                encodeURIComponent(OpenLayers.Util.urlAppend(POST.COPY_FEED_URL,
                    'secret=' + POST.COPY_USER_SECRET)),
            // proxy is ignored for same origin hosts, but we need it to always
            // force the use of the attach proxy, so modify url instead
            data: entry_xml,
            async: false
        });
    } else {
        var req = new OpenLayers.Request.POST({
            url: POST.COPY_FEED_URL,
            params: {'secret': POST.COPY_USER_SECRET},
            proxy: POST.AJAX_PROXY_URL,
            headers: {'Content-Type': 'application/atom+xml'},
            data: entry_xml,
            async: false
        });
    }
    if (req.status === 201 || req.status === 200) {
        return;
    } else {
        var errorText = req.responseText.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        throw new Error('Error with POST Entry: ' + errorText);
    }
};


/**
Updates an existing Entry

@param {Object} - Entry object
*/
POST.put_entry = function (entry) {
    var edit_url = null;
    if (entry.entry.link instanceof Array) {
        for (var i = 0; i < entry.entry.link.length; i++) {
            if (entry.entry.link[i]['@rel'] === 'edit') {
                edit_url = entry.entry.link[i]['@href'];
            }
        }
    } else if (typeof(entry.entry.link) === "object") {
        if (entry.entry.link['@rel'] === 'edit') {
            edit_url = entry.entry.link['@href'];
        }
    }
    if (!edit_url) {
        throw new Error('Unable to find edit url to post update to Entry');
    }
    var entry_xml = POST.generate_entry_xml(entry);
    var req = new OpenLayers.Request.PUT({
        url: edit_url,
        params:  {'secret': POST.USER_SECRET},
        proxy: POST.AJAX_PROXY_URL,
        headers: {'Content-Type': 'application/atom+xml'},
        data: entry_xml,
        async: false
    });
    if (req.status === 201 || req.status === 200) {
        return;
    } else {
        var errorText = req.responseText.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        throw new Error('Error with PUT Entry: ' + errorText);
    }
};
