/**
Create New Entry
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires postTemplates.js
@requires postMap.js
@requires src/common.js
@requires src/validators.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('POST');
// blank objects to start
POST.attachmentWindow = null;
POST.attachmentStore = null;
POST.attachmentGridPanel = null;
POST.locationMapPanel = null;
POST.relatedLinkWindow = null;
POST.relatedLinkStore = null;
POST.relatedLinkCount = 0;
POST.relatedLinkGridPanel = null;
POST.reviewWindow = null;
POST.postingTimer = null;
POST.POST_RESULT = '';

// for progress bar indicator
POST.PROGRESS_AMOUNT = 0;
// complete is 1 so divide by number of pages
POST.PROGRESS_INCREMENT = 0.5;


Ext.onReady(function () {
    
    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();
    
    // Custom field validations
    Ext.form.VTypes.location = function (v) {
        var location_formfield = Ext.getCmp('entry-location');
        var v_result = [false, true];
        if (location_formfield.geom === 'point') {
            v_result = POST.Validator.check_point(v);
        } else if (location_formfield.geom === 'line') {
            v_result = POST.Validator.check_line(v);
        } else if (location_formfield.geom === 'polygon') {
            v_result = POST.Validator.check_polygon(v);
        } else if (location_formfield.geom === 'circle') {
            v_result = POST.Validator.check_circle(v);
        } else if (location_formfield.geom === 'box') {
            v_result = POST.Validator.check_box(v); 
        }
        if (v_result[1] === false) {
            alert('Please note that this Location is outside North America.');
        }
        
        return v_result[0];
    };
    Ext.form.VTypes.locationText = 'Must be a valid location';
    
    var vkeyboard_plugin = new Ext.ux.plugins.VirtualKeyboard();
    
    
    // setting up the location map, layers, and controls
    POST.Map.initialize_location_map();
    POST.Map.initialize_drawing_controls(true);
    
    
    var headerBox = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Posting Tool</div>' +
            '<div id="headerDetailBlock">Version 0.2</div>'
    });
    
    var progressBar = new Ext.ProgressBar({
        id: 'progressBar',
        width: 150,
        style: 'margin-left: 25px',
        text: 'Progress'
    });
    
    var cardNav = function (direction) {
        var card_panel = Ext.getCmp('entry-wizard-panel').getLayout();
        var current_card = parseInt(card_panel.activeItem.id.split('card-')[1], 10);
        
        /* Card validation */
        // validate this card's form items first
        var found_invalid_item = false;
        card_panel.activeItem.cascade(function (item) {
            if (item.isFormField) {
                if (!item.validate()) {
                    found_invalid_item = true;
                }
            }
        });
        // validation check is performed here, comment out to bypass
        if (found_invalid_item) {
            return;
        }
        
        // validation for icon tree selection, effective, and expires on input card
        if (current_card === 0) {
            var tree_selection = Ext.getCmp('icon-select-tree').getSelectionModel().getSelectedNode();
            if (!tree_selection) {
                alert('An icon must be selected.');
                return;
            } else {
                if (!tree_selection.attributes.term) {
                    alert('An icon must be selected.');
                    return;
                }
            }
            var effective_dt = Ext.get('entry-effective-dt').getValue();
            var effective_tm = Ext.get('entry-effective-tm').getValue();
            if (effective_dt && !effective_tm) {
                alert('Effective time must be set.');
                return;
            } else if (!effective_dt && effective_tm) {
                alert('Effective date must be set.');
                return;
            }
            var expires_dt = Ext.get('entry-expires-dt').getValue();
            var expires_tm = Ext.get('entry-expires-tm').getValue();
            if (expires_dt && !expires_tm) {
                alert('Expires time must be set.');
                return;
            } else if (!expires_dt && expires_tm) {
                alert('Expires date must be set.');
                return;
            }
            //TODO: further validation of the values, such as effective must
            //      come before expires?
        }
        
        // change to the next or previous card
        var next_card = current_card + direction;
        card_panel.setActiveItem(next_card);
        // update the next or previous buttons depending on what card is
        // now active in the stack
        if (next_card === 0) {
            Ext.getCmp('card-prev').setDisabled(true);
        } else if (next_card === 1) {
            Ext.getCmp('card-prev').setDisabled(false);
            Ext.getCmp('card-next').setText('<span style="font-weight: bold;' +
                ' font-size: 130%; color: black;">Next</span>');
        } else if (next_card === 2) {
            Ext.getCmp('card-next').setText('<span style="font-weight: bold;' +
                ' font-size: 130%; color: red;">Advanced</span>');
            Ext.getCmp('card-next').setDisabled(false);
        } else if (next_card === 3) {
            Ext.getCmp('card-next').setDisabled(true);
        }
        // progress bar update
        if (direction === 1) {
            POST.PROGRESS_AMOUNT += POST.PROGRESS_INCREMENT;
        } else if (direction === -1) {
            POST.PROGRESS_AMOUNT -= POST.PROGRESS_INCREMENT;
        }
        progressBar.updateProgress(POST.PROGRESS_AMOUNT);
        
        /* Other card operations */
        // cleanup stray GeoExt popup windows which stick around between
        // panel changes
        if (current_card === 1) {
            POST.Common.layer_popup_cleanup(POST.locationLayers.location);
            if (POST.KML.settingsWindow) {
                POST.KML.settingsWindow.hide();
            }
        }
    };
    
    /* inputEntryCard */
    
    var templatePanel = new Ext.Panel({
        width: 200,
        height: 200,
        header: false,
        floating: true,
        autoScroll: true,
        layout: 'fit',
        tbar: new Ext.Toolbar({items: [
            'Templates',
            '->',
            {
                xtype: 'combo',
                width: 100,
                // the store holds the template group names and a group
                // selection defaulted to start but can be changed and will
                // reload the template list store accordingly
                store: POST.ENTRY_TEMPLATE_GROUPS,
                value: POST.ENTRY_TEMPLATE_GROUP_SELECT,
                editable: false,
                forceSelection: true,
                triggerAction: 'all',
                listeners: {
                    select: function (combo, record) {
                        POST.ENTRY_TEMPLATE_GROUP_SELECT = record.data.field1;
                        var t_store = Ext.getCmp('entry-template-list').store;
                        t_store.proxy.setUrl(OpenLayers.Util.urlAppend(POST.ENTRY_TEMPLATE_URL,
                        'group=' + POST.ENTRY_TEMPLATE_GROUP_SELECT));
                        t_store.reload();
                    }
                }
            }
        ]}),
        items: new Ext.list.ListView({
            id: 'entry-template-list',
            store: new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: OpenLayers.Util.urlAppend(POST.ENTRY_TEMPLATE_URL,
                        'group=' + POST.ENTRY_TEMPLATE_GROUP_SELECT)
                }),
                reader: new Ext.data.JsonReader({
                    root: 'templates',
                    fields: ['name', 'num']
                }),
                autoLoad: true
            }),
            hideHeaders: true,
            loadingText: 'Loading Templates',
            columns: [{
                header: 'Template',
                dataIndex: 'name'
            }],
            listeners: {
                click: function (view, idx, node) {
                    var record = view.getRecord(node);
                    OpenLayers.Request.GET({
                        url: POST.ENTRY_TEMPLATE_URL,
                        params: {
                            'group': POST.ENTRY_TEMPLATE_GROUP_SELECT,
                            'template': record.data.num,
                            // cache busting parameter used as well
                            '_dc': new Date().getTime()
                        },
                        failure: function () {
                            alert('Unable to load template.');
                            console.error('Unable to load template');
                        },
                        success: POST.load_template
                    });
                }
            }
        })
    });
    // must set this location relative to the inputEntryCard after rendering
    templatePanel.setPosition(700, 0);

    var iconTree = new Ext.tree.TreePanel({
        id: 'icon-select-tree',
        autoScroll: true,
        animate: true,
        border: true,
        height: 190,
        width: 400,
        loader: new Ext.tree.TreeLoader({
            dataUrl: POST.ICON_LIST_URL,
            requestMethod: 'GET',
            preloadChildren: true,
            // fix a preloadChildren bug, was to be fixed in ExtJS 3.3.2 but
            // doesn't appear to be as of 3.4.0, old ticket URL not longer working
            // http://code.extjs.com:8080/ext/ticket/1430
            load : function (node, callback, scope) {
                if (this.clearOnLoad) {
                    while (node.firstChild) {
                        node.removeChild(node.firstChild);
                    }
                }
                if (this.doPreload(node)) { // preloaded json children
                    this.runCallback(callback, scope || node, [node]);
                } else if (this.directFn || this.dataUrl || this.url) {   
                    // MB
                    if (this.preloadChildren) {
                        if (typeof(callback) !== 'function') {
                            callback = Ext.emptyFn;
                        }
                        callback = callback.createInterceptor(function (node) {
                            for (var i = 0; i < node.childNodes.length; i++) {
                                this.doPreload(node.childNodes[i]);
                            }
                        }, this);
                    }
                    // end-MB
                    this.requestData(node, callback, scope || node);
                }
            }
        }),
        root: {
            nodeType: 'async',
            text: 'Root Node'
        },
        rootVisible: false,
        listeners: {
            render: function () {
                this.getRootNode().expand();
            },
            click: function (node) {
                if (node.attributes.term !== null) {
                    var icon_url = POST.ICON_PREVIEW_URL + node.attributes.term +
                        '/large.png';
                    Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                        icon_url + '" alt="Icon"></div>');
                }
                // simple pre-population of the title value
                if (node.attributes.text) {
                    var sample_title = node.attributes.text;
                    if (node.attributes.term.search('incident') > -1) {
                        sample_title += ' Incident';
                    }
                    sample_title += ' located at _';
                    var entry_title = Ext.getCmp('entry-en-title');
                    if (entry_title.templateValue) {
                        // don't override something from a template
                        return;
                    }
                    var entry_title_val = entry_title.getValue();
                    if (entry_title_val &&
                    entry_title_val.search(' located at _') === -1) {
                        // don't override a customization already made by user
                        return;
                    }
                    // setRawValue means no validation
                    entry_title.setRawValue(sample_title);
                }
            }
        },
        tbar: ['Search:', {
                xtype: 'trigger',
                width: 100,
                triggerClass: 'x-form-clear-trigger',
                onTriggerClick: function () {
                    this.setValue('');
                    iconTree.filter.clear();
                },
                id: 'filter',
                enableKeyEvents: true,
                listeners: {
                    keyup: {buffer: 150, fn: function (field, e) {
                        if (Ext.EventObject.ESC === e.getKey()) {
                            field.onTriggerClick();
                        } else {
                            var val = this.getRawValue();
                            var re = new RegExp('.*' + val + '.*', 'i');
                            iconTree.filter.clear();
                            iconTree.filter.filter(re, 'text');
                        }
                    }}
                }
        }, '->', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeExpand',
                text: 'Expand',
                tooltip: 'Expand the entire icon tree',
                handler: function () {
                    iconTree.expandAll();
                }
        }), '-', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeCollapse',
                text: 'Collapse',
                tooltip: 'Collapse the entire icon tree',
                handler: function () {
                    iconTree.collapseAll();
                }
        })
        ]
    });
    iconTree.filter = new Ext.ux.tree.TreeFilterX(iconTree);
    
    var categoryCombo = new Ext.form.ComboBox({
        fieldLabel: 'Category',
        name: 'entry-category',
        id: 'entry-category',
        allowBlank: true,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 200,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'e_name',
        valueField: 'value',
        tpl: '<tpl for="."><div ext:qtip="{e_tip}" class="x-combo-list-item">{e_name}&nbsp;</div></tpl>',
        store: new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({url: POST.CATEGORY_LIST_URL}),
            reader: new Ext.data.JsonReader({
                root: 'categories',
                fields: ['e_name', 'e_tip', 'f_name', 'f_tip', 'value']
            }),
            autoLoad: true,
            listeners: {
                exception: function () {
                    console.error('Category Data failed to load');
                    alert('Unable to load Category Data.');
                }
            }
        })
    });
    
    var intervalCombo = new Ext.form.ComboBox({
        name: 'entry-expires-interval',
        id: 'entry-expires-interval',
        allowBlank: true,
        editable: false,
        width: 125,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['interval', 'name', 'tip'],
            data: [ [null, '', 'Do not set expires'],
                [1, '1 Hour', 'Expires in 1 hour'],
                [6, '6 Hours', 'Expires in 6 hours'],
                [12, '12 Hours', 'Expires in 12 hours'],
                [24, '24 Hours', 'Expires in 1 day'],
                [48, '48 Hours', 'Expires in 2 days'] ]
        }),
        listeners: {
            select: function (combo, record, index) {
                if (record.data.interval) {
                    var dt_check = Ext.get('entry-expires-dt').getValue();
                    if (dt_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                }
                // any change removes highlighting of initial default
                combo.removeClass('expiresIntervalDefault');
            },
            afterrender: function (combo) {
                if (POST.DEFAULT_EXPIRES_INTERVAL) {
                    // if a default expires interval is provided, modify the
                    // display name, select the default, and highlight it to
                    // make clear to the user it was defaulted.
                    var d_idx = combo.getStore().find('interval',
                        POST.DEFAULT_EXPIRES_INTERVAL);
                    if (d_idx !== -1) {
                        var d_rec = combo.getStore().getAt(d_idx);
                        var d_val = d_rec.get('name');
                        if (d_val.search('-Default') === -1) {
                            d_rec.set('name', d_val + '-Default');
                            d_rec.commit();
                        }
                        combo.setValue(d_rec.get('interval'));
                        combo.addClass('expiresIntervalDefault');
                    }
                }
            }
        }
    });
    
    POST.attachmentStore = new Ext.data.ArrayStore({
        fields: ['title', 'filename', 'num'],
        data: []
    });
    
    function renderRemoveAttachmentButton(val) {
        return '<a style="color: red; font-weight: bold;" onclick="POST.remove_attachment(\'' + val +
            '\')">Remove</a>';
    }

    POST.attachmentGridPanel = new Ext.grid.GridPanel({
        fieldLabel: 'Attachment',
        height: 90,
        width: 600,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        hideHeaders: true,
        store: POST.attachmentStore,
        columns: [{
            dataIndex: 'title'
        }, {
            dataIndex: 'filename',
            width: 200
        }, {
            dataIndex: 'num',
            width: 70,
            renderer: renderRemoveAttachmentButton
        }],
        autoExpandColumn: '0',
        buttonAlign: 'left',
        buttons: [new Ext.Button({
            id: 'attachment-add',
            width: 75,
            text: 'Add',
            tooltip: 'Attachments get uploaded and attached to this Entry',
            handler: POST.add_attachment
        }) ]
    });
    
    var inputEntryCard = new Ext.FormPanel({
        id: 'card-0',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        defaultType: 'textfield',
        items: [ templatePanel,
        {
            xtype: 'compositefield',
            fieldLabel: '<b>Icon</b>',
            //width: 300,
            //defaults: { flex: 1 },
            items: [ iconTree,
            {
                xtype: 'displayfield',
                id: 'icon-preview-box',
                html: '<div style="padding: 15px;"></div>'
            }]
        }, {
            fieldLabel: '<b>Title</b>',
            name: 'entry-en-title',
            id: 'entry-en-title',
            width: '60%',
            minLength: 5,
            minLengthText: 'A good title should say what and where',
            maxLength: 250,
            maxLengthText: 'Titles cannot be longer than 250 characters',
            blankText: 'MASAS requires a title value',
            allowBlank: false
        }, {
            xtype: 'textarea',
            fieldLabel: 'Content',
            name: 'entry-en-content',
            id: 'entry-en-content',
            height: 80,
            width: '75%',
            allowBlank: true
        }, {
            xtype: 'fieldset',
            checkboxToggle: true,
            collapsed: true,
            forceLayout: true,
            title: 'French',
            id: 'entry-add-french',
            defaultType: 'textfield',
            items: [{
                fieldLabel: '<b>Title</b>',
                name: 'entry-fr-title',
                id: 'entry-fr-title',
                width: '60%',
                minLengthText: 'A good title should say what and where',
                maxLength: 250,
                maxLengthText: 'Titles cannot be longer than 250 characters',
                blankText: 'MASAS requires a title value',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Content',
                name: 'entry-fr-content',
                id: 'entry-fr-content',
                height: 80,
                width: '75%',
                allowBlank: true,
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }],
            listeners: {
                expand: function () {
                    // reposition the icon because it was hidden on render
                    Ext.getCmp('entry-fr-title').alignKeyboardIcon();
                    Ext.getCmp('entry-fr-content').alignKeyboardIcon();
                    // enable validation of french values
                    Ext.getCmp('entry-fr-title').allowBlank = false;
                    Ext.getCmp('entry-fr-title').minLength = 5;
                },
                collapse: function () {
                    // disable validation of french values when not using
                    Ext.getCmp('entry-fr-title').allowBlank = true;
                    Ext.getCmp('entry-fr-title').minLength = null;
                }
            }
        }, categoryCombo, {
            xtype: 'radiogroup',
            fieldLabel: 'Severity',
            id: 'entry-severity',
            allowBlank: true,
            width: 500,
            items: [
                {boxLabel: 'Unknown', name: 'entry-severity', inputValue: 'Unknown'},
                {boxLabel: 'Minor', name: 'entry-severity', inputValue: 'Minor'},
                {boxLabel: 'Moderate', name: 'entry-severity', inputValue: 'Moderate'},
                {boxLabel: 'Severe', name: 'entry-severity', inputValue: 'Severe'},
                {boxLabel: 'Extreme', name: 'entry-severity', inputValue: 'Extreme'}
            ]
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Effective',
            // width ensures the validation error icon will display beside fields
            width: 325,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Immediately',
                name: 'entry-effective-immediate',
                id: 'entry-effective-immediate',
                // setting a height so the bottom part of the date/time fields
                // won't be cut off
                height: 23,
                checked: true,
                handler: function (checkbox, checked) {
                    if (checked) {
                        Ext.getCmp('entry-effective-at').hide();
                        Ext.getCmp('entry-effective-dt').hide();
                        Ext.getCmp('entry-effective-dt').reset();
                        Ext.getCmp('entry-effective-tm').hide();
                        Ext.getCmp('entry-effective-tm').reset();
                        // restore expires intervals
                        Ext.getCmp('entry-expires-interval').enable();
                    } else {
                        Ext.getCmp('entry-effective-at').show();
                        Ext.getCmp('entry-effective-dt').show();
                        Ext.getCmp('entry-effective-tm').show();
                        // prevent confusion with effective versus expires
                        Ext.getCmp('entry-expires-interval').reset();
                        Ext.getCmp('entry-expires-interval').disable();
                    }
                }
            }, {
                xtype: 'displayfield',
                id: 'entry-effective-at',
                html: ' at:',
                style: {'padding': '3px 2px 2px 20px'}
            }, {
                xtype: 'datefield',
                name: 'entry-effective-dt',
                id: 'entry-effective-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date()
            }, {
                xtype: 'timefield',
                name: 'entry-effective-tm',
                id: 'entry-effective-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var effective_dt = Ext.get('entry-effective-dt').getValue();
                    if (!effective_dt || effective_dt.length === 0) {
                        effective_dt = null;
                    }
                    return POST.Validator.check_future_time(effective_dt, val);
                }
            }],
            listeners: {afterrender: function () {
                // the date/time fields can't have the hidden property set at
                // render because their special sizing results in problems, so
                // hide them afterwards
                Ext.getCmp('entry-effective-at').hide();
                Ext.getCmp('entry-effective-dt').hide();
                Ext.getCmp('entry-effective-tm').hide();
            }}
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Expires at',
            // width ensures the validation error icon will display beside fields
            width: 425,
            items: [{
                xtype: 'datefield',
                name: 'entry-expires-dt',
                id: 'entry-expires-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'timefield',
                name: 'entry-expires-tm',
                id: 'entry-expires-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var expires_dt = Ext.get('entry-expires-dt').getValue();
                    if (!expires_dt || expires_dt.length === 0) {
                        expires_dt = null;
                    }
                    return POST.Validator.check_future_time(expires_dt, val);
                },
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'displayfield',
                html: 'or Expires in:',
                style: {'padding': '3px 2px 2px 20px'}
            }, intervalCombo ]
        }, POST.attachmentGridPanel ]
    });
    
    /* locationEntryCard */
    
    // setup the map Layers and Tools toolbars
    POST.MapLayers.initialize();
    POST.MapTools.initialize(true);
    
    // create map panel
    POST.locationMapPanel = new GeoExt.MapPanel({
        collapsible: false,
        style: 'margin-bottom: 10px;',
        // 10% room left at the bottom for Location text field
        anchor: '100% 90%',
        map: POST.locationMap,
        tbar: POST.mapToolbarTop,
        bbar: POST.mapToolbarBottom
    });
    
    var locationEntryCard = new Ext.FormPanel({
        id: 'card-1',
        labelWidth: 60,
        // this layout type allows for % based sizing
        layout: 'anchor',
        items: [POST.locationMapPanel, {
            xtype: 'compositefield',
            items: [{
                // form field titles don't seem to display when using the
                // anchor layout so create one instead
                xtype: 'displayfield',
                id: 'entry-location-title',
                width: 75,
                html: '<b>Location:</b> '
            }, {
                xtype: 'textfield',
                name: 'entry-location',
                id: 'entry-location',
                width: 400,
                vtype: 'location',
                validationEvent: false,
                blankText: 'A location is required',
                allowBlank: false
            }, {
                xtype: 'displayfield',
                id: 'entry-kml-notice',
                style: 'margin-left: 50px;',
                html: ''
            }]
        }]
    });
    
    /* postEntryCard */
    
    var postEntryCard = new Ext.Panel({
        id: 'card-2',
        items: [{
            border: false,
            bodyStyle: 'margin-bottom: 50px',
            html: '<div class="postingCard" id="postingBox"><h1>Ready to Post</h1>' + 
                '<a href="#" onclick="POST.preview_entry_xml(); return false;" style="color: grey;">Preview</a>' +
                '<a href="#" onclick="POST.post_new_entry(); return false;" style="color: green;">Post Entry</a>' +
                '</div>'
        }]
    });
    
    if (POST.EMAIL_ADDRESS_LIST) {
        var emailToCombo = new Ext.form.ComboBox({
            fieldLabel: '<b>To</b>',
            name: 'email-to-address',
            id: 'email-to-address',
            width: 300,
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            store: POST.EMAIL_ADDRESS_LIST
        });
    
        postEntryCard.add({
            xtype: 'fieldset',
            id: 'email-to-fieldset',
            hidden: true,
            collapsible: true,
            collapsed: true,
            title: 'Email Forwarding',
            labelWidth: 70,
            listeners: {expand: POST.Common.generate_email_content },
            items: [ emailToCombo, {
                xtype: 'textfield',
                fieldLabel: '<b>Subject</b>',
                name: 'email-to-subject',
                id: 'email-to-subject',
                width: 400,
                minLength: 5,
                minLengthText: 'A subject is required',
                maxLength: 250,
                maxLengthText: 'Subject cannot be longer than 250 characters'
            }, {
                xtype: 'textarea',
                fieldLabel: '<b>Message</b>',
                name: 'email-to-message',
                id: 'email-to-message',
                height: 150,
                width: 800,
                minLength: 5,
                minLengthText: 'A message is required'
            }, {
                xtype: 'displayfield',
                id: 'email-attachment-notice',
                hidden: true,
                html: '<div><span style="color: red; font-weight: bold;">Note:' +
                    '</span> Attachments are not included with this message. ' +
                    'Add the attachments to the forwarded email you receive.</div>'
            }, {
                xtype: 'button',
                fieldLabel: ' ',
                labelSeparator: '',
                text: 'Send',
                width: 75,
                handler: POST.Common.post_email_message
            }]
        });
    }
    
    /* optionalEntryCard */
    
    var statusCombo = new Ext.form.ComboBox({
        fieldLabel: '<b>Status</b>',
        name: 'entry-status',
        id: 'entry-status',
        allowBlank: false,
        editable: false,
        width: 100,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'name',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'tip'],
            // used by MASAS-X to set different values depending on Mode
            data: (POST.CUSTOM_STATUS_VALUES) ? POST.CUSTOM_STATUS_VALUES :
                [ ['Actual', 'Actual Entries'],
                ['Exercise', 'Exercises and other practice entries'],
                ['Test', 'Test Entries'],
                ['Draft', 'Entries which cannot be seen by other users'] ]
        }),
        listeners: {
            afterrender: function (combo) {
                // need to set an initial selection this way because you don't
                // know what the first value might be up front when using
                // custom values, 
                var first_val = combo.getStore().getAt(0);
                combo.setValue(first_val.get('name'));
            }
        }
    });
    
    var colourStoreData = [
        ['', null],
        ['Gray', 'Not Available'],
        ['Blue', 'Information'],
        ['Green', 'Safety'],
        ['Yellow', 'Caution'],
        ['Red', 'Danger'],
        ['Purple', 'Fatal Danger'],
        ['Black', 'Fatal Danger']
    ];
    
    var colourCombo = new Ext.form.ComboBox({
        name: 'entry-colour-combo',
        id: 'entry-colour-combo',
        allowBlank: true,
        editable: false,
        width: 75,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'colour',
        valueField: 'colour',
        colourLabel: 'Colour Code',
        defaultColourLabel: 'Colour Code',
        tpl: '<tpl for="."><div ext:qtip="{context}" class="x-combo-list-item">{colour}&nbsp;</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['colour', 'context'],
            data: colourStoreData
        }),
        defaultStore: colourStoreData,
        listeners: {
            select: function (combo, record, index) {
                if (record.data.colour) {
                    Ext.getCmp('entry-colour-label').enable();
                    Ext.getCmp('entry-colour-label').setValue(combo.colourLabel);
                    Ext.getCmp('entry-colour-context').enable();
                    Ext.getCmp('entry-colour-context').setValue(record.data.context);
                    // set colour for location feature drawing
                    POST.locationLayers.location.defaultColour = record.data.colour.toLowerCase();
                } else {
                    Ext.getCmp('entry-colour-label').reset();
                    Ext.getCmp('entry-colour-label').disable();
                    Ext.getCmp('entry-colour-context').reset();
                    Ext.getCmp('entry-colour-context').disable();
                    POST.locationLayers.location.defaultColour = null;
                }
            }
        }
    });
    
    POST.relatedLinkStore = new Ext.data.ArrayStore({
        fields: ['title', 'url', 'type', 'num'],
        data: []
    });
    
    function renderRemoveLinkButton(val) {
        return '<a style="color: red; font-weight: bold;" onclick="POST.remove_related_link(' +
            val + ')">Remove</a>';
    }

    POST.relatedLinkGridPanel = new Ext.grid.GridPanel({
        fieldLabel: 'Links',
        height: 60,
        //width: 600,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        hideHeaders: true,
        store: POST.relatedLinkStore,
        columns: [{
            dataIndex: 'title',
            width: 200
        }, {
            dataIndex: 'type',
            width: 150
        }, {
            dataIndex: 'url'
        }, {
            dataIndex: 'num',
            width: 70,
            renderer: renderRemoveLinkButton
        }],
        autoExpandColumn: '2',
        buttonAlign: 'left',
        buttons: [new Ext.Button({
            width: 75,
            text: 'Add',
            tooltip: 'Add related links to this Entry',
            handler: POST.add_related_link
        }) ]
    });
    
    var optionalEntryCard = new Ext.FormPanel({
        id: 'card-3',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        items: [{
            xtype: 'displayfield',
            hideLabel: true,
            html: '<div style="margin: 0px 0px 15px 0px; font-size: 140%; '+
                'font-weight: bold;">Advanced Options</div>'
        }, {
            xtype: 'fieldset',
            title: ' Entry Status ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'The status of an Entry can be different than the normal ' +
                    'value of Actual using other values such as Test, allowing ' +
                    'MASAS users to filter Entries accordingly.  Draft Entries ' +
                    'are a special case, allowing you to work on and save an ' +
                    'Entry to the Hub without other users being able to see it ' +
                    'until you set its status to be Actual.'
            }, statusCombo]
        }, {
            xtype: 'fieldset',
            title: ' Certainty ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'If the information in this Entry is an estimate, forecast, ' +
                    'or came from a 3rd party source you can use Certainty ' +
                    'to make that known to other users.'
            }, {
                xtype: 'radiogroup',
                fieldLabel: 'Certainty',
                id: 'entry-certainty',
                allowBlank: true,
                items: [
                    {boxLabel: 'Unknown', name: 'entry-certainty', inputValue: 'Unknown'},
                    {boxLabel: 'Unlikely', name: 'entry-certainty', inputValue: 'Unlikely'},
                    {boxLabel: 'Possible', name: 'entry-certainty', inputValue: 'Possible'},
                    {boxLabel: 'Likely', name: 'entry-certainty', inputValue: 'Likely'},
                    {boxLabel: 'Observed', name: 'entry-certainty', inputValue: 'Observed'}
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: ' Colour ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'A Colour and appropriate Context can be used for this Entry.'
            }, {
                xtype: 'compositefield',
                fieldLabel: 'Colour',
                //width: 400,
                items: [{
                    xtype: 'textfield',
                    id: 'entry-colour-label',
                    width: 100,
                    disabled: true
                }, colourCombo, {
                    xtype: 'displayfield',
                    html: 'Context:',
                    style: {'padding-left': '10px'}
                }, {
                    xtype: 'textfield',
                    id: 'entry-colour-context',
                    width: 250,
                    disabled: true
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: ' Related Links ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'Link(s) to information related to this Entry.'
            }, POST.relatedLinkGridPanel ]
        }, {
            xtype: 'fieldset',
            title: ' Update Permissions ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'The default is that only your account can update an ' +
                    'Entry that you create.  However you can allow other ' +
                    'accounts to update Entries if you want them to share any ' +
                    'information that they can contribute.  Once you or another ' +
                    'account updates your Entry though, they can change the ' +
                    'Update Permissions back to a single account only if necessary.'
            }, {
                xtype: 'radiogroup',
                id: 'entry-update-control',
                allowBlank: false,
                // assume that user would want any group account to edit
                value: (POST.USER_GROUP) ? 'group' : 'user',
                items: [{
                    boxLabel: 'Only My Account May Update',
                    name: 'entry-update-control',
                    inputValue: 'user'
                }, {
                    boxLabel: 'Any Account In My Group(s) May Update',
                    name: 'entry-update-control',
                    inputValue: 'group',
                    disabled: (POST.USER_GROUP) ? false : true
                }, {
                    boxLabel: 'Allow Any MASAS Account To Update',
                    name: 'entry-update-control',
                    inputValue: 'all'
                }]
            }]
        }]
    });
    
    /* entryPanel */
    
    // the top toolbar for the app
    var panel_tbar_items = [];
    // optional support for multiple hubs
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'}
        ]);
        var hubCombo = new Ext.form.ComboBox({
            allowBlank: false,
            editable: false,
            width: 125,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                afterrender: function (combo) {
                    var default_idx = combo.getStore().find('url', POST.FEED_URL);
                    if (default_idx === -1) {
                        console.error('Unable to find Hub selection default');
                        // first value in array instead
                        default_idx = 0; 
                    }
                    var default_val = combo.getStore().getAt(default_idx);
                    combo.setValue(default_val.get('title'));
                },
                select: function (combo, record) {
                    console.debug('Changing feed to ' + record.data.title);
                    if (record.data.url) {
                        POST.FEED_URL = record.data.url;
                    }
                    if (record.data.secret) {
                        POST.USER_SECRET = record.data.secret;
                    }
                    if (record.data.uri) {
                        POST.USER_URI = record.data.uri;
                    }
                    // setup allowable types and sizes for this new feed but
                    // does not clear out old attachments already stored on
                    // the server. Runs async and may take some time
                    // to return because the Hub is being queried
                    Ext.getCmp('attachment-add').enable();
                    OpenLayers.Request.GET({
                        url: POST.SETUP_ATTACH_URL,
                        params: {
                            'feed': POST.FEED_URL,
                            'secret': POST.USER_SECRET,
                            '_dc': new Date().getTime()
                        },
                        failure: function () {
                            alert('Unable to setup attachment handling.');
                            console.error('Unable to setup attachments for new feed');
                            Ext.getCmp('attachment-add').disable();
                        }
                    });
                }
            }
        });
        panel_tbar_items = [
            {xtype: 'tbspacer', width: 15},
            {
                xtype: 'tbtext',
                style: {'fontWeight': 'bold', 'font-size': '13px'},
                text: 'Hub:'
            },
            {xtype: 'tbspacer', width: 25},
            hubCombo
        ];
    }
    // default items
    panel_tbar_items.push.apply(panel_tbar_items, [progressBar, '->', {
        text: '<b>Abort</b>',
        iconCls: 'abortButton',
        width: 75,
        handler: function () {
            window.close();
        }
    }]);
    
    var entryPanel = new Ext.Panel({
        id: 'entry-wizard-panel',
        title: 'New Entry',
        region: 'center',
        layout: 'card',
        activeItem: 0,
        bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        tbar: panel_tbar_items,
        bbar: new Ext.Toolbar({ items: [
            {
                id: 'card-prev',
                text: '<span style="font-weight: bold; font-size: 130%;">Back</span>',
                iconCls: 'previousButton',
                width: 100,
                handler: cardNav.createDelegate(this, [-1]),
                disabled: true
            }, { xtype: 'tbspacer', width: 150 },
            {
                id: 'card-next',
                text: '<span style="font-weight: bold; font-size: 130%;">Next</span>',
                iconCls: 'nextButton',
                iconAlign: 'right',
                width: 100,
                handler: cardNav.createDelegate(this, [1])
            }
        ], buttonAlign: 'center' }),
        // the panels (or "cards") within the layout
        items: [ inputEntryCard, locationEntryCard, postEntryCard, optionalEntryCard ]
    });
    
    
    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [headerBox, entryPanel]
    });
    
    // Clear out any old attachments stored by the demonstration server to start
    // and setup allowable types and sizes, runs async and make take some time
    // to return because the Hub is being queried
    OpenLayers.Request.GET({
        url: POST.SETUP_ATTACH_URL,
        params: {
            'feed': POST.FEED_URL,
            'reset': 'yes',
            'secret': POST.USER_SECRET,
            '_dc': new Date().getTime()
        },
        failure: function () {
            alert('Unable to setup attachment handling.');
            console.error('Unable to purge and setup attachments');
            Ext.getCmp('attachment-add').disable();
        }
    });

});


/**
Loads a selected template's values

@param {Object} - the XHR returned JSON template
*/
POST.load_template = function (xhr) {
    try {
        var template = JSON.parse(xhr.responseText);
    } catch (err) {
        console.error('Load template JSON parse error: ' + err);
        alert('Unable to load template.');
        return;
    }
    //TODO: not resetting all form fields when a user switches between templates
    //      as it could overwrite other fields where the user has info,
    //      causing unintended side affects.  Need to review.
    //Ext.getCmp('card-0').getForm().reset();
    //Ext.getCmp('icon-select-tree').collapseAll();
    //Ext.getCmp('entry-expires-interval').fireEvent('afterrender',
    //    Ext.getCmp('entry-expires-interval'));
    // all template values are optional
    if (template.category) {
        Ext.getCmp('entry-category').setValue(template.category);
    }
    if (template.icon) {
        // need to seach multiple children (true) into the tree to find the
        // right icon match, then expand the tree to this node and select it
        var icon_tree = Ext.getCmp('icon-select-tree');
        // the preloadChildren patch in the tree loader is currently needed
        // for this findChild to work
        var node_match = icon_tree.root.findChild('term', template.icon, true);
        if (node_match) {
            icon_tree.expandPath(node_match.getPath(), null,
                function (e_success, e_last) {
                    if (e_success) {
                        e_last.select();
                    }
                }
            );
            var icon_url = POST.ICON_PREVIEW_URL + template.icon + '/large.png';
            Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                icon_url + '"></div>');
        } else {
            console.error('No match for icon: ' + template.icon);
            alert('Unable to find an icon that matches template value.');
        }
    }
    if (template.en_title) {
        Ext.getCmp('entry-en-title').setValue(template.en_title);
        // prevents overwriting by icon tree selection
        Ext.getCmp('entry-en-title').templateValue = true;
    }
    if (template.fr_title) {
        Ext.getCmp('entry-fr-title').setValue(template.fr_title);
    }
    if (template.en_content) {
        Ext.getCmp('entry-en-content').setValue(template.en_content);
    }
    if (template.fr_content) {
        Ext.getCmp('entry-fr-content').setValue(template.fr_content);
    }
    if (template.severity) {
        Ext.getCmp('entry-severity').setValue(template.severity);
    }
    var colour_combo = Ext.getCmp('entry-colour-combo');
    var colour_store = colour_combo.getStore();
    if (template.colour) {
        colour_combo.colourLabel = template.colour.label;
        colour_store.loadData(template.colour.data);
    } else {
        // reset to original defaults
        colour_combo.colourLabel = colour_combo.defaultColourLabel;
        colour_store.loadData(colour_combo.defaultStore);
        colour_combo.reset();
        Ext.getCmp('entry-colour-label').reset();
        Ext.getCmp('entry-colour-label').disable();
        Ext.getCmp('entry-colour-context').reset();
        Ext.getCmp('entry-colour-context').disable();
    }
    if (template.expires) {
        Ext.getCmp('entry-expires-interval').setValue(template.expires);
    }
    if (template.update_allow) {
        Ext.getCmp('entry-update-control').setValue(template.update_allow);
    }
};


/**
Display the add attachment window, which will allow the upload of the attachment
to the temporary location on the server.
*/
POST.add_attachment = function () {
    // only one attachment window allowed
    if (POST.attachmentWindow) {
        POST.attachmentWindow.destroy();
    }
    var filePanel = new Ext.FormPanel({
        fileUpload: true,
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
                xtype: 'textfield',
                fieldLabel: '<b>Title</b>',
                name: 'attachment-title',
                id: 'attachment-title',
                width: 200
            }, {
                xtype: 'fileuploadfield',
                fieldLabel: '<b>File</b>',
                name: 'attachment-file',
                id: 'attachment-file',
                emptyText: 'Select a file to upload',
                width: 350
        }],
        buttons: [{
            text: 'Upload',
            handler: function () {
                if (filePanel.getForm().isValid()) {
                    filePanel.getForm().submit({
                            url: POST.ADD_ATTACH_URL,
                            submitEmptyText: false,
                            waitMsg: 'Uploading the file...',
                            success: POST.add_attachment_success,
                            failure: function (form, response) {
                                console.error('Upload Attachment Error: ' +
                                    response.result.message);
                                Ext.Msg.alert('Upload Error',
                                    'Unable to upload the file.  ' + response.result.message);
                                POST.attachmentWindow.destroy();
                            }
                    });
                }
            }
        }]
    });
    POST.attachmentWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Add Attachment <a class="titleClose" href="#" ' +
            'onclick="POST.attachmentWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 140,
        layout: 'fit',
        items: [filePanel]
    });
    POST.attachmentWindow.show(this);
};


/**
Add attachment success handler which will also update the attachment store.

@param {Object} - the form used to submit the attachment
@param {Object} - the response from this form
*/
POST.add_attachment_success = function (form, response) {
    try {
        var new_rec = new POST.attachmentStore.recordType({
            title: response.form.getFieldValues()['attachment-title'],
            filename: response.form.getFieldValues()['attachment-file'],
            num: response.result.num
        });
        POST.attachmentStore.add(new_rec);
        POST.attachmentStore.commitChanges();
        POST.USE_ATTACH_PROXY = true;
        console.debug('Added attachment ' + new_rec.data.num);
        POST.attachmentWindow.destroy();
    } catch (err) {
        console.error('Add Attachment Error: ' + err);
        alert('Unable to add this attachment.');
        POST.attachmentWindow.destroy();
    }
};


/**
Removes an attachment from the temporary location on the server

@param {Integer} - the attachment number
*/
POST.remove_attachment = function (num) {
    var r_confirm = confirm('Are you sure you want to remove this attachment?');
    if (r_confirm === true) {
        Ext.Ajax.request({
            url: OpenLayers.Util.urlAppend(POST.REMOVE_ATTACH_URL,
                'num=' + num),
            method: 'DELETE',
            success: POST.remove_attachment_success(num),
            failure: function (response) {
                console.error('Delete Attachment Error: ' + response.responseText);
                alert('Unable to remove attachment.');
            }
        });
    }
};


/**
Remove attachment success handler, updates the attachment store.

@param {Integer} - the attachment number
*/
POST.remove_attachment_success = function (num) {
    var idx = POST.attachmentStore.find('num', num);
    if (idx === -1) {
        console.error('Remove Attachment ' + num + ' Error');
        alert('Unable to remove attachment.');
    } else {
        POST.attachmentStore.removeAt(idx);
        POST.attachmentStore.commitChanges();
        console.debug('Removed Attachment ' + num);
    }
};


/**
Display the add related link window, which enables the addition of related
links to this Entry.
*/
POST.add_related_link = function () {
    // only one link window allowed
    if (POST.relatedLinkWindow) {
        POST.relatedLinkWindow.destroy();
    }
    
    var typeCombo = new Ext.form.ComboBox({
        fieldLabel: '<b>Type</b>',
        name: 'related-link-type',
        id: 'related-link-type',
        allowBlank: false,
        editable: false,
        width: 200,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'value',
        value: 'text/html',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'value'],
            data: [
                ['Web Page', 'text/html'],
                ['Web Map Service', 'application/vnd.ogc.wms'],
                ['Tile Map Service', 'application/vnd.osgeo.tms']
            ]
        })
    });
    
    var linkPanel = new Ext.FormPanel({
        id: 'related-link-panel',
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'textfield',
            fieldLabel: '<b>Title</b>',
            name: 'related-link-title',
            id: 'related-link-title',
            width: 200,
            minLength: 3,
            minLengthText: 'A Title is required',
            blankText: 'A Title is required'
        }, typeCombo, {
            xtype: 'textfield',
            fieldLabel: '<b>URL</b>',
            name: 'related-link-url',
            id: 'related-link-url',
            width: 400,
            minLength: 5,
            minLengthText: 'A URL is required',
            blankText: 'A URL is required',
            vtype: 'url',
            validator: function (val) {
                // the ExtJS URL validation doesn't check for spaces
                if (val.search(/\s/) !== -1) {
                    return 'Blank spaces are not allowed in a URL';
                }
                return true;
            }
        }],
        buttons: [{
            text: 'Add',
            handler: function () {
                if (Ext.getCmp('related-link-panel').getForm().isValid()) {
                    var new_rec = new POST.relatedLinkStore.recordType({
                        title: Ext.getCmp('related-link-title').getValue(),
                        type: Ext.getCmp('related-link-type').getValue(),
                        url: Ext.getCmp('related-link-url').getValue(),
                        num: POST.relatedLinkCount
                    });
                    POST.relatedLinkStore.add(new_rec);
                    POST.relatedLinkStore.commitChanges();
                    console.debug('Added related link ' + POST.relatedLinkCount);
                    // used to provide a unique value for this link as its
                    // store index can change and users might submit identical
                    // links
                    POST.relatedLinkCount += 1;
                    POST.relatedLinkWindow.destroy(); 
                }
            }
        }]
    });
    POST.relatedLinkWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Add Related Link <a class="titleClose" href="#" ' +
            'onclick="POST.relatedLinkWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 160,
        layout: 'fit',
        items: [linkPanel]
    });
    POST.relatedLinkWindow.show(this);
};


/**
Remove a related link from the related links store.

@param {Integer} - the record index number
*/
POST.remove_related_link = function (num) {
    var idx = POST.relatedLinkStore.find('num', num);
    if (idx === -1) {
        console.error('Remove related link ' + num + ' Error');
        alert('Unable to remove link.');
    } else {
        POST.relatedLinkStore.removeAt(idx);
        POST.relatedLinkStore.commitChanges();
        console.debug('Removed related link ' + num);
    }
};


/**
Special handling to create a circle after a point feature is drawn.  Provides
a popup to enter the radius.

@param {Object} - the OpenLayers point feature for the center of the circle
@param {Boolean} - use a KML handler or regular Location
*/
POST.create_circle_feature = function (feature, use_kml) {
    var radius_field = new Ext.form.TextField({
        width: 50,
        blankText: 'A radius is required',
        allowBlank: false
    });
    if (feature.child_polygon) {
        // set to previous value if modifying an existing circle
        if (feature.child_polygon.circle_radius) {
            radius_field.setValue(feature.child_polygon.circle_radius);
        }
    }
    var draw_button = new Ext.Button({
        text: 'Draw',
        width: 50,
        handler: function () {
            if (feature && radius_field) {
                if (feature.child_polygon) {
                    feature.layer.removeFeatures([feature.child_polygon]);
                    feature.child_polygon = null;
                }
                var center = feature.geometry.getCentroid();
                var radius = radius_field.getValue();
                if (center && radius) {
                    var circle_polygon = OpenLayers.Geometry.Polygon.createRegularPolygon(
                        center, radius * 1000, 40);
                    var circle_feature = new OpenLayers.Feature.Vector(circle_polygon);
                    circle_feature.circle_center = feature;
                    circle_feature.circle_radius = radius;
                    feature.layer.addFeatures([circle_feature]);
                    feature.child_polygon = circle_feature;
                } else {
                    // nothing entered for radius so show field error
                    radius_field.markInvalid();
                }
            }
        }
    });
    var save_button = new Ext.Button({
        text: 'Save',
        width: 50,
        handler: function () {
            if (feature) {
                if (feature.child_polygon) {
                    feature.circle_popup.hide();
                    if (use_kml) {
                        // KML specific handler
                        POST.KML.show_feature_window(feature.child_polygon);
                    } else {
                        POST.save_location_feature(feature.child_polygon);
                    }
                }
            }
        }
    });
    feature.circle_popup = new GeoExt.Popup({
        title: 'Draw Circle',
        location: feature,
        width: 275,
        items: [{
            xtype: 'compositefield',
            items: [{
                xtype: 'displayfield',
                html: '<b>Radius (km) :</b>'
            }, radius_field, draw_button, save_button]
        }]
    });
    feature.circle_popup.show();
    radius_field.focus(true, 500);
};


/**
Saves a location feature and its values, both to the on-screen display for the
user and for later processing.

@param {Object} - the OpenLayers feature to be saved
*/
POST.save_location_feature = function (feature) {
    console.debug('Saving Location');
    console.log(feature);
    // add the optional colour after drawing
    if (feature.layer) {
        // resets rendering after a save
        feature.renderIntent = 'default';
        if (feature.layer.defaultColour) {
            feature.attributes.colour = feature.layer.defaultColour;
        }
        feature.layer.redraw();
    }
    // update the location form values with this geometry
    var location_input = document.getElementById('entry-location');
    var location_formfield = Ext.getCmp('entry-location');
    var location_title = Ext.getCmp('entry-location-title');
    var measure_value = null;
    // the notification message for the popup beside the location field
    var notification_str = 'Location Saved';
    if (location_formfield.geom) {
        notification_str = 'Updated Location';
    }
    if (feature.geometry) {
        var point_result = [];
        var feature_points = feature.geometry.getVertices();
        for (var i = 0; i < feature_points.length; i++) {
            var new_point = new OpenLayers.Geometry.Point(feature_points[i].x,
                feature_points[i].y);
            new_point.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            point_result.push(new_point.y.toPrecision(8) + ',' + new_point.x.toPrecision(8));
        }
        if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Point') {
            if (point_result.length === 1) {
                // setting the geom first so validation works when value added
                location_formfield.geom = 'point';
                location_input.value = point_result[0];
                location_title.update('<b>Point:</b>');
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.LineString') {
            if (point_result.length > 1) {
                location_formfield.geom = 'line';
                location_input.value = point_result.join(' ');
                location_title.update('<b>Line:</b>');
                // getGeodesicLength/Area was not returning correct values
                // map units are currently in meters, using getLength okay for now
                var l_measure = feature.geometry.getLength() / 1000;
                measure_value = ['Length',  l_measure.toFixed(2) + ' km'];
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Polygon') {
            if (feature.circle_center && feature.circle_radius) {
                location_formfield.geom = 'circle';
                var center_point = feature.circle_center.geometry.getCentroid();
                center_point.transform(POST.locationMap.getProjectionObject(),
                    new OpenLayers.Projection('EPSG:4326'));
                location_input.value = center_point.y.toPrecision(8) + ',' + 
                    center_point.x.toPrecision(8) + ' ' + parseFloat(feature.circle_radius, 10) * 1000;
                location_title.update('<b>Circle:</b>');
            } else if (feature.is_box) {
                location_formfield.geom = 'box';
                // need to create a whole new bounds otherwise the transform
                // modifies the geometry's actual bounds and won't display
                var orig_bounds = feature.geometry.getBounds().toArray();
                var box_bounds = new OpenLayers.Bounds.fromArray(orig_bounds);
                box_bounds.transform(POST.locationMap.getProjectionObject(),
                    new OpenLayers.Projection('EPSG:4326'));
                var box_points = box_bounds.toArray(true);
                location_input.value = box_points[0].toPrecision(8) + ',' +
                    box_points[1].toPrecision(8) + ' ' + box_points[2].toPrecision(8) +
                    ',' + box_points[3].toPrecision(8);
                location_title.update('<b>Box:</b>');
            } else {
                if (point_result.length > 2) {
                    location_formfield.geom = 'polygon';
                    // close the polygon, as getVertices does not
                    point_result.push(point_result[0]);
                    location_input.value = point_result.join(' ');
                    location_title.update('<b>Polygon:</b>');
                }
            }
            // The approximate area of the polygon in square meters
            var p_measure = feature.geometry.getArea() / 1000000;
            measure_value = ['Area', p_measure.toFixed(2) + ' km2'];
        } else {
            console.error('Unknown geometry type');
            alert('Geometry type not supported.');
            return;
        }
        // display a temporary notification message to better inform the user when
        // the field value that will be used for location is saved/updated
        var msg = Ext.Msg.show({
            msg: notification_str,
            cls: 'locationNotificationMsg',
            closable: false,
            modal: false,
            minWidth: 180
        });
        msg.getDialog().alignTo('entry-location', 'tr?', [25, -25]);
        setTimeout(function () {
            Ext.Msg.hide();
        }, 2500);
    }
    // deselect whatever control was used to draw/save this feature to set
    // the map back up as originally viewed by user
    var feature_controls = POST.locationMap.getControlsBy('featureControl', true);
    for (var c = 0; c < feature_controls.length; c++) {
        feature_controls[c].deactivate();
    }
    // display a measurement popup at the first point of this geometry
    if (measure_value) {
        if (feature.popup) {
            // remove any existing so no duplicates
            feature.popup.destroy();
        }
        feature.popup = new GeoExt.Popup({
            title: measure_value[0],
            location: feature,
            width: 100,
            html: measure_value[1]
        });
        feature.popup.show();
    }
    console.debug(location_formfield.geom + ': ' + location_input.value);
};


/**
Generate the new Entry and its XML and Object from the entered values.

@return {String} - the new XML string
@return {Object} - the new Entry object
*/
POST.generate_entry_xml = function () {
    if (!POST.ATOM_ENTRY) {
        console.error('Atom Entry Missing');
        alert('Unable to load Atom Entry.');
    }
    // cloning ATOM_ENTRY each time required for cases where a user
    // goes back and add/removes values after doing a preview which
    // results in a modified ATOM_ENTRY being used to build the post XML
    var new_entry = MASAS.Common.clone_object(POST.ATOM_ENTRY);
    
    // start out with filling in required categories at set index positions
    var entry_status = Ext.getCmp('entry-status').getValue();
    new_entry.entry.category[0]['@term'] = entry_status;
    var tree_selection = Ext.getCmp('icon-select-tree').getSelectionModel().getSelectedNode();
    new_entry.entry.category[1]['@term'] = tree_selection.attributes.term;
    // optional categories, added to the top of the listing so they appear
    // first in the viewing tool
    var certainty_val = Ext.getCmp('entry-certainty').getValue();
    if (certainty_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Certainty',
            '@scheme': 'masas:category:certainty',
            '@term': certainty_val.getGroupValue()
        });
    }
    var severity_val = Ext.getCmp('entry-severity').getValue();
    if (severity_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Severity',
            '@scheme': 'masas:category:severity',
            '@term': severity_val.getGroupValue()
        });
    }
    var category_val = Ext.getCmp('entry-category').getValue();
    if (category_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Category',
            '@scheme': 'masas:category:category',
            '@term': category_val
        });
    }
    var colour_select = Ext.getCmp('entry-colour-combo').getValue();
    if (colour_select) {
        var colour_cat = {
            '@label': 'Colour',
            '@scheme': 'masas:category:colour',
            '@term': colour_select
        };
        var colour_label = Ext.getCmp('entry-colour-label').getValue();
        if (colour_label) {
            colour_cat['@label'] = colour_label;
        }
        var colour_context = Ext.getCmp('entry-colour-context').getValue();
        if (colour_context) {
            colour_cat['@mea:context'] = colour_context;
            // add mea namespace
            new_entry.entry['@xmlns:mea'] = 'masas:experimental:attribute';
        }
        new_entry.entry.category.splice(0, 0, colour_cat);
    }
    
    var en_title_div = new_entry.entry.title.div.div[0];
    en_title_div['#text'] = MASAS.Common.xml_encode(Ext.get('entry-en-title').getValue());
    var en_content_div = new_entry.entry.content.div.div[0];
    var en_content = Ext.get('entry-en-content').getValue();
    if (en_content) {
        en_content_div['#text'] = MASAS.Common.xml_encode(en_content);
    }
    if (Ext.getCmp('entry-add-french').collapsed) {
        // remove the french divs by converting from an array of two
        // objects to just one
        new_entry.entry.title.div.div = en_title_div;
        new_entry.entry.content.div.div = en_content_div;
    } else {
        var fr_title_div = new_entry.entry.title.div.div[1];
        fr_title_div['#text'] = MASAS.Common.xml_encode(Ext.get('entry-fr-title').getValue());
        var fr_content_div = new_entry.entry.content.div.div[1];
        var fr_content = Ext.get('entry-fr-content').getValue();
        if (fr_content) {
            fr_content_div['#text'] = MASAS.Common.xml_encode(fr_content);
        }
    }
    
    var links_list = [];
    POST.relatedLinkStore.each(function(rec) {
        links_list.push({
            '@href': MASAS.Common.xml_encode(rec.data.url),
            '@rel': 'related',
            '@title': MASAS.Common.xml_encode(rec.data.title),
            '@type': rec.data.type
        });
    });
    if (links_list.length === 1) {
        // add as an object for single link
        new_entry.entry.link = links_list[0];
    } else if (links_list.length > 1) {
        new_entry.entry.link = links_list;
    }
    
    // GeoRSS format is "lat lon lat lon"
    var location_formfield = Ext.getCmp('entry-location');
    new_entry.entry['georss:' + location_formfield.geom] =
        location_formfield.getValue().replace(/,/g, ' ');
    
    var effective_im = Ext.getCmp('entry-effective-immediate').getValue();
    var effective_dt = Ext.get('entry-effective-dt').getValue();
    var effective_tm = Ext.get('entry-effective-tm').getValue();
    // use a newly entered effective time
    if (!effective_im && effective_dt && effective_tm) {
        // using Ext parsing and formatting extensions added to normal Date    
        var effective_str = effective_dt + 'T' + effective_tm + ':00';
        var effective_date = Date.parseDate(effective_str, 'c');
        effective_date = MASAS.Common.adjust_time(effective_date,
            MASAS.Common.Local_UTC_Offset);
        new_entry.entry['met:effective'] = effective_date.format('Y-m-d\\TH:i:00\\Z');
        // add met namespace
        new_entry.entry['@xmlns:met'] = 'masas:experimental:time';
    }
    
    var expires_dt = Ext.get('entry-expires-dt').getValue();
    var expires_tm = Ext.get('entry-expires-tm').getValue();
    var expires_it = Ext.getCmp('entry-expires-interval').getValue();
    var expires_date = null;
    if (expires_dt && expires_tm) {
        // using Ext parsing and formatting extensions added to normal Date    
        var expires_str = expires_dt + 'T' + expires_tm + ':00';
        expires_date = Date.parseDate(expires_str, 'c');
    } else if (expires_it) {
        expires_date = new Date();
        // a time value in the future
        var expires_diff = parseInt(expires_it, 10);
        expires_date = MASAS.Common.adjust_time(expires_date, expires_diff);
    }
    if (expires_date) {
        expires_date = MASAS.Common.adjust_time(expires_date,
            MASAS.Common.Local_UTC_Offset);
        new_entry.entry['age:expires'] = expires_date.format('Y-m-d\\TH:i:00\\Z');
        // add age namespace
        new_entry.entry['@xmlns:age'] = 'http://purl.org/atompub/age/1.0';
    }
    
    var update_control_val = Ext.getCmp('entry-update-control').getValue().getGroupValue();
    if (update_control_val === 'group' || update_control_val === 'all') {
        if (update_control_val === 'group') {
            new_entry.entry['app:control'] = {'mec:update': POST.USER_GROUP};
        } else if (update_control_val === 'all') {
            new_entry.entry['app:control'] = {'mec:update': 'all'};
        }
        // add app and mec namespaces
        new_entry.entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        new_entry.entry['@xmlns:mec'] = 'masas:extension:control';
    }
    // draft entries also need to add the AtomPub elements
    if (entry_status === 'Draft') {
        if (new_entry.entry['app:control']) {
            new_entry.entry['app:control']['app:draft'] = 'yes';
        } else {
            new_entry.entry['app:control'] = {'app:draft': 'yes'};
            // add app namespaces
            new_entry.entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        }
    }
    
    var new_xml = xmlJsonClass.json2xml(new_entry, '');
    new_xml = MASAS.Common.format_xml(new_xml);
    new_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + new_xml;
    
    return [new_xml, new_entry];
};


/**
Show a preview window to review the new Entry both with a template for user
friendlier display and the XML.
*/
POST.preview_entry_xml = function () {
    // only one review window allowed
    if (POST.reviewWindow) {
        POST.reviewWindow.destroy();
    }
    
    var preview_xml_vals = POST.generate_entry_xml();
    var display_xml = preview_xml_vals[0];
    preview_xml_vals[1].entry.id = 'New Entry';
    // reformat effective and expires for local time presentation and removing
    // namespace prefixes
    if (preview_xml_vals[1].entry['met:effective']) {
        preview_xml_vals[1].entry.effective = MASAS.Common.local_time_adjust(
            preview_xml_vals[1].entry['met:effective']);
    }
    if (preview_xml_vals[1].entry['age:expires']) {
        preview_xml_vals[1].entry.expires = MASAS.Common.local_time_adjust(
            preview_xml_vals[1].entry['age:expires']);
    }
    
    try {
        // XTemplate from post-templates.js
        var display_html = MASAS.Template.EntryToHTML.apply(preview_xml_vals[1]);
    } catch (err) {
        console.error('Preview Entry Template Error: ' + err);
        alert('Entry Template Error.');
        return;
    }
    // preview before posting needs to add attachments separately, after posting
    // the XTemplate will show instead    
    if (POST.attachmentStore.getCount() > 0) {
        display_html += '<div class="TemplateDisplayBox"><h2>Attachments</h2>';
        POST.attachmentStore.each(function (rec) {
            display_html += '<p><b>' + rec.data.title + '</b> - ' + rec.data.filename + '</p>';
        });
        display_html += '</div>';
    }
    // preview confirms any attached kml layer
    var kml_attach = Ext.getCmp('entry-kml-notice').getValue();
    if (kml_attach.length > 0) {
        display_html += '<div class="TemplateDisplayBox"><h2>' + kml_attach +
            '</h2></div>';
    }
    // preview for update control also needs to be shown separately
    if (preview_xml_vals[1].entry['app:control']) {
        if (preview_xml_vals[1].entry['app:control']['mec:update']) {
            var update_control_val = Ext.getCmp('entry-update-control').getValue().getGroupValue();
            if (update_control_val === 'all') {
                display_html += '<div class="TemplateDisplayBox"><h2>Entry Updates</h2>';
                display_html += '<p><b>Allow All</b></p></div>';
            }
        }
    }
    display_xml = display_xml.replace(/</g, '&lt;');
    display_xml = display_xml.replace(/>/g, '&gt;');
    display_html += '<div class="reviewClose"><a href="#" onclick="' +
        'POST.reviewWindow.close(); return false;">Close Preview</a></div>' +
        '<div class="reviewXmlShow"><a href="#" onclick="' +
        'document.getElementById(\'review-xml-box\').style.display=\'block\'; return false;">' +
        'Show XML</a></div><div id="review-xml-box" style="display: none;">' +
        '<pre>' + display_xml + '</pre></div>';
    
    POST.reviewWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Entry Preview <a class="titleClose" href="#" ' +
            'onclick="POST.reviewWindow.close(); return false;">Close</a>',
        closable: true,
        width: 700,
        height: 500,
        autoScroll: true,
        bodyStyle: 'background-color: white;',
        layout: 'fit',
        html: display_html
    });
    POST.reviewWindow.show(this);
};


/**
Post this new Entry to the server.
*/
POST.post_new_entry = function () {
    var posting_html = '<h1>Posting Entry</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: the following is not working to prevent going back
    //Ext.getCmp('card-prev').setDisabled();
    var new_entry_xml_vals = POST.generate_entry_xml();
    console.debug('Posting New Entry');
    if (POST.USE_ATTACH_PROXY === true) {
        var do_post = new OpenLayers.Request.POST({
            url: POST.ATTACH_PROXY_URL +
                encodeURIComponent(OpenLayers.Util.urlAppend(POST.FEED_URL,
                    'secret=' + POST.USER_SECRET)),
            // proxy is ignored for same origin hosts, but we need it to always
            // force the use of the attach proxy, so modify url instead
            data: new_entry_xml_vals[0],
            callback: POST.Common.post_complete_result
        });
    } else {
        var do_post = new OpenLayers.Request.POST({
            url: POST.FEED_URL,
            params: {'secret': POST.USER_SECRET},
            proxy: POST.AJAX_PROXY_URL,
            headers: {'Content-Type': 'application/atom+xml'},
            data: new_entry_xml_vals[0],
            callback: POST.Common.post_complete_result
        });
    }
};
