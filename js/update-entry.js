/**
Update Entry
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires postTemplates.js
@requires postMap.js
@requires src/common.js
@requires src/validators.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('POST');
// blank objects to start
POST.selectStore = null;
POST.selectGridPanel = null;
POST.oldAtom = null;
POST.oldEntryStatus = null;
POST.remoteEntryWindow = null;
POST.attachmentWindow = null;
POST.attachmentStore = null;
POST.attachmentGridPanel = null;
POST.locationMapPanel = null;
POST.relatedLinkWindow = null;
POST.relatedLinkStore = null;
POST.relatedLinkCount = 0;
POST.relatedLinkGridPanel = null;
POST.reviewWindow = null;
POST.postingTimer = null;
POST.POST_RESULT = '';

// for progress bar indicator
POST.PROGRESS_AMOUNT = 0;
// complete is 1 so divide by number of pages
POST.PROGRESS_INCREMENT = 0.35;


Ext.onReady(function () {
    
    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();
    
    // Custom field validations
    Ext.form.VTypes.location = function (v) {
        var location_formfield = Ext.getCmp('entry-location');
        var v_result = [false, true];
        if (location_formfield.geom === 'point') {
            v_result = POST.Validator.check_point(v);
        } else if (location_formfield.geom === 'line') {
            v_result = POST.Validator.check_line(v);
        } else if (location_formfield.geom === 'polygon') {
            v_result = POST.Validator.check_polygon(v);
        } else if (location_formfield.geom === 'circle') {
            v_result = POST.Validator.check_circle(v);
        } else if (location_formfield.geom === 'box') {
            v_result = POST.Validator.check_box(v); 
        }
        if (v_result[1] === false) {
            alert('Please note that this Location is outside North America.');
        }
        
        return v_result[0];
    };
    Ext.form.VTypes.locationText = 'Must be a valid location';
    
    var vkeyboard_plugin = new Ext.ux.plugins.VirtualKeyboard();
    
    
    // setting up select map, layers, and controls
    POST.Map.initialize_select_map();
    
    // setting up the location map, layers, and controls
    POST.Map.initialize_location_map();
    POST.Map.initialize_drawing_controls(true);
    
    
    var headerBox = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Posting Tool</div>' +
            '<div id="headerDetailBlock">Version 0.2</div>'
    });
    
    var progressBar = new Ext.ProgressBar({
        id: 'progressBar',
        width: 150,
        style: 'margin-left: 25px',
        text: 'Progress'
    });
    
    var cardNav = function (direction) {
        var card_panel = Ext.getCmp('entry-wizard-panel').getLayout();
        var current_card = parseInt(card_panel.activeItem.id.split('card-')[1], 10);
        
        /* Card validation */
        // select an entry to update validation, comment out to bypass
        if (!POST.oldAtom) {
            alert('Select an Entry first.');
            return;
        }
        
        // validate this card's form items first, first card has no forms
        if (current_card !== 0) {
            var found_invalid_item = false;
            card_panel.activeItem.cascade(function (item) {
                if (item.isFormField) {
                    if (!item.validate()) {
                        found_invalid_item = true;
                    }
                }
            });
            // form based validation check, comment out to bypass
            if (found_invalid_item) {
                return;
            }
        }
        
        // validation for icon tree selection, effective, and expires on input card
        if (current_card === 1) {
            var tree_selection = Ext.getCmp('icon-select-tree').getSelectionModel().getSelectedNode();
            if (!tree_selection) {
                alert('An icon must be selected.');
                return;
            } else {
                if (!tree_selection.attributes.term) {
                    alert('An icon must be selected.');
                    return;
                }
            }
            var effective_dt = Ext.get('entry-effective-dt').getValue();
            var effective_tm = Ext.get('entry-effective-tm').getValue();
            if (effective_dt && !effective_tm) {
                alert('Effective time must be set.');
                return;
            } else if (!effective_dt && effective_tm) {
                alert('Effective date must be set.');
                return;
            }
            var expires_dt = Ext.get('entry-expires-dt').getValue();
            var expires_tm = Ext.get('entry-expires-tm').getValue();
            if (expires_dt && !expires_tm) {
                alert('Expires time must be set.');
                return;
            } else if (!expires_dt && expires_tm) {
                alert('Expires date must be set.');
                return;
            }
            //TODO: further validation of the values, such as effective must
            //      come before expires?
        }
        
        // change to the next or previous card
        var next_card = current_card + direction;
        card_panel.setActiveItem(next_card);
        // update the next or previous buttons depending on what card is
        // now active in the stack
        if (next_card === 0) {
            Ext.getCmp('card-prev').setDisabled(true);
        } else if (next_card === 1) {
            Ext.getCmp('card-prev').setDisabled(false);
        } else if (next_card === 2) {
            Ext.getCmp('card-next').setText('<span style="font-weight: bold;' +
                ' font-size: 130%; color: black;">Next</span>');
        } else if (next_card === 3) {
            Ext.getCmp('card-next').setText('<span style="font-weight: bold;' +
                ' font-size: 130%; color: red;">Advanced</span>');
            Ext.getCmp('card-next').setDisabled(false);
        } else if (next_card === 4) {
            Ext.getCmp('card-next').setDisabled(true);
        }
        // progress bar update
        if (direction === 1) {
            POST.PROGRESS_AMOUNT += POST.PROGRESS_INCREMENT;
        } else if (direction === -1) {
            POST.PROGRESS_AMOUNT -= POST.PROGRESS_INCREMENT;
        }
        progressBar.updateProgress(POST.PROGRESS_AMOUNT);
        
        /* Other card operations */
        // cleanup stray GeoExt popup windows which stick around between
        // panel changes
        if (current_card === 0) {
            POST.Common.layer_popup_cleanup(POST.selectLayers.select);
        } else if (current_card === 2) {
            POST.Common.layer_popup_cleanup(POST.locationLayers.location);
            if (POST.KML.settingsWindow) {
                POST.KML.settingsWindow.hide();
            }
        }
        if (next_card === 1) {
            // when loading a past entry and the moving to this card the
            // keyboard icons need to be re-align because they were in place
            // during the 1st render
            if (!Ext.getCmp('entry-add-french').collapsed) {
                Ext.getCmp('entry-fr-title').alignKeyboardIcon();
                Ext.getCmp('entry-fr-content').alignKeyboardIcon();
            }
        }
    };
    
    /* selectEntryCard */
    
    var historyCombo = new Ext.form.ComboBox({
        id: 'history-interval',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 150,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'period',
        // default is current or future effective
        value: 1,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'period'],
            data: [
                ['Current/Active', 0],
                ['Current and Future', 1],
                ['Future Effective', 2],
                ['Past 24 Hours', 24],
                ['Past 48 Hours', 48],
                ['Past Week', 168],
                ['Past Month', 720]
            ]
        }),
        listeners: {select: POST.load_user_entries}
    });

    POST.selectStore = new GeoExt.data.FeatureStore({
        layer: POST.selectLayers.select,
        fields: [
            {name: 'icon', type: 'string'},
            {name: 'colour', type: 'string'},
            {name: 'title', type: 'string'},
            {name: 'published', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'updated', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'effective', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'expires', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'point', type: 'string'},
            {name: 'content', type: 'string'},
            {name: 'links', type: 'auto'},
            {name: 'CAP', type: 'string'}
        ],
        sortInfo: {field: 'updated', direction: 'DESC'},
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.HTTP({
                url: '',
                format: new OpenLayers.Format.MASASFeed({
                    internalProjection: POST.selectMap.getProjectionObject(),
                    externalProjection: new OpenLayers.Projection('EPSG:4326')
                })
            })
        }),
        autoLoad: false,
        listeners: {
            load: function () {
                POST.selectControls.loading.minimizeControl();
                var do_filter = true;
                /* the initial page load doesn't render the history-entries-only
                   until after this store loading has already taken place,
                   so the initial loading default is to filter and when the
                   user unchecks this box later, it will exist and won't filter
                   allowing users to edit CAP entries directly if necessary */
                if (Ext.getCmp('history-entries-only')) {
                    if (!Ext.getCmp('history-entries-only').checked) {
                        do_filter = false;
                    }
                }
                if (do_filter) {
                    this.filterBy(function (record, id) {
                        //console.log(record);
                        if (record.get('CAP') === 'N') {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
                if (this.data.length > 0) {
                    this.suspendEvents();
                    this.each(function (rec) {
                        rec.set('published', MASAS.Common.adjust_time(rec.get('published'),
                            MASAS.Common.UTC_Local_Offset));
                        rec.set('updated', MASAS.Common.adjust_time(rec.get('updated'),
                            MASAS.Common.UTC_Local_Offset));
                        if (rec.get('effective')) {
                            rec.set('effective', MASAS.Common.adjust_time(rec.get('effective'),
                                MASAS.Common.UTC_Local_Offset));
                        }
                        rec.set('expires', MASAS.Common.adjust_time(rec.get('expires'),
                            MASAS.Common.UTC_Local_Offset));
                    }, this);
                    this.resumeEvents();
                    this.commitChanges();
                }
            },
            exception: function () {
                console.error('Error loading previous entries');
                alert('Unable to load previous Entries.');
            }
        }
    });
    
    // row expander
    var expander = new Ext.ux.grid.RowExpander({
        tpl: new Ext.Template('<p>{content}</p>')
    });
    
    // icons displayed in rows
    function renderEventIcon(value, metadata, record) {
        // apply a background colour with the icon
        if (record.data.colour && metadata) {
            metadata.css = record.data.colour.toLowerCase() + 'EntryBackground';
        }
        return '<img alt="Event" height="18" src="' + value + '">';
    }
    
    function renderCloneButton(value) {
        if (value.Atom) {
            return '<button type="button" style="color: blue;" onclick="POST.load_past_entry(\'' +
                value.Atom.href + '\', \'Clone\')">Clone</button>';
        }
    }
    
    function renderUpdateButton(value) {
        if (value.Atom) {
            return '<button type="button" style="color: blue;" onclick="POST.load_past_entry(\'' +
                value.Atom.href + '\', \'Update\')">Update</button>';
        }
    }
    
    function renderEndButton(value) {
        if (value.Atom) {
            return '<button type="button" style="color: red;" onclick="POST.load_past_entry(\'' +
                value.Atom.href + '\', \'End\')">End</button>';
        }
    }
    
    // expired entry check
    function renderEntryExpires(value, metadata, record) {
        if (!value) {
            value = record.data[this.renderIndex];
        }
        var expires_epoch = value.format('U') * 1000;
        var now_epoch = new Date().getTime();
        if (expires_epoch <= now_epoch) {
            metadata.css = "EntryExpiredTime";
        }
        return value.format('M j H:i:s');
    }
    
    var select_grid_columns = [expander, {
        header: 'Icon',
        width: 35,
        renderer: renderEventIcon,
        sortable: true,
        dataIndex: 'icon'
    }, {
        header: 'Title',
        sortable: true,
        dataIndex: 'title'
    }, {
        header: 'Published',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        hidden: true,
        dataIndex: 'published'
    }, {
        header: 'Updated',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        dataIndex: 'updated'
    }, {
        header: 'Effective',
        id: 'select-column-effective',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        hidden: true,
        dataIndex: 'effective'
    }, {
        header: 'Expires',
        renderer: renderEntryExpires,
        width: 110,
        sortable: true,
        dataIndex: 'expires',
        renderIndex: 'expires'
    }];
    if (POST.UPDATE_OPERATION === 'Clone') {
        select_grid_columns.push({
            width: 100,
            renderer: renderCloneButton,
            sortable: false,
            dataIndex: 'links'
        });
    } else {
        select_grid_columns.push({
            width: 100,
            renderer: renderUpdateButton,
            sortable: false,
            dataIndex: 'links'
        });
        select_grid_columns.push({
            width: 75,
            renderer: renderEndButton,
            sortable: false,
            dataIndex: 'links'
        });
    }
    
    var selectGridModelOptions = {};
    // update the selectFeature control used by the grid selection model
    // to support touch devices because the normal mouse based hover control
    // won't work
    if (POST.TOUCH_ENABLE) {
        selectGridModelOptions.config = {
            controlConfig: {
                eventListeners: {
                    // use feature 'highlight' events to prevent conflict
                    // with selection model's 'select' events
                    featurehighlighted: POST.Common.show_select_popup,
                    featureunhighlighted: POST.Common.close_select_popup
                }
            }
        };
    }
    
    // create grid panel configured with feature store
    POST.selectGridPanel = new Ext.grid.GridPanel({
        title: 'Previous Entries',
        anchor: '100% 35%',
        collapsible: false,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        store: POST.selectStore,
        columns: select_grid_columns,
        autoExpandColumn: '2',
        plugins: expander,
        sm: new GeoExt.grid.FeatureSelectionModel(selectGridModelOptions)
    });
    
    // create map panel
    var selectMapPanel = new GeoExt.MapPanel({
        style: 'margin: 10px 0px;',
        // 40% room left at the top for historycombo and bottom for selectgrid
	anchor: '100% 60%',
        collapsible: false,
        map: POST.selectMap
    });
    
    var selectEntryCard = new Ext.FormPanel({
        id: 'card-0',
        labelWidth: 100,
        // this layout type allows for % based sizing
	layout: 'anchor',
        items: [{
            xtype: 'compositefield',
            height: 30,
            fieldLabel: '<b>Your Postings</b>',
            items: [ historyCombo,
                {
                    boxLabel: 'Only in Map View',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'map-view-filter',
                    id: 'map-view-filter',
                    checked: false,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }, {
                    boxLabel: 'Entries Only',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'history-entries-only',
                    id: 'history-entries-only',
                    checked: true,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }, {
                    boxLabel: 'Show Mine',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'history-entries-allowed',
                    id: 'history-entries-allowed',
                    checked: false,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }, {
                    xtype: 'button',
                    text: '<b>+</b>',
                    width: 30,
                    tooltip: 'Load an Entry manually',
                    style: 'margin-left: 300px;',
                    handler: POST.load_remote_entry,
                    hidden: (POST.UPDATE_OPERATION === 'Clone') ? false : true
                }
            ]
        }, selectMapPanel, POST.selectGridPanel ]
    });
    
    /* inputEntryCard */

    var iconTree = new Ext.tree.TreePanel({
        id: 'icon-select-tree',
        autoScroll: true,
        animate: true,
        border: true,
        height: 175,
        width: 400,
        loader: new Ext.tree.TreeLoader({
            dataUrl: POST.ICON_LIST_URL,
            requestMethod: 'GET',
            preloadChildren: true,
            // fix a preloadChildren bug, was to be fixed in ExtJS 3.3.2 but
            // doesn't appear to be as of 3.4.0, old ticket URL not longer working
            // http://code.extjs.com:8080/ext/ticket/1430
            load : function (node, callback, scope) {
                if (this.clearOnLoad) {
                    while (node.firstChild) {
                        node.removeChild(node.firstChild);
                    }
                }
                if (this.doPreload(node)) { // preloaded json children
                    this.runCallback(callback, scope || node, [node]);
                } else if (this.directFn || this.dataUrl || this.url) {   
                    // MB
                    if (this.preloadChildren) {
                        if (typeof(callback) !== 'function') {
                            callback = Ext.emptyFn;
                        }
                        callback = callback.createInterceptor(function (node) {
                            for (var i = 0; i < node.childNodes.length; i++) {
                                this.doPreload(node.childNodes[i]);
                            }
                        }, this);
                    }
                    // end-MB
                    this.requestData(node, callback, scope || node);
                }
            }
        }),
        root: {
            nodeType: 'async',
            text: 'Root Node'
        },
        rootVisible: false,
        listeners: {
            render: function () {
                this.getRootNode().expand();
            },
            click: function (node) {
                if (node.attributes.term !== null) {
                    var icon_url = POST.ICON_PREVIEW_URL + node.attributes.term +
                        '/large.png';
                    Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                        icon_url + '" alt="Icon"></div>');
                }
            }
        },
        tbar: ['Search:', {
                xtype: 'trigger',
                width: 100,
                triggerClass: 'x-form-clear-trigger',
                onTriggerClick: function () {
                    this.setValue('');
                    iconTree.filter.clear();
                },
                id: 'filter',
                enableKeyEvents: true,
                listeners: {
                    keyup: {buffer: 150, fn: function (field, e) {
                        if (Ext.EventObject.ESC === e.getKey()) {
                            field.onTriggerClick();
                        } else {
                            var val = this.getRawValue();
                            var re = new RegExp('.*' + val + '.*', 'i');
                            iconTree.filter.clear();
                            iconTree.filter.filter(re, 'text');
                        }
                    }}
                }
        }, '->', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeExpand',
                text: 'Expand',
                tooltip: 'Expand the entire icon tree',
                handler: function () {
                    iconTree.expandAll();
                }
        }), '-', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeCollapse',
                text: 'Collapse',
                tooltip: 'Collapse the entire icon tree',
                handler: function () {
                    iconTree.collapseAll();
                }
        })
        ]
    });
    iconTree.filter = new Ext.ux.tree.TreeFilterX(iconTree);
    
    var categoryCombo = new Ext.form.ComboBox({
        fieldLabel: 'Category',
        name: 'entry-category',
        id: 'entry-category',
        allowBlank: true,
        editable: false,
        width: 200,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'e_name',
        valueField: 'value',
        tpl: '<tpl for="."><div ext:qtip="{e_tip}" class="x-combo-list-item">{e_name}&nbsp;</div></tpl>',
        store: new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({url: POST.CATEGORY_LIST_URL}),
            reader: new Ext.data.JsonReader({
                root: 'categories',
                fields: ['e_name', 'e_tip', 'f_name', 'f_tip', 'value']
            }),
            autoLoad: true,
            listeners: {
                exception: function () {
                    console.error('Category Data failed to load');
                    alert('Unable to load Category Data.');
                }
            }
        })
    });
    
    var intervalCombo = new Ext.form.ComboBox({
        name: 'entry-expires-interval',
        id: 'entry-expires-interval',
        allowBlank: true,
        editable: false,
        width: 125,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
        // used to determine conflict between DateTime and Interval
        dateTimeConflict: false,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['interval', 'name', 'tip'],
            data: [ [null, '', 'Do not set expires'],
                [1, '1 Hour', 'Expires in 1 hour'],
                [6, '6 Hours', 'Expires in 6 hours'],
                [12, '12 Hours', 'Expires in 12 hours'],
                [24, '24 Hours', 'Expires in 1 day'],
                [48, '48 Hours', 'Expires in 2 days'],
                // only an Update Entry can have this value since it expires in the
                // past and is acting to End the Entry
                [99, 'Right Now', 'Expire immediately, Ends the Entry'] ]
        }),
        listeners: {
            select: function (combo, record, index) {
                if (record.data.interval) {
                    if (combo.dateTimeConflict) {
                        // make sure that both datetime and interval aren't
                        // used, otherwise they'd conflict
                        var dt_check = Ext.get('entry-expires-dt').getValue();
                        if (dt_check) {
                            alert('Choose either a specified time OR an interval, not both.');
                        }
                    } else {
                        // upon first load of this Entry, allow the user to
                        // quickly update the expires interval, but any further
                        // updates will result in a datetime conflict check
                        Ext.getCmp('entry-expires-dt').setValue('');
                        Ext.getCmp('entry-expires-tm').setValue('');
                        combo.dateTimeConflict = true;
                    }
                }
                // any change removes highlighting of initial default
                combo.removeClass('expiresIntervalDefault');
            },
            afterrender: function (combo) {
                if (POST.DEFAULT_EXPIRES_INTERVAL) {
                    // if a default expires interval is provided, modify the
                    // display name, select the default, and highlight it to
                    // make clear to the user it was defaulted.
                    var d_idx = combo.getStore().find('interval',
                        POST.DEFAULT_EXPIRES_INTERVAL);
                    if (d_idx !== -1) {
                        var d_rec = combo.getStore().getAt(d_idx);
                        var d_val = d_rec.get('name');
                        if (d_val.search('-Default') === -1) {
                            d_rec.set('name', d_val + '-Default');
                            d_rec.commit();
                        }
                        combo.setValue(d_rec.get('interval'));
                        combo.addClass('expiresIntervalDefault');
                    }
                }
            }
        }
    });
    
    POST.attachmentStore = new Ext.data.ArrayStore({
        fields: ['title', 'filename', 'num'],
        data: []
    });
    
    function renderRemoveAttachmentButton(val) {
        return '<a style="color: red; font-weight: bold;" onclick="POST.remove_attachment(\'' + val +
            '\')">Remove</a>';
    }

    POST.attachmentGridPanel = new Ext.grid.GridPanel({
        fieldLabel: 'Attachment',
        height: 90,
        width: 600,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        hideHeaders: true,
        store: POST.attachmentStore,
        columns: [{
            dataIndex: 'title'
        }, {
            dataIndex: 'filename',
            width: 200
        }, {
            dataIndex: 'num',
            width: 70,
            renderer: renderRemoveAttachmentButton
        }],
        autoExpandColumn: '0',
        buttonAlign: 'left',
        buttons: [new Ext.Button({
            id: 'attachment-add',
            width: 75,
            text: 'Add',
            tooltip: 'Attachments get uploaded and attached to this Entry',
            handler: POST.add_attachment
        }) ]
    });
    
    var inputEntryCard = new Ext.FormPanel({
        id: 'card-1',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        defaultType: 'textfield',
        items: [{
            xtype: 'compositefield',
            fieldLabel: '<b>Icon</b>',
            //width: 300,
            //defaults: { flex: 1 },
            items: [ iconTree,
            {
                xtype: 'displayfield',
                id: 'icon-preview-box',
                html: '<div style="padding: 15px;"></div>'
            }]
        }, {
            fieldLabel: '<b>Title</b>',
            name: 'entry-en-title',
            id: 'entry-en-title',
            width: '60%',
            minLength: 5,
            minLengthText: 'A good title should say what and where',
            maxLength: 250,
            maxLengthText: 'Titles cannot be longer than 250 characters',
            blankText: 'MASAS requires a title value',
            allowBlank: false
        }, {
            xtype: 'textarea',
            fieldLabel: 'Content',
            name: 'entry-en-content',
            id: 'entry-en-content',
            height: 80,
            width: '75%',
            allowBlank: true
        }, {
            xtype: 'fieldset',
            checkboxToggle: true,
            collapsed: true,
            forceLayout: true,
            title: 'French',
            id: 'entry-add-french',
            defaultType: 'textfield',
            items: [{
                fieldLabel: '<b>Title</b>',
                name: 'entry-fr-title',
                id: 'entry-fr-title',
                width: '60%',
                minLengthText: 'A good title should say what and where',
                maxLength: 250,
                maxLengthText: 'Titles cannot be longer than 250 characters',
                blankText: 'MASAS requires a title value',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Content<',
                name: 'entry-fr-content',
                id: 'entry-fr-content',
                height: 80,
                width: '75%',
                allowBlank: true,
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }],
            listeners: {
                expand: function () {
                    // reposition the icon because it was hidden on render
                    Ext.getCmp('entry-fr-title').alignKeyboardIcon();
                    Ext.getCmp('entry-fr-content').alignKeyboardIcon();
                    // enable validation of french values
                    Ext.getCmp('entry-fr-title').allowBlank = false;
                    Ext.getCmp('entry-fr-title').minLength = 5;
                },
                collapse: function () {
                    // disable validation of french values when not using
                    Ext.getCmp('entry-fr-title').allowBlank = true;
                    Ext.getCmp('entry-fr-title').minLength = null;
                }
            }
        }, categoryCombo, {
            xtype: 'radiogroup',
            fieldLabel: 'Severity',
            id: 'entry-severity',
            allowBlank: true,
            //width: 500,
            anchor: '60%',
            items: [
                {boxLabel: 'Unknown', name: 'entry-severity', inputValue: 'Unknown'},
                {boxLabel: 'Minor', name: 'entry-severity', inputValue: 'Minor'},
                {boxLabel: 'Moderate', name: 'entry-severity', inputValue: 'Moderate'},
                {boxLabel: 'Severe', name: 'entry-severity', inputValue: 'Severe'},
                {boxLabel: 'Extreme', name: 'entry-severity', inputValue: 'Extreme'}
            ]
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Effective',
            //TODO: setting a width ensured the validation error icon would
            //      appear beside the fields instead of the far right, but in
            //      this case, the width resulted in hiding the fields entirely
            //width: 325,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Immediately',
                name: 'entry-effective-immediate',
                id: 'entry-effective-immediate',
                // setting a height so the bottom part of the date/time fields
                // won't be cut off
                height: 23,
                checked: true,
                handler: function (checkbox, checked) {
                    // using disabled here instead of hide because for updates
                    // the fields are rendered when the first card is in view
                    // and so the fields overlap and can't use the render
                    // listener like in new-entry
                    if (checked) {
                        Ext.getCmp('entry-effective-dt').disable();
                        Ext.getCmp('entry-effective-dt').reset();
                        Ext.getCmp('entry-effective-tm').disable();
                        Ext.getCmp('entry-effective-tm').reset();
                        // restore expires intervals
                        Ext.getCmp('entry-expires-interval').enable();
                    } else {
                        Ext.getCmp('entry-effective-dt').enable();
                        Ext.getCmp('entry-effective-tm').enable();
                        // prevent confusion with effective versus expires
                        Ext.getCmp('entry-expires-interval').reset();
                        Ext.getCmp('entry-expires-interval').disable();
                    }
                }
            }, {
                xtype: 'displayfield',
                id: 'entry-effective-at',
                html: ' at:',
                style: {'padding': '3px 2px 2px 20px'}
            }, {
                xtype: 'datefield',
                name: 'entry-effective-dt',
                id: 'entry-effective-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                disabled: true
            }, {
                xtype: 'timefield',
                name: 'entry-effective-tm',
                id: 'entry-effective-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                disabled: true,
                validator: function (val) {
                    var effective_dt = Ext.get('entry-effective-dt').getValue();
                    if (!effective_dt || effective_dt.length === 0) {
                        effective_dt = null;
                    }
                    return POST.Validator.check_future_time(effective_dt, val);
                }
            }]
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Expires at',
            //width: 425,
            items: [{
                xtype: 'datefield',
                name: 'entry-expires-dt',
                id: 'entry-expires-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'timefield',
                name: 'entry-expires-tm',
                id: 'entry-expires-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var expires_dt = Ext.get('entry-expires-dt').getValue();
                    if (!expires_dt || expires_dt.length === 0) {
                        expires_dt = null;
                    }
                    return POST.Validator.check_future_time(expires_dt, val);
                },
                listeners: {select: function () {
                    var it_check = Ext.getCmp('entry-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'displayfield',
                html: 'or Expires in:',
                style: {'padding': '3px 2px 2px 20px'}
            }, intervalCombo ]
        }, POST.attachmentGridPanel ],
        listeners: {
            show: function () {
                // Renders the grid again, to properly show the vertical
                // scrollbar and autoexpand, since the size calculations are
                // different because the Panel wasn't visible to start
                POST.attachmentGridPanel.syncSize();
            }
        }
    });
    
    /* locationEntryCard */
    
    // setup the map Layers and Tools toolbars
    POST.MapLayers.initialize();
    POST.MapTools.initialize(true);
    
    // create map panel
    POST.locationMapPanel = new GeoExt.MapPanel({
        id: 'locationMapPanel',
        collapsible: false,
        style: 'margin-bottom: 10px;',
        // 10% room left at the bottom for Location text field
        anchor: '100% 90%',
        map: POST.locationMap,
        tbar: POST.mapToolbarTop,
        bbar: POST.mapToolbarBottom
    });

    var locationEntryCard = new Ext.FormPanel({
        id: 'card-2',
        labelWidth: 60,
        // this layout type allows for % based sizing
        layout: 'anchor',
        items: [POST.locationMapPanel, {
            xtype: 'compositefield',
            items: [{
                // form field titles don't seem to display when using the
                // anchor layout so create one instead
                xtype: 'displayfield',
                id: 'entry-location-title',
                width: 75,
                html: '<b>Location:</b> '
            }, {
                xtype: 'textfield',
                name: 'entry-location',
                id: 'entry-location',
                width: 400,
                vtype: 'location',
                validationEvent: false,
                blankText: 'A location is required',
                allowBlank: false
            }, {
                xtype: 'displayfield',
                id: 'entry-kml-notice',
                style: 'margin-left: 50px;',
                html: ''
            }]
        }]
    });
    
    /* postEntryCard */
    
    var postEntryCard = new Ext.Panel({
        id: 'card-3',
        items: [{
            border: false,
            bodyStyle: 'margin-bottom: 50px',
            html: '<div class="postingCard" id="postingBox"><h1>Ready to Post</h1>' + 
                '<a href="#" onclick="POST.preview_entry_xml(); return false;" ' +
                'style="color: grey;">Preview</a><a href="#" onclick="POST.' + 
                (('Update' === POST.UPDATE_OPERATION) ? 'post_entry_update' : 'post_new_entry') +
                '(); return false;" style="color: green;">Post Entry</a></div>'
        }]
    });
    
    if (POST.EMAIL_ADDRESS_LIST) {
        var emailToCombo = new Ext.form.ComboBox({
            fieldLabel: '<b>To</b>',
            name: 'email-to-address',
            id: 'email-to-address',
            width: 300,
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            store: POST.EMAIL_ADDRESS_LIST
        });
    
        postEntryCard.add({
            xtype: 'fieldset',
            id: 'email-to-fieldset',
            hidden: true,
            collapsible: true,
            collapsed: true,
            title: 'Email Forwarding',
            labelWidth: 70,
            listeners: {expand: POST.Common.generate_email_content },
            items: [ emailToCombo, {
                xtype: 'textfield',
                fieldLabel: '<b>Subject</b>',
                name: 'email-to-subject',
                id: 'email-to-subject',
                width: 400,
                minLength: 5,
                minLengthText: 'A subject is required',
                maxLength: 250,
                maxLengthText: 'Subject cannot be longer than 250 characters'
            }, {
                xtype: 'textarea',
                fieldLabel: '<b>Message</b>',
                name: 'email-to-message',
                id: 'email-to-message',
                height: 150,
                width: 800,
                minLength: 5,
                minLengthText: 'A message is required'
            }, {
                xtype: 'displayfield',
                id: 'email-attachment-notice',
                hidden: true,
                html: '<div><span style="color: red; font-weight: bold;">Note:' +
                    '</span> Attachments are not included with this message. ' +
                    'Add the attachments to the forwarded email you receive.</div>'
            }, {
                xtype: 'button',
                fieldLabel: ' ',
                labelSeparator: '',
                text: 'Send',
                width: 75,
                handler: POST.Common.post_email_message
            }]
        });
    }
    
    /* optionalEntryCard */
    
    var statusCombo = new Ext.form.ComboBox({
        fieldLabel: '<b>Status</b>',
        name: 'entry-status',
        id: 'entry-status',
        allowBlank: false,
        editable: false,
        width: 100,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'name',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'tip'],
            // used by MASAS-X to set different values depending on Mode
            data: (POST.CUSTOM_STATUS_VALUES) ? POST.CUSTOM_STATUS_VALUES :
                [ ['Actual', 'Actual Entries'],
                ['Exercise', 'Exercises and other practice entries'],
                ['Test', 'Test Entries'],
                ['Draft', 'Entries which cannot be seen by other users'] ]
        }),
        listeners: {
            afterrender: function (combo) {
                // need to set an initial selection this way because you don't
                // know what the first value might be up front when using
                // custom values, 
                var first_val = combo.getStore().getAt(0);
                combo.setValue(first_val.get('name'));
            },
            beforeselect: function (combo, record, index) {
                if (record.data.name === POST.oldEntryStatus) {
                    // values can stay the same
                    return true;
                } else if (POST.oldEntryStatus === 'Draft') {
                    // or change as long as they were a Draft
                    return true;
                } else {
                    // but otherwise they cannot change, such as back to a
                    // Draft, or from an Actual to a Test
                    alert('Status cannot be changed from ' + POST.oldEntryStatus);
                    return false;
                }
            }
        }
    });
    
    var colourStoreData = [
        ['', null],
        ['Gray', 'Not Available'],
        ['Blue', 'Information'],
        ['Green', 'Safety'],
        ['Yellow', 'Caution'],
        ['Red', 'Danger'],
        ['Purple', 'Fatal Danger'],
        ['Black', 'Fatal Danger']
    ];
    
    var colourCombo = new Ext.form.ComboBox({
        name: 'entry-colour-combo',
        id: 'entry-colour-combo',
        allowBlank: true,
        editable: false,
        width: 75,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'colour',
        valueField: 'colour',
        colourLabel: 'Colour Code',
        defaultColourLabel: 'Colour Code',
        tpl: '<tpl for="."><div ext:qtip="{context}" class="x-combo-list-item">{colour}&nbsp;</div></tpl>',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['colour', 'context'],
            data: colourStoreData
        }),
        defaultStore: colourStoreData,
        listeners: {
            select: function (combo, record, index) {
                var location_layer = POST.locationLayers.location;
                if (record.data.colour) {
                    Ext.getCmp('entry-colour-label').enable();
                    Ext.getCmp('entry-colour-label').setValue(combo.colourLabel);
                    Ext.getCmp('entry-colour-context').enable();
                    Ext.getCmp('entry-colour-context').setValue(record.data.context);
                    // set colour for location feature drawing
                    location_layer.defaultColour = record.data.colour.toLowerCase();
                } else {
                    Ext.getCmp('entry-colour-label').reset();
                    Ext.getCmp('entry-colour-label').disable();
                    Ext.getCmp('entry-colour-context').reset();
                    Ext.getCmp('entry-colour-context').disable();
                    location_layer.defaultColour = null;
                }
            }
        }
    });
    
    POST.relatedLinkStore = new Ext.data.ArrayStore({
        fields: ['title', 'url', 'type', 'num'],
        data: []
    });
    
    function renderRemoveLinkButton(val) {
        return '<a style="color: red; font-weight: bold;" onclick="POST.remove_related_link(' +
            val + ')">Remove</a>';
    }

    POST.relatedLinkGridPanel = new Ext.grid.GridPanel({
        fieldLabel: 'Links',
        height: 60,
        //width: 600,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        hideHeaders: true,
        store: POST.relatedLinkStore,
        columns: [{
            dataIndex: 'title',
            width: 200
        }, {
            dataIndex: 'type',
            width: 150
        }, {
            dataIndex: 'url'
        }, {
            dataIndex: 'num',
            width: 70,
            renderer: renderRemoveLinkButton
        }],
        autoExpandColumn: '2',
        buttonAlign: 'left',
        buttons: [new Ext.Button({
            width: 75,
            text: 'Add',
            tooltip: 'Add related links to this Entry',
            handler: POST.add_related_link
        }) ]
    });
    
    var optionalEntryCard = new Ext.FormPanel({
        id: 'card-4',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        items: [{
            xtype: 'displayfield',
            hideLabel: true,
            html: '<div style="margin: 0px 0px 15px 0px; font-size: 140%; '+
                'font-weight: bold;">Advanced Options</div>'
        }, {
            xtype: 'fieldset',
            title: ' Entry Status ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'The status of an Entry can be different than the normal ' +
                    'value of Actual using other values such as Test, allowing ' +
                    'MASAS users to filter Entries accordingly.  Draft Entries ' +
                    'are a special case, allowing you to work on and save an ' +
                    'Entry to the Hub without other users being able to see it ' +
                    'until you set its status to be Actual.'
            }, statusCombo]
        }, {
            xtype: 'fieldset',
            title: ' Certainty ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'If the information in this Entry is an estimate, forecast, ' +
                    'or came from a 3rd party source you can use Certainty ' +
                    'to make that known to other users.'
            }, {
                xtype: 'radiogroup',
                fieldLabel: 'Certainty',
                id: 'entry-certainty',
                allowBlank: true,
                items: [
                    {boxLabel: 'Unknown', name: 'entry-certainty', inputValue: 'Unknown'},
                    {boxLabel: 'Unlikely', name: 'entry-certainty', inputValue: 'Unlikely'},
                    {boxLabel: 'Possible', name: 'entry-certainty', inputValue: 'Possible'},
                    {boxLabel: 'Likely', name: 'entry-certainty', inputValue: 'Likely'},
                    {boxLabel: 'Observed', name: 'entry-certainty', inputValue: 'Observed'}
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: ' Colour ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'A Colour and appropriate Context can be used for this Entry.'
            }, {
                xtype: 'compositefield',
                fieldLabel: 'Colour',
                //width: 400,
                items: [{
                    xtype: 'textfield',
                    id: 'entry-colour-label',
                    width: 100,
                    disabled: true
                }, colourCombo, {
                    xtype: 'displayfield',
                    html: 'Context:',
                    style: {'padding-left': '10px'}
                }, {
                    xtype: 'textfield',
                    id: 'entry-colour-context',
                    width: 250,
                    disabled: true
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: ' Related Links ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'Link(s) to information related to this Entry.'
            }, POST.relatedLinkGridPanel ]
        }, {
            xtype: 'fieldset',
            title: ' Update Permissions ',
            collapsible: true,
            titleCollapse: true,
            items: [{
                xtype: 'displayfield',
                html: 'The default is that only your account can update an ' +
                    'Entry that you create.  However you can allow other ' +
                    'accounts to update Entries if you want them to share any ' +
                    'information that they can contribute.  Once you or another ' +
                    'account updates your Entry though, they can change the ' +
                    'Update Permissions back to a single account only if necessary.'
            }, {
                xtype: 'radiogroup',
                id: 'entry-update-control',
                allowBlank: false,
                // assume that user would want any group account to edit
                value: (POST.USER_GROUP) ? 'group' : 'user',
                items: [{
                    boxLabel: 'Only My Account May Update',
                    name: 'entry-update-control',
                    inputValue: 'user'
                }, {
                    boxLabel: 'Any Account In My Group(s) May Update',
                    name: 'entry-update-control',
                    inputValue: 'group',
                    disabled: (POST.USER_GROUP) ? false : true
                }, {
                    boxLabel: 'Allow Any MASAS Account To Update',
                    name: 'entry-update-control',
                    inputValue: 'all'
                }]
            }]
        }],
        listeners: {
            show: function () {
                // Renders the grid again, to properly show the vertical
                // scrollbar and autoexpand, since the size calculations are
                // different because the Panel wasn't visible to start
                POST.relatedLinkGridPanel.syncSize();
            }
        }
    });
    
    /* entryPanel */
    
    // the top toolbar for the app
    var panel_tbar_items = [];
    // optional support for multiple hubs
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'},
            {name: 'readOnly', mapping: 'readOnly'}
        ]);
        var hubCombo = new Ext.form.ComboBox({
            allowBlank: false,
            editable: false,
            width: 125,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                afterrender: function (combo) {
                    var default_idx = combo.getStore().find('url', POST.FEED_URL);
                    if (default_idx === -1) {
                        console.error('Unable to find Hub selection default');
                        // first value in array instead
                        default_idx = 0; 
                    }
                    var default_val = combo.getStore().getAt(default_idx);
                    combo.setValue(default_val.get('title'));
                },
                select: function (combo, record) {
                    console.debug('Changing feed to ' + record.data.title);
                    if (record.data.url) {
                        POST.FEED_URL = record.data.url;
                    }
                    if (record.data.secret) {
                        POST.USER_SECRET = record.data.secret;
                    }
                    if (record.data.uri) {
                        POST.USER_URI = record.data.uri;
                    }
                    // reload existing entries only on the first card to allow
                    // the user to find postings by Hub
                    var card_panel = Ext.getCmp('entry-wizard-panel').getLayout();
                    var current_card = card_panel.activeItem.id.split('card-')[1];
                    if (current_card === '0') {
                        POST.load_user_entries();
                    }
                    if (record.data.readOnly) {
                        alert('This is a Read Only feed which can be used to' +
                            ' make selections, but prior to Posting you must' +
                            ' choose another Hub.');
                    } else {
                        // setup allowable types and sizes for this new feed but
                        // does not clear out old attachments already stored on
                        // the server. Runs async and may take some time
                        // to return because the Hub is being queried
                        Ext.getCmp('attachment-add').enable();
                        OpenLayers.Request.GET({
                            url: POST.SETUP_ATTACH_URL,
                            params: {
                                'feed': POST.FEED_URL,
                                'secret': POST.USER_SECRET,
                                '_dc': new Date().getTime()
                            },
                            failure: function () {
                                alert('Unable to setup attachment handling.');
                                console.error('Unable to setup attachments for new feed');
                                Ext.getCmp('attachment-add').disable();
                            }
                        });
                    }
                }
            }
        });
        panel_tbar_items = [
            {xtype: 'tbspacer', width: 15},
            {
                xtype: 'tbtext',
                style: {'fontWeight': 'bold', 'font-size': '13px'},
                text: 'Hub:'
            },
            {xtype: 'tbspacer', width: 25},
            hubCombo
        ];
    }
    // default items
    panel_tbar_items.push.apply(panel_tbar_items, [progressBar, '->', {
        text: '<b>Abort</b>',
        iconCls: 'abortButton',
        width: 75,
        handler: function () {
            window.close();
        }
    }]);
    
    var entryPanel = new Ext.Panel({
        id: 'entry-wizard-panel',
        title: POST.UPDATE_OPERATION + ' Entry',
        region: 'center',
        layout: 'card',
        activeItem: 0,
        bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        tbar: panel_tbar_items,
        bbar: new Ext.Toolbar({ items: [
            {
                id: 'card-prev',
                text: '<span style="font-weight: bold; font-size: 130%;">Back</span>',
                iconCls: 'previousButton',
                width: 100,
                handler: cardNav.createDelegate(this, [-1]),
                disabled: true
            }, { xtype: 'tbspacer', width: 150 },
            {
                id: 'card-next',
                text: '<span style="font-weight: bold; font-size: 130%;">Next</span>',
                iconCls: 'nextButton',
                iconAlign: 'right',
                width: 100,
                handler: cardNav.createDelegate(this, [1])
            }
        ], buttonAlign: 'center' }),
        // the panels (or "cards") within the layout
        items: [ selectEntryCard, inputEntryCard, locationEntryCard,
            postEntryCard, optionalEntryCard ]
    });
    
    
    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [headerBox, entryPanel]
    });
    
    
    POST.selectMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
        new OpenLayers.Projection('EPSG:4326'), POST.selectMap.getProjectionObject()), 4);
    
    POST.load_user_entries();
    
});


/**
Load previous Entries created by this user according to selection criteria so
that the user can select which Entry they want to modify.
*/
POST.load_user_entries = function () {
    POST.selectControls.loading.maximizeControl();
    var history_val = Ext.getCmp('history-interval').getValue();
    var map_val = Ext.getCmp('map-view-filter').getValue();
    var feed_url = OpenLayers.Util.urlAppend(POST.FEED_URL,
        'secret=' + POST.USER_SECRET + '&lang=en');
    var allowed_val = Ext.getCmp('history-entries-allowed').getValue();
    // by default all editable by this author or created by this author
    if (allowed_val) {
        feed_url += '&author=' + POST.USER_URI;
    } else {
        feed_url += '&update_allow=' + POST.USER_URI;
    }
    // default is 0 for entries that are still current and so no datetime
    // parameters are added to the query
    if (history_val !== 0) {
        var since_date = new Date();
        // convert local to UTC first
        since_date = MASAS.Common.adjust_time(since_date,
            MASAS.Common.Local_UTC_Offset);
        // current or effective time starts right now, all others are a time
        // value in the past
        if (history_val > 2) {
            var diff_hours = parseInt(history_val, 10);
            // using a negative number for the past
            since_date = MASAS.Common.adjust_time(since_date,
                -Math.abs(diff_hours));
        }
        // using Ext extensions to Date for formatting
        var since_string = since_date.format('Y-m-d\\TH:i:s\\Z');
        feed_url += '&dtsince=' + since_string;
        // default search is updated which works for time values in the past
        if (history_val === 1) {
            // change search value so that anything not yet expired is assumed
            // to be current and/or future effective
            feed_url += '&dtval=expires';
        } else if (history_val === 2) {
            // not yet effective, in the future
            feed_url += '&dtval=effective';
        }
    }
    var selectLayer = POST.selectLayers.select;
    if (map_val) {
        // current viewport only, creating new bounds because of the transform
        var current_bounds = POST.selectMap.getExtent().toArray();
        var current_view = new OpenLayers.Bounds.fromArray(current_bounds);
        current_view.transform(POST.selectMap.getProjectionObject(),
            new OpenLayers.Projection('EPSG:4326'));
        feed_url += '&bbox=' + current_view.toBBOX(4);
        if (selectLayer.events.listeners.featuresadded.length > 0) {
            // update feed again whenever the map view changes
            POST.selectMap.events.register('moveend', null, POST.load_user_entries);
            // since the zoomToExtent created in post-map as anonymous, remove all
            selectLayer.events.remove('featuresadded');
        }
    } else {
        if (selectLayer.events.listeners.featuresadded.length === 0) {
            // restore normal behaviour of bringing all entries into focus
            POST.selectMap.events.unregister('moveend', null, POST.load_user_entries);
            selectLayer.events.register('featuresadded', null, function () {
                POST.selectMap.zoomToExtent(selectLayer.getDataExtent());
            });
        }
    }
    // cache busting parameter
    feed_url += '&_dc=' + new Date().getTime();
    // use the proxy if available and this is an absolute URL versus
    // a local relative URL
    if (POST.AJAX_PROXY_URL && Ext.form.VTypes.url(POST.FEED_URL)) {
        feed_url = POST.AJAX_PROXY_URL + encodeURIComponent(feed_url);
    }
    console.debug('Loading User Entries');
    console.log(feed_url);
    POST.selectStore.proxy.protocol.options.url = feed_url;
    POST.selectStore.load();
    // showing the effective column in the select grid for both future and
    // recently created entries to provide more info when selecting whereas
    // "current" shouldn't need it unless the user manually enables it
    try {
        var select_columns = POST.selectGridPanel.getColumnModel();
        var effective_column = select_columns.getIndexById('select-column-effective');
        select_columns.setHidden(effective_column,
            (history_val === 0) ? true : false);
    } catch(err) {
        console.error('Show effective column error: ' + err);
    }
};


/**
Display the remote entry window, which will allow the loading of an Entry
via a provided URL instead.
*/
POST.load_remote_entry = function () {
    // only one window allowed
    if (POST.remoteEntryWindow) {
        POST.remoteEntryWindow.destroy();
    }
    if (POST.UPDATE_OPERATION === 'Update') {
        var display_help = 'Enter the URL of the Entry you would like to' +
        ' manually load and Update.  Ensure that you have permission to Update' +
        ' this Entry before proceeding.';
    } else {
        var display_help = 'Enter the URL of the Entry you would like to' +
        ' manually load and Clone.';
    }
    var remotePanel = new Ext.FormPanel({
        id: 'remote-entry-panel',
        frame: true,
        labelWidth: 50,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'displayfield',
            html: display_help
        }, {
            xtype: 'textfield',
            fieldLabel: '<b>URL</b>',
            name: 'remote-entry-url',
            id: 'remote-entry-url',
            width: 300,
            minLength: 5,
            minLengthText: 'An Entry URL is required',
            blankText: 'An Entry URL is required',
            allowBlank: false
        }],
        buttons: [{
            text: '<b>' + POST.UPDATE_OPERATION + '</b>',
            handler: function () {
                if (Ext.getCmp('remote-entry-panel').getForm().isValid()) {
                    POST.load_past_entry(Ext.getCmp('remote-entry-url').getValue(),
                        POST.UPDATE_OPERATION);
                }
            }
        }]
    });
    POST.remoteEntryWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Load Entry <a class="titleClose" href="#" ' +
            'onclick="POST.remoteEntryWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 140,
        layout: 'fit',
        items: [remotePanel]
    });
    POST.remoteEntryWindow.show(this);
};


/**
Loads an Entry selected by the user.

@param {String} - the link to this Atom Entry
@param {String} - the operation to perform on this Entry
*/
POST.load_past_entry = function (atom_link, op_type) {
    if (!atom_link) {
        console.error('Entry link missing');
        alert('Entry Link Unavailable.');
        return;
    }
    if (POST.remoteEntryWindow) {
        POST.remoteEntryWindow.destroy();
    }
    console.debug('Loading Past Entry: ' + atom_link);
    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();
    // sync loading these urls
    var atom_get = new OpenLayers.Request.GET({
        url: atom_link,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': POST.Common.check_url_secret(atom_link),
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: POST.AJAX_PROXY_URL
    });
    if (atom_get.status !== 200) {
        mask.hide();
        console.error('Load Past Entry Error: ' + atom_get.responseText);
        alert('Unable to load Atom Entry.');
        return;
    }
    var xml_doc = MASAS.Common.parse_xml(atom_get.responseText);
    if (!xml_doc) {
        mask.hide();
        console.error('Error parsing past entry XML');
        alert('Unable to parse Atom Entry.');
        return;
    }
    var atom_json = xmlJsonClass.xml2json(xml_doc, '  ');
    try {
        POST.oldAtom = JSON.parse(atom_json);
    } catch (err) {
        mask.hide();
        console.error('Past entry JSON parse error: ' + err);
        alert('Unable to parse Atom Entry.');
        POST.oldAtom = null;
        return;
    }
    var value_result = POST.load_past_values(xml_doc);
    if (!value_result) {
        mask.hide();
        alert('Unable to Load Entry.');
        return;
    }
    if (op_type === 'End') {
        Ext.getCmp('entry-expires-dt').setValue('');
        Ext.getCmp('entry-expires-tm').setValue('');
        Ext.getCmp('entry-expires-interval').setValue(99);
        Ext.getCmp('entry-expires-interval').addClass('expiresIntervalDefault');
        // simple update to the title, english only for now
        var orig_title = Ext.getCmp('entry-en-title').getValue();
        Ext.getCmp('entry-en-title').setValue(orig_title + ' - Ended');
    }
    // advance to the next card
    mask.hide();
    var next_button = Ext.getCmp('card-next');
    next_button.handler.call(next_button.scope, next_button, Ext.EventObject);
};


/**
Loads the Entry's values into the form fields for the user to modify.

@param {Object} - the Entry's XML doc for any XML specific parsing
*/
POST.load_past_values = function (xml_doc) {
    if (!POST.oldAtom) {
        console.error('oldAtom missing, unable to load past values');
        return false;
    }
    console.debug('Loading Past Values');
    console.log(POST.oldAtom);
    
    // reset all of the form fields in case user is changing the selected entry
    Ext.getCmp('card-1').getForm().reset();
    Ext.getCmp('icon-select-tree').collapseAll();
    Ext.getCmp('entry-add-french').collapse();
    Ext.getCmp('attachment-add').enable();
    Ext.getCmp('card-2').getForm().reset();
    POST.locationLayers.location.removeAllFeatures();
    Ext.getCmp('card-4').getForm().reset();
    var colour_combo = Ext.getCmp('entry-colour-combo');
    var colour_store = colour_combo.getStore();
    colour_combo.colourLabel = colour_combo.defaultColourLabel;
    colour_store.loadData(colour_combo.defaultStore);
    colour_combo.reset();
    Ext.getCmp('entry-colour-label').reset();
    Ext.getCmp('entry-colour-label').disable();
    Ext.getCmp('entry-colour-context').reset();
    Ext.getCmp('entry-colour-context').disable();
    POST.relatedLinkStore.removeAll();
    POST.relatedLinkStore.commitChanges();
    // Clear out any old attachments stored by the demonstration server
    // as well as those in the ExtJS store and setup allowable types and
    // sizes via a blocking non-async request because the store will need
    // to be ready for loading any carryover attachments first
    //TODO: changing hubs might mean the new hub doesn't support same list
    //      of attachments that will be loaded as part of carrying over this
    //      update and its values
    OpenLayers.Request.GET({
        url: POST.SETUP_ATTACH_URL,
        params: {
            'feed': POST.FEED_URL,
            'reset': 'yes',
            'secret': POST.USER_SECRET,
            '_dc': new Date().getTime()
        },
        async: false,
        failure: function () {
            alert('Unable to setup attachment handling.');
            console.error('Unable to purge and setup attachments');
            Ext.getCmp('attachment-add').disable();
        }
    });
    POST.attachmentStore.removeAll();
    POST.attachmentStore.commitChanges();
    // kml drawing reset
    POST.KML.settingsWindow = null;
    
    for (var i = 0; i < POST.oldAtom.entry.category.length; i++) {
        var cat_attrib = POST.oldAtom.entry.category[i];
        if (cat_attrib['@scheme'] === 'masas:category:status') {
            Ext.getCmp('entry-status').setValue(cat_attrib['@term']);
            // saving original Status for comparison
            POST.oldEntryStatus = cat_attrib['@term'];
        } else if (cat_attrib['@scheme'] === 'masas:category:category') {
            Ext.getCmp('entry-category').setValue(cat_attrib['@term']);
        } else if (cat_attrib['@scheme'] === 'masas:category:severity') {
            Ext.getCmp('entry-severity').setValue(cat_attrib['@term']);
        } else if (cat_attrib['@scheme'] === 'masas:category:certainty') {
            Ext.getCmp('entry-certainty').setValue(cat_attrib['@term']);
        } else if (cat_attrib['@scheme'] === 'masas:category:icon') {
            // need to seach multiple children (true) into the tree to find the
            // right icon match, then expand the tree to this node and select it
            var icon_tree = Ext.getCmp('icon-select-tree');
            // the preloadChildren patch in the tree loader is currently needed
            // for this findChild to work
            var node_match = icon_tree.root.findChild('term', cat_attrib['@term'], true);
            if (node_match) {
                icon_tree.expandPath(node_match.getPath(), null,
                    function (e_success, e_last) {
                        if (e_success) {
                            e_last.select();
                        }
                    }
                );
                var icon_url = POST.ICON_PREVIEW_URL + cat_attrib['@term'] +
                    '/large.png';
                Ext.getCmp('icon-preview-box').setValue('<div style="padding: 15px;"><img src="' +
                    icon_url + '"></div>');
            } else {
                console.error('No match for icon: ' + cat_attrib['@term']);
                alert('Unable to find an icon that matches original value.  Please select again.');
            }
        } else if (cat_attrib['@scheme'] === 'masas:category:colour') {
            colour_combo.setValue(cat_attrib['@term']);
            // colour for location feature
            var location_layer = POST.locationLayers.location;
            location_layer.defaultColour = cat_attrib['@term'].toLowerCase();
            // optional label, use to match with template if possible
            if (cat_attrib['@label']) {
                Ext.getCmp('entry-colour-label').setValue(cat_attrib['@label']);
                Ext.getCmp('entry-colour-label').enable();
                colour_combo.colourLabel = cat_attrib['@label'];
                // async request that attempts to find a template with this
                // label and corresponding colour/context values.  Makes it
                // easier for user to change from Yellow to Red, etc.
                OpenLayers.Request.GET({
                    url: POST.COLOUR_DATA_URL,
                    params: {
                        'label': cat_attrib['@label'],
                        '_dc': new Date().getTime()
                    },
                    success: function (xhr) {
                        try {
                            var colour_json = JSON.parse(xhr.responseText);
                        } catch (err) {
                            // defaults will be used instead
                            console.error('Colour data JSON parse error: ' + err);
                            return;
                        }
                        if (colour_json.data) {
                            var colour_store = Ext.getCmp('entry-colour-combo').getStore();
                            colour_store.loadData(colour_json.data);
                            console.debug('Colour data store updated');
                        } else {
                            console.debug('No colour label match');
                        }
                    },
                    failure: function () {
                        // defaults will be used instead
                        console.error('Colour data search error');
                    }
                });
            }
            // optional context
            if (cat_attrib['@mea:context']) {
                Ext.getCmp('entry-colour-context').setValue(cat_attrib['@mea:context']);
                Ext.getCmp('entry-colour-context').enable();
            }
        }
    }
    
    // title is language sensitive requiring english
    var found_en_title = false;
    var found_fr_title = false;
    if (POST.oldAtom.entry.title.div.div instanceof Array) {
        // multiple divs for each language
        for (var j = 0; j < POST.oldAtom.entry.title.div.div.length; j++) {
            if (POST.oldAtom.entry.title.div.div[j]['@xml:lang'] === 'en') {
                found_en_title = true;
                Ext.getCmp('entry-en-title').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.title.div.div[j]['#text']));
            } else if (POST.oldAtom.entry.title.div.div[j]['@xml:lang'] === 'fr') {
                found_fr_title = true;
                Ext.getCmp('entry-fr-title').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.title.div.div[j]['#text']));
            }
        }
    } else if (typeof(POST.oldAtom.entry.title.div.div) === "object") {
        if (POST.oldAtom.entry.title.div.div['@xml:lang'] === 'en') {
            found_en_title = true;
            Ext.getCmp('entry-en-title').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.title.div.div['#text']));
        }
    }
    // cannot handle french only entries right now
    if (!found_en_title) {
        console.error('French only entry found');
        return false;
    }
    if (found_fr_title) {
        Ext.getCmp('entry-add-french').expand();
    }
    // in contrast content will just be blank if no english present or if its not
    // xhtml but an image or other media-type that is a content placeholder
    if (POST.oldAtom.entry.content['@type'] === 'xhtml') {
        if (POST.oldAtom.entry.content.div.div instanceof Array) {
            // multiple divs for each language
            for (var k = 0; k < POST.oldAtom.entry.content.div.div.length; k++) {
                if (POST.oldAtom.entry.content.div.div[k]['@xml:lang'] === 'en') {
                    Ext.getCmp('entry-en-content').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.content.div.div[k]['#text']));
                } else if (POST.oldAtom.entry.content.div.div[k]['@xml:lang'] === 'fr') {
                    Ext.getCmp('entry-fr-content').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.content.div.div[k]['#text']));
                }
            }
        } else if (typeof(POST.oldAtom.entry.content.div.div) === "object") {
            if (POST.oldAtom.entry.content.div.div['@xml:lang'] === 'en') {
                Ext.getCmp('entry-en-content').setValue(MASAS.Common.xml_decode(POST.oldAtom.entry.content.div.div['#text']));
            }
        }
    }
    
    var now_epoch = new Date().getTime();
    if (POST.oldAtom.entry['met:effective']) {
        try {
            var effective_date = Date.parseDate(POST.oldAtom.entry['met:effective'],
                'Y-m-d\\TH:i:s\\Z');
            effective_date = MASAS.Common.adjust_time(effective_date,
                MASAS.Common.UTC_Local_Offset);
            // if the entry has already become effective, don't fill in the previous
            // date as any update should need a new effective time
            var effective_epoch = effective_date.format('U') * 1000;
            if (effective_epoch >= now_epoch) {
                Ext.getCmp('entry-effective-immediate').setValue(false);
                Ext.getCmp('entry-effective-dt').enable();
                Ext.getCmp('entry-effective-dt').setValue(effective_date);
                Ext.getCmp('entry-effective-tm').enable();
                Ext.getCmp('entry-effective-tm').setValue(effective_date);
            }
        } catch (err) {
            console.error('Effective Error: ' + err);
        }
    }
    if (POST.oldAtom.entry['age:expires']) {
        try {
            var expire_date = Date.parseDate(POST.oldAtom.entry['age:expires'],
                'Y-m-d\\TH:i:s\\Z');
            expire_date = MASAS.Common.adjust_time(expire_date,
                MASAS.Common.UTC_Local_Offset);
            // if the entry has already expired, don't fill in the previous date
            // as any update should need a new expires time
            var expire_epoch = expire_date.format('U') * 1000;
            if (expire_epoch >= now_epoch) {
                Ext.getCmp('entry-expires-dt').setValue(expire_date);
                //TODO: should this be highlighted?
                //Ext.getCmp('entry-expires-dt').addClass('expiresIntervalDefault');
                Ext.getCmp('entry-expires-tm').setValue(expire_date);
            } else {
                // setup a default interval if available
                Ext.getCmp('entry-expires-interval').fireEvent('afterrender',
                    Ext.getCmp('entry-expires-interval'));
            }
        } catch (err) {
            console.error('Expires Error: ' + err);
        }
    }
    
    // there should always be multiple links to check, 
    if (POST.oldAtom.entry.link instanceof Array) {
        for (var l = 0; l < POST.oldAtom.entry.link.length; l++) {
            var import_data = null;
            var new_rec = null;
            if (POST.oldAtom.entry.link[l]['@rel'] === 'related') {
                // required attributes
                import_data = {
                    url: POST.oldAtom.entry.link[l]['@href'],
                    num: POST.relatedLinkCount
                };
                // optional, filling in defaults if missing
                if (POST.oldAtom.entry.link[l]['@title']) {
                    import_data.title = POST.oldAtom.entry.link[l]['@title'];
                } else {
                    import_data.title = 'Link';
                }
                if (POST.oldAtom.entry.link[l]['@type']) {
                    import_data.type = POST.oldAtom.entry.link[l]['@type'];
                } else {
                    import_data.type = 'text/html';
                }
                new_rec = new POST.relatedLinkStore.recordType(import_data);
                POST.relatedLinkStore.add(new_rec);
                POST.relatedLinkStore.commitChanges();
                POST.relatedLinkCount += 1;
                console.debug('Imported link: ' + import_data.title);
            }
            if (POST.oldAtom.entry.link[l]['@rel'] === 'enclosure') {
                // required attributes
                import_data = {
                    type: POST.oldAtom.entry.link[l]['@type'],
                    // load content items, additional secret handling
                    url: OpenLayers.Util.urlAppend(POST.oldAtom.entry.link[l]['@href'],
                        'secret=' + POST.Common.check_url_secret(
                            POST.oldAtom.entry.link[l]['@href']) )
                };
                // optional, filling in defaults if missing
                if ('@title' in POST.oldAtom.entry.link[l]) {
                    import_data.title = POST.oldAtom.entry.link[l]['@title'];
                } else {
                    import_data.title = 'Attachment';
                }
                var do_import = new OpenLayers.Request.POST({
                    url: POST.IMPORT_ATTACH_URL,
                    data: OpenLayers.Util.getParameterString(import_data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    async: false
                });
                if (do_import.status === 201 || do_import.status === 200) {
                    var attach_num = parseInt(do_import.getResponseHeader('Attach-Num'), 10);
                    // special handling for "first" kml file found, assuming
                    // its the custom layer
                    if (import_data.type === 'application/vnd.google-earth.kml+xml' &&
                    !POST.KML.settingsWindow) {
                        console.debug('Importing KML layer features');
                        var kml_parser = new OpenLayers.Format.KML({
                            kmlns: 'http://www.opengis.net/kml/2.2',
                            kvpAttributes: true,
                            externalProjection: new OpenLayers.Projection('EPSG:4326'),
                            internalProjection: POST.locationMap.getProjectionObject()
                        });
                        POST.locationLayers.kml.addFeatures(kml_parser.read(do_import.responseText));
                        // show icons properly
                        for (var m = 0; m < POST.locationLayers.kml.features.length; m++) {
                            var kml_feature = POST.locationLayers.kml.features[m];
                            if (kml_feature.geometry &&
                            kml_feature.geometry instanceof OpenLayers.Geometry.Point) {
                                kml_feature.renderIntent = 'icon';
                            }
                        }
                        POST.locationLayers.kml.redraw();
                        // restore settings
                        POST.KML.settingsWindow = new POST.KML.SettingsWindow();
                        POST.KML.settingsWindow.form_panel.attachNum = attach_num;
                        POST.KML.settingsWindow.form_panel.attachTitle.setValue(import_data.title);
                        POST.KML.settingsWindow.form_panel.deleteBtn.enable();
                        POST.KML.settingsWindow.form_panel.saveBtn.disable();
                        // update the result display
                        Ext.getCmp('entry-kml-notice').setValue('<b>Attached Layer:</b>' +
                            '&nbsp;&nbsp;' + import_data.title);
                    } else {
                        new_rec = new POST.attachmentStore.recordType({
                            title: import_data.title,
                            filename: import_data.type,
                            num: attach_num
                        });
                        POST.attachmentStore.add(new_rec);
                        POST.attachmentStore.commitChanges();
                    }
                    POST.USE_ATTACH_PROXY = true;
                    console.debug('Imported attachment: ' + import_data.title);
                } else {
                    console.error('Error importing attachment: ' +
                        POST.oldAtom.entry.link[l]['@href']);
                    alert('Unable to import previous attachment.');
                }
            }
        }
    }
    
    var no_update_control = true;
    if (POST.oldAtom.entry['app:control']) {
        if (POST.oldAtom.entry['app:control']['mec:update']) {
            no_update_control = false;
            if (POST.oldAtom.entry['app:control']['mec:update'].search(/all/) !== -1) {
                Ext.getCmp('entry-update-control').setValue('all');
            } else {
                // otherwise assume that because its not "all" then until custom
                // update control capability is available in this tool, set an
                // existing default value
                if (POST.USER_GROUP) {
                    Ext.getCmp('entry-update-control').setValue('group');
                } else {
                    // this user doesn't have a group to use, so an update
                    // value can't be carried forward at all
                    Ext.getCmp('entry-update-control').setValue('user');
                }
            }
        }
    }
    if (no_update_control) {
        // if an update control value wasn't present in the original message
        // that means only the user can update, so carry this selection forward
        Ext.getCmp('entry-update-control').setValue('user');
    }
    
    //NOTE: assuming that the Hub has ensured only Draft Entries are using the
    //      Draft category and therefore no need to check AtomPub elements
    
    // create a feature to add to the map and input boxes by re-using
    // the MASASFeed parser and parsing the xml Document
    var georss_geometries = new OpenLayers.Format.MASASFeed().parseLocations(xml_doc);
    if (georss_geometries.length > 0) {
        var location_geometry = georss_geometries[0].transform(
            new OpenLayers.Projection('EPSG:4326'),
            POST.locationMap.getProjectionObject()
        );
        var location_feature = new OpenLayers.Feature.Vector(location_geometry);
        // special handling for circle and box needed as they use polygons
        if (POST.oldAtom.entry['georss:circle']) {
            // use the GeoRSS values to replace location_feature with what a
            // user entered/drawn circle should look like
            var circle_text = POST.oldAtom.entry['georss:circle'].replace(/,/g, ' ');
            var circle_vals = circle_text.split(' ');
            var center_geom = new OpenLayers.Geometry.Point(circle_vals[1],
                circle_vals[0]).transform(new OpenLayers.Projection('EPSG:4326'),
                    POST.locationMap.getProjectionObject());
            var center_feature = new OpenLayers.Feature.Vector(center_geom);
            // GeoRSS uses meters for radius which is the same as the map units
            var circle_polygon = OpenLayers.Geometry.Polygon.createRegularPolygon(
                center_geom, circle_vals[2], 40);
            var circle_feature = new OpenLayers.Feature.Vector(circle_polygon);
            // references between the polygon/circle and the point/center
            circle_feature.circle_center = center_feature;
            // convert to what a user entered radius normally looks like
            var radius_km = parseFloat(circle_vals[2], 10) / 1000;
            circle_feature.circle_radius = radius_km.toString();
            center_feature.child_polygon = circle_feature;
            // add the center point to the layer as well for editing
            POST.locationLayers.location.addFeatures([center_feature]);
            location_feature = circle_feature;
        }
        if (POST.oldAtom.entry['georss:box']) {
            location_feature.is_box = true;
        }
        POST.locationLayers.location.addFeatures([location_feature]);
        POST.save_location_feature(location_feature, true);
        // zoom to show the new feature, GeoExt will override and use the mapPanel
        // center,zoom,extent values when the panel is first opened, after that
        // the normal OpenLayers methods work for any subsequent loads
        if (POST.oldAtom.entry['georss:point']) {
            var feature_point = location_feature.geometry.getCentroid();
            var point_latlon = new OpenLayers.LonLat(feature_point.x, feature_point.y);
            POST.locationMapPanel.center = point_latlon;
            POST.locationMapPanel.zoom = 12;
            POST.locationMap.setCenter(point_latlon, 12);
        } else {
            var feature_extent = location_feature.layer.getDataExtent().toArray();
            var extent_bounds = new OpenLayers.Bounds.fromArray(feature_extent);
            POST.locationMapPanel.extent = extent_bounds;
            POST.locationMap.zoomToExtent(extent_bounds);
        }        
    } else {
        console.error('No location geometries found in past entry');
        alert('No Locations found for this Entry.');
    }
    
    return true;
};


/**
Display the add attachment window, which will allow the upload of the attachment
to the temporary location on the server.
*/
POST.add_attachment = function () {
    // only one attachment window allowed
    if (POST.attachmentWindow) {
        POST.attachmentWindow.destroy();
    }
    var filePanel = new Ext.FormPanel({
        fileUpload: true,
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
                xtype: 'textfield',
                fieldLabel: '<b>Title</b>',
                name: 'attachment-title',
                id: 'attachment-title',
                width: 200
            }, {
                xtype: 'fileuploadfield',
                fieldLabel: '<b>File</b>',
                name: 'attachment-file',
                id: 'attachment-file',
                emptyText: 'Select a file to upload',
                width: 350
        }],
        buttons: [{
            text: 'Upload',
            handler: function () {
                if (filePanel.getForm().isValid()) {
                    filePanel.getForm().submit({
                            url: POST.ADD_ATTACH_URL,
                            submitEmptyText: false,
                            waitMsg: 'Uploading the file...',
                            success: POST.add_attachment_success,
                            failure: function (form, response) {
                                console.error('Upload Attachment Error: ' +
                                    response.result.message);
                                Ext.Msg.alert('Upload Error',
                                    'Unable to upload the file.  ' + response.result.message);
                                POST.attachmentWindow.destroy();
                            }
                    });
                }
            }
        }]
    });
    POST.attachmentWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Add Attachment <a class="titleClose" href="#" ' +
            'onclick="POST.attachmentWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 140,
        layout: 'fit',
        items: [filePanel]
    });
    POST.attachmentWindow.show(this);
};


/**
Add attachment success handler which will also update the attachment store.

@param {Object} - the form used to submit the attachment
@param {Object} - the response from this form
*/
POST.add_attachment_success = function (form, response) {
    try {
        var new_rec = new POST.attachmentStore.recordType({
            title: response.form.getFieldValues()['attachment-title'],
            filename: response.form.getFieldValues()['attachment-file'],
            num: response.result.num
        });
        POST.attachmentStore.add(new_rec);
        POST.attachmentStore.commitChanges();
        POST.USE_ATTACH_PROXY = true;
        console.debug('Added attachment ' + new_rec.data.num);
        POST.attachmentWindow.destroy();
    } catch (err) {
        console.error('Add Attachment Error: ' + err);
        alert('Unable to add this attachment.');
        POST.attachmentWindow.destroy();
    }
};


/**
Removes an attachment from the temporary location on the server

@param {Integer} - the attachment number
*/
POST.remove_attachment = function (num) {
    var r_confirm = confirm('Are you sure you want to remove this attachment?');
    if (r_confirm === true) {
        Ext.Ajax.request({
            url: OpenLayers.Util.urlAppend(POST.REMOVE_ATTACH_URL,
                'num=' + num),
            method: 'DELETE',
            success: POST.remove_attachment_success(num),
            failure: function (response) {
                console.error('Delete Attachment Error: ' + response.responseText);
                alert('Unable to remove attachment.');
            }
        });
    }
};


/**
Remove attachment success handler, updates the attachment store.

@param {Integer} - the attachment number
*/
POST.remove_attachment_success = function (num) {
    var idx = POST.attachmentStore.find('num', num);
    if (idx === -1) {
        console.error('Remove Attachment ' + num + ' Error');
        alert('Unable to remove attachment.');
    } else {
        POST.attachmentStore.removeAt(idx);
        POST.attachmentStore.commitChanges();
        console.debug('Removed Attachment ' + num);
    }
};


/**
Display the add related link window, which enables the addition of related
links to this Entry.
*/
POST.add_related_link = function () {
    // only one link window allowed
    if (POST.relatedLinkWindow) {
        POST.relatedLinkWindow.destroy();
    }
    
    var typeCombo = new Ext.form.ComboBox({
        fieldLabel: '<b>Type</b>',
        name: 'related-link-type',
        id: 'related-link-type',
        allowBlank: false,
        editable: false,
        width: 200,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'value',
        value: 'text/html',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'value'],
            data: [
                ['Web Page', 'text/html'],
                ['Web Map Service', 'application/vnd.ogc.wms'],
                ['Tile Map Service', 'application/vnd.osgeo.tms']
            ]
        })
    });
    
    var linkPanel = new Ext.FormPanel({
        id: 'related-link-panel',
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'textfield',
            fieldLabel: '<b>Title</b>',
            name: 'related-link-title',
            id: 'related-link-title',
            width: 200,
            minLength: 3,
            minLengthText: 'A Title is required',
            blankText: 'A Title is required'
        }, typeCombo, {
            xtype: 'textfield',
            fieldLabel: '<b>URL</b>',
            name: 'related-link-url',
            id: 'related-link-url',
            width: 400,
            minLength: 5,
            minLengthText: 'A URL is required',
            blankText: 'A URL is required',
            vtype: 'url',
            validator: function (val) {
                // the ExtJS URL validation doesn't check for spaces
                if (val.search(/\s/) !== -1) {
                    return 'Blank spaces are not allowed in a URL';
                }
                return true;
            }
        }],
        buttons: [{
            text: 'Add',
            handler: function () {
                if (Ext.getCmp('related-link-panel').getForm().isValid()) {
                    var new_rec = new POST.relatedLinkStore.recordType({
                        title: Ext.getCmp('related-link-title').getValue(),
                        type: Ext.getCmp('related-link-type').getValue(),
                        url: Ext.getCmp('related-link-url').getValue(),
                        num: POST.relatedLinkCount
                    });
                    POST.relatedLinkStore.add(new_rec);
                    POST.relatedLinkStore.commitChanges();
                    console.debug('Added related link ' + POST.relatedLinkCount);
                    // used to provide a unique value for this link as its
                    // store index can change and users might submit identical
                    // links
                    POST.relatedLinkCount += 1;
                    POST.relatedLinkWindow.destroy(); 
                }
            }
        }]
    });
    POST.relatedLinkWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Add Related Link <a class="titleClose" href="#" ' +
            'onclick="POST.relatedLinkWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 160,
        layout: 'fit',
        items: [linkPanel]
    });
    POST.relatedLinkWindow.show(this);
};


/**
Remove a related link from the related links store.

@param {Integer} - the record index number
*/
POST.remove_related_link = function (num) {
    var idx = POST.relatedLinkStore.find('num', num);
    if (idx === -1) {
        console.error('Remove related link ' + num + ' Error');
        alert('Unable to remove link.');
    } else {
        POST.relatedLinkStore.removeAt(idx);
        POST.relatedLinkStore.commitChanges();
        console.debug('Removed related link ' + num);
    }
};


/**
Special handling to create a circle after a point feature is drawn.  Provides
a popup to enter the radius.

@param {Object} - the OpenLayers point feature for the center of the circle
@param {Boolean} - use a KML handler or regular Location
*/
POST.create_circle_feature = function (feature, use_kml) {
    var radius_field = new Ext.form.TextField({
        width: 50,
        blankText: 'A radius is required',
        allowBlank: false
    });
    if (feature.child_polygon) {
        // set to previous value if modifying an existing circle
        if (feature.child_polygon.circle_radius) {
            radius_field.setValue(feature.child_polygon.circle_radius);
        }
    }
    var draw_button = new Ext.Button({
        text: 'Draw',
        width: 50,
        handler: function () {
            if (feature && radius_field) {
                if (feature.child_polygon) {
                    feature.layer.removeFeatures([feature.child_polygon]);
                    feature.child_polygon = null;
                }
                var center = feature.geometry.getCentroid();
                var radius = radius_field.getValue();
                if (center && radius) {
                    var circle_polygon = OpenLayers.Geometry.Polygon.createRegularPolygon(
                        center, radius * 1000, 40);
                    var circle_feature = new OpenLayers.Feature.Vector(circle_polygon);
                    circle_feature.circle_center = feature;
                    circle_feature.circle_radius = radius;
                    feature.layer.addFeatures([circle_feature]);
                    feature.child_polygon = circle_feature;
                } else {
                    // nothing entered for radius so show field error
                    radius_field.markInvalid();
                }
            }
        }
    });
    var save_button = new Ext.Button({
        text: 'Save',
        width: 50,
        handler: function () {
            if (feature) {
                if (feature.child_polygon) {
                    feature.circle_popup.hide();
                    if (use_kml) {
                        // KML specific handler
                        POST.KML.show_feature_window(feature.child_polygon);
                    } else {
                        POST.save_location_feature(feature.child_polygon);
                    }
                }
            }
        }
    });
    feature.circle_popup = new GeoExt.Popup({
        title: 'Draw Circle',
        location: feature,
        width: 275,
        items: [{
            xtype: 'compositefield',
            items: [{
                xtype: 'displayfield',
                html: '<b>Radius (km) :</b>'
            }, radius_field, draw_button, save_button]
        }]
    });
    feature.circle_popup.show();
    radius_field.focus(true, 500);
};


/**
Saves a location feature and its values, both to the on-screen display for the
user and for later processing.

@param {Object} - the OpenLayers feature to be saved
@param {Boolean} - whether to display the result popup
*/
POST.save_location_feature = function (feature, no_popup) {
    console.debug('Saving Location');
    console.log(feature);
    // add the optional colour after drawing
    if (feature.layer) {
        // resets rendering after a save
        feature.renderIntent = 'default';
        if (feature.layer.defaultColour) {
            feature.attributes.colour = feature.layer.defaultColour;
        }
        feature.layer.redraw();
    }
    // update the location form values with this geometry
    var location_input = document.getElementById('entry-location');
    var location_formfield = Ext.getCmp('entry-location');
    var location_title = Ext.getCmp('entry-location-title');
    var measure_value = null;
    if (feature.geometry) {
        var point_result = [];
        var feature_points = feature.geometry.getVertices();
        for (var i = 0; i < feature_points.length; i++) {
            var new_point = new OpenLayers.Geometry.Point(feature_points[i].x,
                feature_points[i].y);
            new_point.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            point_result.push(new_point.y.toPrecision(8) + ',' + new_point.x.toPrecision(8));
        }
        if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Point') {
            if (point_result.length === 1) {
                // setting the geom first so validation works when value added
                location_formfield.geom = 'point';
                location_input.value = point_result[0];
                location_title.update('<b>Point:</b>');
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.LineString') {
            if (point_result.length > 1) {
                location_formfield.geom = 'line';
                location_input.value = point_result.join(' ');
                location_title.update('<b>Line:</b>');
                // getGeodesicLength/Area was not returning correct values
                // map units are currently in meters, using getLength okay for now
                var l_measure = feature.geometry.getLength() / 1000;
                measure_value = ['Length',  l_measure.toFixed(2) + ' km'];
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Polygon') {
            if (feature.circle_center && feature.circle_radius) {
                location_formfield.geom = 'circle';
                var center_point = feature.circle_center.geometry.getCentroid();
                center_point.transform(POST.locationMap.getProjectionObject(),
                    new OpenLayers.Projection('EPSG:4326'));
                location_input.value = center_point.y.toPrecision(8) + ',' + 
                    center_point.x.toPrecision(8) + ' ' + parseFloat(feature.circle_radius, 10) * 1000;
                location_title.update('<b>Circle:</b>');
            } else if (feature.is_box) {
                location_formfield.geom = 'box';
                // need to create a whole new bounds otherwise the transform
                // modifies the geometry's actual bounds and won't display
                var orig_bounds = feature.geometry.getBounds().toArray();
                var box_bounds = new OpenLayers.Bounds.fromArray(orig_bounds);
                box_bounds.transform(POST.locationMap.getProjectionObject(),
                    new OpenLayers.Projection('EPSG:4326'));
                var box_points = box_bounds.toArray(true);
                location_input.value = box_points[0].toPrecision(8) + ',' +
                    box_points[1].toPrecision(8) + ' ' + box_points[2].toPrecision(8) +
                    ',' + box_points[3].toPrecision(8);
                location_title.update('<b>Box:</b>');
            } else {
                if (point_result.length > 2) {
                    location_formfield.geom = 'polygon';
                    // close the polygon, as getVertices does not
                    point_result.push(point_result[0]);
                    location_input.value = point_result.join(' ');
                    location_title.update('<b>Polygon:</b>');
                }
            }
            // The approximate area of the polygon in square meters
            var p_measure = feature.geometry.getArea() / 1000000;
            measure_value = ['Area', p_measure.toFixed(2) + ' km2'];
        } else {
            console.error('Unknown geometry type');
            alert('Geometry type not supported.');
            return;
        }
        // display a temporary notification message to better inform the user when
        // the field value that will be used for location has been updated
        if (!no_popup) {
            var msg = Ext.Msg.show({
                msg: 'Updated Location',
                cls: 'locationNotificationMsg',
                closable: false,
                modal: false,
                minWidth: 180
            });
            msg.getDialog().alignTo('entry-location', 'tr?', [25, -25]);
            setTimeout(function () {
                Ext.Msg.hide();
            }, 2500);
        }
    }
    // deselect whatever control was used to draw/save this feature to set
    // the map back up as originally viewed by user
    var feature_controls = POST.locationMap.getControlsBy('featureControl', true);
    for (var c = 0; c < feature_controls.length; c++) {
        feature_controls[c].deactivate();
    }
    // display a measurement popup at the first point of this geometry
    if (measure_value && !no_popup) {
        if (feature.popup) {
            // remove any existing so no duplicates
            feature.popup.destroy();
        }
        feature.popup = new GeoExt.Popup({
            title: measure_value[0],
            location: feature,
            width: 100,
            html: measure_value[1]
        });
        feature.popup.show();
    }
    console.debug(location_formfield.geom + ': ' + location_input.value);
};


/**
Generate the new Entry and its XML and Object from the entered values.

@return {String} - the new XML string
@return {Object} - the new Entry object
*/
POST.generate_entry_xml = function () {
    if (!POST.ATOM_ENTRY) {
        console.error('Atom Entry Missing');
        alert('Unable to load Atom Entry.');
    }
    // cloning ATOM_ENTRY each time required for cases where a user
    // goes back and add/removes values after doing a preview which
    // results in a modified ATOM_ENTRY being used to build the post XML
    var new_entry = MASAS.Common.clone_object(POST.ATOM_ENTRY);
    
    // start out with filling in required categories at set index positions
    var entry_status = Ext.getCmp('entry-status').getValue();
    new_entry.entry.category[0]['@term'] = entry_status;
    var tree_selection = Ext.getCmp('icon-select-tree').getSelectionModel().getSelectedNode();
    new_entry.entry.category[1]['@term'] = tree_selection.attributes.term;
    // optional categories, added to the top of the listing so they appear
    // first in the viewing tool
    var certainty_val = Ext.getCmp('entry-certainty').getValue();
    if (certainty_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Certainty',
            '@scheme': 'masas:category:certainty',
            '@term': certainty_val.getGroupValue()
        });
    }
    var severity_val = Ext.getCmp('entry-severity').getValue();
    if (severity_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Severity',
            '@scheme': 'masas:category:severity',
            '@term': severity_val.getGroupValue()
        });
    }
    var category_val = Ext.getCmp('entry-category').getValue();
    if (category_val) {
        new_entry.entry.category.splice(0, 0, {
            '@label': 'Category',
            '@scheme': 'masas:category:category',
            '@term': category_val
        });
    }
    var colour_select = Ext.getCmp('entry-colour-combo').getValue();
    if (colour_select) {
        var colour_cat = {
            '@label': 'Colour',
            '@scheme': 'masas:category:colour',
            '@term': colour_select
        };
        var colour_label = Ext.getCmp('entry-colour-label').getValue();
        if (colour_label) {
            colour_cat['@label'] = colour_label;
        }
        var colour_context = Ext.getCmp('entry-colour-context').getValue();
        if (colour_context) {
            colour_cat['@mea:context'] = colour_context;
            // add mea namespace
            new_entry.entry['@xmlns:mea'] = 'masas:experimental:attribute';
        }
        new_entry.entry.category.splice(0, 0, colour_cat);
    }
    
    var en_title_div = new_entry.entry.title.div.div[0];
    en_title_div['#text'] = MASAS.Common.xml_encode(Ext.get('entry-en-title').getValue());
    var en_content_div = new_entry.entry.content.div.div[0];
    var en_content = Ext.get('entry-en-content').getValue();
    if (en_content) {
        en_content_div['#text'] = MASAS.Common.xml_encode(en_content);
    }
    if (Ext.getCmp('entry-add-french').collapsed) {
        // remove the french divs by converting from an array of two
        // objects to just one
        new_entry.entry.title.div.div = en_title_div;
        new_entry.entry.content.div.div = en_content_div;
    } else {
        var fr_title_div = new_entry.entry.title.div.div[1];
        fr_title_div['#text'] = MASAS.Common.xml_encode(Ext.get('entry-fr-title').getValue());
        var fr_content_div = new_entry.entry.content.div.div[1];
        var fr_content = Ext.get('entry-fr-content').getValue();
        if (fr_content) {
            fr_content_div['#text'] = MASAS.Common.xml_encode(fr_content);
        }
    }
    
    var links_list = [];
    POST.relatedLinkStore.each(function(rec) {
        links_list.push({
            '@href': MASAS.Common.xml_encode(rec.data.url),
            '@rel': 'related',
            '@title': MASAS.Common.xml_encode(rec.data.title),
            '@type': rec.data.type
        });
    });
    if (links_list.length === 1) {
        // add as an object for single link
        new_entry.entry.link = links_list[0];
    } else if (links_list.length > 1) {
        new_entry.entry.link = links_list;
    }
    
    // GeoRSS format is "lat lon lat lon"
    var location_formfield = Ext.getCmp('entry-location');
    new_entry.entry['georss:' + location_formfield.geom] =
        location_formfield.getValue().replace(/,/g, ' ');
    
    var effective_im = Ext.getCmp('entry-effective-immediate').getValue();
    var effective_dt = Ext.get('entry-effective-dt').getValue();
    var effective_tm = Ext.get('entry-effective-tm').getValue();
    var effective_date = null;
    // use a newly entered effective time
    if (!effective_im && effective_dt && effective_tm) {
        // using Ext parsing and formatting extensions added to normal Date    
        var effective_str = effective_dt + 'T' + effective_tm + ':00';
        effective_date = Date.parseDate(effective_str, 'c');
    } else {
        if (effective_im && POST.oldAtom.entry['met:effective']) {
            // calculate update according to a previous effective time
            var effective_old = Date.parseDate(POST.oldAtom.entry['met:effective'],
                'Y-m-d\\TH:i:s\\Z');
            effective_old = MASAS.Common.adjust_time(effective_old,
                MASAS.Common.UTC_Local_Offset);
            var effective_old_epoch = effective_old.format('U') * 1000;
            var now_epoch = new Date().getTime();
            if (effective_old_epoch < now_epoch) {
                // when the previous effective time has already been reached,
                // just carry it forward with no change
                effective_date = effective_old;
            } else {
                // otherwise if the user has selected effective immediate, a
                // new effective time is used
                effective_date = new Date();
            }
        }
    }
    if (effective_date) {
        effective_date = MASAS.Common.adjust_time(effective_date,
            MASAS.Common.Local_UTC_Offset);
        new_entry.entry['met:effective'] = effective_date.format('Y-m-d\\TH:i:00\\Z');
        // add met namespace
        new_entry.entry['@xmlns:met'] = 'masas:experimental:time';
    }
    
    var expires_dt = Ext.get('entry-expires-dt').getValue();
    var expires_tm = Ext.get('entry-expires-tm').getValue();
    var expires_it = Ext.getCmp('entry-expires-interval').getValue();
    var expires_date = null;
    if (expires_dt && expires_tm) {
        // using Ext parsing and formatting extensions added to normal Date    
        var expires_str = expires_dt + 'T' + expires_tm + ':00';
        expires_date = Date.parseDate(expires_str, 'c');
    } else if (expires_it) {
        expires_date = new Date();
        // 99 is expires right now, all others are time value in the future
        if (expires_it !== 99) {
            var expires_diff = parseInt(expires_it, 10);
            expires_date = MASAS.Common.adjust_time(expires_date, expires_diff);
        }
    }
    if (expires_date) {
        expires_date = MASAS.Common.adjust_time(expires_date,
            MASAS.Common.Local_UTC_Offset);
        new_entry.entry['age:expires'] = expires_date.format('Y-m-d\\TH:i:00\\Z');
        // add age namespace
        new_entry.entry['@xmlns:age'] = 'http://purl.org/atompub/age/1.0';
    }
    
    var update_control_val = Ext.getCmp('entry-update-control').getValue().getGroupValue();
    if (update_control_val === 'group' || update_control_val === 'all') {
        if (update_control_val === 'group') {
            new_entry.entry['app:control'] = {'mec:update': POST.USER_GROUP};
        } else if (update_control_val === 'all') {
            new_entry.entry['app:control'] = {'mec:update': 'all'};
        }
        // add app and mec namespaces
        new_entry.entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        new_entry.entry['@xmlns:mec'] = 'masas:extension:control';
    }
    // draft entries also need to add the AtomPub elements
    if (entry_status === 'Draft') {
        if (new_entry.entry['app:control']) {
            new_entry.entry['app:control']['app:draft'] = 'yes';
        } else {
            new_entry.entry['app:control'] = {'app:draft': 'yes'};
            // add app namespaces
            new_entry.entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        }
    }
    
    var new_xml = xmlJsonClass.json2xml(new_entry, '');
    new_xml = MASAS.Common.format_xml(new_xml);
    new_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + new_xml;
    
    return [new_xml, new_entry];
};


/**
Show a preview window to review the new Entry both with a template for user
friendlier display and the XML.
*/
POST.preview_entry_xml = function () {
    // only one review window allowed
    if (POST.reviewWindow) {
        POST.reviewWindow.destroy();
    }
    
    var preview_xml_vals = POST.generate_entry_xml();
    var display_xml = preview_xml_vals[0];
    preview_xml_vals[1].entry.id = 'Entry ' + POST.UPDATE_OPERATION;
    // reformat effective and expires for local time presentation and removing
    // namespace prefixes
    if (preview_xml_vals[1].entry['met:effective']) {
        preview_xml_vals[1].entry.effective = MASAS.Common.local_time_adjust(
            preview_xml_vals[1].entry['met:effective']);
    }
    if (preview_xml_vals[1].entry['age:expires']) {
        preview_xml_vals[1].entry.expires = MASAS.Common.local_time_adjust(
            preview_xml_vals[1].entry['age:expires']);
    }
    try {
        // XTemplate from post-templates.js
        var display_html = MASAS.Template.EntryToHTML.apply(preview_xml_vals[1]);
    } catch (err) {
        console.error('Preview Entry Template Error: ' + err);
        alert('Entry Template Error.');
        return;
    }
    // preview before posting needs to add attachments separately, after posting
    // the XTemplate will show instead    
    if (POST.attachmentStore.getCount() > 0) {
        display_html += '<div class="TemplateDisplayBox"><h2>Attachments</h2>';
        POST.attachmentStore.each(function (rec) {
            display_html += '<p><b>' + rec.data.title + '</b> - ' + rec.data.filename + '</p>';
        });
        display_html += '</div>';
    }
    // preview confirms any attached kml layer
    var kml_attach = Ext.getCmp('entry-kml-notice').getValue();
    if (kml_attach.length > 0) {
        display_html += '<div class="TemplateDisplayBox"><h2>' + kml_attach +
            '</h2></div>';
    }
    // preview for update control also needs to be shown separately
    if (preview_xml_vals[1].entry['app:control']) {
        if (preview_xml_vals[1].entry['app:control']['mec:update']) {
            var update_control_val = Ext.getCmp('entry-update-control').getValue().getGroupValue();
            if (update_control_val === 'all') {
                display_html += '<div class="TemplateDisplayBox"><h2>Entry Updates</h2>';
                display_html += '<p><b>Allow All</b></p></div>';
            }
        }
    }
    display_xml = display_xml.replace(/</g, '&lt;');
    display_xml = display_xml.replace(/>/g, '&gt;');
    display_html += '<div class="reviewClose"><a href="#" onclick="' +
        'POST.reviewWindow.close(); return false;">Close Preview</a></div>' +
        '<div class="reviewXmlShow"><a href="#" onclick="' +
        'document.getElementById(\'review-xml-box\').style.display=\'block\'; return false;">' +
        'Show XML</a></div><div id="review-xml-box" style="display: none;">' +
        '<pre>' + display_xml + '</pre></div>';
    
    POST.reviewWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Entry Preview <a class="titleClose" href="#" ' +
            'onclick="POST.reviewWindow.close(); return false;">Close</a>',
        closable: true,
        width: 700,
        height: 500,
        autoScroll: true,
        bodyStyle: 'background-color: white;',
        layout: 'fit',
        html: display_html
    });
    POST.reviewWindow.show(this);
};


/**
Post this new Entry to the server.  Used for cloning.
*/
POST.post_new_entry = function () {
    var posting_html = '<h1>Posting Entry</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: the following is not working to prevent going back
    //Ext.getCmp('card-prev').setDisabled();
    var new_entry_xml_vals = POST.generate_entry_xml();
    console.debug('Posting New Entry');
    if (POST.USE_ATTACH_PROXY === true) {
        var do_post = new OpenLayers.Request.POST({
                url: POST.ATTACH_PROXY_URL +
                    encodeURIComponent(OpenLayers.Util.urlAppend(POST.FEED_URL,
                        'secret=' + POST.USER_SECRET)),
                // proxy is ignored for same origin hosts, but we need it to always
                // force the use of the attach proxy, so modify url instead
                data: new_entry_xml_vals[0],
                callback: POST.Common.post_complete_result
        });
    } else {
        var do_post = new OpenLayers.Request.POST({
                url: POST.FEED_URL,
                params: {'secret': POST.USER_SECRET},
                proxy: POST.AJAX_PROXY_URL,
                headers: {'Content-Type': 'application/atom+xml'},
                data: new_entry_xml_vals[0],
                callback: POST.Common.post_complete_result
        });
    }
};


/**
Post this Entry update to the server.
*/
POST.post_entry_update = function () {
    var posting_html = '<h1>Posting Entry</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: not working
    //Ext.getCmp('card-prev').setDisabled();
    // find the edit link to update Atom
    var edit_url = null;
    var media_url = null;
    if (POST.oldAtom.entry.link instanceof Array) {
        for (var i = 0; i < POST.oldAtom.entry.link.length; i++) {
            if (POST.oldAtom.entry.link[i]['@rel'] === 'edit') {
                edit_url = POST.oldAtom.entry.link[i]['@href'];
            }
            if (POST.oldAtom.entry.link[i]['@rel'] === 'edit-media') {
                media_url = POST.oldAtom.entry.link[i]['@href'];
            }
        }
    } else if (typeof(POST.oldAtom.entry.link) === "object") {
        if (POST.oldAtom.entry.link['@rel'] === 'edit') {
            edit_url = POST.oldAtom.entry.link['@href'];
        }
    }
    if (edit_url === null) {
        console.error('Unable to find edit url to post update to Entry');
        alert('Cannot Post this Entry.');
        return;
    }
    // must first update the attachments
    if (POST.USE_ATTACH_PROXY === true && media_url !== null) {
        console.debug('Posting attachment updates first');
        var put_media = new OpenLayers.Request.PUT({
                url: POST.ATTACH_PROXY_URL,
                data: OpenLayers.Util.getParameterString({
                    url: OpenLayers.Util.urlAppend(media_url, 'secret=' + POST.USER_SECRET)
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                async: false
            });
        if (put_media.status !== 200) {
            console.error('Attachments update failed: ' + put_media.responseText);
            alert('Attachments did not Update.');
            //TODO: quit here and skip further Entry XML update?
        }
    }
    // then update XML, if attachments, will show up as two browser requests
    // while the proxy will merge to send single request to the Hub
    var new_entry_xml_vals = POST.generate_entry_xml();
    console.debug('Posting Entry Update');
    var put_entry = new OpenLayers.Request.PUT({
        url: edit_url,
        params:  {'secret': POST.USER_SECRET},
        proxy: POST.AJAX_PROXY_URL,
        headers: {'Content-Type': 'application/atom+xml'},
        data: new_entry_xml_vals[0],
        callback: POST.Common.post_complete_result
    });
};
