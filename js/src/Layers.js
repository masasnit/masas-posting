/**
MASAS Posting Tool - Layers
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

Classes and functions used to support map layers, layer menus, and other layer
related functionality.

TODO: change references for map and mapPanel to allow both viewing and posting
      tools to share without modification
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,google */

Ext.ns('MASAS.Layers');


/**
 * Creates layer records for the layer store.
 */
MASAS.Layers.LayerReader = Ext.extend(GeoExt.data.LayerReader, {
    /** Create a record(s) and associated layer(s) suitable for the store
     * 
     * @param {Array} - List of objects with layer data needed to create layers
     * @return {Object} - records {Array} and totalRecords {Integer} properties
     */
    readRecords: function (recs) {
        var records = [];
        if (recs) {
            if (!Ext.isArray(recs)) {
                recs = [recs];
            }
            // the GeoExt LayerRecord used for this reader
            var recordType = this.recordType;
            var fields = recordType.prototype.fields;
            var i, j, rec, data, field, value;
            for (i = 0; i < recs.length; i++) {
                try {
                    rec = recs[i];
                    data = {};
                    // based on the defined fields for this record, add the
                    // appropriate value from the layer input
                    for (j = 0; j < fields.length; j++){
                        field = fields.items[j];
                        if (rec[field.mapping]) {
                            value = field.convert(rec[field.mapping]);
                        } else if (rec[field.name]) {
                            value = field.convert(rec[field.name]);
                        } else {
                            if (field.defaultValue) {
                                value = field.defaultValue;
                            } else {
                                continue;
                            }
                        }
                        data[field.name] = value;
                    }
                    data.layer = this.generateLayer(data);
                    records.push(new recordType(data, data.layer.id));
                } catch (err) {
                    console.error('Layer record create error: ' + err);
                }
            }
        }
        
        return {
            records: records,
            totalRecords: (this.totalRecords != null) ? this.totalRecords : records.length
        };
    },
    
    
    /** Generate a new layer for this new record
     *
     * @param {Object} - the record's data with layer config
     * @return {Object} - OpenLayers.Layer
     */
    generateLayer: function (data) {
        var layer;
        var layer_options = {
            isBaseLayer: (data.base_layer) ? data.base_layer : false,
            visibility: (data.visibility) ? data.visibility : false,
            attribution: (data.attribution) ? data.attribution : null
        };
        if (!data.base_layer) {
            layer_options['opacity'] = (data.opacity) ? parseFloat(data.opacity) : 0.5;
        }
        if (data.zoom_levels) {
            layer_options['numZoomLevels'] = data.zoom_levels;
        }
        if (data.type in this.layerTypes) {
            layer = this.layerTypes[data.type](layer_options, data);
        } else {
            throw new Error('Unknown layer mimeType');
        }
        //TODO: additional layer handling such as adding events or controls
        
        return layer;
    },
    
    
    /** Layer mimeTypes to use to generate and return a new OpenLayers layer.
     * These types apply to all layer stores and new types should be added here.
     *
     * @param {Object} - layer_options - generic layer options
     * @param {Object} - data - the original data used to create each record
     */
    layerTypes: {
        
        /** WMS
         *
         * Supports WMS base layers and overlays.  If the service doesn't
         * support 900913, 3857 is equivalent, or 4326 using a transform is also
         * supported.  But other projections and transforms are not.
         */
        'application/vnd.ogc.wms': function (layer_options, data) {
            //TODO: singleTile required for any sources?
            var layer_params = {
                format: 'image/png',
                transparent: (data.base_layer) ? false : true
            };
            
            if (data.url.search('EPSG:4326') > -1 || data.url.search('epsg:4326') > -1) {
                /** Replace with a method that combines both getURL and
                * getFullRequestString to accomodate 4326 layers
                */
               layer_options.getURL = function (bounds) {
                   var view_bounds = bounds.clone();
                   view_bounds = this.adjustBounds(view_bounds);
                   view_bounds.transform(POST.locationMap.getProjectionObject(),
                       new OpenLayers.Projection('EPSG:4326'));
                   var imageSize = this.getImageSize();
                   var reverse_axis = false;
                   // reverse axis order for 4326 with WMS 1.3
                   if (data.url.search('1.3.0') > -1) {
                        reverse_axis = true;
                   }
                   var newParams = {
                       'BBOX': view_bounds.toBBOX(6, reverse_axis),
                       'WIDTH': imageSize.w,
                       'HEIGHT': imageSize.h,
                       'TRANSPARENT': (this.params.TRANSPARENT) ? 'TRUE' : 'FALSE'
                   };
                   
                   return OpenLayers.Layer.Grid.prototype.getFullRequestString.apply(this,
                       [newParams]);
               };
            }
            
            return new OpenLayers.Layer.WMS(data.title, data.url, layer_params,
                layer_options);
        },
        
        /** TMS
         */
        'application/vnd.osgeo.tms': function (layer_options, data) {
            return new OpenLayers.Layer.TMS(data.title, data.url, layer_options);
        },
        
        /** OpenWeatherMap
         *
         * Customized layer that loads OWM data via JSONP.
         */
        'owm': function (layer_options, data) {
            layer_options.url = data.url;
            return new OpenLayers.Layer.Vector.OWMWeather(data.title, layer_options);
        },
        
        /** Google
         */
        'google': function (layer_options, data) {
            if (data.map_type && data.map_type === 'satellite') {
                layer_options['type'] = google.maps.MapTypeId.HYBRID;
            } else {
                layer_options['type'] = google.maps.MapTypeId.ROADMAP;
            }
            
            return new OpenLayers.Layer.Google(data.title, layer_options);
        },
        
        /** OpenStreetMap
         *
         * A user defined URL can load OSM custom tiles.
         */
        'osm': function (layer_options, data) {
            layer_options['tileOptions'] = {crossOriginKeyword: null};
            // single source server and the tileset URL scheme will be added
            // to that URL, should have trailing /, ie. http://a.tile.openstreetmap.org/
            data.url = data.url + '${z}/${x}/${y}.png';
            return new OpenLayers.Layer.OSM(data.title, data.url, layer_options);
        },
        
        /** OpenStreetMap
         *
         * Default base layer to load OSM tiles from MapQuest.
         */
        'osmm': function (layer_options, data) {
            layer_options['tileOptions'] = {crossOriginKeyword: null};
            return new OpenLayers.Layer.OSM(data.title, [
                    'http://otile1.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
                    'http://otile2.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
                    'http://otile3.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
                    'http://otile4.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png'
                ], layer_options);
        },
        
        /** OpenStreetMap
         *
         * Default base layer to load MapQuest provided aerials that conform
         * to the OSM tile scheme.  Doesn't offer higher resolutions for many
         * area of Canada, overview only.
         */
        'osma': function (layer_options, data) {
            layer_options['tileOptions'] = {crossOriginKeyword: null};
            return new OpenLayers.Layer.OSM(data.title, [
                    'http://otile1.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png',
                    'http://otile2.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png',
                    'http://otile3.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png',
                    'http://otile4.mqcdn.com/tiles/1.0.0/sat/${z}/${x}/${y}.png'
                ], layer_options);
        },
        
        //TODO: add ArcGIS Cache for tile support
        /** ArcGIS REST
         * 
         * Most REST services don't support 900913, but they might support
         * 102100 or 3857 which are equivalent.  Add these to the URL, or if
         * only 4326 is supported, a transform will take place.
         * The default image format is png, although some maps only support
         * jpg or are faster with jpg.  Specify the format in the URL.
         * The export URL must be used for a REST service, as compared to the
         * service's description page ie.
         * ArcGIS/rest/services/World_Street_Map/MapServer vs
         * ArcGIS/rest/services/World_Street_Map/MapServer/export
         */
        'application/vnd.esri.arcgis.rest': function (layer_options, data) {
            var layer_params = {
                transparent: (data.base_layer) ? false : true
            };
            
            if (data.url.search('SR=4326') > -1 || data.url.search('sr=4326') > -1) {
                /** Replace with a method to accomodate 4326 layers
                */
               layer_options.getURL = function (bounds) {
                   var view_bounds = bounds.clone();
                   view_bounds = this.adjustBounds(view_bounds);
                   view_bounds.transform(POST.locationMap.getProjectionObject(),
                       new OpenLayers.Projection('EPSG:4326'));
                   console.log('this', this);
                   var imageSize = this.getImageSize();
                   var newParams = {
                       'BBOX': view_bounds.toBBOX(),
                       'SIZE': imageSize.w + ',' + imageSize.h,
                       'F': 'image',
                       'BBOXSR': '4326',
                       'IMAGESR': '4326'
                   };
                   //NOTE: ignoring layer filtering code from ArcGIS93Rest.js
                   //      if needed, I believe it can be added to the initial
                   //      URL by the user
                   var requestString = this.getFullRequestString(newParams);
                   
                   return requestString;
               };
            }
            
            return new OpenLayers.Layer.ArcGIS93Rest(data.title, data.url,
                layer_params, layer_options);
        },
        
        //TODO: is cache busting needed when loading KML to ensure dynamic
        //      KML services are refreshed?
        /** KML
         */
        'application/vnd.google-earth.kml+xml': function (layer_options, data) {
            layer_options['projection'] = new OpenLayers.Projection('EPSG:4326');
            layer_options['strategies'] = [new OpenLayers.Strategy.Fixed()];
            layer_options['protocol'] = new OpenLayers.Protocol.HTTP({
                url: data.url,
                format: new OpenLayers.Format.KML({
                    extractStyles: true, 
                    extractAttributes: true,
                    maxDepth: 1
                })
            });
            
            return new OpenLayers.Layer.Vector(data.title, layer_options);
        }
    }
});



/**
 * A store that synchronizes layers in an {OpenLayers.Map} with a
 * layer store holding {<GeoExt.data.LayerRecord>} entries.  Use the
 * MASAS.Layers.LayerStore constructor instead of this mixin directly.
 *
 * Derived from GeoExt/data/LayerStore.js
 */
MASAS.Layers.LayerStoreMixin = function () {
    return {
        /** In addition to being used by the StoreMgr, this is also set as a
         * attribute in the layer to identify its related to this store
         *
         * config[storeId]
         */
        storeId: null,
        
        /** Map that this store will be in sync with. If not provided, the
         * store will not be bound to a map to start.
         *
         * config[map]
         */
        map: null,
        
        /** Additional fields that will be added to each record in this store.
         * Default fields for every layer record are 'layer' (OpenLayers.Layer)
         * and 'title' (String).  title is mapped to the layer name, because
         * name is already used for record properties.  The value of this option
         * is an ExtJS store field definition object.
         *
         * config[fields]
         */
        fields: null,
        
        /** The reader used to produce GeoExt.data.LayerRecord objects.
         * If not provided, MASAS.Layers.LayerReader will be used.
         *
         * config[reader]
         */
        reader: null,
        
        /** An array of 'type', 'name' that this layer store instance supports.
         * New types should first be added to the Reader.
         *
         * config[layer_types]
         */
        layer_types: [],
        
        /** This store holds base layers.  Default is false.
         *
         * config[base_layers]
         */
        base_layers: false,
        
        
        /** Constructor
         *
         * @param {Object} - config
         */
        constructor: function (config) {
            config = config || {};
            config.reader = config.reader ||
                new MASAS.Layers.LayerReader({}, config.fields);
            delete config.fields;
            var map = config.map instanceof GeoExt.MapPanel ?
                config.map.map : config.map;
            delete config.map;
            arguments.callee.superclass.constructor.call(this, config);
            // Fires when the store is bound to a map, either now or later
            this.addEvents('bind');
            if(map) {
                this.bind(map);
            }
        },
        
        
        /** Bind this store to a map instance, once bound the store
         * is synchronized with the map and vice-versa.
         *
         * @param {Object} - OpenLayers.Map  The map instance.
         */
        bind: function (map) {
            if (this.map) {
                // already bound
                return;
            }
            this.map = map;
            map.events.on({
                'changelayer': this.onChangeLayer,
                'removelayer': this.onRemoveLayer,
                scope: this
            });
            this.on({
                'load': this.onLoad,
                'clear': this.onClear,
                'add': this.onAdd,
                'remove': this.onRemove,
                'update': this.onUpdate,
                scope: this
            });
            this.data.on({
                'replace' : this.onReplace,
                scope: this
            });
            //TODO: firing twice?
            this.fireEvent('bind', this, map);
        },
        
        
        /** Unbind this store from the map it is currently bound.
         */
        unbind: function () {
            if (this.map) {
                this.map.events.un({
                    'changelayer': this.onChangeLayer,
                    'removelayer': this.onRemoveLayer,
                    scope: this
                });
                this.un('load', this.onLoad, this);
                this.un('clear', this.onClear, this);
                this.un('add', this.onAdd, this);
                this.un('remove', this.onRemove, this);
                this.data.un('replace', this.onReplace, this);
                this.map = null;
            }
        },
        
        
        /** Handler for a map's changelayer event.  Applies to all layers
         * present on the map.  Syncs any changes to the appropriate record.
         *
         * NOTE: not listening for the changebaselayer event.  When switching
         *       between base layers, onChangeLayer is fired for the old
         *       base layer first then for the new base layer, with visibility
         *       being set accordingly.
         * 
         *  @param {Object} - event
         */
        onChangeLayer: function (evt) {
            var layer = evt.layer;
            // determine if this layer corresponds to this store's records
            var recordIndex = this.findBy(function (rec, id) {
                return rec.getLayer() === layer;
            });
            if (recordIndex > -1) {
                var record = this.getAt(recordIndex);
                // assumes the only change that may take place on the layer
                // is the visibility, all other changes should happen to the
                // the record instead
                if (evt.property === 'visibility') {
                    // fires the 'update' event the 1st time, which should
                    // be ignored for visibility only changes
                    record.set('visibility', layer.getVisibility());
                    // save this change in the record silently so that it
                    // won't fire the 'update' event a 2nd time with the
                    // operation being a commit
                    record.commit(true);
                    // update the store as per .commitChanges()
                    this.modified = [];
                    this.removed  = [];
                }
            }
        },
        
        
        /** Handler for a map's removelayer event.  Currently used to ensure
         * proper cleanup for IE.
         *  
         *  @param {Object} - event
         */
        onRemoveLayer: function (evt) {
            //TODO: replace the check for undloadDestroy with a listener for the
            //      map's beforedestroy event, doing unbind(). This can be done
            //      as soon as http://trac.openlayers.org/ticket/2136 is fixed.
            if (!this.map.unloadDestroy) {
                this.unbind();
            }
        },
        
        
        /** Handler for a store's load event.  If the add option is used, the
         * records have already been appended properly.  If not, then it means
         * the store has been cleared first and additional cleanup, then
         * adding is needed.
         * 
         * @param {Object} - Ext.data.Store
         * @param {Object} - Array(Ext.data.Record)
         * @param {Object} - load options
         */
        onLoad: function(store, records, options) {
            if (!Ext.isArray(records)) {
                records = [records];
            }
            if (options && !options.add) {
                if (!this._removing) {
                    this._removing = true;
                    // the previous records have already been removed, so try to
                    // remove any layers that may have been associated with them
                    var old_layers = this.map.getLayersBy('layerStoreId', this.storeId);
                    var layer;
                    for (var i = old_layers.length - 1; i >= 0; --i) {
                        this.map.removeLayer(old_layers[i]);
                    }
                    // the records have been added directly to the MixedCollection
                    // and so they need to go through the rest of the add process
                    this.fireEvent('add', this, records, this.data.length);
                    delete this._removing;
                }
            }
        },
        
        
        /** Handler for a store's clear event, currently only called with
         * store.removeAll()
         *
         * @param {Object} - Ext.data.Store
         * @param {Array} - Array(Ext.data.Record)
         */
        onClear: function(store, records) {
            if (!this._removing) {
                this._removing = true;
                var layer;
                for (var i = records.length - 1; i >= 0; --i) {
                    layer = records[i].getLayer();
                    this.map.removeLayer(layer);
                }
                delete this._removing;
            }
        },
        
        
        /** Handler for a store's add event.  Adds to the layer as well.
         * 
         * @param {Object} - Ext.data.Store
         * @param {Object} - Array(Ext.data.Record)
         * @param {Integer}
         */
        onAdd: function (store, records, index) {
            if (!this._adding) {
                this._adding = true;
                var layer;
                for (var i = records.length - 1; i >= 0; --i) {
                    layer = records[i].getLayer();
                    // setting this attribute, used to connect layers back to a
                    // store here, because the LayerReader doesn't know about
                    // store attributes
                    layer.layerStoreId = this.storeId;
                    this.map.addLayer(layer);
                    //if (index !== this.map.layers.length - 1) {
                    //    this.map.setLayerIndex(layer, index);
                    //}
                }
                delete this._adding;
            }
        },
        
        
        /** Handler for a store's remove event. Removes from the layer as well.
         * 
         * @param {Object} - Ext.data.Store
         * @param {Object} - Ext.data.Record
         * @param {Integer}
         */
        onRemove: function (store, record, index) {
            if (!this._removing) {
                var layer = record.getLayer();
                if (this.map.getLayer(layer.id) != null) {
                    this._removing = true;
                    this.removeMapLayer(record);
                    delete this._removing;
                }
            }
        },
        
        
        /** Handler for a store's update event.  Syncs any changes to the store
         * with the layer.
         * 
         *  @param {Object} - Ext.data.Store
         *  @param {Object} - Ext.data.Record
         *  @param {String} - operation type
         */
        onUpdate: function (store, record, operation) {
            // 'update' will fire for Ext.data.Record.COMMIT as well, but it
            // won't have anything in record.modified so ignore
            if (operation === Ext.data.Record.EDIT) {
                if (record.modified) {
                    var layer = record.getLayer();
                    var layer_reload = false;
                    if (record.modified.title) {
                        layer.setName(record.get('title'));
                    }
                    if (record.modified.url) {
                        layer.url = record.get('url');
                        layer_reload = true;
                    }
                    if (record.modified.opacity) {
                        layer.setOpacity(parseFloat(record.get('opacity')));
                        layer_reload = true;
                    }
                    if (layer_reload) {
                        layer.redraw();
                    }
                }
            }
        },
        
        
        /** Removes a record's layer from the bound map.
         * 
         *  @param {Object} - Ext.data.Record
         */
        removeMapLayer: function (record) {
            this.map.removeLayer(record.getLayer());
        },
        
        
        /** Handler for a store's data collections' replace event
         * 
         *  @param {String} - key
         *  @param {Object} - A record that has been replaced.
         *  @param {Object} - A record that is replacing oldRecord.
         */
        onReplace: function (key, oldRecord, newRecord) {
            this.removeMapLayer(oldRecord);
        },
        
        
        /** Get the record for the specified layer
         * 
         *  @param {Object} - OpenLayers.Layer
         *  @return {Object} - GeoExt.data.LayerRecord or undefined if not found 
         */
        getByLayer: function (layer) {
            var index = this.findBy(function (r) {
                return r.getLayer() === layer;
            });
            if (index > -1) {
                return this.getAt(index);
            }
        },
        
        //TODO: confirm how well destroy/cleanup works, especially on IE
        
        /** Cleanup
         */
        destroy: function () {
            this.unbind();
            MASAS.Layers.LayerStore.superclass.destroy.call(this);
        }
    };
};


/**
 * Constructor for a store that contains GeoExt.data.LayerRecord objects.
 */
MASAS.Layers.LayerStore = Ext.extend(Ext.data.Store, new MASAS.Layers.LayerStoreMixin());



/**
 * Creates a menu with a tree of items representing the layers present in
 * an associated layer store.
 *
 * Derived from https://github.com/opengeo/gxp/  LayerMenu
 */
MASAS.Layers.LayerMenu = Ext.extend(Ext.menu.Menu, {
    /** mouse users can open this menu by hovering and click directly on the
     * checkbox.  Touch users can only open this menu using a touch/click and
     * then use the sub menus to hide/show the layer
     *
     * config[ignoreParentClicks]
     */
    ignoreParentClicks: false,
    
    /** The store containing layer records to be viewed in this menu.
     * 
     * config[layers] - MASAS.Layers.LayerStore
     */
    layers: null,
    
    /** Allow new layers to be created
     *
     * config[createNewLayers]
     */
    createNewLayers: false,
    
    
    /** Private method called to initialize the component.
     */
    initComponent: function () {
        MASAS.Layers.LayerMenu.superclass.initComponent.apply(this, arguments);
        this.layers.on({
            'add': this.updateLayers,
            'update': this.updateLayers,
            'remove': this.updateLayers,
            'clear': this.updateLayers,
            scope: this
        });
        this.updateLayers();
    },
    
    
    /** Private method called during the render sequence.
     */
    onRender : function (ct, position) {
        MASAS.Layers.LayerMenu.superclass.onRender.apply(this, arguments);
    },
    
    
    /** Private method called during the destroy sequence.
     */
    beforeDestroy: function () {
        if (this.layers && this.layers.on) {
            this.layers.un('add', this.updateLayers, this);
            this.layers.un('update', this.updateLayers, this);
            this.layers.un('remove', this.updateLayers, this);
            this.layers.un('clear', this.updateLayers, this);
        }
        delete this.layers;
        MASAS.Layers.LayerMenu.superclass.beforeDestroy.apply(this, arguments);
    },
    
    
    /** Listener that rebuilds the menu whenever there is a layer store change.
     *
     * @param {Object} - when 'update' fires, the layer store is supplied
     * @param {Object} - when 'update' fires, the record that was updated
     * @param {String} - when 'update' fires, the type of operation
     */
    updateLayers: function (update_store, update_record, operation) {
        //NOTE: there is a problem with the Show/Hide menu items triggering the
        //      'update' event, which we need to make sure visibility is correct
        //      in both the store and the layer, but it also triggers a rebuild of
        //      this menu and that occurs while the Show/Hide menu is still open.
        //      Causes errors because their DOM values are no longer there. The
        //      EDIT check also handles this problem.
        // Changes to a record should result in two triggers of the 'update'
        // event, one with Ext.data.Record.EDIT and the second when those
        // changes are saved or comitted wth Ext.data.Record.COMMIT.
        // Ignore the first change for rebuilds.
        if (operation && operation === Ext.data.Record.EDIT) {
            return;
        }
        this.removeAll();
        var layer_store = this.layers;
        var perm_separator = false;
        layer_store.each(function (record) {
            var layer = record.getLayer();
            // permanent layers should come first in the store's listing and
            // once all of them have been added, use a separator prior to the
            // next layer that is assumed to be a custom one. Don't use at
            // all for stores with no permanents.
            if (record.get('permanent')) {
                perm_separator = true;
            } else {
                if (perm_separator) {
                    this.add('-');
                    perm_separator = false;
                }
            }
            
            var item_menus = [{
                'text': 'Show',
                handler: function (cmp, evt) {
                    var menu_parent = cmp.parentMenu.parentMenu.activeItem;
                    menu_parent.setChecked(true);
                }
            }];
            if (!record.get('base_layer')) {
                // if you hide a base layer it means there isn't a proper
                // switch to another
                item_menus.push({
                    'text': 'Hide',
                    handler: function (cmp, evt) {
                        var menu_parent = cmp.parentMenu.parentMenu.activeItem;
                        menu_parent.setChecked(false);
                    }
                });
            }
            item_menus.push('-');
            // entry layers shouldn't be added to the toolbar
            if (this.createNewLayers) {
                item_menus.push({
                    xtype: 'menucheckitem',
                    'text': 'On Toolbar',
                    // reference for the toolbar restore procedure
                    ref: 'toolbarCheckItem',
                    checked: (record.get('button')) ? true : false,
                    listeners: {
                        checkchange: function (item, checked) {
                            console.debug(((checked) ? 'Add ' : 'Remove ') +
                                record.get('title') + ' toolbar');
                            var topbar = POST.locationMapPanel.getTopToolbar();
                            var is_base = record.get('base_layer');
                            var tb_button = record.get('button');
                            if (checked) {
                                if (tb_button) {
                                    // duplicate check
                                    return;
                                }
                                MASAS.Layers.add_toolbar_button(record, topbar, true);
                            } else {
                                Ext.Msg.confirm('Toolbar Remove',
                                    'Are you sure that you want to remove this' +
                                    ' layer from the toolbar?',
                                    function (confirm) {
                                        if (confirm === 'yes') {
                                            topbar.remove(tb_button);
                                            record.data['button'] = null;
                                        } else {
                                            // restore the checked value
                                            item.setChecked(true, true);
                                        }
                                    }
                                );
                            }
                            // call doLayout to refresh the view which causes any
                            // unrendered child Components to be rendered
                            POST.locationMapPanel.doLayout();
                        }
                    }
                });
            }
            item_menus.push({
                'text': 'Settings',
                handler: function (cmp, evt) {
                    console.debug(record.get('title') + ' layer settings');
                    var settings_win = new MASAS.Layers.SettingsWindow(record,
                        record.store);
                    settings_win.show();
                }
            });
            if (record.get('legend_url')) {
                item_menus.push({
                    'text': 'Legend',
                    handler: function (cmp, evt) {
                        console.debug(record.get('title') + ' layer legend');
                        var legend_win = new MASAS.Layers.LegendWindow(record);
                        legend_win.show();
                        legend_win.alignTo(POST.locationMapPanel.el, 'tr-tr?', [0, 2]);
                    }
                });
            }
            if (!record.get('permanent')) {
                item_menus.push({
                    'text': 'Delete',
                    handler: function (cmp, evt) {
                        Ext.Msg.confirm('Delete Layer',
                            'Are you sure that you want to delete this layer?',
                            function (confirm) {
                                if (confirm === 'yes') {
                                    console.debug('Removing layer ' +
                                        record.get('title'));
                                    layer_store.remove(record);
                                }
                            }
                        );
                    }
                });
            }
            
            var item = new Ext.menu.CheckItem({
                text: record.get('title'),
                checked: record.getLayer().getVisibility(),
                // a radio button group instead for base layers
                group: (record.get('base_layer')) ? 'baseLayers' : null,
                menu: {
                    plain: true,
                    items: item_menus
                },
                listeners: {
                    checkchange: function (item, checked) {
                        console.debug(record.get('title') + ' layer turned ' +
                            ((checked) ? 'on': 'off'));
                        var rec_layer = record.getLayer();
                        if (record.get('base_layer')) {
                            // base layers use radio buttons and checkchange
                            // is first fired on the original layer's radio
                            // with checked=false, then it fires for the new
                            // layer's radio with checked=true.  Visibility
                            // changes are taken care of by setBaseLayer
                            if (checked) {
                                POST.locationMap.setBaseLayer(rec_layer);
                            }
                        } else {
                            // load the layer again, instead of from cache, when
                            // the user turns a layer off then on again
                            if (checked) {
                                if (!rec_layer.getVisibility()) {
                                    rec_layer.redraw(true);
                                }
                            }
                            rec_layer.setVisibility(checked);
                        }
                        // update an associated toolbar button without triggering
                        // again and causing a loop, especially toggle groups
                        if (record.get('button')) {
                            record.get('button').toggle(checked, true);
                        }
                    }
                }
            });
            this.add(item);
            // saves the new menu reference each time its rebuilt so any toolbar
            // buttons can access it, without firing any record/store events
            record.data['menu'] = item;
        
        }, this);
        
        if (this.createNewLayers) {
            this.add('-');
            this.add(new Ext.menu.Item({
                text: '<b>Add New Layer</b>',
                handler: function (cmp, evt) {
                    console.debug('Add new layer settings');
                    var settings_win = new MASAS.Layers.SettingsWindow(null,
                        layer_store);
                    settings_win.show();
                }
            }) );
        }
    }
});


/**
 * Factory function that creates a window to add/update a layer's settings.
 *
 * @param {Object} - the layer record, if updating
 * @param {Object} - the layer store
 */
MASAS.Layers.SettingsWindow = function (record, store) {
    var form_items = [{
        xtype: 'textfield',
        name: 'title',
        fieldLabel: 'Name',
        width: 200,
        allowBlank: false,
        readOnly: (record && record.get('permanent')) ? true : false
    }, {
        xtype: 'combo',
        name: 'type',
        fieldLabel: 'Type',
        width: 150,
        // by not allowing someone to type a value in here, it makes
        // selection on a tablet device much easier
        editable: false,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        readOnly: (record) ? true : false,
        displayField: 'name',
        valueField: 'mimeType',
        allowBlank: false,
        store: new Ext.data.ArrayStore({
            id: 1,
            fields: ['name', 'mimeType'],
            // the types supported by this layer store
            data: store.layer_types
        }),
        listeners: {
            change: function (cmp, newValue) {
                // WMS layers can add a legend as well
                if (newValue === 'application/vnd.ogc.wms') {
                    cmp.ownerCt.legend.show();
                } else {
                    cmp.ownerCt.legend.hide();
                }
            }
        }
    }, {
        xtype: 'textfield',
        name: 'url',
        fieldLabel: 'URL',
        width: 350,
        allowBlank: false,
        readOnly: (record && record.get('permanent')) ? true : false
    }, {
        xtype: 'textfield',
        name: 'legend_url',
        // accessible as .legend for 'type' changes
        ref: 'legend',
        fieldLabel: 'Legend',
        width: 350,
        // WMS layers offer a legend, all others don't
        allowBlank: true,
        hidden: true
    }];
    if (!store.base_layers) {
        // don't allow opacity changes for base layers
        form_items.push({
            xtype: 'combo',
            name: 'opacity',
            fieldLabel: 'Opacity',
            width: 100,
            // by not allowing someone to type a value in here, it makes
            // selection on a tablet device much easier
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'name',
            valueField: 'val',
            value: '0.5',
            allowBlank: false,
            store: new Ext.data.ArrayStore({
                id: 1,
                fields: ['name', 'val'],
                data: [['Light', '0.2'], ['Medium', '0.5'], ['Dark', '0.9']]
            })
        });
    }
    if (record && record.get('attribution')) {
        form_items.push({
            xtype: 'displayfield',
            fieldLabel: 'Source',
            html: record.get('attribution')
        });
    }
    if (record && record.get('description')) {
        form_items.push({
            xtype: 'displayfield',
            fieldLabel: 'Description',
            html: record.get('description')
        });
    }
    
    return new Ext.Window({
        title: 'Layer Settings',
        layout: 'fit',
        width: 475,
        height: 250,
        buttonAlign: 'left',
        layerRecord: record,
        layerStore: store,
        items: new Ext.FormPanel({
            // from the Window this is accessed as .formPanel
            ref: 'formPanel',
            labelWidth: 70,
            bodyStyle: 'padding: 3px 5px;',
            autoScroll: true,
            items: form_items
        }),
        buttons: [{
            text: 'Help',
            handler: MASAS.Layers.show_layer_help
        }, '->', {
            text: (record) ? 'Update' : 'Add',
            handler: function (cmp) {
                var settings_win = cmp.ownerCt.ownerCt;
                var settings_form = settings_win.formPanel.getForm();
                if (!settings_form.isValid()) {
                    return;
                }
                if (record) {
                    console.debug('Updating layer settings');
                    // in addition to updating the record, should fire the
                    // 'update' event that updates the layer as well
                    settings_form.updateRecord(settings_win.layerRecord);
                    // save this change in the store and in this record
                    settings_win.layerStore.commitChanges();
                } else if (store) {
                    var settings_vals = settings_form.getValues();
                    // should be visible when its first added
                    settings_vals.visibility = true;
                    // have to set the values manually as the ExtJS valueFields
                    // aren't retrieved
                    settings_vals.type = settings_form.findField('type').getValue();
                    if (!store.base_layers) {
                        settings_vals.opacity = settings_form.findField('opacity').getValue();
                    }
                    // simplistic check, not able to do in the layer reader
                    var duplicate_check = settings_win.layerStore.findExact('url',
                        settings_vals.url);
                    if (duplicate_check > -1) {
                        console.debug('Duplicate layer: ' + settings_vals.url);
                        //TODO: warn the user too?
                    } else {
                        console.debug('Adding new layer ' + settings_vals.title);
                        settings_win.layerStore.loadData(settings_vals, true);
                        //TODO: if this is an overlay layer, it will be visible
                        //      as part of adding it, however for base layers
                        //      the user must use the menu again to activate
                        //      the new base layer.
                    }
                }
                settings_win.close();
            }
        }, {
            text: 'Cancel',
            handler: function (cmp) {
                cmp.ownerCt.ownerCt.close();
            }
        }],
        listeners: {
            render: function (cmp) {
                if (cmp.layerRecord) {
                    cmp.formPanel.getForm().loadRecord(cmp.layerRecord);
                    if (cmp.layerRecord.get('type') === 'application/vnd.ogc.wms') {
                        cmp.formPanel.legend.show();
                    }
                }
            }
        }
    });
};


/**
 * Factory function that creates a window to display a layer's WMS legend
 *
 * @param {Object} - the layer record
 */
MASAS.Layers.LegendWindow = function (record) {
    return new Ext.Window({
        width: 150,
        autoHeight: true,
        title: 'Legend',
        autoScroll: true,
        // helps support touch users so they can collapse and close easier
        collapsible: true,
        closable: false,
        buttonAlign: 'center',
        items: [{
            html: '<img alt="Legend" height="300" width="125" src="' +
                record.get('legend_url') + '">'
        }],
        buttons: [{
            text: 'Close',
            handler: function (cmp) {
                cmp.ownerCt.ownerCt.close();
            }
        }]
    });
};


/**
 * Layer Settings help window
 */
MASAS.Layers.show_layer_help = function () {
    var win_title = 'Layer Settings Info';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 425,
        height: 450,
        autoScroll: true,
        bodyStyle: {
            'padding': '10px',
            'background-color': 'white'
        },
        html: '<p><b>Name</b> is the name for this layer that' +
            ' will be displayed in the Layers Menu and can be changed at' +
            ' any time.</p>' +
            '<p><b>Type</b> can be selected for a layer when its' +
            ' first created, but cannot be changed later without starting over.' +
            ' There are a limited number of layer types available, select the' +
            ' appropriate one.</p>' +
            '<p><b>URL</b> is required and should be a valid URL' +
            ' that can access this layer.  For layers that are protected by' +
            ' a password with HTTP Basic Authentication, be sure to add' +
            ' the authentication information information as follows: <br>' +
            '<pre> http://username:password@www.example.com</pre>' +
            ' For Web Map Service layers, ensure that the necessary LAYERS' +
            ' and STYLES are part of the URL parameters.  An example:' +
            '<pre> http://www.example.com/wms?LAYERS=xxx&STYLES=xxx </pre>' +
            ' The default map projection is 900913, however if a WMS server' +
            ' does not support it, you can use 4326 instead:' +
            '<pre> &SRS=EPSG:4326</pre>' +
            ' For ArcGIS REST layers, use the export service URL and ensure' +
            ' that any necessary layers are part of the URL parameters.  An example:' +
            '<pre> /rest/services/Map/MapServer/export?layers=show:0 </pre>' +
            ' If the ArcGIS server does not support 900913 you can use the' +
            ' equivalent 3857 or the well known 4326 instead:' +
            '<pre> &BBOXSR=4326&IMAGESR=4326 </pre></p>' +
            '<p><b>Legend</b> is an optional value for Web Map' +
            ' Service layers and enables a Legend popup window for that layer.' +
            ' The URL for the Legend can be entered and it should look' +
            ' like the following with LAYER and STYLE customized to match' +
            ' the legend you want to see.  Password protected layers should' +
            ' follow the same format as noted above for the URL.' +
            '<pre> http://www.example.com/wms?SERVICE=WMS&VERSION=1.1.1</pre>' +
            '<pre>&REQUEST=GetLegendGraphic&LAYER=xxx&STYLE=xxx</pre>' +
            '<pre>&FORMAT=image/png&WIDTH=100&HEIGHT=250</pre></p>' +
            '<p><b>Opacity</b> can be adjusted to make the layer' +
            ' appear lighter or darker on top of the map and other layers.</p>'
    });
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Add a layer button to the toolbar
 * 
 * @param {Object} - layer record
 * @param {Object} - toolbar
 */
MASAS.Layers.add_toolbar_button = function (record, toolbar, do_insert) {
    var is_base = record.get('base_layer');
    var button_config = {
        text: record.get('title'),
        pressed: (record.get('visibility')) ? true : false,
        enableToggle: true,
        toggleHandler: function (btn, state) {
            // base layers can't be toggled 'off', you can only
            // select another base layer.  This silently
            // changes an 'off' attempt back to on.
            if (!state) {
                if (is_base) {
                    btn.toggle(true, true);
                    return;
                }
            }
            var rec_menu = record.get('menu');
            rec_menu.setChecked(state);
        }
    };
    if (is_base) {
        button_config['toggleGroup'] = 'baseLayers';
    }
    // when the user adds a new button, if its a base layer, add it to the
    // left of the separator line
    if (is_base && do_insert) {
        var tb_button = toolbar.insertButton(4, button_config);
    } else {
        var tb_button = toolbar.addButton(button_config);
    }
    // saving record values to be used for toolbar
    // state saving and later recreation
    tb_button.layer_name = record.get('title');
    tb_button.layer_type = record.get('type');
    tb_button.layer_url = record.get('url');
    tb_button.layer_store = record.store.storeId;
    // saving button reference for the menu to access
    // without triggering an update event, prevents
    // looping
    record.data['button'] = tb_button;
};
