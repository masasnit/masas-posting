/**
MASAS Posting Tool - Common Functions
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('MASAS.Common');
Ext.namespace('POST.Common');


/* ExtJS Overrides */

/**
 * Adds the values and keys functions for objects
 * Doesn't overwite if already has these functions
 */
Ext.applyIf(Ext, {
    getKeys: function (obj) {
        var keys = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }
        return keys;
    },
    getValues: function (obj) {
        var vals = [];
        for (var k in obj) {
            if (obj.hasOwnProperty(k)) {
                vals.push(obj[k]);
            }
        }
        return vals;
    }
});


/* Functions that are fairly standalone and may be shared with other tools and
   so they are in the MASAS namespace */

/**
Uses the browser's builtin parser for XML

@param {String} - the plain text version of some XML
@return {Object} - an XML DOM object
*/
MASAS.Common.parse_xml = function (xml) {
    var doc = null;
    if (window.DOMParser) {
        try {
            doc = (new DOMParser()).parseFromString(xml, 'text/xml');
        } catch (err) {
            doc = null;
        }
    } else if (window.ActiveXObject) {
        try {
            doc = new ActiveXObject('Microsoft.XMLDOM');
            doc.async = false;
            if (!doc.loadXML(xml)) {
                doc = null;
            }
        } catch (err) {
            doc = null;
        }
    }
    return doc;
};


/**
Clones an object

@param {Object} - the source object to be cloned
@return {Object} - a new object cloned from the source
*/
MASAS.Common.clone_object = function (obj) {
    if (obj === null || typeof(obj) !== 'object') {
        return obj;
    }
    var temp = new obj.constructor(); // changed (twice)
    var key = null;
    for (key in obj) {
        temp[key] = MASAS.Common.clone_object(obj[key]);
    }
    return temp;
};


/**
Nicely formats/pretty-prints XML

@param {String} - XML to be formatted
@return {String} - the newly formatted XML
*/
MASAS.Common.format_xml = function (xml) {
    var reg = /(>)(<)(\/*)/g;
    var wsexp = / *(.*) +\n/g;
    var contexp = /(<.+>)(.+\n)/g;
    xml = xml.replace(reg, '$1\n$2$3').replace(wsexp, '$1\n').replace(contexp, '$1\n$2');
    var formatted = '';
    var lines = xml.split('\n');
    var indent = 0;
    var lastType = 'other';
    // 4 types of tags - single, closing, opening, other (text, doctype, comment) - 4*4 = 16 transitions 
    var transitions = {
        'single->single': 0,
        'single->closing': -1,
        'single->opening': 0,
        'single->other': 0,
        'closing->single': 0,
        'closing->closing': -1,
        'closing->opening': 0,
        'closing->other': 0,
        'opening->single': 1,
        'opening->closing': 0,
        'opening->opening': 1,
        'opening->other': 1,
        'other->single': 0,
        'other->closing': -1,
        'other->opening': 0,
        'other->other': 0
    };
    for (var i = 0; i < lines.length; i++) {
        var ln = lines[i];
        var single = Boolean(ln.match(/<.+\/>/)); // is this line a single tag? ex. <br />
        var closing = Boolean(ln.match(/<\/.+>/)); // is this a closing tag? ex. </a>
        var opening = Boolean(ln.match(/<[^!].*>/)); // is this even a tag (that's not <!something>)
        var type = single ? 'single' : closing ? 'closing' : opening ? 'opening' : 'other';
        var fromTo = lastType + '->' + type;
        lastType = type;
        var padding = '';

        indent += transitions[fromTo];
        for (var j = 0; j < indent; j++) {
            padding += '  ';
        }
        if (fromTo === 'opening->closing') {
            formatted = formatted.substr(0, formatted.length - 1) + ln + '\n'; // substr removes line break (\n) from prev loop
        } else {
            formatted += padding + ln + '\n';
        }
    }
    return formatted;
};


/**
Encode any XML reserved characters in a block of text, such as
<tag>here & there</tag>

@param {String} - text to check for reserved characters
@return {String} - text which has had entities substituted instead
*/
MASAS.Common.xml_encode = function (text) {
    if (!text) {
        // blank or missing element support
        return '';
    }
    text = text.replace(/\&/g, '&amp;');
    text = text.replace(/</g, '&lt;');
    text = text.replace(/>/g, '&gt;');
    return text;
};


/**
Decode any entities back into reserved XML characters

@param {String} - text to check for entities
@return {String} - text which has had entities replaced with characters
*/
MASAS.Common.xml_decode = function (text) {
    if (!text) {
        // blank or missing element support
        return '';
    }
    text = text.replace(/&amp;/g, '&');
    text = text.replace(/&lt;/g, '<');
    text = text.replace(/&gt;/g, '>');
    return text;
};


/**
Constants that will be set for time operations
*/
// used to convert from UTC to local time, ie for Eastern its -4
MASAS.Common.UTC_Local_Offset = new Date().getTimezoneOffset() / -60;
// used to convert from local time to UTC, ie for Eastern is +4
MASAS.Common.Local_UTC_Offset = new Date().getTimezoneOffset() / 60;


/**
Change a datetime value to account for a timezone offset.  Similar to view tool.

@param {Date} - datetime object to change
@param {Integer} - the timezone offset in +/- hours
@return {Date} - adjusted datetime object
*/
MASAS.Common.adjust_time = function (date, offset) {
    var dt = date.clone();
    if (offset !== 0) {
        var hrOff = parseInt(offset, 10);
        var minOff = offset % hrOff * 60;
        //test for non-integer offsets
        if (minOff) {
            var min = dt.getMinutes() + minOff;
            dt.setMinutes(min);
        }
        var hr = dt.getHours() + hrOff;
        dt.setHours(hr);
    }
    return dt;
};


/**
Change a normal Atom datetime string in ISO with UTC for the timezone
to a new datetime string using the local browser's timezone. 

@param {String} - ISO datetime string in UTC
@return {String} - the local datetime string
*/
MASAS.Common.local_time_adjust = function (date) {
    var date_adj = Date.parseDate(date, 'Y-m-d\\TH:i:s\\Z');
    date_adj = MASAS.Common.adjust_time(date_adj, MASAS.Common.UTC_Local_Offset);
    return Ext.util.Format.date(date_adj, 'l, M j Y - H:i:s');
};


/* Functions that have POST dependencies and so are in the POST namespace */

/**
When selecting an entry/alert to modify, show a GeoExt popup on the map
with some basic details about that entry/alert.

@param {Object} - the event object with information about the map
feature that was selected
*/
POST.Common.show_select_popup = function (evt) {
    if (evt.feature) {
        // clear an existing popup first
        if (evt.feature.popup) {
            evt.feature.popup.destroy();
            evt.feature.popup = null;
        }
        // don't show a popup for an already selected feature, such
        // as when a user has hovered, then clicked to select
        if (evt.feature.isSelected) {
            return;
        }
        // start out with normal popup content
        var content = '<div> Author: ' + evt.feature.attributes.author_name + '</div>' +
            '<div style="padding: 5px;">' + evt.feature.attributes.content + '</div>';
        // highlight the corresponding grid record
        var grid_view = POST.selectGridPanel.getView();
        var row_num = POST.selectStore.find('fid', evt.feature.fid);
        // the icon may be visible on the map but not in the table because
        // it was filtered out, either as Entry or Alert depending, note the find
        // returns -1 while there can be valid row 0
        if (row_num !== -1) {
            grid_view.focusRow(row_num);
            Ext.fly(grid_view.getRow(row_num)).addClass('x-grid3-row-selected');
        } else {
            // add to the popup content
            content += '<div style="padding: 5px; color: #FF3333;">' +
                'Unable to modify based on current selection criteria </div>';
        }
        evt.feature.popup = new GeoExt.Popup({
            title: evt.feature.attributes.title,
            html: content,
            location: evt.feature,
            width: 350,
	        minHeight: 100,
	        autoScroll: true,
	        // hide the max/min, and pin buttons
	        closable: true,
	        collapsible: false,
	        unpinnable: false,
	        panIn: false
        });
        evt.feature.popup.show();
        // display associated geometry in addition to the popup if its not
        // already present on the map, ignores points for now
        if (evt.feature.originalGeom && !evt.feature.originalGeomFeature) {
            if (evt.feature.originalGeom.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
                var new_geom = evt.feature.originalGeom.clone();
                var new_feature = new OpenLayers.Feature.Vector(new_geom);
                POST.selectLayers.geometry.addFeatures([new_feature]);
                evt.feature.originalGeomFeature = new_feature;
            }
        }
    }
};


/**
After selecting an entry/alert to modify, close its info popup.

@param {Object} - the event object with information about the map
feature that was selected
*/
POST.Common.close_select_popup = function (evt) {
    if (evt.feature && evt.feature.popup) {
        evt.feature.popup.destroy();
        evt.feature.popup = null;
        // ensure grid is unfocused
        var grid_view = POST.selectGridPanel.getView();
        var row_num = POST.selectStore.find('fid', evt.feature.fid);
        if (row_num !== -1) {
            Ext.fly(grid_view.getRow(row_num)).removeClass('x-grid3-row-selected');
        }
        // remove associated geometry
        if (evt.feature.originalGeomFeature) {
            POST.selectLayers.geometry.removeFeatures([evt.feature.originalGeomFeature]);
            evt.feature.originalGeomFeature = null;
        }
    }
};


/**
Loads current feed entries from the Hub chosen for posting based on the
viewport of the Location map.  Two layers used, one for the icon locations
and the other to display the original geometry, both geometries are provided
by the MASAS feed parser.
*/
POST.Common.load_current_entries = function () {
    var current_layer = POST.locationLayers.current;
    // append the parameters as needed to load the current feed
    var feed_url = OpenLayers.Util.urlAppend(POST.FEED_URL,
        'secret=' + POST.USER_SECRET + '&lang=en');
    // current viewport only, creating new bounds because of the transform
    var current_bounds = POST.locationMap.getExtent().toArray();
    var current_view = new OpenLayers.Bounds.fromArray(current_bounds);
    current_view.transform(POST.locationMap.getProjectionObject(),
        new OpenLayers.Projection('EPSG:4326'));
    feed_url += '&bbox=' + current_view.toBBOX(4);
    // cache busting parameter
    feed_url += '&_dc=' + new Date().getTime();
    // support proxies
    if (POST.AJAX_PROXY_URL) {
        feed_url = POST.AJAX_PROXY_URL + encodeURIComponent(feed_url);
    }
    POST.locationControls.currentLoading.maximizeControl();
    if (!current_layer.protocol.url) {
        current_layer.protocol.url = feed_url;
        current_layer.protocol.options.url = feed_url;
        // this will also load the data the first time its used
        current_layer.setVisibility(true);
    } else {
        current_layer.protocol.url = feed_url;
        current_layer.protocol.options.url = feed_url;
        current_layer.setVisibility(true);
        // forcing a reload following times since url may have changed
        current_layer.strategies[0].load();
    }
};


/**
Display a basic info popup for current feed entries.  Geometry for the entry
is not displayed on the map, like the viewing tool, just the icon.

@param {Object} - the event object with information about the map
feature that was selected
*/
POST.Common.show_current_popup = function (evt) {
    if (evt.feature) {
        if (evt.feature.popup) {
            if (evt.feature.popup.draggable) {
                // if the window pin has been pushed, this window will be moveable
                // and so should then need to use the X to close.
                return;
            }
            evt.feature.popup.destroy();
            evt.feature.popup = null;
        }
        var updated_adj = MASAS.Common.local_time_adjust(evt.feature.attributes.updated);
        var content = '<div> Updated: ' + updated_adj + '</div>' +
            '<div> Author: ' + evt.feature.attributes.author_name + '</div>' +
            '<div style="padding: 5px;">' + evt.feature.attributes.content + '</div>';
        evt.feature.popup = new GeoExt.Popup({
            title: evt.feature.attributes.title,
            html: content,
            location: evt.feature,
            width: 350,
	        minHeight: 100,
	        autoScroll: true,
	        closable: true,
	        collapsible: false,
	        panIn: false,
	        listeners: {close: function (evt) {
	            // when the close button is clicked after a window has become
	            // draggable, need to reset that value to allow new windows again
	            evt.draggable = false;
	        } }
        });
        evt.feature.popup.show();
        // display associated geometry in addition to the popup if its not
        // already present on the map, ignores points for now
        if (evt.feature.originalGeom && !evt.feature.originalGeomFeature) {
            if (evt.feature.originalGeom.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
                var geom_layer = POST.locationLayers.geometry;
                var new_geom = evt.feature.originalGeom.clone();
                // originalGeom should be in 4326 projection from Atom/GeoRSS
                new_geom.transform(new OpenLayers.Projection('EPSG:4326'),
                    POST.locationMap.getProjectionObject());
                var new_feature = new OpenLayers.Feature.Vector(new_geom);
                geom_layer.addFeatures([new_feature]);
                evt.feature.originalGeomFeature = new_feature;
            }
        }
    }
};


/**
After selecting a current feed entry, close its info popup.

@param {Object} - the event object with information about the map
feature that was selected
*/
POST.Common.close_current_popup = function (evt) {
    if (evt.feature && evt.feature.popup) {
        // if the window pin has been pushed, this window will be moveable
        // and so should then need to use the X to close.
        if (!evt.feature.popup.draggable) {
            evt.feature.popup.destroy();
            evt.feature.popup = null;
            // remove associated geometry, however it does stick around if
            // user has clicked the pushpin, then used close box instead
            if (evt.feature.originalGeomFeature) {
                var geom_layer = POST.locationLayers.geometry;
                geom_layer.removeFeatures([evt.feature.originalGeomFeature]);
                evt.feature.originalGeomFeature = null;
            }
        }
    }
};


/**
If a posting attempt times out, displays the failure on the results page.
*/
POST.Common.post_timeout_error = function () {
    //TODO: cancel the do_post request handler
    console.error('Posting Timeout');
    var result_html = '<h1 style="color: red;">Failed Post</h1>' +
        '<div style="margin: 25px;">Posting Timed Out</div>' +
        '<a href="#" onclick="window.close(); return false;" style="color: blue;">Return</a>';
    document.getElementById('postingBox').innerHTML = result_html;
};


/**
After a successful post, display results and additional operations such
as reviewing the new entry and forwarding via email.

@param {Object} - The XMLHttpRequest Object
*/
POST.Common.post_complete_result = function (xhr) {
    var result_html = '';
    clearTimeout(POST.postingTimer);
    // needs to be ==  not  ===
    if (xhr.status == '200' || xhr.status == '201') {
        POST.POST_RESULT = xhr.responseText;
        result_html = '<h1 style="color: green;">Successful Post</h1>' +
            '<a href="#" onclick="POST.Common.review_new_entry(); return false;" style="color: grey;">Review New Entry</a>' +
            '<a href="#" onclick="window.close(); return false;" style="color: blue;">Return</a>';
        if (POST.EMAIL_ADDRESS_LIST) {
            Ext.getCmp('email-to-fieldset').show();
        }
        console.debug('Successful Post');
    } else {
        var postingErrorText = xhr.responseText.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        result_html = '<h1 style="color: red;">Failed Post</h1>' +
            '<div style="margin: 25px;"><pre>' + postingErrorText + '</pre></div>' +
            '<a href="#" onclick="window.close(); return false;" style="color: blue;">Return</a>';
        console.error('Posting Error: ' + postingErrorText);
    }
    document.getElementById('postingBox').innerHTML = result_html;
};


/**
Create a popup window to review the newly created entry.
*/
POST.Common.review_new_entry = function () {
    // only one review window allowed
    if (POST.reviewWindow) {
        POST.reviewWindow.destroy();
    }
    
    var display_xml = POST.POST_RESULT;
    var display_html = '';
    try {
        var atom_json = xmlJsonClass.xml2json(MASAS.Common.parse_xml(POST.POST_RESULT),
	    '  ');
        var newAtom = JSON.parse(atom_json);
        // make some changes to Atom values for template presentation.
        // if a prefix is in use it needs to be changed for the template
        // to work, : isn't allowed
	if (newAtom.entry.hasOwnProperty('met:effective')) {
            newAtom.entry.effective = newAtom.entry['met:effective'];
        }
        if (newAtom.entry.hasOwnProperty('age:expires')) {
            newAtom.entry.expires = newAtom.entry['age:expires'];
        }
        // reformat updated, effective, and expires to local time
        newAtom.entry.updated = MASAS.Common.local_time_adjust(newAtom.entry.updated);
	if (newAtom.entry.effective) {
            newAtom.entry.effective = MASAS.Common.local_time_adjust(newAtom.entry.effective);
        }
        if (newAtom.entry.expires) {
            newAtom.entry.expires = MASAS.Common.local_time_adjust(newAtom.entry.expires);
        }
        // XTemplate from post-templates.js
        display_html += MASAS.Template.EntryToHTML.apply(newAtom);
    } catch (err) {
        console.error('Review Entry Template Error: ' + err);
        alert('Entry Template Error.');
        return;
    }
    display_xml = display_xml.replace(/</g, '&lt;');
    display_xml = display_xml.replace(/>/g, '&gt;');
    display_html += '<div class="reviewClose"><a href="#" onclick="' +
        'POST.reviewWindow.close(); return false;">Close Review</a></div>' +
        '<div class="reviewXmlShow"><a href="#" onclick="' +
        'document.getElementById(\'review-xml-box\').style.display=\'block\'; return false;">' +
        'Show XML</a></div><div id="review-xml-box" style="display: none;">' +
        '<pre>' + display_xml + '</pre></div>';
    
    POST.reviewWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'New Entry Review <a class="titleClose" href="#" ' +
            'onclick="POST.reviewWindow.close(); return false;">Close</a>',
        closable: true,
        width: 700,
        height: 500,
        autoScroll: true,
        bodyStyle: 'background-color: white;',
        layout: 'fit',
        html: display_html
    });
    POST.reviewWindow.show(this);
};


/**
When the email forwarding fieldset is opened, generate the text content for
the display/form fields.
*/
POST.Common.generate_email_content = function () {
    var message_subject = '';
    var message_txt = '';
    
    if (POST.EMAIL_HEADER) {
        message_txt += POST.EMAIL_HEADER;
    }
    
    message_subject += 'Fwd: MASAS ';
    message_txt += '\n\n--- Forwarded MASAS ';
    if (POST.CAP_MESSAGE) {
        message_subject += 'Alert Message: ';
        message_txt += 'Alert Message ';
    } else {
        message_subject += 'Entry: ';
        message_txt += 'Entry ';
    }
    message_txt += '---\n\n';
    
    if (POST.CAP_MESSAGE) {
        var cap_xml_vals = POST.generate_cap_xml();
        try {
            // XTemplate from post-templates.js
            message_txt += MASAS.Template.CAPToEmail.apply(cap_xml_vals[1]);
        } catch (err) {
            console.error('Email CAP Template Error: ' + err);
            alert('CAP Template Error.');
        }
    }
    
    try {
        var atom_json = xmlJsonClass.xml2json(MASAS.Common.parse_xml(POST.POST_RESULT),
	    '  ');
        var newAtom = JSON.parse(atom_json);
        // make some changes to Atom values for template presentation.
        // if a prefix is in use it needs to be changed for the template
        // to work, : isn't allowed
	if (newAtom.entry.hasOwnProperty('met:effective')) {
            newAtom.entry.effective = newAtom.entry['met:effective'];
        }
        if (newAtom.entry.hasOwnProperty('age:expires')) {
            newAtom.entry.expires = newAtom.entry['age:expires'];
        }
        // reformat updated, effective, and expires to local time
        newAtom.entry.updated = MASAS.Common.local_time_adjust(newAtom.entry.updated);
	if (newAtom.entry.effective) {
            newAtom.entry.effective = MASAS.Common.local_time_adjust(newAtom.entry.effective);
        }
        if (newAtom.entry.expires) {
            newAtom.entry.expires = MASAS.Common.local_time_adjust(newAtom.entry.expires);
        }
        // XTemplates from post-templates.js
        if (POST.CAP_MESSAGE) {
            message_txt += '\n--- Entry Summary ---\n\n';
            message_txt += MASAS.Template.EntrySummaryToEmail.apply(newAtom);
        } else {
            message_txt += MASAS.Template.EntryToEmail.apply(newAtom);
        }
        try {
            if (newAtom.entry.title.div.div instanceof Array) {
                // assuming first value is the english one
                if (newAtom.entry.title.div.div[0]['#text']) {
                    message_subject += newAtom.entry.title.div.div[0]['#text'];
                }
            } else if (newAtom.entry.title.div.div['#text']) {
                message_subject += newAtom.entry.title.div.div['#text'];
            }
        } catch (err) {
            console.error('Email Entry Title Error: ' + err);
        }
    } catch (err) {
        console.error('Email Entry Template Error: ' + err);
        alert('Entry Template Error.');
    }
    
    if (POST.EMAIL_FOOTER) {
        message_txt += POST.EMAIL_FOOTER;
    }
    
    Ext.getCmp('email-to-message').setValue(message_txt);
    Ext.getCmp('email-to-subject').setValue(message_subject);
    
    if (POST.attachmentStore) {
        if (POST.attachmentStore.getCount() > 0) {
            Ext.getCmp('email-attachment-notice').show();
        }
    }
};


/**
Forward the entry summary email, submitting the form fields to
the server proxy.

@param {Object} - the Send button Object
*/
POST.Common.post_email_message = function (btn) {
    var email_to = Ext.getCmp('email-to-address').getValue();
    if (!email_to) {
        alert('Email To address not selected.');
        return;
    }
    var email_subject = Ext.getCmp('email-to-subject').getValue();
    if (!email_subject) {
        alert('Email Subject not entered.');
        return;
    }
    var email_message = Ext.getCmp('email-to-message').getValue();
    if (!email_message) {
        alert('Email Message not entered.');
        return;
    }
    // this check and disabling the button prevents multiple attempts
    // due to any delay in sending to server
    if (btn.already_sent) {
        if (!confirm('Do you want to send this email again?')) {
            return;
        }
    } else {
        btn.already_sent = true;
    }
    btn.disable();
    var do_post = new OpenLayers.Request.POST({
        url: POST.EMAIL_FORWARD_URL,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: OpenLayers.Util.getParameterString({'to': email_to,
            'subject': email_subject, 'message': email_message}),
        callback: function (xhr) {
            btn.enable();
            // needs to be ==  not  ===
            if (xhr.status == '200' || xhr.status == '201') {
                console.debug('Email Forward Successful');
                btn.label.update('<span style="color: green; font-weight: bold;"> Sent </span>');
            } else {
                console.error('Email Forward Failed');
                btn.label.update('<span style="color: red; font-weight: bold;"> Error </span>');
            }
        }
    });
};


/**
Determine whether a URL is a MASAS protected URL requiring a secret and returns
the appropriate secret value.  This ensures that for outside MASAS URLs the
user's secret will not be exposed.

@param {String} - the target URL
@return {String} - the MASAS secret
*/
POST.Common.check_url_secret = function (url) {
    // first checking the current feed URL
    if (url.search(POST.FEED_URL) !== -1) {
	return POST.USER_SECRET;
    }
    // checking any other available feeds
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 0) {
	for (var i; i < POST.FEED_SETTINGS.length; i++) {
	    if (url.search(POST.FEED_SETTINGS[i]['url']) !== -1) {
		return POST.FEED_SETTINGS[i]['secret'];
	    }
	}
    }
    
    // a default value instead
    return 'none';
};


/**
 * Cleanup stray GeoExt popup windows which stick around between panel changes
 *
 * @param {Object} - the map layer to cleanup
 */
POST.Common.layer_popup_cleanup = function (map_layer) {
    if (map_layer.features) {
        for (var i = 0; i < map_layer.features.length; i++) {
            var layer_feature = map_layer.features[i];
            if (layer_feature.popup) {
                layer_feature.popup.destroy();
            }
        }
    }
};
