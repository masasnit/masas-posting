/**
MASAS Posting Tool - KML Drawing Layer
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

Classes and functions used to support drawing and attaching KML layers
*/

/*global POST,Ext,OpenLayers,GeoExt,google */

Ext.ns('POST.KML');

// instance placeholder
POST.KML.settingsWindow = null;


/**
 * Factory function to create a window to display and input KML layer settings
 *
 * @return {Object} - settings window
 */
POST.KML.SettingsWindow = function () {
    var formPanel = new Ext.FormPanel({
        ref: 'form_panel',
        frame: true,
        fileUpload: true,
        labelWidth: 25,
        buttonAlign: 'left',
        // used to save layer's attachment location number as returned by proxy
        // so it can be deleted if needed
        attachNum: null,
        items: [{
            xtype: 'textfield',
            fieldLabel: '<b>Title</b>',
            name: 'attachment-title',
            ref: 'attachTitle',
            width: 150,
            allowBlank: false,
            msgTarget: 'side'
        }, {
            xtype: 'hidden',
            name: 'attachment-file',
            ref: 'attachFile',
            value: ''
        }, {
            xtype: 'hidden',
            name: 'kml',
            value: 'yes'
        }],
        buttons: [{
            text: 'Help',
            width: 40,
            handler: POST.KML.show_settings_help
        }, '->', {
            text: 'Delete',
            ref: '../deleteBtn',
            width: 50,
            disabled: true,
            handler: function (btn) {
                var d_confirm = confirm('Are you sure you want to delete?');
                if (d_confirm === true) {
                    Ext.Ajax.request({
                        url: OpenLayers.Util.urlAppend(POST.REMOVE_ATTACH_URL,
                            'num=' + formPanel.attachNum),
                        method: 'DELETE',
                        success: function () {
                            console.debug('Deleted layer attachment ' +
                                formPanel.attachNum);
                            formPanel.attachNum = null;
                            // will need to save again before deleting
                            btn.disable();
                            formPanel.saveBtn.enable();
                            // update the result display
                            Ext.getCmp('entry-kml-notice').setValue('');
                        },
                        failure: function (response) {
                            console.error('Delete Layer Error: ' + response.responseText);
                            alert('Unable to delete.');
                        }
                    });
                }
            }
        }, {
            text: 'Save',
            ref: '../saveBtn',
            width: 50,
            handler: function (btn) {
                var form_cmp = formPanel.getForm();
                if (form_cmp.isValid()) {
                    var kml_str = POST.KML.export_kml(POST.locationLayers.kml,
                        formPanel.attachTitle.getValue());
                    if (!kml_str) {
                        console.debug('Save layer attempt with no features');
                        alert('Draw features before saving this layer.');
                        return;
                    }
                    // hidden field update
                    formPanel.attachFile.setValue(kml_str);
                    // submit same as attachments
                    form_cmp.submit({
                        url: POST.ADD_ATTACH_URL,
                        submitEmptyText: false,
                        waitMsg: 'Saving...',
                        success: function (form, response) {
                            formPanel.attachNum = response.result.num;
                            console.debug('Saved layer attachment ' +
                                formPanel.attachNum);
                            // will need to delete before saving again
                            btn.disable();
                            formPanel.deleteBtn.enable();
                            // update the result display
                            Ext.getCmp('entry-kml-notice').setValue('<b>Attached Layer:</b>' +
                                '&nbsp;&nbsp;' + formPanel.attachTitle.getValue());
                            POST.USE_ATTACH_PROXY = true;
                        },
                        failure: function (form, response) {
                            console.error('Save Layer Error: ' +
                                response.result.message);
                            alert('Unable to save.  ' + response.result.message);
                        }
                    });
                }
            }
        }]
    });
    
    return new Ext.Window({
        width: 250,
        height: 100,
        title: 'KML Layer',
        closable: false,
        items: [formPanel]
    });
};


/**
 * After drawing/saving a feature, show a window to enter additional information
 *
 * @param {Object} - OpenLayers feature
 */
POST.KML.show_feature_window = function (feature) {
    // deselect whatever control was used to draw/save this feature to set
    // the map back up as originally viewed by user
    var feature_controls = POST.locationMap.getControlsBy('featureControl', true);
    for (var c = 0; c < feature_controls.length; c++) {
        feature_controls[c].deactivate();
    }
    // info form
    var form_items = [{
        xtype: 'textfield',
        name: 'name',
        fieldLabel: '<b>Name</b>',
        width: 350,
        allowBlank: false
    }, {
        xtype: 'textarea',
        name: 'description',
        fieldLabel: 'Description',
        height: 50,
        width: 350,
        allowBlank: true
    }];
    // point needs an icon, other geometry needs colour
    if (feature.geometry && feature.geometry instanceof OpenLayers.Geometry.Point) {
        var iconTree = new Ext.tree.TreePanel({
            id: 'kml-icon-select-tree',
            autoScroll: true,
            animate: true,
            border: true,
            height: 125,
            width: 325,
            loader: new Ext.tree.TreeLoader({
                dataUrl: POST.ICON_LIST_URL,
                requestMethod: 'GET',
                preloadChildren: true,
                // fix a preloadChildren bug, was to be fixed in ExtJS 3.3.2 but
                // doesn't appear to be as of 3.4.0, old ticket URL not longer working
                // http://code.extjs.com:8080/ext/ticket/1430
                load : function (node, callback, scope) {
                    if (this.clearOnLoad) {
                        while (node.firstChild) {
                            node.removeChild(node.firstChild);
                        }
                    }
                    if (this.doPreload(node)) { // preloaded json children
                        this.runCallback(callback, scope || node, [node]);
                    } else if (this.directFn || this.dataUrl || this.url) {   
                        // MB
                        if (this.preloadChildren) {
                            if (typeof(callback) !== 'function') {
                                callback = Ext.emptyFn;
                            }
                            callback = callback.createInterceptor(function (node) {
                                for (var i = 0; i < node.childNodes.length; i++) {
                                    this.doPreload(node.childNodes[i]);
                                }
                            }, this);
                        }
                        // end-MB
                        this.requestData(node, callback, scope || node);
                    }
                }
            }),
            root: {
                nodeType: 'async',
                text: 'Root Node'
            },
            rootVisible: false,
            listeners: {
                render: function () {
                    this.getRootNode().expand();
                },
                
                click: function (node) {
                    if (node.attributes.term !== null) {
                        var icon_url = POST.ICON_PREVIEW_URL + node.attributes.term +
                            '/small.png';
                        Ext.getCmp('kml-icon-preview-box').setValue('<div style="padding: 55px 0px 0px 15px;"><img src="' +
                            icon_url + '" alt="Icon"></div>');
                    }
                },
                
                // restore a previously selected icon for existing features
                // once the tree's values have loaded
                load: function () {
                    if (feature.attributes.icon) {
                        // the preloadChildren patch in the tree loader is currently needed
                        // for this findChild to work
                        var node_match = iconTree.root.findChild('term',
                            feature.attributes.icon, true);
                        if (node_match) {
                            iconTree.expandPath(node_match.getPath(), null,
                                function (e_success, e_last) {
                                    if (e_success) {
                                        e_last.select();
                                    }
                                }
                            );
                            var icon_url = POST.ICON_PREVIEW_URL + feature.attributes.icon +
                                '/small.png';
                            Ext.getCmp('kml-icon-preview-box').setValue('<div style="padding: 55px 0px 0px 15px;"><img src="' +
                                icon_url + '" alt="Icon"></div>');
                        }
                    }
                }
            },
            tbar: ['Search:', {
                xtype: 'trigger',
                width: 100,
                triggerClass: 'x-form-clear-trigger',
                onTriggerClick: function () {
                    this.setValue('');
                    iconTree.filter.clear();
                },
                enableKeyEvents: true,
                listeners: {
                    keyup: {buffer: 150, fn: function (field, e) {
                        if (Ext.EventObject.ESC === e.getKey()) {
                            field.onTriggerClick();
                        } else {
                            var val = this.getRawValue();
                            var re = new RegExp('.*' + val + '.*', 'i');
                            iconTree.filter.clear();
                            iconTree.filter.filter(re, 'text');
                        }
                    }}
                }
            }, '->', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeExpand',
                text: 'Expand',
                tooltip: 'Expand the entire icon tree',
                handler: function () {
                    iconTree.expandAll();
                }
            }), '-', new Ext.Button({
                cls: 'x-btn-text-icon',
                iconCls: 'iconTreeCollapse',
                text: 'Collapse',
                tooltip: 'Collapse the entire icon tree',
                handler: function () {
                    iconTree.collapseAll();
                }
            })
            ]
        });
        iconTree.filter = new Ext.ux.tree.TreeFilterX(iconTree);
        form_items.push({
            xtype: 'compositefield',
            fieldLabel: '<b>Icon</b>',
            items: [ iconTree, {
                xtype: 'displayfield',
                id: 'kml-icon-preview-box',
                html: '<div></div>'
            }]
        });
    } else {
        form_items.push({
            xtype: 'combo',
            name: 'colour',
            fieldLabel: '<b>Colour</b>',
            width: 100,
            // by not allowing someone to type a value in here, it makes
            // selection on a tablet device much easier
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            value: 'Blue',
            allowBlank: false,
            // based on category colours
            store: ['Blue', 'Black', 'Red', 'Yellow', 'Green', 'Gray', 'Purple']
        });
    }
    
    var win = new Ext.Window({
        title: 'Feature Settings',
        layout: 'fit',
        width: 500,
        height: 325,
        closable: false,
        items: new Ext.FormPanel({
            // from the Window this is accessed as .formPanel
            ref: 'formPanel',
            labelWidth: 70,
            bodyStyle: 'padding: 3px 5px;',
            autoScroll: true,
            items: form_items
        }),
        buttons: [{
            text: 'Save',
            handler: function (cmp) {
                var settings_win = cmp.ownerCt.ownerCt;
                var settings_form = settings_win.formPanel.getForm();
                // validate the simple fields
                if (!settings_form.isValid()) {
                    return;
                }
                var form_vals = settings_form.getValues();
                // required value
                feature.attributes.name = form_vals.name;
                // optional values
                feature.attributes.description = form_vals.description || '';
                if (form_vals.colour) {
                    feature.attributes.colour = form_vals.colour;
                }
                var icon_tree = Ext.getCmp('kml-icon-select-tree');
                if (icon_tree) {
                    var tree_selection = icon_tree.getSelectionModel().getSelectedNode();
                    if (tree_selection) {
                        feature.attributes.icon = tree_selection.attributes.term;
                    } else {
                        alert('An icon must be selected.');
                        return;
                    }
                }
                if (feature.layer) {
                    // resets rendering after a save
                    if (feature.geometry &&
                    feature.geometry instanceof OpenLayers.Geometry.Point) {
                        feature.renderIntent = 'icon';
                    } else {
                        feature.renderIntent = 'default';
                    }
                    feature.layer.redraw();
                }
                settings_win.close();
            }
        }, {
            text: 'Cancel',
            handler: function (cmp) {
                if (feature.layer) {
                    // resets rendering after a cancelled save attempt
                    if (feature.geometry &&
                    feature.geometry instanceof OpenLayers.Geometry.Point) {
                        //TODO: if a point has been modified and saved again,
                        //      then when another feature is saved, the point's
                        //      style reverts back to default
                        feature.renderIntent = 'icon';
                    } else {
                        feature.renderIntent = 'default';
                    }
                    feature.layer.redraw();
                }
                // don't use cancel to delete existing features, delete button
                // should be used for that instead
                if (!feature.attributes.name) {
                    feature.destroy();
                }
                cmp.ownerCt.ownerCt.close();
            }
        }],
        listeners: {
            render: function (cmp) {
                if (feature.attributes) {
                    // restore previous vlues for existing features except
                    // for icon selections, that's done in treePanel load
                    cmp.formPanel.getForm().setValues(feature.attributes);
                }
            }
        }
    });
    win.show();
};


/**
 * KML Layer Settings help window
 */
POST.KML.show_settings_help = function () {
    var win_title = 'KML Layer Help';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 350,
        height: 400,
        autoScroll: true,
        bodyStyle: {
            'padding': '10px',
            'background-color': 'white'
        },
        html: '<p>The KML Layer allows you to draw additional map features' +
            ' such as points/icons, lines, and polygons on the map that will' +
            ' be attached to the Entry you create.  The <b>Location</b> of an' +
            ' Entry will be the primary feature MASAS users will continue to' +
            ' see, however they will have the option of displaying the new' +
            ' Layer with its supplemental information.</p><br>' +
            ' <p>When you draw a feature, a popup window will ask you' +
            ' for some additional information about that feature:</p>' +
            '<p><b>Name</b> is required and should be a descriptive name' +
            ' for that feature, ie. "Roadblock"</p>' +
            '<p><b>Description</b> is any optional information you may' +
            ' have about that feature, ie. "From South St to Main St."</p>' +
            '<p><b>Icon</b> is required and can be chosen from the existing' +
            ' MASAS Icon list.</p>' +
            '<p><b>Colour</b> is required and can be any colour that is' +
            ' appropriate for this feature.  You may want to provide some' +
            ' context in the description, ie. Red = "Closed"</p><br>' +
            '<p>If you need to change a feature, click the Modify button,' +
            ' make your changes, then click the Save button, which will also' +
            ' allow you to update the feature information.</p><br>' +
            '<p>When you have completed adding features to the KML Layer,' +
            ' give the new Layer a <b>Title</b> ie. "Detour Routes" and click' +
            ' the <b>Save</b> button.  If you need to make any changes after' +
            ' saving you first have to <b>Delete</b> what is stored on the' +
            ' server and then <b>Save</b> the new changes.</p>'
    });
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Export a layer's feature as KML
 *
 * @param {Object} - the OpenLayers layer to export
 * @param {String} - title for the export
 * @return {String} - KML XML
*/
POST.KML.export_kml = function (layer, title) {
    var export_features = [];
    for (var i = 0; i < layer.features.length; i++) {
        export_features.push(layer.features[i]);
    }
    if (export_features.length === 0) {
        return null;
    }
    var kmlWriter = new OpenLayers.Format.KML({
        kmlns: 'http://www.opengis.net/kml/2.2',
        foldersName: title,
        foldersDesc: null,
        externalProjection: new OpenLayers.Projection('EPSG:4326'),
        internalProjection: layer.map.getProjectionObject()
    });
    
    return kmlWriter.write(export_features);
};
