/**
Update/Cancel CAP Alert
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires postTemplates.js
@requires postMap.js
@requires src/common.js
@requires src/validators.js
*/

/*global MASAS,POST,Ext,OpenLayers,GeoExt,xmlJsonClass */
Ext.namespace('POST');
// blank objects to start
POST.selectStore = null;
POST.selectGridPanel = null;
POST.oldAtom = null;
POST.oldCAP = null;
POST.locationMapPanel = null;
POST.areaZoneStore = null;
POST.areaSearchGrid = null;
POST.currentAreaGrid = null;
POST.remoteAlertWindow = null;
POST.reviewWindow = null;
POST.postingTimer = null;
POST.POST_RESULT = '';

// for progress bar indicator
POST.PROGRESS_AMOUNT = 0;
// complete is 1 so divide by number of pages
POST.PROGRESS_INCREMENT = 0.35;


Ext.onReady(function () {
    
    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();
    
    // Custom field validations
    Ext.form.VTypes.location = function (v) {
        // for cases where event location isn't being used
        if (v === 'Not Available') {
            return true;
        }
        var location_formfield = Ext.getCmp('cap-location');
        var v_result = [false, true];
        if (location_formfield.geom === 'point') {
            v_result = POST.Validator.check_point(v);
        } else if (location_formfield.geom === 'line') {
            v_result = POST.Validator.check_line(v);
        } else if (location_formfield.geom === 'polygon') {
            v_result = POST.Validator.check_polygon(v);
        } else if (location_formfield.geom === 'circle') {
            v_result = POST.Validator.check_circle(v);
        } else if (location_formfield.geom === 'box') {
            v_result = POST.Validator.check_box(v); 
        }
        if (v_result[1] === false) {
            alert('Please note that this Location is outside North America.');
        }
        
        return v_result[0];
    };
    Ext.form.VTypes.locationText = 'Must be a valid location';
    
    var vkeyboard_plugin = new Ext.ux.plugins.VirtualKeyboard();
    
    
    // setting up select map, layers, and controls
    POST.Map.initialize_select_map();
    
    // setting up the location map, layers, and controls
    POST.Map.initialize_location_map();
    POST.Map.initialize_drawing_controls(false);
    
    
    var headerBox = new Ext.BoxComponent({
        region: 'north',
        height: 30,
        html: '<div id="headerTitleBlock">MASAS Posting Tool</div>' +
            '<div id="headerDetailBlock">Version 0.2</div>'
    });
    
    var progressBar = new Ext.ProgressBar({
        id: 'progressBar',
        width: 150,
        style: 'margin-left: 25px',
        text: 'Progress'
    });
    
    var cardNav = function (direction) {
        var card_panel = Ext.getCmp('cap-wizard-panel').getLayout();
        var current_card = parseInt(card_panel.activeItem.id.split('card-')[1], 10);
        
        /* Card validation */
        // first card's validation is update/cancel selection, comment out to bypass
        if (!POST.oldCAP) {
            alert('Select an Alert first.');
            return;
        }
        
        // validate this card's form items first, first card has no forms
        if (current_card !== 0) {
            var found_invalid_item = false;
            card_panel.activeItem.cascade(function (item) {
                if (item.isFormField) {
                    if (!item.validate()) {
                        found_invalid_item = true;
                    }
                }
            });
            // form based validation check, comment out to bypass
            if (found_invalid_item) {
                return;
            }
        }
        
        // validation for effective and expires on input card
        if (current_card === 1) {
            var effective_dt = Ext.get('cap-effective-dt').getValue();
            var effective_tm = Ext.get('cap-effective-tm').getValue();
            if (effective_dt && !effective_tm) {
                alert('Effective time must be set.');
                return;
            } else if (!effective_dt && effective_tm) {
                alert('Effective date must be set.');
                return;
            }
            var expires_dt = Ext.get('cap-expires-dt').getValue();
            var expires_tm = Ext.get('cap-expires-tm').getValue();
            if (expires_dt && !expires_tm) {
                alert('Expires time must be set.');
                return;
            } else if (!expires_dt && expires_tm) {
                alert('Expires date must be set.');
                return;
            }
            //TODO: further validation of the values, such as effective must
            //      come before expires?
        }
        
        // special validation for area on location card
        if (current_card === 2) {
            // need to select at least one type of area input
            if (Ext.getCmp('areaCurrentSet').collapsed &&
            Ext.getCmp('areaSelectSet').collapsed &&
            Ext.getCmp('areaCreateSet').collapsed) {
                // area type validation, comment out to bypass
                alert('Area input type is required.');
                return;
            }
            if (POST.currentAreaGrid.doValidation) {
                if (!POST.currentAreaGrid.getSelectionModel().hasSelection()) {
                    // current area select validations, comment out to bypass
                    alert('Area selection is required.');
                    return;
                }
            }
            if (POST.areaSearchGrid.doValidation) {
                if (!POST.areaSearchGrid.getSelectionModel().hasSelection()) {
                    // area select validations, comment out to bypass
                    alert('Area selection is required.');
                    return;
                }
            }
            if (Ext.get('cap-location').getValue() === 'Not Available') {
                // a custom area needs a location point and since validation for
                // the location allows Not Available, disallow if discard isn't checked
                if (!Ext.getCmp('areaCreateSet').collapsed || !Ext.getCmp('discard-event-location').checked) {
                    alert('A Location is Required.');
                    return;
                }
            }
            // when a Custom Area is chosen, search for any applicable
            // SGC geocodes to use, async search as part of leaving this card
            if (!Ext.getCmp('areaCreateSet').collapsed) {
                POST.custom_geocode_search();
            }
        }
        
        // change to the next or previous card
        var next_card = current_card + direction;
        card_panel.setActiveItem(next_card);
        // update the next or previous buttons depending on what card is
        // now active in the stack
        if (next_card === 0) {
            Ext.getCmp('card-prev').setDisabled(true);
        } else if (next_card === 1) {
            Ext.getCmp('card-prev').setDisabled(false);
        } else if (next_card === 2) {
            Ext.getCmp('card-next').setDisabled(false);
        } else if (next_card === 3) {
            Ext.getCmp('card-next').setDisabled(true);
        }
        // progress bar update
        if (direction === 1) {
            POST.PROGRESS_AMOUNT += POST.PROGRESS_INCREMENT;
        } else if (direction === -1) {
            POST.PROGRESS_AMOUNT -= POST.PROGRESS_INCREMENT;
        }
        progressBar.updateProgress(POST.PROGRESS_AMOUNT);
        
        /* Other card operations */
        // cleanup stray GeoExt popup windows which stick around between
        // panel changes
        if (current_card === 0) {
            POST.Common.layer_popup_cleanup(POST.selectLayers.select);
        } else if (current_card === 2) {
            POST.Common.layer_popup_cleanup(POST.locationLayers.location);
        }
        if (next_card === 1) {
            // when loading a past alert and the moving to this card the
            // keyboard icons need to be re-align because they were in place
            // during the 1st render
            if (!Ext.getCmp('cap-add-french').collapsed) {
                Ext.getCmp('cap-fr-headline').alignKeyboardIcon();
                Ext.getCmp('cap-fr-description').alignKeyboardIcon();
                Ext.getCmp('cap-fr-instruction').alignKeyboardIcon();
                Ext.getCmp('cap-fr-contact').alignKeyboardIcon();
            }
        }
    };
    
    /* selectCAPCard */
    
    var historyCombo = new Ext.form.ComboBox({
        id: 'history-interval',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 150,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'period',
        // default is current or future effective
        value: 1,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['name', 'period'],
            data: [
                ['Current/Active', 0],
                ['Current and Future', 1],
                ['Future Effective', 2],
                ['Past 24 Hours', 24],
                ['Past 48 Hours', 48],
                ['Past Week', 168],
                ['Past Month', 720]
            ]
        }),
        listeners: {select: POST.load_user_entries}
    });

    POST.selectStore = new GeoExt.data.FeatureStore({
        layer: POST.selectLayers.select,
        fields: [
            {name: 'icon', type: 'string'},
            {name: 'title', type: 'string'},
            {name: 'published', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'updated', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'effective', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'expires', type: 'date', dateFormat: 'Y-m-d\\TH:i:s\\Z'},
            {name: 'point', type: 'string'},
            {name: 'content', type: 'string'},
            {name: 'links', type: 'auto'},
            {name: 'CAP', type: 'string'}
        ],
        sortInfo: {field: 'updated', direction: "DESC"},
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.HTTP({
                url: '',
                format: new OpenLayers.Format.MASASFeed({
                    internalProjection: POST.selectMap.getProjectionObject(),
                    externalProjection: new OpenLayers.Projection('EPSG:4326')
                })
            })
        }),
        autoLoad: false,
        listeners: {
            load: function () {
                POST.selectControls.loading.minimizeControl();
                this.filterBy(function (record, id) {
                    //console.log(record);
                    if (record.get('CAP') === 'Y') {
                        return true;
                    } else {
                        return false;
                    }
                });
                if (this.data.length > 0) {
                    this.suspendEvents();
                    this.each(function (rec) {
                        rec.set('published', MASAS.Common.adjust_time(rec.get('published'),
                            MASAS.Common.UTC_Local_Offset));
                        rec.set('updated', MASAS.Common.adjust_time(rec.get('updated'),
                            MASAS.Common.UTC_Local_Offset));
                        if (rec.get('effective')) {
                            rec.set('effective', MASAS.Common.adjust_time(rec.get('effective'),
                                MASAS.Common.UTC_Local_Offset));
                        }
                        rec.set('expires', MASAS.Common.adjust_time(rec.get('expires'),
                            MASAS.Common.UTC_Local_Offset));
                    }, this);
                    this.resumeEvents();
                    this.commitChanges();
                }
            },
            exception: function () {
                console.error('Unable to load previous alerts');
                alert('Unable to load previous Alerts.');
            }
        }
    });
    
    // row expander
    var expander = new Ext.ux.grid.RowExpander({
        tpl: new Ext.Template('<p>{content}</p>')
    });
    
    // icons displayed in rows
    function renderEventIcon(val) {
        return '<img alt="Event" height="18" src="' + val + '">';
    }
    
    function renderCloneButton(val) {
        if (val.Atom && val.CAP) {
            return '<button type="button" style="color: blue;" onclick="POST.load_past_alert(\'' +
                val.Atom.href + '\', \'' + val.CAP.href + '\', \'Clone\')">Clone</button>';
        }
    }
    
    function renderUpdateButton(val) {
        if (val.Atom && val.CAP) {
            return '<button type="button" style="color: blue;" onclick="POST.load_past_alert(\'' +
                val.Atom.href + '\', \'' + val.CAP.href + '\', \'Update\')">Update</button>';
        }
    }
    
    function renderCancelButton(val) {
        if (val.Atom && val.CAP) {
            return '<button type="button" style="color: red;" onclick="POST.load_past_alert(\'' +
                val.Atom.href + '\', \'' + val.CAP.href + '\', \'Cancel\')">Cancel</button>';
        }
    }
    
    // expired entry check
    function renderEntryExpires(value, metadata, record) {
        if (!value) {
            value = record.data[this.renderIndex];
        }
        var expires_epoch = value.format('U') * 1000;
        var now_epoch = new Date().getTime();
        if (expires_epoch <= now_epoch) {
            metadata.css = "EntryExpiredTime";
        }
        return value.format('M j H:i:s');
    }
    
    var select_grid_columns = [expander, {
        header: 'Icon',
        width: 35,
        renderer: renderEventIcon,
        sortable: true,
        dataIndex: 'icon'
    }, {
        header: 'Title',
        sortable: true,
        dataIndex: 'title'
    }, {
        header: 'Published',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        hidden: true,
        dataIndex: 'published'
    }, {
        header: 'Updated',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        dataIndex: 'updated'
    }, {
        header: 'Effective',
        id: 'select-column-effective',
        renderer: Ext.util.Format.dateRenderer('M j H:i:s'),
        width: 110,
        sortable: true,
        hidden: true,
        dataIndex: 'effective'
    }, {
        header: 'Expires',
        renderer: renderEntryExpires,
        width: 110,
        sortable: true,
        dataIndex: 'expires',
        renderIndex: 'expires'
    }];
    if (POST.UPDATE_OPERATION === 'Clone') {
        select_grid_columns.push({
            width: 100,
            renderer: renderCloneButton,
            sortable: false,
            dataIndex: 'links'
        });
    } else {
        select_grid_columns.push({
            width: 100,
            renderer: renderUpdateButton,
            sortable: false,
            dataIndex: 'links'
        });
        select_grid_columns.push({
            width: 100,
            renderer: renderCancelButton,
            sortable: false,
            dataIndex: 'links'
        });
    }
    
    var selectGridModelOptions = {};
    // update the selectFeature control used by the grid selection model
    // to support touch devices because the normal mouse based hover control
    // won't work
    if (POST.TOUCH_ENABLE) {
        selectGridModelOptions.config = {
            controlConfig: {
                eventListeners: {
                    // use feature 'highlight' events to prevent conflict
                    // with selection model's 'select' events
                    featurehighlighted: POST.Common.show_select_popup,
                    featureunhighlighted: POST.Common.close_select_popup
                }
            }
        };
    }
    
    // create grid panel configured with feature store
    POST.selectGridPanel = new Ext.grid.GridPanel({
        title: 'Previous Alerts',
        anchor: '100% 35%',
        collapsible: false,
        autoScroll: true,
        stripeRows: true,
        columnLines: true,
        header: false,
        store: POST.selectStore,
        columns: select_grid_columns,
        autoExpandColumn: '2',
        plugins: expander,
        sm: new GeoExt.grid.FeatureSelectionModel(selectGridModelOptions)
    });
    
    // create map panel
    var selectMapPanel = new GeoExt.MapPanel({
        style: 'margin: 10px 0px;',
        // 40% room left at the top for historycombo and bottom for selectgrid
	anchor: '100% 60%',
        collapsible: false,
        map: POST.selectMap
    });
    
    var selectCAPCard = new Ext.FormPanel({
        id: 'card-0',
        labelWidth: 100,
        // this layout type allows for % based sizing
	layout: 'anchor',
        items: [{
            xtype: 'compositefield',
            height: 30,
            fieldLabel: '<b>Your Postings</b>',
            items: [ historyCombo,
                {
                    boxLabel: 'Only in Map View',
                    xtype: 'checkbox',
                    style: 'margin-left: 25px;',
                    name: 'map-view-filter',
                    id: 'map-view-filter',
                    checked: false,
                    allowBlank: true,
                    handler: POST.load_user_entries
                }, {
                    xtype: 'button',
                    text: '<b>+</b>',
                    width: 30,
                    tooltip: 'Load an Alert manually',
                    style: 'margin-left: 500px;',
                    handler: POST.load_remote_alert,
                    hidden: (POST.UPDATE_OPERATION === 'Clone') ? false : true
                }
            ]
        }, selectMapPanel, POST.selectGridPanel]
    });
    
    /* inputCAPCard */
    
    var intervalCombo = new Ext.form.ComboBox({
        name: 'cap-expires-interval',
        id: 'cap-expires-interval',
        allowBlank: true,
        editable: false,
        width: 125,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'interval',
        tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
        // used to determine conflict between DateTime and Interval
        dateTimeConflict: false,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['interval', 'name', 'tip'],
            data: [ [null, '', 'Do not set expires'],
                [1, '1 Hour', 'Expires in 1 hour'],
                [6, '6 Hours', 'Expires in 6 hours'],
                [12, '12 Hours', 'Expires in 12 hours'],
                [24, '24 Hours', 'Expires in 1 day'],
                [48, '48 Hours', 'Expires in 2 days'] ]
        }),
        listeners: {
            select: function (combo, record, index) {
                if (record.data.interval) {
                    if (combo.dateTimeConflict) {
                        // make sure that both datetime and interval aren't
                        // used, otherwise they'd conflict
                        var dt_check = Ext.get('cap-expires-dt').getValue();
                        if (dt_check) {
                            alert('Choose either a specified time OR an interval, not both.');
                        }
                    } else {
                        // upon first load of this Entry, allow the user to
                        // quickly update the expires interval, but any further
                        // updates will result in a datetime conflict check
                        Ext.getCmp('cap-expires-dt').setValue('');
                        Ext.getCmp('cap-expires-tm').setValue('');
                        combo.dateTimeConflict = true;
                    }
                }
                // any change removes highlighting of initial default
                combo.removeClass('expiresIntervalDefault');
            },
            afterrender: function (combo) {
                if (POST.DEFAULT_EXPIRES_INTERVAL) {
                    // if a default expires interval is provided, modify the
                    // display name, select the default, and highlight it to
                    // make clear to the user it was defaulted.
                    var d_idx = combo.getStore().find('interval',
                        POST.DEFAULT_EXPIRES_INTERVAL);
                    if (d_idx !== -1) {
                        var d_rec = combo.getStore().getAt(d_idx);
                        var d_val = d_rec.get('name');
                        if (d_val.search('-Default') === -1) {
                            d_rec.set('name', d_val + '-Default');
                            d_rec.commit();
                        }
                        combo.setValue(d_rec.get('interval'));
                        combo.addClass('expiresIntervalDefault');
                    }
                }
            }
        }
    });
    
    var inputCAPCard = new Ext.FormPanel({
        id: 'card-1',
        labelWidth: 80,
        bodyStyle: 'padding: 10px;',
        defaultType: 'textfield',
        items: [{
            xtype: 'displayfield',
            fieldLabel: '<b>Status</b>',
            id: 'cap-status',
            html: 'Status'
        }, {
            xtype: 'displayfield',
            fieldLabel: '<b>Event</b>',
            id: 'cap-en-event',
            html: 'Event'
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Urgency</b>',
            id: 'cap-urgency',
            allowBlank: false,
            //width: 500,
            anchor: '60%',
            items: [{boxLabel: 'Unknown', name: 'cap-urgency', inputValue: 'Unknown'},
                {boxLabel: 'Past', name: 'cap-urgency', inputValue: 'Past'},
                {boxLabel: 'Future', name: 'cap-urgency', inputValue: 'Future'},
                {boxLabel: 'Expected', name: 'cap-urgency', inputValue: 'Expected'},
                {boxLabel: 'Immediate', name: 'cap-urgency', inputValue: 'Immediate'}]
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Severity</b>',
            id: 'cap-severity',
            allowBlank: false,
            //width: 500,
            anchor: '60%',
            items: [{boxLabel: 'Unknown', name: 'cap-severity', inputValue: 'Unknown'},
                {boxLabel: 'Minor', name: 'cap-severity', inputValue: 'Minor'},
                {boxLabel: 'Moderate', name: 'cap-severity', inputValue: 'Moderate'},
                {boxLabel: 'Severe', name: 'cap-severity', inputValue: 'Severe'},
                {boxLabel: 'Extreme', name: 'cap-severity', inputValue: 'Extreme'}]
        }, {
            xtype: 'radiogroup',
            fieldLabel: '<b>Certainty</b>',
            id: 'cap-certainty',
            allowBlank: false,
            //width: 500,
            anchor: '60%',
            items: [{boxLabel: 'Unknown', name: 'cap-certainty', inputValue: 'Unknown'},
                {boxLabel: 'Unlikely', name: 'cap-certainty', inputValue: 'Unlikely'},
                {boxLabel: 'Possible', name: 'cap-certainty', inputValue: 'Possible'},
                {boxLabel: 'Likely', name: 'cap-certainty', inputValue: 'Likely'},
                {boxLabel: 'Observed', name: 'cap-certainty', inputValue: 'Observed'}]
        }, {
            fieldLabel: '<b>Headline</b>',
            name: 'cap-en-headline',
            id: 'cap-en-headline',
            width: '75%',
            minLength: 5,
            minLengthText: 'A good headline should say what and where',
            maxLength: 160,
            maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
            blankText: 'MASAS requires a headline value',
            allowBlank: false
        }, {
            xtype: 'textarea',
            fieldLabel: 'Description',
            name: 'cap-en-description',
            id: 'cap-en-description',
            height: 50,
            width: '75%'
        }, {
            xtype: 'textarea',
            fieldLabel: 'Instruction',
            name: 'cap-en-instruction',
            id: 'cap-en-instruction',
            height: 30,
            width: '75%'
        }, {
            fieldLabel: 'Contact',
            name: 'cap-en-contact',
            id: 'cap-en-contact',
            width: '50%'
        }, {
            fieldLabel: 'Web Link',
            name: 'cap-en-web',
            id: 'cap-en-web',
            width: '50%',
            allowBlank: true,
            vtype: 'url'
        }, {
            xtype: 'fieldset',
            checkboxToggle: true,
            collapsed: true,
            forceLayout: true,
            title: 'French',
            id: 'cap-add-french',
            defaultType: 'textfield',
            items: [{
                fieldLabel: '<b>Headline</b>',
                name: 'cap-fr-headline',
                id: 'cap-fr-headline',
                width: '75%',
                minLengthText: 'A good headline should say what and where',
                maxLength: 160,
                maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
                blankText: 'MASAS requires a headline value',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Description',
                name: 'cap-fr-description',
                id: 'cap-fr-description',
                height: 50,
                width: '75%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                xtype: 'textarea',
                fieldLabel: 'Instruction',
                name: 'cap-fr-instruction',
                id: 'cap-fr-instruction',
                height: 30,
                width: '75%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                fieldLabel: 'Contact',
                name: 'cap-fr-contact',
                id: 'cap-fr-contact',
                width: '50%',
                keyboardConfig: { language: 'French', showIcon: true },
                plugins: vkeyboard_plugin
            }, {
                fieldLabel: 'Web Link',
                name: 'cap-fr-web',
                id: 'cap-fr-web',
                width: '50%',
                allowBlank: true,
                vtype: 'url'
            }],
            listeners: {
                expand: function () {
                    // reposition the icon because it was hidden on 1st render
                    Ext.getCmp('cap-fr-headline').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-description').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-instruction').alignKeyboardIcon();
                    Ext.getCmp('cap-fr-contact').alignKeyboardIcon();
                    // enable validation of french values
                    Ext.getCmp('cap-fr-headline').allowBlank = false;
                    Ext.getCmp('cap-fr-headline').minLength = 5;
                    Ext.getCmp('cap-fr-area-description').show();
                    if (!Ext.getCmp('areaCreateSet').collapsed) {
                        Ext.getCmp('cap-fr-area-description').allowBlank = false;
                    }
                },
                collapse: function () {
                    // disable validation of french values when not using
                    Ext.getCmp('cap-fr-headline').allowBlank = true;
                    Ext.getCmp('cap-fr-headline').minLength = null;
                    Ext.getCmp('cap-fr-area-description').hide();
                    Ext.getCmp('cap-fr-area-description').allowBlank = true;
                }
            }
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Effective',
            //TODO: setting a width ensured the validation error icon would
            //      appear beside the fields instead of the far right, but in
            //      this case, the width resulted in hiding the fields entirely
            //width: 325,
            items: [{
                xtype: 'checkbox',
                boxLabel: 'Immediately',
                name: 'cap-effective-immediate',
                id: 'cap-effective-immediate',
                // setting a height so the bottom part of the date/time fields
                // won't be cut off
                height: 23,
                checked: true,
                handler: function (checkbox, checked) {
                    // using disabled here instead of hide because for updates
                    // the fields are rendered when the first card is in view
                    // and so the fields overlap and can't use the render
                    // listener like in new-entry
                    if (checked) {
                        Ext.getCmp('cap-effective-dt').disable();
                        Ext.getCmp('cap-effective-dt').reset();
                        Ext.getCmp('cap-effective-tm').disable();
                        Ext.getCmp('cap-effective-tm').reset();
                    } else {
                        Ext.getCmp('cap-effective-dt').enable();
                        Ext.getCmp('cap-effective-tm').enable();
                    }
                }
            }, {
                xtype: 'displayfield',
                id: 'cap-effective-at',
                html: ' at:',
                style: {'padding': '3px 2px 2px 20px'}
            }, {
                xtype: 'datefield',
                name: 'cap-effective-dt',
                id: 'cap-effective-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                disabled: true
            }, {
                xtype: 'timefield',
                name: 'cap-effective-tm',
                id: 'cap-effective-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                disabled: true,
                validator: function (val) {
                    var effective_dt = Ext.get('cap-effective-dt').getValue();
                    if (!effective_dt || effective_dt.length === 0) {
                        effective_dt = null;
                    }
                    return POST.Validator.check_future_time(effective_dt, val);
                }
            }]
        }, {
            xtype: 'compositefield',
            fieldLabel: 'Expires at',
            //width: 425,
            items: [{
                xtype: 'datefield',
                name: 'cap-expires-dt',
                id: 'cap-expires-dt',
                hideLabel: true,
                width: 95,
                format: 'Y-m-d',
                minValue: new Date(),
                listeners: {select: function () {
                    var it_check = Ext.getCmp('cap-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'timefield',
                name: 'cap-expires-tm',
                id: 'cap-expires-tm',
                hideLabel: true,
                width: 60,
                format: 'H:i',
                increment: 60,
                validator: function (val) {
                    var expires_dt = Ext.get('cap-expires-dt').getValue();
                    if (!expires_dt || expires_dt.length === 0) {
                        expires_dt = null;
                    }
                    return POST.Validator.check_future_time(expires_dt, val);
                },
                listeners: {select: function () {
                    var it_check = Ext.getCmp('cap-expires-interval').getValue();
                    if (it_check) {
                        alert('Choose either a specified time OR an interval, not both.');
                    }
                } }
            }, {
                xtype: 'displayfield',
                html: 'or Expires in:',
                style: {'padding': '3px 2px 2px 20px'}
            }, intervalCombo ]
        } ]
    });
    
    /* locationCAPCard */
    
    // setup the map Layers and Tools toolbars
    POST.MapLayers.initialize();
    POST.MapTools.initialize(false);
    
    // create map panel
    POST.locationMapPanel = new GeoExt.MapPanel({
        id: 'locationMapPanel',
        collapsible: false,
        style: 'margin-bottom: 5px;',
        // 32% room left at the bottom for Location and Areas
        anchor: '100% 68%',
        map: POST.locationMap,
        tbar: POST.mapToolbarTop,
        bbar: POST.mapToolbarBottom
    });
    
    var currentAreaSelection = new Ext.grid.CheckboxSelectionModel();
    POST.currentAreaGrid = new Ext.grid.GridPanel({
        hideHeaders: true,
        fieldLabel: 'Current',
        hideLabel: true,
        name: 'cap-area-current',
        id: 'cap-area-current',
        width: 250,
        height: 75,
        doValidation: true,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['pos', 'name'],
            data: []
        }),
        cm: new Ext.grid.ColumnModel({
            defaults: { sortable: false },
            columns: [currentAreaSelection, {
                header: "Name",
                width: 225,
                dataIndex: "name"
            }]
        }),
        sm: currentAreaSelection
    });
    
    var areaSearchCombo = new Ext.form.ComboBox({
        id: 'area-search-radius',
        width: 90,
	editable: false,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        emptyText: 'within...',
        displayField: 'name',
        valueField: 'value',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['value', 'name'],
            data: [ [5, '5 km'], [50, '50 km'], [100, '100 km'] ]
        }),
        listeners: {select: POST.existing_area_search}
    });
    
    var areaTypeCombo = new Ext.form.ComboBox({
        id: 'area-search-type',
        width: 150,
	editable: false,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        valueField: 'value',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['value', 'name'],
            data: [ ['csd', 'Cities/Towns - CSD'], ['cd', 'Counties - CD'],
                ['other', 'Other'] ]
        }),
        value: 'csd',
        listeners: {select: POST.existing_area_search}
    });
    
    POST.areaZoneStore = new GeoExt.data.FeatureStore({
        layer: POST.locationLayers.sgc,
        fields: [
            {name: 'e_name', type: 'string'},
            {name: 'f_name', type: 'string'},
            {name: 'sgc', type: 'string'}
        ],
        proxy: new GeoExt.data.ProtocolProxy({
            protocol: new OpenLayers.Protocol.HTTP({
                url: POST.AREA_ZONE_URL,
                format: new OpenLayers.Format.GeoJSON({
                    internalProjection: POST.locationMap.getProjectionObject(),
                    externalProjection: new OpenLayers.Projection('EPSG:4326')
                })
            })
        }),
        autoLoad: false
    });
    
    POST.areaSearchGrid = new Ext.grid.GridPanel({
        hideHeaders: true,
        fieldLabel: '<b>Select</b>',
        name: 'cap-area-zones',
        id: 'cap-area-zones',
        width: 250,
        height: 75,
        doValidation: false,
        store: POST.areaZoneStore,
        columns: [{
            header: "Name",
            width: 245,
            dataIndex: "e_name"
        }],
        sm: new GeoExt.grid.FeatureSelectionModel() 
    });

    var locationCAPCard = new Ext.FormPanel({
        id: 'card-2',
        labelWidth: 80,
        // this layout type allows for % based sizing
        layout: 'anchor',
        items: [POST.locationMapPanel, {
            xtype: 'compositefield',
	    height: 35,
            items: [{
                // form field titles don't seem to display when using the
                // anchor layout so create one instead
                xtype: 'displayfield',
                id: 'cap-location-title',
                html: '<b>Location:</b> '
            }, {
                xtype: 'textfield',
                name: 'cap-location',
                id: 'cap-location',
                width: 400,
                vtype: 'location',
                validationEvent: false,
                blankText: 'A location is required',
                allowBlank: false
	    }, {
		boxLabel: '<span style="color: #606060">Use Alert Area for Location instead of this Point</span>',
		xtype: 'checkbox',
		style: 'margin-left: 100px',
		name: 'discard-event-location',
		id: 'discard-event-location',
		checked: false
	    }]
        }, {
            layout: 'column',
            border: false,
            items: [{
                xtype: 'fieldset',
                width: '20%',
                style: 'margin-right: 15px; padding: 5px;',                
                checkboxToggle: true,
                collapsed: false,
                title: 'Current Areas',
                id: 'areaCurrentSet',
                items: [ POST.currentAreaGrid ],
                listeners: {
                    expand: function () {
                        POST.currentAreaGrid.doValidation = true;
                    },
                    collapse: function () {
                        POST.currentAreaGrid.doValidation = false;
                    }
                }
            }, {
                xtype: 'fieldset',
                width: '30%',
                style: 'margin-right: 15px; padding: 5px;',
                labelWidth: 45,
                checkboxToggle: true,
                // default for now is to use create new instead
                collapsed: true,
                title: 'Add Area: Defined Boundaries (CAP-CP)',
                id: 'areaSelectSet',
                items: [{
                    xtype: 'compositefield',
                    fieldLabel: '<b>Search</b>',
                    items: [ areaSearchCombo, areaTypeCombo ]
                }, POST.areaSearchGrid ],
                listeners: {
                    expand: function () {
                        POST.locationLayers.sgc.setVisibility(true);
                        // collapse the create fieldset, toggle validated fields
                        Ext.getCmp('areaCreateSet').collapse();
                        areaSearchCombo.allowBlank = false;
                        POST.areaSearchGrid.doValidation = true;
                    },
                    collapse: function () {
                        // hiding the SGC layer because if its not going to be used
                        // it can clutter the map
                        POST.locationLayers.sgc.setVisibility(false);
                        areaSearchCombo.allowBlank = true;
                        POST.areaSearchGrid.doValidation = false;
                    }
                }
            }, { xtype: 'displayfield', hideLabel: true, html: '<b> OR </b>' }, {
                xtype: 'fieldset',
                width: '20%',
                style: 'margin-left: 15px; padding: 5px;',
                checkboxToggle: true,
                // default for now is to use create new instead
                collapsed: true,
                title: 'Add Area: Custom Boundary',
                id: 'areaCreateSet',
                items: [{
                    xtype: 'displayfield',
                    hideLabel: true,
                    html: 'Using Location as the center of a new area circle'
                }, {
                    xtype: 'numberfield',
                    fieldLabel: '<b>Radius</b> (km)',
                    name: 'cap-radius',
                    id: 'cap-radius',
                    width: 50,
                    blankText: 'A radius in km between 0 and 500 is required',
                    allowBlank: false,
		    allowNegative: false,
		    maxValue: 500
                }, {
                    xtype: 'textfield',
                    fieldLabel: '<b>Description</b>',
                    name: 'cap-en-area-description',
                    id: 'cap-en-area-description',
                    width: 250,
                    maxLength: 100,
                    blankText: 'An area description is required',
                    allowBlank: false
                }, {
                    xtype: 'textfield',
                    fieldLabel: '<b>French</b>',
                    name: 'cap-fr-area-description',
                    id: 'cap-fr-area-description',
                    width: 250,
                    maxLength: 100,
                    blankText: 'A French area description is required',
                    allowBlank: true,
                    hidden: true
                }],
                listeners: {
                    expand: function () {
                        // collapse the select fieldset, toggle validated fields
                        Ext.getCmp('areaSelectSet').collapse();
                        Ext.getCmp('cap-radius').allowBlank = false;
                        Ext.getCmp('cap-en-area-description').allowBlank = false;
                        if (!Ext.getCmp('cap-add-french').collapsed) {
                            Ext.getCmp('cap-fr-area-description').allowBlank = false;
                        }
                    },
                    collapse: function () {
                        Ext.getCmp('cap-radius').allowBlank = true;
                        Ext.getCmp('cap-en-area-description').allowBlank = true;
                        Ext.getCmp('cap-fr-area-description').allowBlank = true;
                    }
                },
                // container to hold SGC geocode search results, with default
                sgcGeocode: ['none']
            }]
        }]
    });
    
    /* postCAPCard */
    
    var postCAPCard = new Ext.Panel({
        id: 'card-3',
        items: [{
            border: false,
            bodyStyle: 'margin-bottom: 50px',
            html: '<div class="postingCard" id="postingBox"><h1>Ready to Post</h1>' + 
                '<a href="#" onclick="POST.preview_cap_xml(); return false;" ' +
                'style="color: grey;">Preview</a><a href="#" onclick="POST.' + 
                (('Clone' === POST.UPDATE_OPERATION) ? 'post_new_alert' : 'post_alert_update') +
                '(); return false;" style="color: green;">Post Alert</a></div>'
        }]
    });
    
    if (POST.EMAIL_ADDRESS_LIST) {
        var emailToCombo = new Ext.form.ComboBox({
            fieldLabel: '<b>To</b>',
            name: 'email-to-address',
            id: 'email-to-address',
            width: 300,
            editable: false,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            store: POST.EMAIL_ADDRESS_LIST
        });
    
        postCAPCard.add({
            xtype: 'fieldset',
            id: 'email-to-fieldset',
            hidden: true,
            collapsible: true,
            collapsed: true,
            title: 'Email Forwarding',
            labelWidth: 70,
            listeners: {expand: POST.Common.generate_email_content },
            items: [ emailToCombo, {
                xtype: 'textfield',
                fieldLabel: '<b>Subject</b>',
                name: 'email-to-subject',
                id: 'email-to-subject',
                width: 400,
                minLength: 5,
                minLengthText: 'A subject is required',
                maxLength: 250,
                maxLengthText: 'Subject cannot be longer than 250 characters'
            }, {
                xtype: 'textarea',
                fieldLabel: '<b>Message</b>',
                name: 'email-to-message',
                id: 'email-to-message',
                height: 150,
                width: 800,
                minLength: 5,
                minLengthText: 'A message is required'
            }, {
                xtype: 'button',
                fieldLabel: ' ',
                labelSeparator: '',
                text: 'Send',
                width: 75,
                handler: POST.Common.post_email_message
            }]
        });
    }
    
    /* capPanel */
    
    // the top toolbar for the app
    var panel_tbar_items = [];
    // optional support for multiple hubs
    if (POST.FEED_SETTINGS && POST.FEED_SETTINGS.length > 1) {
        var hubComboRecord = Ext.data.Record.create([
            {name: 'title', mapping: 'title'},
            {name: 'url', mapping: 'url'},
            {name: 'secret', mapping: 'secret'},
            {name: 'uri', mapping: 'uri'},
            {name: 'readOnly', mapping: 'readOnly'}
        ]);
        var hubCombo = new Ext.form.ComboBox({
            allowBlank: false,
            editable: false,
            width: 125,
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'title',
            store: new Ext.data.ArrayStore({
                fields: hubComboRecord,
                data: POST.FEED_SETTINGS
            }),
            listeners: {
                afterrender: function (combo) {
                    var default_idx = combo.getStore().find('url', POST.FEED_URL);
                    if (default_idx === -1) {
                        console.error('Unable to find Hub selection default');
                        // first value in array instead
                        default_idx = 0; 
                    }
                    var default_val = combo.getStore().getAt(default_idx);
                    combo.setValue(default_val.get('title'));
                },
                select: function (combo, record) {
                    console.debug('Changing feed to ' + record.data.title);
                    if (record.data.url) {
                        POST.FEED_URL = record.data.url;
                    }
                    if (record.data.secret) {
                        POST.USER_SECRET = record.data.secret;
                    }
                    if (record.data.uri) {
                        POST.USER_URI = record.data.uri;
                    }
                    // reload existing entries only on the first card to allow
                    // the user to find postings by Hub
                    var card_panel = Ext.getCmp('cap-wizard-panel').getLayout();
                    var current_card = card_panel.activeItem.id.split('card-')[1];
                    if (current_card === '0') {
                        POST.load_user_entries();
                    }
                    if (record.data.readOnly) {
                        alert('This is a Read Only feed which can be used to' +
                            ' make selections, but prior to Posting you must' +
                            ' choose another Hub.');
                    }
                }
            }
        });
        panel_tbar_items = [
            {xtype: 'tbspacer', width: 15},
            {
                xtype: 'tbtext',
                style: {'fontWeight': 'bold', 'font-size': '13px'},
                text: 'Hub:'
            },
            {xtype: 'tbspacer', width: 25},
            hubCombo
        ];
    }
    // default items
    panel_tbar_items.push.apply(panel_tbar_items, [progressBar, '->', {
        text: '<b>Abort</b>',
        iconCls: 'abortButton',
        width: 75,
        handler: function () {
            window.close();
        }
    }]);
    
    var capPanel = new Ext.Panel({
        id: 'cap-wizard-panel',
        title: POST.UPDATE_OPERATION + ' CAP Alert',
        region: 'center',
        layout: 'card',
        activeItem: 0,
        bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        tbar: panel_tbar_items,
        bbar: new Ext.Toolbar({ items: [
            {
                id: 'card-prev',
                text: '<span style="font-weight: bold; font-size: 130%;">Back</span>',
                iconCls: 'previousButton',
                width: 100,
                handler: cardNav.createDelegate(this, [-1]),
                disabled: true
            }, { xtype: 'tbspacer', width: 150 },
            {
                id: 'card-next',
                text: '<span style="font-weight: bold; font-size: 130%;">Next</span>',
                iconCls: 'nextButton',
                iconAlign: 'right',
                width: 100,
                handler: cardNav.createDelegate(this, [1])
            }
        ], buttonAlign: 'center' }),
        // the panels (or "cards") within the layout
        items: [ selectCAPCard, inputCAPCard, locationCAPCard, postCAPCard ]
    });
    
    
    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [headerBox, capPanel]
    });


    POST.selectMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
        new OpenLayers.Projection('EPSG:4326'), POST.selectMap.getProjectionObject()), 4);

    POST.load_user_entries();
});


/**
Load previous Entries created by this user according to selection criteria so
that the user can select which Entry they want to modify.  Will filter
out Entries so only CAP messages can be selected.
*/
POST.load_user_entries = function () {
    POST.selectControls.loading.maximizeControl();
    var history_val = Ext.getCmp('history-interval').getValue();
    var map_val = Ext.getCmp('map-view-filter').getValue();
    var feed_url = OpenLayers.Util.urlAppend(POST.FEED_URL,
        'secret=' + POST.USER_SECRET + '&lang=en&author=' + POST.USER_URI);
    // default is 0 for entries that are still current and so no datetime
    // parameters are added to the query
    if (history_val !== 0) {
        var since_date = new Date();
        // convert local to UTC first
        since_date = MASAS.Common.adjust_time(since_date,
            MASAS.Common.Local_UTC_Offset);
        // current or effective time starts right now, all others are a time
        // value in the past
        if (history_val > 2) {
            var diff_hours = parseInt(history_val, 10);
            // using a negative number for the past
            since_date = MASAS.Common.adjust_time(since_date,
                -Math.abs(diff_hours));
        }
        // using Ext extensions to Date for formatting
        var since_string = since_date.format('Y-m-d\\TH:i:s\\Z');
        feed_url += '&dtsince=' + since_string;
        // default search is updated which works for time values in the past
        if (history_val === 1) {
            // change search value so that anything not yet expired is assumed
            // to be current and/or future effective
            feed_url += '&dtval=expires';
        } else if (history_val === 2) {
            // not yet effective, in the future
            feed_url += '&dtval=effective';
        }
    }
    var selectLayer = POST.selectLayers.select;
    if (map_val) {
        // current viewport only, creating new bounds because of the transform
        var current_bounds = POST.selectMap.getExtent().toArray();
        var current_view = new OpenLayers.Bounds.fromArray(current_bounds);
        current_view.transform(POST.selectMap.getProjectionObject(),
            new OpenLayers.Projection('EPSG:4326'));
        feed_url += '&bbox=' + current_view.toBBOX(4);
        if (selectLayer.events.listeners.featuresadded.length > 0) {
            // update feed again whenever the map view changes
            POST.selectMap.events.register('moveend', null, POST.load_user_entries);
            // since the zoomToExtent created in post-map as anonymous, remove all
            selectLayer.events.remove('featuresadded');
        }
    } else {
        if (selectLayer.events.listeners.featuresadded.length === 0) {
            // restore normal behaviour of bringing all entries into focus
            POST.selectMap.events.unregister('moveend', null, POST.load_user_entries);
            selectLayer.events.register('featuresadded', null, function () {
                POST.selectMap.zoomToExtent(selectLayer.getDataExtent());
            });
        }
    }
    // cache busting parameter
    feed_url += '&_dc=' + new Date().getTime();
    // use the proxy if available and this is an absolute URL versus
    // a local relative URL
    if (POST.AJAX_PROXY_URL && Ext.form.VTypes.url(POST.FEED_URL)) {
        feed_url = POST.AJAX_PROXY_URL + encodeURIComponent(feed_url);
    }
    console.debug('Loading User Entries');
    console.log(feed_url);
    POST.selectStore.proxy.protocol.options.url = feed_url;
    POST.selectStore.load();
    // showing the effective column in the select grid for both future and
    // recently created entries to provide more info when selecting whereas
    // "current" shouldn't need it unless the user manually enables it
    try {
        var select_columns = POST.selectGridPanel.getColumnModel();
        var effective_column = select_columns.getIndexById('select-column-effective');
        select_columns.setHidden(effective_column,
            (history_val === 0) ? true : false);
    } catch(err) {
        console.error('Show effective column error: ' + err);
    }
};


/**
Display the remote alert window, which will allow the loading of a CAP alert
via a provided URL instead.  Only works for Cloning.
*/
POST.load_remote_alert = function () {
    // only one window allowed
    if (POST.remoteAlertWindow) {
        POST.remoteAlertWindow.destroy();
    }
    var remotePanel = new Ext.FormPanel({
        id: 'remote-alert-panel',
        frame: true,
        labelWidth: 50,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'displayfield',
            html: 'Enter the URL of the Alert you would like to manually load' +
                ' and Clone.'
        }, {
            xtype: 'textfield',
            fieldLabel: '<b>URL</b>',
            name: 'remote-alert-url',
            id: 'remote-alert-url',
            width: 300,
            minLength: 5,
            minLengthText: 'An Alert URL is required',
            blankText: 'An Alert URL is required',
            allowBlank: false
        }],
        buttons: [{
            text: '<b>Clone</b>',
            handler: function () {
                if (Ext.getCmp('remote-alert-panel').getForm().isValid()) {
                    POST.load_past_alert('atom_link',
                        Ext.getCmp('remote-alert-url').getValue(), 'Clone');
                }
            }
        }]
    });
    POST.remoteAlertWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'Load Alert <a class="titleClose" href="#" ' +
            'onclick="POST.remoteAlertWindow.close(); return false;">Close</a>',
        closable: true,
        width: 500,
        height: 140,
        layout: 'fit',
        items: [remotePanel]
    });
    POST.remoteAlertWindow.show(this);
};


/**
Loads a CAP message selected by the user.

@param {String} - the link to this Atom Entry
@param {String} - the link to this CAP message
@param {String} - the operation to perform on this Entry
*/
POST.load_past_alert = function (atom_link, cap_link, msg_type) {
    if (!cap_link || !atom_link) {
        console.error('Links missing');
        alert('Entry or Alert Link Unavailable');
        return;
    }
    if (POST.remoteAlertWindow) {
        POST.remoteAlertWindow.destroy();
    }
    POST.UPDATE_OPERATION = msg_type;
    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();
    if (POST.UPDATE_OPERATION === 'Clone') {
        // clone doesn't need to load associated Atom values for use later
        // so just create a placeholder
        POST.oldAtom = {entry: {}};
    } else {
        console.debug('Loading Past Entry: ' + atom_link);
        // sync loading these urls
        var atom_get = new OpenLayers.Request.GET({
            url: atom_link,
            // secret on request line for now along with cache busting parameter
            params: {
                'secret': POST.USER_SECRET,
                '_dc': new Date().getTime()
            },
            async: false,
            proxy: POST.AJAX_PROXY_URL
        });
        if (atom_get.status !== 200) {
            mask.hide();
            console.error('Load Past Entry Error: ' + atom_get.responseText);
            alert('Unable to load Atom Entry.');
            return;
        }
        var xml_doc = MASAS.Common.parse_xml(atom_get.responseText);
        if (!xml_doc) {
            mask.hide();
            console.error('Error parsing past entry XML');
            alert('Unable to parse Atom Entry.');
            return;
        }
        var atom_json = xmlJsonClass.xml2json(xml_doc, '  ');
        try {
            POST.oldAtom = JSON.parse(atom_json);
        } catch (err) {
            mask.hide();
            console.error('Past entry JSON parse error: ' + err);
            alert('Unable to parse Atom Entry.');
            POST.oldAtom = null;
            return;
        }
    }
    console.debug('Loading Past CAP: ' + cap_link);
    var cap_get = new OpenLayers.Request.GET({
        url: cap_link,
        // use a secret on request line if needed for this cap_link along with
        // cache busting parameter
        params: {
            'secret': POST.Common.check_url_secret(cap_link),
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: POST.AJAX_PROXY_URL
    });
    if (cap_get.status !== 200) {
        mask.hide();
        console.error('Load Past CAP Error: ' + cap_get.responseText);
        alert('Unable to load CAP Alert.');
        return;
    }
    var cap_json = xmlJsonClass.xml2json(MASAS.Common.parse_xml(cap_get.responseText),
        '  ');
    try {
        POST.oldCAP = JSON.parse(cap_json);
    } catch (err) {
        mask.hide();
        console.error('Past CAP JSON parse error: ' + err);
        alert('Unable to parse CAP Entry.');
        POST.oldCAP = null;
        return;
    }
    if (POST.oldCAP.alert.msgType === 'Cancel') {
        mask.hide();
        alert('Cannot use an already Cancelled Message.');
        return;
    }
    var value_result = POST.load_past_values();
    if (!value_result) {
        mask.hide();
        alert('Unable to Load CAP Message.');
        return;
    }
    if (msg_type === 'Cancel') {
        // set Cancel to expire in 1 hour as default
        Ext.getCmp('cap-expires-dt').setValue('');
        Ext.getCmp('cap-expires-tm').setValue('');
        Ext.getCmp('cap-expires-interval').setValue(1);
        Ext.getCmp('cap-expires-interval').addClass('expiresIntervalDefault');
        // simple update to the headline, english only for now
        var orig_headline = Ext.getCmp('cap-en-headline').getValue();
        Ext.getCmp('cap-en-headline').setValue(orig_headline + ' - Cancelled');
    }
    // advance to the next card
    mask.hide();
    var next_button = Ext.getCmp('card-next');
    next_button.handler.call(next_button.scope, next_button, Ext.EventObject);
};


/**
Loads the CAP message's values into the form fields for the user to modify.
*/
POST.load_past_values = function () {
    if (!POST.oldAtom || !POST.oldCAP) {
        console.error('oldAtom or oldCAP missing, unable to load past values');
        return false;
    }
    console.debug('Loading Past Values');
    console.log(POST.oldAtom);
    console.log(POST.oldCAP);
    
    // reset all of the form fields in case user is changing the selected alert
    Ext.getCmp('card-1').getForm().reset();
    Ext.getCmp('cap-add-french').collapse();
    // if an english only alert is selected, this will be hidden instead, but
    // resetting in case a bilingual alert is chosen instead
    Ext.getCmp('cap-add-french').show();
    Ext.getCmp('card-2').getForm().reset();
    POST.locationLayers.location.removeAllFeatures();
    POST.locationLayers.sgc.removeAllFeatures();
    Ext.getCmp('cap-area-current').store.removeAll();
    Ext.getCmp('areaSelectSet').collapse();
    Ext.getCmp('areaCreateSet').collapse();
    
    Ext.getCmp('cap-status').setValue(POST.oldCAP.alert.status);
    // info blocks may be english only or english and french
    //TODO: add better ability to detect the correct info block
    var en_info = null;
    var fr_info = null;
    if (POST.oldCAP.alert.info instanceof Array) {
        if (POST.oldCAP.alert.info[0].language === 'en-CA') {
            en_info = POST.oldCAP.alert.info[0];
        }
        if (POST.oldCAP.alert.info[1].language === 'fr-CA') {
            fr_info = POST.oldCAP.alert.info[1];
        }
    } else if (typeof(POST.oldCAP.alert.info) === "object") {
        // english only can be detected a couple ways, missing, en-US, en-CA
        if (!POST.oldCAP.alert.info.language) {
            en_info = POST.oldCAP.alert.info;
        } else if (POST.oldCAP.alert.info.language.search('en') !== -1) {
            en_info = POST.oldCAP.alert.info;
        }
    }
    if (!en_info) {
        return false;
    }
    
    Ext.getCmp('cap-en-event').setValue(en_info.event);
    Ext.getCmp('cap-urgency').setValue(en_info.urgency);
    Ext.getCmp('cap-severity').setValue(en_info.severity);
    Ext.getCmp('cap-certainty').setValue(en_info.certainty);
    if (en_info.headline) {
        Ext.getCmp('cap-en-headline').setValue(en_info.headline);
    }
    if (en_info.description) {
        Ext.getCmp('cap-en-description').setValue(en_info.description);
    }
    if (en_info.instruction) {
        Ext.getCmp('cap-en-instruction').setValue(en_info.instruction);
    }
    if (en_info.contact) {
        Ext.getCmp('cap-en-contact').setValue(en_info.contact);
    }
    if (en_info.web) {
        Ext.getCmp('cap-en-web').setValue(en_info.web);
    }
    
    //TODO: not using the original CAP effective or expires time as it would
    //      require better parsing of the timezone offset, as well as parsing
    //      multiple info blocks to find that last expiring one.  Instead
    //      just relying on Hub to do that in Entry expires for now.
    var now_epoch = new Date().getTime();
    if (POST.oldAtom.entry['met:effective']) {
        try {
            var effective_date = Date.parseDate(POST.oldAtom.entry['met:effective'],
                'Y-m-d\\TH:i:s\\Z');
            effective_date = MASAS.Common.adjust_time(effective_date,
                MASAS.Common.UTC_Local_Offset);
            // if the entry has already expired, don't fill in the previous date
            // as any update should need a new expires time
            var effective_epoch = effective_date.format('U') * 1000;
            if (effective_epoch >= now_epoch) {
                Ext.getCmp('cap-effective-immediate').setValue(false);
                Ext.getCmp('cap-effective-dt').enable();
                Ext.getCmp('cap-effective-dt').setValue(effective_date);
                Ext.getCmp('cap-effective-tm').enable();
                Ext.getCmp('cap-effective-tm').setValue(effective_date);
            }
        } catch (err) {
            console.error('Effective Error: ' + err);
        }
    }
    if (POST.oldAtom.entry['age:expires']) {
        try {
            var expire_date = Date.parseDate(POST.oldAtom.entry['age:expires'],
                'Y-m-d\\TH:i:s\\Z');
            expire_date = MASAS.Common.adjust_time(expire_date,
                MASAS.Common.UTC_Local_Offset);
            // if the entry has already expired, don't fill in the previous date
            // as any update should need a new expires time
            var expire_epoch = expire_date.format('U') * 1000;
            if (expire_epoch >= now_epoch) {
                Ext.getCmp('cap-expires-dt').setValue(expire_date);
                //TODO: should this be highlighted?
                //Ext.getCmp('cap-expires-dt').addClass('expiresIntervalDefault');
                Ext.getCmp('cap-expires-tm').setValue(expire_date);
            } else {
                // setup a default interval if available
                Ext.getCmp('cap-expires-interval').fireEvent('afterrender',
                    Ext.getCmp('cap-expires-interval'));
            }
        } catch (err) {
            console.error('Expires Error: ' + err);
        }
    }
    
    //TODO: handle multiple parameters and other types
    var event_location_param = null;
    var event_location_geom = 'point';
    if (en_info.parameter) {
        if (typeof(en_info.parameter) === 'object') {
            if (en_info.parameter.valueName === 'layer:CAPAN:eventLocation:point') {
                event_location_param = en_info.parameter.value;
            } else if (en_info.parameter.valueName === 'layer:CAPAN:eventLocation:line') {
                event_location_param = en_info.parameter.value;
                event_location_geom = 'line';
            }
        } else if (typeof(en_info.parameter) === 'array') {
            for (var i = 0; i < en_info.parameter.length; i++) {
                if (en_info.parameter[i].valueName === 'layer:CAPAN:eventLocation:point') {
                    event_location_param = en_info.parameter[i].value;
                    break;
                } else if (en_info.parameter[i].valueName === 'layer:CAPAN:eventLocation:line') {
                    event_location_param = en_info.parameter[i].value;
                    event_location_geom = 'line';
                    break;
                }
            }
        }
    }
    if (event_location_param) {
        // set the geom first so validation works properly for setValue
        Ext.getCmp('cap-location').geom = event_location_geom;
        Ext.getCmp('cap-location').setValue(event_location_param);
        var location_layer = POST.locationLayers.location;
        // zoom to show the new feature, GeoExt will override and use the mapPanel
        // center,zoom,extent values when the panel is first opened, after that
        // the normal OpenLayers methods work for any subsequent loads
        if (event_location_geom === 'point') {
            var location_vals = event_location_param.split(',');
            var location_xy = new OpenLayers.LonLat(location_vals[1], location_vals[0]);
            location_xy = location_xy.transform(new OpenLayers.Projection('EPSG:4326'),
                POST.locationMap.getProjectionObject());
            var location_point = new OpenLayers.Geometry.Point(location_xy.lon, location_xy.lat);
            var location_feature = new OpenLayers.Feature.Vector(location_point);
            location_layer.addFeatures([location_feature]);
            POST.locationMapPanel.center = location_xy;
            POST.locationMapPanel.zoom = 12;
            POST.locationMap.setCenter(location_xy, 12);
        } else if (event_location_geom === 'line') {
            var location_vals = event_location_param.split(' ');
            var line_points = [];
            for (var i = 0; i < location_vals.length; i++) {
                var seg_vals = location_vals[i].split(',');
                var seg_point = new OpenLayers.Geometry.Point(seg_vals[1], seg_vals[0]);
                seg_point = seg_point.transform(new OpenLayers.Projection('EPSG:4326'),
                    POST.locationMap.getProjectionObject());
                line_points.push(seg_point);
            }
            var location_line = new OpenLayers.Geometry.LineString(line_points);
            var location_feature = new OpenLayers.Feature.Vector(location_line);
            location_layer.addFeatures([location_feature]);
            var feature_extent = location_feature.layer.getDataExtent().toArray();
            var extent_bounds = new OpenLayers.Bounds.fromArray(feature_extent);
            POST.locationMapPanel.extent = extent_bounds;
            POST.locationMap.zoomToExtent(extent_bounds);
        }
    } else {
        //TODO: since the map doesn't have anything to zoom to, could instead
        //      use the Entry's geometry instead
        Ext.getCmp('cap-location').setValue('Not Available');
        Ext.getCmp('discard-event-location').setValue(true);
    }
    
    if (!en_info.area) {
        return false;
    }
    var area_current = Ext.getCmp('cap-area-current');
    // current area array position used for an ID for now
    if (en_info.area instanceof Array) {
        for (var i = 0; i < en_info.area.length; i++) {
            area_current.store.add(new Ext.data.Record({pos: i, name: en_info.area[i].areaDesc}));
        }
    } else if (typeof(en_info.area) === "object") {
        area_current.store.add(new Ext.data.Record({pos: 0, name: en_info.area.areaDesc}));
    }
    area_current.getSelectionModel().selectAll();
    
    if (fr_info) {
        Ext.getCmp('cap-add-french').expand();
        if (fr_info.headline) {
            Ext.getCmp('cap-fr-headline').setValue(fr_info.headline);
        }
        if (fr_info.description) {
            Ext.getCmp('cap-fr-description').setValue(fr_info.description);
        }
        if (fr_info.instruction) {
            Ext.getCmp('cap-fr-instruction').setValue(fr_info.instruction);
        }
        if (fr_info.contact) {
            Ext.getCmp('cap-fr-contact').setValue(fr_info.contact);
        }
        if (fr_info.web) {
            Ext.getCmp('cap-fr-web').setValue(fr_info.web);
        }
    } else {
        // no updates to add french to an original english only alert for now
        Ext.getCmp('cap-add-french').hide();
    }
    
    return true;
};


/**
Saves a location feature and its values, both to the on-screen display for the
user and for later processing.

@param {Object} - the OpenLayers feature to be saved
*/
POST.save_location_feature = function (feature) {
    console.debug('Saving Location');
    console.log(feature);
    // highlight the geometry that will be used for location
    if (feature.layer) {
        for (var f = 0; f < feature.layer.features.length; f++) {
            feature.layer.features[f].renderIntent = 'default';
        }
        feature.renderIntent = 'select';
        feature.layer.redraw();
    }
    // update the location form values with this geometry
    var location_input = document.getElementById('cap-location');
    var location_formfield = Ext.getCmp('cap-location');
    var location_title = Ext.getCmp('cap-location-title');
    if (feature.geometry) {
        var point_result = [];
        var feature_points = feature.geometry.getVertices();
        for (var i = 0; i < feature_points.length; i++) {
            var new_point = new OpenLayers.Geometry.Point(feature_points[i].x,
                feature_points[i].y);
            new_point.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            point_result.push(new_point.y.toPrecision(8) + ',' + new_point.x.toPrecision(8));
        }
        if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.Point') {
            if (point_result.length === 1) {
                // setting the geom first so validation works when value added
                location_formfield.geom = 'point';
                location_input.value = point_result[0];
                location_title.update('<b>Point:</b>');
            }
        } else if (feature.geometry.CLASS_NAME === 'OpenLayers.Geometry.LineString') {
            if (point_result.length > 1) {
                location_formfield.geom = 'line';
                location_input.value = point_result.join(' ');
                location_title.update('<b>Line:</b>');
            }
        } else {
            console.error('Unknown geometry type');
            alert('Geometry type not supported.');
            return;
        }
        // display a temporary notification message to better inform the user when
        // the field value that will be used for location is saved/updated
        var msg = Ext.Msg.show({
            msg: 'Updated Location',
            cls: 'locationNotificationMsg',
            closable: false,
            modal: false,
            minWidth: 180
        });
        msg.getDialog().alignTo('cap-location', 'tr?', [25, -25]);
        setTimeout(function () {
            Ext.Msg.hide();
        }, 2500);
    }
    // deselect whatever control was used to draw/save this feature to set
    // the map back up as originally viewed by user
    var feature_controls = POST.locationMap.getControlsBy('featureControl', true);
    for (var c = 0; c < feature_controls.length; c++) {
        feature_controls[c].deactivate();
    }
    console.debug(location_formfield.geom + ': ' + location_input.value);
};


/**
Using the entered Location, requests that the server perform a search of
all nearby SGC areas and return the results so they can be displayed for the
user's selection.
*/
POST.existing_area_search = function () {
    //TODO: only using the first point of a line here, expand to support other
    //      geometries later
    var location_txt = Ext.getCmp('cap-location').getValue();
    var location_vals = location_txt.split(" ");
    var point_value = location_vals[0];
    var search_radius = Ext.getCmp('area-search-radius').value;
    var search_type = Ext.getCmp('area-search-type').value;
    if (POST.Validator.check_point(point_value)[0] === false || location_txt === 'Not Available') {
        alert('A valid Location is needed for Area Search.');
        return;
    }
    if (!search_radius) {
        alert('Search Radius Missing.');
        return;
    }
    // options.url is what's used for actual loading
    //NOTE: not using cache busting parameter here as shouldn't be needed
    var new_url = OpenLayers.Util.urlAppend(POST.AREA_ZONE_URL,
        'point=' + point_value + '&radius=' + search_radius + '&type=' + search_type);
    POST.areaZoneStore.proxy.protocol.options.url = new_url;
    POST.areaZoneStore.load();
};


/**
For a custom area, currently just a circle, requests that the server perform a
search of all associated SGC areas, with the results added to this new custom
area as the applicable SGCs.
*/
POST.custom_geocode_search = function () {
    //TODO: only using the first point of a line here, expand to support other
    //      geometries later
    var location_txt = Ext.getCmp('cap-location').getValue();
    var location_vals = location_txt.split(" ");
    var point_value = location_vals[0];
    var search_radius = Ext.getCmp('cap-radius').getValue();
    OpenLayers.Request.GET({
        url: POST.AREA_ZONE_URL,
        //NOTE: not using cache busting parameter here as shouldn't be needed
        params: {'point': point_value, 'radius': search_radius, 'type': 'csd',
            'geocode': 'yes'},
        success: function (xhr) {
            try {
                // should be a list of any applicable geocodes
                Ext.getCmp('areaCreateSet').sgcGeocode = JSON.parse(xhr.responseText);
            } catch (err) {
                // not warning user, will continue with existing default 'none'
                console.error('Custom Geocode Error: ' + err);
            }
        },
        failure: function () {
            // not warning user, will continue with existing default 'none'
            console.error('Error geocoding custom boundary');
        }
    });
};


/**
Generate the new CAP message and its XML and Object from the entered values.

@return {String} - the new XML string
@return {Object} - the new CAP object
*/
POST.generate_cap_xml = function () {
    if (!POST.CAP_MESSAGE) {
        console.error('CAP Object missing');
        alert('Unable to load CAP Message.');
    }
    // cloning CAP_MESSAGE each time required for cases where a user
    // goes back and add/removes values after doing a preview which
    // results in a modified CAP_MESSAGE being used to build the post XML
    var new_message = MASAS.Common.clone_object(POST.CAP_MESSAGE);
    
    //TODO: consider using only UTC for this sent date??
    var sent_time = new Date();
    // using Ext extensions to Date for formatting
    new_message.alert.sent = sent_time.format('Y-m-d\\TH:i:sP');
    new_message.alert.status = POST.oldCAP.alert.status;
    if (POST.UPDATE_OPERATION !== 'Clone') {
        new_message.alert.msgType = POST.UPDATE_OPERATION;
        if (POST.oldCAP.alert.references) {
            new_message.alert.references = POST.oldCAP.alert.references + ' ' +
                POST.oldCAP.alert.sender + ',' + POST.oldCAP.alert.identifier + ',' +
                POST.oldCAP.alert.sent;
        } else {
            new_message.alert.references = POST.oldCAP.alert.sender + ',' +
                POST.oldCAP.alert.identifier + ',' + POST.oldCAP.alert.sent;
        }
    }
    // new info_block order for english users
    var en_info = new_message.alert.info[0];
    var fr_info = new_message.alert.info[1];
    // info blocks may be english only or english and french
    //TODO: add better ability to detect the correct info block
    var old_en_info = null;
    var old_fr_info = null;
    if (POST.oldCAP.alert.info instanceof Array) {
        if (POST.oldCAP.alert.info[0].language === 'en-CA') {
            old_en_info = POST.oldCAP.alert.info[0];
        }
        if (POST.oldCAP.alert.info[1].language === 'fr-CA') {
            old_fr_info = POST.oldCAP.alert.info[1];
        }
    } else if (typeof(POST.oldCAP.alert.info) === "object") {
        // english only can be detected a couple ways, missing, en-US, en-CA
        if (!POST.oldCAP.alert.info.language) {
            old_en_info = POST.oldCAP.alert.info;
        } else if (POST.oldCAP.alert.info.language.search('en') !== -1) {
            old_en_info = POST.oldCAP.alert.info;
        }
    }
    
    // carrying these forward, user can't modify
    en_info.event = old_en_info.event;
    en_info.category = old_en_info.category;
    en_info.eventCode = old_en_info.eventCode;
    // adding some form based french values, if no french block will delete later
    if (old_fr_info) {
        fr_info.event = old_fr_info.event;
        fr_info.category = old_fr_info.category;
        fr_info.eventCode = old_fr_info.eventCode;
    }
    en_info.urgency = Ext.getCmp('cap-urgency').getValue().getGroupValue();
    en_info.severity = Ext.getCmp('cap-severity').getValue().getGroupValue();
    en_info.certainty = Ext.getCmp('cap-certainty').getValue().getGroupValue();
    fr_info.urgency = Ext.getCmp('cap-urgency').getValue().getGroupValue();
    fr_info.severity = Ext.getCmp('cap-severity').getValue().getGroupValue();
    fr_info.certainty = Ext.getCmp('cap-certainty').getValue().getGroupValue();
    
    var effective_im = Ext.getCmp('cap-effective-immediate').getValue();
    var effective_dt = Ext.get('cap-effective-dt').getValue();
    var effective_tm = Ext.get('cap-effective-tm').getValue();
    // use a newly entered effective time
    if (!effective_im && effective_dt && effective_tm) {
        en_info.effective = effective_dt + 'T' + effective_tm + ':00' +
            sent_time.format('P');
        fr_info.effective = effective_dt + 'T' + effective_tm + ':00' +
            sent_time.format('P');
    } else {
        if (effective_im && old_en_info.effective) {
            // calculate update according to a previous effective time
            if (POST.oldAtom.entry['met:effective']) {
                // using Atom effective instead of CAP for simplicity
                var effective_old = Date.parseDate(POST.oldAtom.entry['met:effective'],
                    'Y-m-d\\TH:i:s\\Z');
                effective_old = MASAS.Common.adjust_time(effective_old,
                    MASAS.Common.UTC_Local_Offset);
                var effective_old_epoch = effective_old.format('U') * 1000;
                var now_epoch = new Date().getTime();
                if (effective_old_epoch < now_epoch) {
                    // when the previous effective time has already been reached
                    // no need for effective anymore
                    delete en_info.effective;
                    delete fr_info.effective;
                } else {
                    // otherwise change to effective immediately, need to create
                    // a new value that is 30 minutes older than this new sent
                    // time to ensure the Hub will replace the original value
                    var effective_time = new Date();
                    effective_time.setTime(now_epoch - 1800000);
                    en_info.effective = effective_time.format('Y-m-d\\TH:i:00P');
                    fr_info.effective = effective_time.format('Y-m-d\\TH:i:00P');
                }
            } else {
                // likely a rare case where effective was ignored or unable to
                // be processed by the Hub, so just remove to make effective
                // immediate
                delete en_info.effective;
                delete fr_info.effective;
            }
        } else {
            // no effective was previously in use, ignore
            delete en_info.effective;
            delete fr_info.effective;
        }
    }
    
    var expires_dt = Ext.get('cap-expires-dt').getValue();
    var expires_tm = Ext.get('cap-expires-tm').getValue();
    var expires_it = Ext.getCmp('cap-expires-interval').getValue();
    if (expires_dt && expires_tm) {
        en_info.expires = expires_dt + 'T' + expires_tm + ':00' +
            sent_time.format('P');
        fr_info.expires = expires_dt + 'T' + expires_tm + ':00' +
            sent_time.format('P');
    } else if (expires_it) {
        var expires_time = new Date();
        var expires_diff = parseInt(expires_it, 10);
        expires_time = MASAS.Common.adjust_time(expires_time, expires_diff);
        en_info.expires = expires_time.format('Y-m-d\\TH:i:00P');
        fr_info.expires = expires_time.format('Y-m-d\\TH:i:00P');
    } else {
        delete en_info.expires;
        delete fr_info.expires;
    }
    
    en_info.headline = MASAS.Common.xml_encode(Ext.get('cap-en-headline').getValue());
    fr_info.headline = MASAS.Common.xml_encode(Ext.get('cap-fr-headline').getValue());
    // clean out defaults for area that the user didn't fill in
    en_info.headline = en_info.headline.replace(' for (area)', '');
    fr_info.headline = fr_info.headline.replace(' pour (area)', '');
    var en_description = Ext.get('cap-en-description').getValue();
    if (en_description) {
        en_info.description = MASAS.Common.xml_encode(en_description);
    } else {
        delete en_info.description;
    }
    var fr_description = Ext.get('cap-fr-description').getValue();
    if (fr_description) {
        fr_info.description = MASAS.Common.xml_encode(fr_description);
    } else {
        delete fr_info.description;
    }
    var en_instruction = Ext.get('cap-en-instruction').getValue();
    if (en_instruction) {
        en_info.instruction = MASAS.Common.xml_encode(en_instruction);
    } else {
        delete en_info.instruction;
    }
    var fr_instruction = Ext.get('cap-fr-instruction').getValue();
    if (fr_instruction) {
        fr_info.instruction = MASAS.Common.xml_encode(fr_instruction);
    } else {
        delete fr_info.instruction;
    }
    var en_contact = Ext.get('cap-en-contact').getValue();
    if (en_contact) {
        en_info.contact = MASAS.Common.xml_encode(en_contact);
    } else {
        delete en_info.contact;
    }
    var fr_contact = Ext.get('cap-fr-contact').getValue();
    if (fr_contact) {
        fr_info.contact = MASAS.Common.xml_encode(fr_contact);
    } else {
        delete fr_info.contact;
    }
    var en_web = Ext.get('cap-en-web').getValue();
    if (en_web) {
        en_info.web = MASAS.Common.xml_encode(en_web);
    } else {
        delete en_info.web;
    }
    var fr_web = Ext.get('cap-fr-web').getValue();
    if (fr_web) {
        fr_info.web = MASAS.Common.xml_encode(fr_web);
    } else {
        delete fr_info.web;
    }
    
    if (Ext.getCmp('discard-event-location').checked) {
        // location will be based on area polygons instead
        delete en_info.parameter;
        delete fr_info.parameter;
    } else {
        var location_formfield = Ext.getCmp('cap-location');
        if (location_formfield.geom === 'point') {
            en_info.parameter.valueName = 'layer:CAPAN:eventLocation:point';
            fr_info.parameter.valueName = 'layer:CAPAN:eventLocation:point';
        } else if (location_formfield.geom === 'line') {
            en_info.parameter.valueName = 'layer:CAPAN:eventLocation:line';
            fr_info.parameter.valueName = 'layer:CAPAN:eventLocation:line';
        }
        en_info.parameter.value = location_formfield.getValue();
        fr_info.parameter.value = location_formfield.getValue();
    }
    
    var en_area_blocks = [];
    var fr_area_blocks = [];
    // add any old selected areas first before adding new ones
    if (!Ext.getCmp('areaCurrentSet').collapsed) {
        var current_vals = POST.currentAreaGrid.getSelectionModel().getSelections();
        if (old_en_info.area instanceof Array) {
            if (current_vals.length !== 0) {
                for (var i = 0; i < current_vals.length; i++) {
                    old_en_info.area[current_vals[i].data.pos].areaDesc = MASAS.Common.xml_encode(old_en_info.area[current_vals[i].data.pos].areaDesc);
                    en_area_blocks.push(old_en_info.area[current_vals[i].data.pos]);
                }
            }
        } else if (typeof(old_en_info.area) === "object") {
            if (current_vals.length !== 0) {
                // only one old area and its the only selection as well
                old_en_info.area.areaDesc = MASAS.Common.xml_encode(old_en_info.area.areaDesc);
                en_area_blocks.push(old_en_info.area);
            }
        }
        // assuming that english area ordering is the same as french...
        if (old_fr_info) {
            if (old_fr_info.area instanceof Array) {
                if (current_vals.length !== 0) {
                    for (var j = 0; j < current_vals.length; j++) {
                        old_fr_info.area[current_vals[j].data.pos].areaDesc = MASAS.Common.xml_encode(old_fr_info.area[current_vals[j].data.pos].areaDesc);
                        fr_area_blocks.push(old_fr_info.area[current_vals[j].data.pos]);
                    }
                }
            } else if (typeof(old_fr_info.area) === "object") {
                if (current_vals.length !== 0) {
                    // only one old area and its the only selection as well
                    old_fr_info.area.areaDesc = MASAS.Common.xml_encode(old_fr_info.area.areaDesc);
                    fr_area_blocks.push(old_fr_info.area);
                }
            }
        }
    }
    if (!Ext.getCmp('areaCreateSet').collapsed) {
        // custom area with any found geocodes that match
        //TODO: only using the first point of a line here, expand to support other
        //      geometries later
        var location_txt = Ext.getCmp('cap-location').getValue();
        var location_vals = location_txt.split(" ");
        var point_value = location_vals[0];
        var circle_val = point_value + ' ' + Ext.get('cap-radius').getValue();
        var geocode_list = Ext.getCmp('areaCreateSet').sgcGeocode;
        var new_geocodes = [];
        for (var k = 0; k < geocode_list.length; k++) {
            new_geocodes.push({valueName: 'profile:CAP-CP:Location:0.3',
                value: geocode_list[k]});
        }
        en_area_blocks.push({areaDesc: MASAS.Common.xml_encode(Ext.get('cap-en-area-description').getValue()),
            circle: circle_val, geocode: new_geocodes});
        fr_area_blocks.push({areaDesc: MASAS.Common.xml_encode(Ext.get('cap-fr-area-description').getValue()),
            circle: circle_val, geocode: new_geocodes});
    } else if (!Ext.getCmp('areaSelectSet').collapsed) {    
        var zone_vals = POST.areaSearchGrid.getSelectionModel().getSelections();
        for (var k = 0; k < zone_vals.length; k++) {
            var each_zone = zone_vals[k];
            var zone_feature = each_zone.data.feature.clone();
            zone_feature.geometry.transform(POST.locationMap.getProjectionObject(),
                new OpenLayers.Projection('EPSG:4326'));
            // accounting for both single polygons associated with an SGC and
            // multipolygons as provided in the areaSearchGrid GeoJSON data 
            var zone_points = [];
            var zone_poly = '';
            if (zone_feature.geometry.components.length > 1) {
                zone_poly = [];
                for (var l = 0; l < zone_feature.geometry.components.length; l++) {
                    zone_points = [];
                    for (var m = 0; m < zone_feature.geometry.components[l].components[0].components.length; m++) {
                        zone_points.push(zone_feature.geometry.components[l].components[0].components[m].y.toPrecision(8) +
                            ',' + zone_feature.geometry.components[l].components[0].components[m].x.toPrecision(8));
                    }
                    zone_poly.push(zone_points.join(' '));
                }
            } else {
                for (var l = 0; l < zone_feature.geometry.components[0].components.length; l++) {
                    zone_points.push(zone_feature.geometry.components[0].components[l].y.toPrecision(8) +
                        ',' + zone_feature.geometry.components[0].components[l].x.toPrecision(8));
                }
                zone_poly = zone_points.join(' ');
            }
            en_area_blocks.push({areaDesc: each_zone.data.e_name,
                polygon: zone_poly,
                geocode: {valueName: 'profile:CAP-CP:Location:0.3',
                    value: each_zone.data.sgc}
            });
            fr_area_blocks.push({areaDesc: each_zone.data.f_name,
                polygon: zone_poly,
                geocode: {valueName: 'profile:CAP-CP:Location:0.3',
                    value: each_zone.data.sgc}
            });
        }
    }
    // check for single areas which don't get set as an array of objects
    if (en_area_blocks.length === 1) {
        en_info.area = en_area_blocks[0];
    } else {
        en_info.area = en_area_blocks;
    }
    if (fr_area_blocks.length === 1) {
        fr_info.area = fr_area_blocks[0];
    } else {
        fr_info.area = fr_area_blocks;
    }
    
    if (Ext.getCmp('cap-add-french').collapsed) {
        // remove the french info block by converting from an array of two
        // objects to just one
        new_message.alert.info = en_info;
    }
    
    var new_xml = xmlJsonClass.json2xml(new_message, '');
    new_xml = MASAS.Common.format_xml(new_xml);
    new_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + new_xml;
    
    return [new_xml, new_message];
};


/**
Show a preview window to review the new CAP message both with a template for user
friendlier display and the XML.
*/
POST.preview_cap_xml = function () {
    // only one review window allowed
    if (POST.reviewWindow) {
        POST.reviewWindow.destroy();
    }
    
    var preview_xml_vals = POST.generate_cap_xml();
    var display_xml = preview_xml_vals[0];
    try {
        // XTemplate from post-templates.js
        var display_html = MASAS.Template.CAPToHTML.apply(preview_xml_vals[1]);
    } catch (err) {
        console.error('Preview CAP Template Error: ' + err);
        alert('CAP Template Error.');
        return;
    }
    display_xml = display_xml.replace(/</g, '&lt;');
    display_xml = display_xml.replace(/>/g, '&gt;');
    display_html += '<div class="reviewClose"><a href="#" onclick="' +
        'POST.reviewWindow.close(); return false;">Close Preview</a></div>' +
        '<div class="reviewXmlShow"><a href="#" onclick="' +
        'document.getElementById(\'review-xml-box\').style.display=\'block\'; return false;">' +
        'Show XML</a></div><div id="review-xml-box" style="display: none;">' +
        '<pre>' + display_xml + '</pre></div>';
    
    POST.reviewWindow = new Ext.Window({
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        title: 'CAP Preview <a class="titleClose" href="#" ' +
            'onclick="POST.reviewWindow.close(); return false;">Close</a>',
        closable: true,
        width: 700,
        height: 500,
        autoScroll: true,
        bodyStyle: 'background-color: white;',
        layout: 'fit',
        html: display_html
    });
    POST.reviewWindow.show(this);
};


/**
Post this new CAP message to the server.  Used by cloning.
*/
POST.post_new_alert = function () {
    var posting_html = '<h1>Posting Alert</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: the following is not working to prevent going back
    //Ext.getCmp('card-prev').setDisabled();
    var new_cap_xml_vals = POST.generate_cap_xml();
    console.debug('Posting New CAP Alert');
    var do_post = new OpenLayers.Request.POST({
        url: POST.FEED_URL,
        params: {'secret': POST.USER_SECRET},
        proxy: POST.AJAX_PROXY_URL,
        headers: {'Content-Type': 'application/common-alerting-protocol+xml'},
        data: new_cap_xml_vals[0],
        callback: POST.Common.post_complete_result
    });
};


/**
Post this CAP message update to the server.
*/
POST.post_alert_update = function () {
    var posting_html = '<h1>Posting Alert</h1><div class="postingWait"></div>';
    document.getElementById('postingBox').innerHTML = posting_html;
    POST.postingTimer = setTimeout(POST.Common.post_timeout_error, 15000);
    // once a post is made, don't allow going back to retry with this session's data
    //TODO: not working
    //Ext.getCmp('card-prev').setDisabled();
    // find the content link to update/cancel CAP content
    var content_url = null;
    for (var i = 0; i < POST.oldAtom.entry.link.length; i++) {
        if (POST.oldAtom.entry.link[i]['@rel'] === 'edit-media') {
            content_url = POST.oldAtom.entry.link[i]['@href'];
        }
    }
    if (content_url === null) {
        console.error('Unable to find edit url to post update to Alert');
        alert('Cannot Post this Alert.');
        return;
    }
    var new_cap_xml_vals = POST.generate_cap_xml();
    console.debug('Posting Alert Update');
    var do_post = new OpenLayers.Request.PUT({
        url: content_url,
        params: {'secret': POST.USER_SECRET},
        proxy: POST.AJAX_PROXY_URL,
        headers: {'Content-Type': 'application/common-alerting-protocol+xml'},
        data: new_cap_xml_vals[0],
        callback: POST.Common.post_complete_result
    });
};
