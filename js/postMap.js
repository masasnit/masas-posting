/**
MASAS Posting Tool - Maps
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires mapTools.js
@requires mapLayers.js
@requires src/common.js
*/

/*global POST,Ext,OpenLayers,GeoExt,google */
Ext.namespace('POST.Map');

POST.locationMap = null;
POST.locationLayers = {};
POST.locationControls = {};
POST.selectMap = null;
POST.selectLayers = {};
POST.selectControls = {};

// short version from viewing tool used for styling
POST.Map.category_colour_map = {
    'red': '#FF0000',
    'yellow': '#FFFF00',
    'green': '#008000',
    'gray': '#696969',
    'black': '#000000',
    'blue': '#0000FF',
    'purple': '#800080'
};

// default style for displaying all geometry types but not icons.
// similar to viewing tool but with thicker lines
POST.Map.default_map_style = new OpenLayers.Style({
    pointRadius: 8,
    fillOpacity: 0.5,
    fillColor: '${myFillColor}',
    strokeOpacity: 0.7,
    strokeColor: '${myStrokeColor}',
    strokeWidth: '${myStrokeWidth}'
}, {
    context: {
        myStrokeWidth: function (feature) {
            if (feature.geometry &&
            feature.geometry instanceof OpenLayers.Geometry.LineString) {
                // make lines easier to see by being thicker
                return 6;
            }
            return 2;
        },
        myStrokeColor: function (feature) {
            if (feature.geometry &&
            feature.geometry instanceof OpenLayers.Geometry.LineString) {
                // lines are shown in their respective colour
                if (feature.attributes.colour) {
                    var colour = feature.attributes.colour.toLowerCase();
                    if (POST.Map.category_colour_map[colour]) {
                        return POST.Map.category_colour_map[colour];
                    }
                }
            }
            // black is the default for all other geometries as its
            // used to outline the fill colour
            return '#000000';
        },
        myFillColor: function (feature) {
            if (feature.attributes.colour) {
                var colour = feature.attributes.colour.toLowerCase();
                if (POST.Map.category_colour_map[colour]) {
                    return POST.Map.category_colour_map[colour];
                }
            }
            // OpenLayers default fill colour
            return '#ee9900';
        }
    }
});


/**
 * Create and configure the Location map, that is used by all applications.
 * Map customizations that will apply to all applications should be done
 * here.  Further customizations for individual applications should be done in
 * those applications .onReady sections.
 */
POST.Map.initialize_location_map = function () {
    /**
    Initialize the Location Map
    */
    POST.locationMap = new OpenLayers.Map('locationMap', {
        projection: new OpenLayers.Projection('EPSG:900913'),
        displayProjection: new OpenLayers.Projection('EPSG:4326'),
        theme: null
    });
    
    // Override so that it zooms to the user's default view.  With no default
    // extent set for GeoExt, it will use this instead.
    POST.locationMap.zoomToMaxExtent = POST.MapTools.zoom_to_saved_extent;
   
    
    /**
    Add the base layers to Location Map.  Customize as necessary.
    */
    
    // Google Maps can fail to load, which can happen with some networks
    // with restrictive firewalls
    if ('google' in window) {
        var google_street = new OpenLayers.Layer.Google('Google Street', {
            type: google.maps.MapTypeId.ROADMAP,
            isBaseLayer: true,
            numZoomLevels: 21
        });
        var google_satellite = new OpenLayers.Layer.Google('Google Satellite', {
            type: google.maps.MapTypeId.HYBRID,
            isBaseLayer: true,
            numZoomLevels: 21
        });
        POST.locationMap.addLayers([google_street, google_satellite]);
    } else {
        console.error('Unable to load google maps, OSM fallback');
        alert('A problem with your network prevented the Google Map from ' +
            'loading properly.  Open Street Map will be used instead.  If ' +
            'you still want to use the Google Map, you can reload this ' +
            'webpage to try again.');
    }
    var osm = new OpenLayers.Layer.OSM('OpenStreetMap');
    POST.locationMap.addLayer(osm);
   
    
    /* Permanent layers for Location Map */
    
    /**
     * The location drawing layer
     */
    POST.locationLayers.location = new OpenLayers.Layer.Vector('Location', {
        isBaseLayer: false,
        visibility: true,
        styleMap: new OpenLayers.StyleMap({
            'default': POST.Map.default_map_style
        })
    });
    
    /**
     * The current feed entries layer.
     */
    POST.locationLayers.current = new OpenLayers.Layer.Vector('Current', {
        isBaseLayer: false,
        visibility: false,
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                // simple style that supports only point icons
                pointRadius: 15,
                externalGraphic: '${icon}',
                graphicOpacity: 0.8
            }),
            'select': new OpenLayers.Style({ pointRadius: 18 })
        }),
        projection: new OpenLayers.Projection('EPSG:4326'),
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: new OpenLayers.Protocol.HTTP({
            // set later based on viewport
            url: '',
            format: new OpenLayers.Format.MASASFeed()
        }),
        eventListeners: {
            loadend: function () {
                POST.locationControls.currentLoading.minimizeControl();
            }//,
            //TODO: the viewing tool uses checkGeometry after loading to adjust
            //      the icon positions so that if you are "inside" a polygon or
            //      at an edge you will see the icon there instead.  However the
            //      code in checkGeometry would need modification to make it
            //      less viewing tool specific to be used.  Is it even needed?
            /*
            featuresadded: function () {
                this.protocol.format.checkGeometry({onLoad: true});
            }
            */
        }
    });
    
    /**
     * The current feed entries original geometry (lines/polygons) layer.
     */
    POST.locationLayers.geometry = new OpenLayers.Layer.Vector('CurrentGeom', {
        isBaseLayer: false,
        visibility: false,
        styleMap: new OpenLayers.StyleMap({
            'default': POST.Map.default_map_style
        })
    });
    
    /**
     * The KML drawing layer.
     */
    // additional style for icons
    var kml_icon_style = new OpenLayers.Style({
        pointRadius: 15,
        externalGraphic: '${myIcon}',
        graphicOpacity: 0.8
    }, {
        context: {
            myIcon: function (feature) {
                if (feature.attributes.icon) {
                    return POST.ICON_PREVIEW_URL + feature.attributes.icon +
                        '/small.png';
                }
            }
        }
    });
    POST.locationLayers.kml = new OpenLayers.Layer.Vector('KML', {
        isBaseLayer: false,
        visibility: false,
        styleMap: new OpenLayers.StyleMap({
            'default': POST.Map.default_map_style,
            'icon': kml_icon_style
        })
    });
    
    /**
     * The address search layer.
     */
    POST.locationLayers.address = new OpenLayers.Layer.Vector('AddressSearch', {
        isBaseLayer: false,
        visibility: true
    });
    
    /**
     * SGC display for CAP alerts
     */
    POST.locationLayers.sgc = new OpenLayers.Layer.Vector('SGC', {
        isBaseLayer: false,
        visibility: false
    });
    
    POST.locationMap.addLayers(Ext.getValues(POST.locationLayers));
    
    /* Permanent controls for Location Map */
    
    /**
     * Navigation history
     */
    POST.locationControls.navHistory = new OpenLayers.Control.NavigationHistory();
    
    /**
     * Map layer attribution
     */
    POST.locationControls.attribution = new OpenLayers.Control.Attribution();
    
    /**
     * Current layer loading panel
     */
    POST.locationControls.currentLoading = new OpenLayers.Control.LoadingPanel();
    
    /**
     * Current layer feature selection
     * 
     * Not using [layer] here so that popups won't obscure the view
     * when a user is trying to select another location feature on the map
     */
    POST.locationControls.currentSelect = new OpenLayers.Control.SelectFeature(POST.locationLayers.current, {
        // mouse users can hover or click while touch users can only click
        hover: (POST.TOUCH_ENABLE) ? false : true,
        highlightOnly: true,
        autoActivate: true,
        eventListeners: {
            featurehighlighted: POST.Common.show_current_popup,
            featureunhighlighted: POST.Common.close_current_popup
        }
    });
    
    POST.locationMap.addControls(Ext.getValues(POST.locationControls));
};


/**
 * Create and configure the Select map, that is used by the modify applications.
 * Map customizations that will apply to modify applications should be done
 * here.  Further customizations for individual applications should be done in
 * those applications .onReady sections.
 */
POST.Map.initialize_select_map = function () {
    /**
     * Initialize the Select Map
     */
    POST.selectMap = new OpenLayers.Map('selectMap', {
        projection: new OpenLayers.Projection('EPSG:900913'),
        displayProjection: new OpenLayers.Projection('EPSG:4326'),
        theme: null
    });
    
    
    /**
     * Add a single base layer to the Select Map.
     */
    if ('google' in window) {
        var google_street = new OpenLayers.Layer.Google('Google Street', {
            type: google.maps.MapTypeId.ROADMAP,
            isBaseLayer: true,
            numZoomLevels: 21
        });
        POST.selectMap.addLayer(google_street);
    } else {
        // When Google Maps fails to load, which can happen with some networks
        // with restrictive firewalls, offer a fallback option.
        // The alert dialog should already have been shown to the user
        var osm = new OpenLayers.Layer.OSM('OpenStreetMap', [
            'http://otile1.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
            'http://otile2.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
            'http://otile3.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png',
            'http://otile4.mqcdn.com/tiles/1.0.0/map/${z}/${x}/${y}.png'
        ], {
            tileOptions: {crossOriginKeyword: null},
            isBaseLayer: true,
            numZoomLevels: 19
        });
        POST.selectMap.addLayer(osm);
    }
    
    /* Permanent layers for Location Map */
    
    /**
     * The select entries to update/cancel layer.
     */
    POST.selectLayers.select = new OpenLayers.Layer.Vector('Select', {
        isBaseLayer: false,
        visibility: true,
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                // simple style that supports only point icons
                pointRadius: 15,
                externalGraphic: '${icon}',
                graphicOpacity: 0.8
            }),
            'select': new OpenLayers.Style({ pointRadius: 18 })
        })
    });
    POST.selectLayers.select.events.on({
        featureselected: function (evt) {
            if (!POST.TOUCH_ENABLE) {
                // mouse users can hover and so to differentiate when a click
                // to select a feature takes place, close the hover popup
                evt.feature.isSelected = true;
                POST.Common.close_select_popup(evt);
            }
        },
        featureunselected: function (evt) {
            if (!POST.TOUCH_ENABLE) {
                evt.feature.isSelected = false;
            }
        },
        featuresadded: function () {
            POST.selectMap.zoomToExtent(POST.selectLayers.select.getDataExtent());
        }
    });
    
    /**
     * The select entries original geometry (lines/polygons) layer.
     */
    POST.selectLayers.geometry = new OpenLayers.Layer.Vector('SelectGeom', {
        isBaseLayer: false,
        visibility: true,
        styleMap: new OpenLayers.StyleMap({
            'default': POST.Map.default_map_style
        })
    });
    
    POST.selectMap.addLayers(Ext.getValues(POST.selectLayers));
    
    /* Permanent controls for Select Map */
    
    /**
     * Select layer loading panel
     */
    POST.selectControls.loading = new OpenLayers.Control.LoadingPanel();
    
    /**
     * Select layer feature selection
     */
    if (!POST.TOUCH_ENABLE) {
        // mouse users can hover over features
        POST.selectControls.feature = new OpenLayers.Control.SelectFeature(POST.selectLayers.select, {
            hover: true,
            highlightOnly: true,
            autoActivate: true,
            eventListeners: {
                featurehighlighted: POST.Common.show_select_popup,
                featureunhighlighted: POST.Common.close_select_popup
            }
        });
    }
    
    POST.selectMap.addControls(Ext.getValues(POST.selectControls));
};


/**
 * Drawing and editing controls used on the Location layer.
 *
 * @param {Boolean} - Entry setup = true, otherwise CAP
 */
POST.Map.initialize_drawing_controls = function (entry_setup) {
    var location_layer = POST.locationLayers.location;
    
    /**
     * Feature editing panel, common to both Entry and CAP
     */
    POST.locationControls.editPanel = new OpenLayers.Control.Panel({
        displayClass: 'editPanel',
        allowDepress: true
    });
    POST.locationControls.locationModify = new OpenLayers.Control.ModifyFeature(location_layer, {
        displayClass: 'modifyButton',
        title: 'Modify Drawing',
        hover: false,
        // used by save_location_feature to deactivate feature controls when
        // any feature modification operation has been completed
        featureControl: true,
        // used to deactivate this panel's controls when another panel is
        // active
        controlPanelName: 'edit',
        // the tool tip shown for this control
        tipText: 'Click a drawing, then click and drag points to modify, Save when done',
        // aligns the top of the tool tip with the top of the icon
        tipOffset: [0, 0]
    });
    POST.locationControls.editPanel.addControls([
        POST.locationControls.locationModify,
        // using [location_layer] to allow selection when current layer is active
        new OpenLayers.Control.SelectFeature([location_layer], {
            displayClass: 'saveButton',
            title: 'Save Drawing',
            hover: false,
            featureControl: true,
            controlPanelName: 'edit',
            onSelect: POST.save_location_feature,
            tipText: 'Click a drawing to save as new Location',
            tipOffset: [0, 40]
        }),
        new OpenLayers.Control.SelectFeature([location_layer], {
            displayClass: 'deleteButton',
            title: 'Delete Drawing',
            hover: false,
            featureControl: true,
            controlPanelName: 'edit',
            onSelect: function (feature) {
                if (feature) {
                    if (confirm('Delete this Drawing?')) {
                        feature.destroy();
                    }
                }
            },
            tipText: 'Click a drawing to delete',
            tipOffset: [0, 80]
        })
    ]);
    // tooltips added after creation so they don't override the normal
    // activate event, but add a second one
    for (var i = 0; i < POST.locationControls.editPanel.controls.length; i++) {
        POST.locationControls.editPanel.controls[i].events.register('activate',
            POST.locationControls.editPanel.controls[i],
            function () {
                if (!this.toolTip) {
                    var tip = new Ext.Tip({
                        width: 90,
                        html: this.tipText,
                        bodyStyle: 'font-size: 10px;',
                        header: false
                    });
                    this.toolTip = tip;
                }
                this.toolTip.show();
                // uses the top right corner of the panel for alignment as its
                // a known div with an id that can be accessed, individual
                // control id's aren't available for alignment purposes
                this.toolTip.el.alignTo(this.panel_div.offsetParent.id,
                    'tl-tr', this.tipOffset);
                // prevent any confusion by deactivating the other panel's controls
                Ext.each(POST.locationMap.getControlsBy('controlPanelName', 'draw'),
                    function (item) {
                        item.deactivate();
                    });
            });
        POST.locationControls.editPanel.controls[i].events.register('deactivate',
            POST.locationControls.editPanel.controls[i],
            function () {
                if (this.toolTip) {
                    this.toolTip.hide();
                }
            });
    }
    POST.locationMap.addControl(POST.locationControls.editPanel);
    
    
    /**
     * Feature drawing panel, additional geometries for Entry vs CAP.
     */
    POST.locationControls.drawPanel = new OpenLayers.Control.Panel({
        displayClass: 'drawPanel',
        allowDepress: true
    });
    var draw_controls = [
        new OpenLayers.Control.DrawFeature(location_layer,
            OpenLayers.Handler.Point, {
                displayClass: 'pointButton',
                title: 'Draw Point',
                featureControl: true,
                controlPanelName: 'draw',
                featureAdded: POST.save_location_feature,
                tipText: 'Click on the map to draw a new point',
                tipOffset: [0, 0]
            }),
        new OpenLayers.Control.DrawFeature(location_layer,
            OpenLayers.Handler.Path, {
                displayClass: 'lineButton',
                title: 'Draw Line',
                featureControl: true,
                controlPanelName: 'draw',
                featureAdded: POST.save_location_feature,
                tipText: 'Single-click to start and add points, double-click to end',
                tipOffset: [0, 40]
            })
    ];
    if (entry_setup) {
        draw_controls.push(new OpenLayers.Control.DrawFeature(location_layer,
            OpenLayers.Handler.Polygon, {
                displayClass: 'polygonButton',
                title: 'Draw Polygon',
                featureControl: true,
                controlPanelName: 'draw',
                featureAdded: POST.save_location_feature,
                tipText: 'Single-click to start and add points, double-click to end',
                tipOffset: [0, 80]
            }));
        draw_controls.push(new OpenLayers.Control.DrawFeature(location_layer,
            OpenLayers.Handler.Point, {
                displayClass: 'circleButton',
                title: 'Draw Circle',
                featureControl: true,
                controlPanelName: 'draw',
                featureAdded: POST.create_circle_feature,
                tipText: 'Click to draw the centre, set Radius, then Draw, Save when done',
                tipOffset: [0, 120]
            }));
        draw_controls.push(new OpenLayers.Control.DrawFeature(location_layer,
            OpenLayers.Handler.RegularPolygon, {
                displayClass: 'boxButton',
                title: 'Draw Box',
                featureControl: true,
                controlPanelName: 'draw',
                featureAdded: function (feature) {
                    feature.is_box = true;
                    POST.save_location_feature(feature);
                },
                handlerOptions: { sides: 4, irregular: true },
                tipText: 'Click to draw one corner, drag to other corner',
                tipOffset: [0, 160]
            }));
    }
    POST.locationControls.drawPanel.addControls(draw_controls);
    // tooltip setup similar to editPanel
    for (var i = 0; i < POST.locationControls.drawPanel.controls.length; i++) {
        POST.locationControls.drawPanel.controls[i].events.register('activate',
            POST.locationControls.drawPanel.controls[i],
            function () {
                if (!this.toolTip) {
                    var tip = new Ext.Tip({
                        width: 75,
                        html: this.tipText,
                        bodyStyle: 'font-size: 10px;',
                        header: false
                    });
                    this.toolTip = tip;
                }
                this.toolTip.show();
                this.toolTip.el.alignTo(this.panel_div.offsetParent.id,
                    'tl-tr', this.tipOffset);
                Ext.each(POST.locationMap.getControlsBy('controlPanelName', 'edit'),
                    function (item) {
                        item.deactivate();
                    });
            });
        POST.locationControls.drawPanel.controls[i].events.register('deactivate',
            POST.locationControls.drawPanel.controls[i],
            function () {
                if (this.toolTip) {
                    this.toolTip.hide();
                }
            });
    }
    POST.locationMap.addControl(POST.locationControls.drawPanel);
    
    
    /**
     * Circles need special feature modification event handlers
     */
    if (entry_setup) {
        location_layer.events.register('beforefeaturemodified', null, function (obj) {
            if (obj.feature.circle_center) {
                //TODO: openlayers 2.13.1 changes meant the previous method
                //      of using the modifyfeature's built in selectfeature
                //      control won't work.
                alert('Circles cannot be modified.  Create a new circle and' +
                    ' save it instead.  You can delete old circle when complete.');
                setTimeout(function () {
                    POST.locationControls.locationModify.deactivate();
                }, 500);
                
                // cancel the modify attempt
                return false;
            }
        });
        
        var kml_layer = POST.locationLayers.kml;
        kml_layer.events.register('beforefeaturemodified', null, function (obj) {
            if (obj.feature.circle_center) {
                //TODO: openlayers 2.13.1 changes meant the previous method
                //      of using the modifyfeature's built in selectfeature
                //      control won't work.
                alert('Circles cannot be modified.  Create a new circle and' +
                    ' save it instead.  You can delete old circle when complete.');
                setTimeout(function () {
                    POST.locationControls.locationModify.deactivate();
                }, 500);
                
                // cancel the modify attempt
                return false;
            }
        });
    }
};
