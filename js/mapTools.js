/**
MASAS Posting Tool - Map Tools / Bottom Toolbar
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings and handlers needed for
the bottom toolbar and tools of the map.

@requires src/common.js
@requires src/AddressSearch.js
@requires src/KML.js
*/

/*global POST,MASAS,Ext,OpenLayers,GeoExt,google */

Ext.ns('POST.MapTools');

// stores button to clear location if needed
POST.MapTools.locationClearBtn = null;


/**
 * Initalize the toolbar the map and controls are setup and the GeoExt
 * MapPanel is ready to be created
 *
 * @param {Boolean} - Entry setup = true, otherwise CAP
 */
POST.MapTools.initialize = function(entry_setup) {
    
    // Tools menu items
    var tools_menu = [{
        text: 'Import Geometry',
        handler: POST.MapTools.import_geometry_window
    }, {
        text: 'Clear All Features',
        handler: POST.MapTools.clear_all_features
    }, {
        xtype: 'menucheckitem',
        text: 'Scale Line',
        listeners: {
            'checkchange': POST.MapTools.scale_line_toggle
        }
    }, {
        xtype: 'menucheckitem',
        text: 'Graticule',
        listeners: {
            'checkchange': POST.MapTools.graticule_toggle
        }
    }];
    // streetview is optional
    if ('google' in window) {
        tools_menu.push({
            text: 'Google Street View',
            handler: POST.MapTools.streetview_activate
        });
    }
    tools_menu.push({
        text: 'Reset Layer Loading Indicator',
        handler: function () {
            // the layer loading indicator can get stuck in place sometimes
            // while waiting for a slow layer to load, this will reset it
            POST.locationControls.currentLoading.counter = 0;
            POST.locationControls.currentLoading.minimizeControl();
        }
    });
    
    // POST.locationControls.navHistory must already have been created
    var navPrevious = new GeoExt.Action({
        text: 'Previous',
        control: POST.locationControls.navHistory.previous,
        disabled: true,
        tooltip: 'Zoom to previous view from history',
        iconCls: 'btnLeftArrow'
    });
    var navNext = new GeoExt.Action({
        text: 'Next',
        control: POST.locationControls.navHistory.next,
        disabled: true,
        tooltip: 'Zoom to next view in history',
        iconCls: 'btnRightArrow',
        iconAlign: 'right'
    });
    
    // Bottom toolbar on the map
    POST.mapToolbarBottom = [{
        xtype: 'tbspacer',
        width: 10
    }, {
        text: 'Tools',
        menu: {
            ignoreParentClicks: true,
            plain: true,
            items: tools_menu
        }
    }, {
        xtype: 'tbseparator',
        width: 20
    }, {
        xtype: 'button',
        text: 'Saved Extent',
        tooltip: 'Zoom to your saved map extent',
        handler: POST.MapTools.zoom_to_saved_extent
    }, {
        xtype: 'tbseparator',
        width: 10
    },
    navPrevious,
    navNext,
    {
        xtype: 'tbseparator',
        width: 10
    }, {
        xtype: 'button',
        iconCls: 'btnGeolocate',
        width: 40,
        tooltip: 'Zoom to your current location (GPS) on the map',
        handler: POST.MapTools.zoom_to_geolocation
    }, {
        xtype: 'tbspacer',
        width: 50
    }];
    
    // optional address search box, relies on a back-end geocoder
    if (POST.ADDRESS_SEARCH_URL) {
        // a button to clear any address locations from the map.  If I put this
        // in the toolbar after the search box, it doesn't show up, not sure why
        POST.MapTools.locationClearBtn = new Ext.Button({
            tooltip: 'Clear address locations',
            hidden: true,
            iconCls: 'btnDelete',
            handler: function (btn, evt) {
                POST.locationLayers.address.destroyFeatures();
                btn.hide();
                //TODO: decide if the search box also needs to be cleared, or if the
                //      previous value stays to assist user in refining their search
            }
        });
        POST.mapToolbarBottom.push(POST.MapTools.locationClearBtn);
        POST.mapToolbarBottom.push(new MASAS.AddressSearchCombo({
            'url': POST.ADDRESS_SEARCH_URL,
            'layer': POST.locationLayers.address,
            'clearBtn': POST.MapTools.locationClearBtn
        }));
    }
    
    POST.mapToolbarBottom.push('->');
    
    POST.mapToolbarBottom.push(new Ext.Button({
        text: 'View Current Feed',
        width: 120,
        tooltip: 'View current feed entries',
        enableToggle: true,
        handler: POST.MapTools.current_view_toggle
    }));
    
    if (entry_setup) {
        POST.mapToolbarBottom.push('|');
        POST.mapToolbarBottom.push(new Ext.Button({
            text: 'KML Layer',
            width: 90,
            tooltip: 'Enable the KML drawing layer',
            enableToggle: true,
            handler: POST.MapTools.toggle_kml
        }));
    }
};


/**
 * Shows a window that allows geometry to be imported to the map
 */
POST.MapTools.import_geometry_window = function () {
    var win_title = 'Import Geometry';
    var win = new Ext.Window({
        title: win_title,
        closable: true,
        width: 500,
        height: 150,
        layout: 'fit',
        items: []
    });
    var filePanel = new Ext.FormPanel({
        fileUpload: true,
        frame: true,
        labelWidth: 40,
        defaults: { allowBlank: false, msgTarget: 'side' },
        items: [{
            xtype: 'displayfield',
            html: 'Importing geometry only, from a shapefile in EPSG:4326' +
                ' projection, is currently supported.  Once imported, Save' +
                ' your selections accordingly.'
        }, {
            xtype: 'fileuploadfield',
            fieldLabel: '<b>File</b>',
            name: 'attachment-file',
            emptyText: 'Select a file to import',
            width: 350
        }],
        buttons: [{
            text: 'Import',
            handler: function (cmp) {
                if (filePanel.getForm().isValid()) {
                    filePanel.getForm().submit({
                        url: POST.GEOMETRY_IMPORT_URL,
                        submitEmptyText: false,
                        waitMsg: 'Uploading the file...',
                        success: function (form, response) {
                            if (response.result && response.result.data) {
                                // importing to whatever is currently active
                                var target_layer = POST.locationLayers.location;
                                if (POST.locationLayers.kml.getVisibility()) {
                                    target_layer = POST.locationLayers.kml;
                                }
                                var json_parser = new OpenLayers.Format.GeoJSON({
                                    externalProjection: new OpenLayers.Projection('EPSG:4326'),
                                    internalProjection: POST.locationMap.getProjectionObject()
                                });
                                target_layer.addFeatures(json_parser.read(response.result.data));
                                POST.locationMap.zoomToExtent(target_layer.getDataExtent());
                                console.debug('Imported geometry');
                                Ext.Msg.alert('Success', 'Import Geometry Complete');
                            } else {
                                console.error('Import error with loaded geometry');
                                Ext.Msg.alert('Import Error',
                                    'Unable to import the geometry.');
                            }
                            // access window this way as form doesn't have
                            // ownerCt, ref, or other common methods
                            win.close();
                        },
                        failure: function (form, response) {
                            console.error('Import Error: ' +
                                response.result.message);
                            Ext.Msg.alert('Import Error',
                                'Unable to upload the file.  ' +
                                    response.result.message);
                            win.close();
                        }
                    });
                }
            }
        }]
    });
    // adding to items this way to allow access to win by form handlers
    win.add(filePanel);
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" ' +
        'onclick="Ext.getCmp(\'' + win.getId() +
        '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Removes all features from the location or kml layer, whichever is currently
 * in use.
 */
POST.MapTools.clear_all_features = function () {
    Ext.Msg.confirm('Clear All Features',
        'Are you sure that you want to remove all features from the map?',
        function (confirm) {
            if (confirm === 'yes') {
                if (POST.locationLayers.kml.getVisibility()) {
                    POST.locationLayers.kml.removeAllFeatures();
                } else {
                    POST.locationLayers.location.removeAllFeatures();
                }
            }
        }
    );
};


/**
 * Zooms the map to a saved extent.  Normally its a cookie value that is saved
 * using the Viewing Tool Save Settings button.  Is triggered by both the
 * Saved View button on the toolbar and when the Location Map's initial
 * extent is set by GeoExt.MapPanel
 */
POST.MapTools.zoom_to_saved_extent = function () {
    if (POST.MAP_DEFAULT_VIEW) {
        var default_view_bounds = new OpenLayers.Bounds.fromString(POST.MAP_DEFAULT_VIEW);
        default_view_bounds.transform(new OpenLayers.Projection('EPSG:4326'),
            POST.locationMap.getProjectionObject());
        POST.locationMap.zoomToExtent(default_view_bounds);
        console.debug('Zoom to map default view');
    } else {
        var mapstate = Ext.state.Manager.get('map');
        if (mapstate) {
            POST.locationMap.setCenter(new OpenLayers.LonLat(mapstate.x, mapstate.y),
                mapstate.zoom);
            console.debug('Zoom to state saved extent');
        } else {
            POST.locationMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
                new OpenLayers.Projection('EPSG:4326'),
                POST.locationMap.getProjectionObject()), 4);
            console.debug('Zoom to north america view');
        }
    }
};


/**
 * Uses the browser's geolocation functions to zoom the map to the user's
 * approximate location.  If GPS is available its fairly accurate, but if not
 * the browser may try Geo IP based methods which are pretty rough.
 */
POST.MapTools.zoom_to_geolocation = function () {
    if (!navigator.geolocation) {
        console.debug('Geolocation not supported by browser');
        alert('Geolocation is not supported by your browser.');
        return;
    }
    if (Ext.Msg.isVisible()) {
        // since Ext.Msg is a singleton, will have to wait until another
        // operation such as a feed load is complete
        console.debug('Geolocate wait until other operation completes');
        alert('Please wait until other loading operations are complete.');
        return;
    }
    Ext.Msg.show({
        title: null,
        msg: 'Attempting to locate your current position...',
        buttons: Ext.Msg.CANCEL,
        // since there is no cleanup or way to cancel a pending
        // getCurrentPosition call, cancels are very simple
        fn: function () {
            console.debug('Geolocate Cancel');
        },
        closable: false,
        wait: true,
        modal: true,
        minWidth: Ext.Msg.minProgressWidth,
        waitConfig: null
    });
    navigator.geolocation.getCurrentPosition(POST.MapTools.geolocation_result_success,
        POST.MapTools.geolocation_result_error, {
            //TODO: work out the best timeout value to use
            timeout: 30000,
            // allow a cached position if less than 1 minute old
            maximumAge: 60000
        }
    );
};


/**
 * Success handler for geolocation
 *
 * @param {Object} - the resulting position with values in .coords
 */
POST.MapTools.geolocation_result_success = function (position) {
    if (Ext.Msg.isVisible()) {
        Ext.Msg.hide();
        console.debug('Geolocation Success');
        // use the location layer similar to address search
        var location_xy = new OpenLayers.LonLat(position.coords.longitude,
            position.coords.latitude);
        location_xy.transform(new OpenLayers.Projection('EPSG:4326'),
            POST.locationMap.getProjectionObject());
        var location_point = new OpenLayers.Geometry.Point(location_xy.lon,
            location_xy.lat);
        if (POST.TOUCH_ENABLE) {
	    // its more difficult for a tablet user to click exactly on the spot
	    // that has just been geolocated, so assume they want a point in
	    // that location and create it for them
	    var location_feature = new OpenLayers.Feature.Vector(location_point);
	    POST.save_location_feature(location_feature, true);
	    POST.locationLayers.location.addFeatures([location_feature]);
	} else {
            var location_feature = new OpenLayers.Feature.Vector(location_point, null, {
                fillColor: 'red',
                fillOpacity: 1,
                strokeColor: 'red',
                strokeOpacity: 1,
                pointRadius: 6,
                //TODO: reverse geocode the location and add the address to this label
                //      and to the address search box?
                label: 'Your Location',
                labelYOffset: 15
            });
            POST.locationLayers.address.addFeatures([location_feature]);
            // enable the clear button on the toolbar
            if (POST.MapTools.locationClearBtn) {
                POST.MapTools.locationClearBtn.show();
            }
        }
        POST.locationMap.setCenter(location_xy, 14);
    }
    // if the message box wasn't visible, assume the user cancelled
    // the geolocation and do nothing
};


/**
 * Error handler for geolocation
 * 
 * @param {Object} - error results, usually a numeric value in .code
 */
POST.MapTools.geolocation_result_error = function (error) {
    if (Ext.Msg.isVisible()) {
        Ext.Msg.hide();
        var err_msg = 'Geolocation Error';
        // relying on codes vs error.message as it should be more cross-browser
        var err_codes = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        if (error.code) {
            err_msg += ' - ' + err_codes[error.code];
        }
        console.error(err_msg);
        alert(err_msg);
    }
};


/**
 * Hide or Show the scale line
 *
 * @param {Object} - the Ext.menu.checkItem component
 * @param {Boolean} - checked state
 */
POST.MapTools.scale_line_toggle = function (cmp, checked) {
    console.debug('Scale line turned ' + ((checked) ? 'on': 'off'));
    if (checked) {
        // need to create/re-create each time its added
        POST.locationControls.scale = new OpenLayers.Control.ScaleLine();
        POST.locationMap.addControl(POST.locationControls.scale);
    } else {
        POST.locationMap.removeControl(POST.locationControls.scale);
        POST.locationControls.scale = null;
    }
};


/**
 * Hide or Show the lat/lon graticule
 *
 * @param {Object} - the Ext.menu.checkItem component
 * @param {Boolean} - checked state
 */
POST.MapTools.graticule_toggle = function (cmp, checked) {
    console.debug('Scale line turned ' + ((checked) ? 'on': 'off'));
    if (!POST.locationControls.graticule) {
        POST.locationControls.graticule = new OpenLayers.Control.Graticule();
        POST.locationMap.addControl(POST.locationControls.graticule);
    }
    if (checked) {
        POST.locationControls.graticule.activate();
    } else {
       POST.locationControls.graticule.deactivate();
    }
};


/**
 * Show the Google StreetView Panel
 */
POST.MapTools.streetview_activate = function (cmp, checked) {
    console.debug('StreetView activated');
    if (!POST.locationControls.streetview) {
        // create on demand only
        POST.locationControls.streetview = new GeoExt.ux.GoogleStreetView();
        POST.locationMap.addControl(POST.locationControls.streetview);
    }
    POST.locationControls.streetview.activate();
};


/**
 * Hide or Show the Current MASAS feed
 *
 * @param {Object} - the Ext toggle button component
 */
POST.MapTools.current_view_toggle = function (btn) {
    var current_layer = POST.locationLayers.current;
    var current_layer_geom = POST.locationLayers.geometry;
    if (!current_layer  || !current_layer_geom) {
        console.error('Current layers not found');
        return;
    }
    if (btn.pressed) {
        current_layer_geom.setVisibility(true);
        // current_layer visibility will be set during loading
        POST.Common.load_current_entries();
        // update again whenever the map view changes
        POST.locationMap.events.register('moveend', null,
            POST.Common.load_current_entries);
        console.debug('Show current entries');
    } else {
        current_layer.setVisibility(false);
        // cleaning up as some geom features can get left behind
        current_layer_geom.removeAllFeatures();
        current_layer_geom.setVisibility(false);
        POST.locationMap.events.unregister('moveend', null,
            POST.Common.load_current_entries);
        console.debug('Hide current entries');
    }
};


/**
 * Enable/disable the KML Drawing Layer
 *
 * @param {Object} - the Ext toggle button component
 */
POST.MapTools.toggle_kml = function (btn) {
    console.debug('KML drawing layer turned ' + ((btn.pressed) ? 'on' : 'off'));
    POST.locationLayers.kml.setVisibility(btn.pressed);
    // edit/draw controls need to be toggled between the default location
    // layer and the KML drawing layer as well as the appropriate handlers
    var control_layer = POST.locationLayers.kml;
    var control_handler = POST.KML.show_feature_window;
    var kml_controls = true;
    // KML settings window that is bottom right corner of the map to allow
    // for saving/upload/delete as attachment on proxy server
    if (!POST.KML.settingsWindow) {
        POST.KML.settingsWindow = new POST.KML.SettingsWindow();
        // show a tooltip regarding this layer only once
        var layer_tip = new Ext.ToolTip({
            target: btn.getEl(),
            width: 200,
            height: 125,
            autoHide: false,
            closable: true,
            html: 'This offers a whiteboard capability for advanced users.' +
                ' Click Help for more info.',
            bodyStyle: {
                background: '#FFFFFF',
                'font-weight': 'bold',
                color: '#000000',
                padding: '5px'
            },
            anchor: 'bottom'
        });
        layer_tip.show();
        setTimeout(function () {
            layer_tip.destroy();
        }, 12000);
    }
    if (btn.pressed) {
        POST.KML.settingsWindow.show();
        POST.KML.settingsWindow.alignTo(POST.locationMapPanel.el, 'br-br?', [0, -28]);
    } else {
        POST.KML.settingsWindow.hide();
        // reset controls back to original state for location editing
        control_layer = POST.locationLayers.location;
        control_handler = POST.save_location_feature;
        kml_controls = false;
    }
    for (var i = 0; i < POST.locationControls.editPanel.controls.length; i++) {
        if (POST.locationControls.editPanel.controls[i].displayClass === 'modifyButton') {
            POST.locationControls.editPanel.controls[i].layer = control_layer;
            if (kml_controls) {
                // need to use the different render intent because the default
                // "point" style for KML is an icon, which won't exist for
                // lines, polygons, etc
                POST.locationControls.editPanel.controls[i].vertexRenderIntent = 'select';
            } else {
                POST.locationControls.editPanel.controls[i].vertexRenderIntent = 'default';
            }
        } else if (POST.locationControls.editPanel.controls[i].displayClass === 'deleteButton') {
            // selectFeature controls are different and using [] allows selection
            // when the current layer is active
            POST.locationControls.editPanel.controls[i].setLayer([control_layer]);
        } else if (POST.locationControls.editPanel.controls[i].displayClass === 'saveButton') {
            POST.locationControls.editPanel.controls[i].setLayer([control_layer]);
            POST.locationControls.editPanel.controls[i].onSelect = control_handler;
        }
    }
    for (var i = 0; i < POST.locationControls.drawPanel.controls.length; i++) {
        POST.locationControls.drawPanel.controls[i].layer = control_layer;
        if (POST.locationControls.drawPanel.controls[i].displayClass === 'circleButton') {
            POST.locationControls.drawPanel.controls[i].featureAdded = function (feature) {
                POST.create_circle_feature(feature, kml_controls);
            };
        } else {
            POST.locationControls.drawPanel.controls[i].featureAdded = control_handler;
        }
    }
};
